'use strict';
angular.module('softvApp')
	.factory('CiudadCarteraFactory', function($http, $q, globalService, $localStorage) {
		var factory = {};
		var paths = {
			getTipoServicioList: '/VendedoresL/GetListTipServ',
			getFechaList: '/VendedoresL/GetListFechaCiudadCartera',
			getDetalleCartera: '/VendedoresL/GetListDetalleCartera',
			GetSp_MuestraDistribuidorCartera: '/Procesos/GetSp_MuestraDistribuidorCartera',
			GetSp_MuestraCompaniaCartera: '/Procesos/GetSp_MuestraCompaniaCartera',
			GetSp_MuestraPeriodoCartera: '/Procesos/GetSp_MuestraPeriodoCartera',
			GetGenera_CarteraDIG_PorPeriodo_ALL: '/Procesos/GetGenera_CarteraDIG_PorPeriodo_ALL'
		};

		factory.GetServicios = function() {
			var deferred = $q.defer();
			var config = {
				headers: {
					'Authorization': $localStorage.currentUser.token
				}
			};
			$http.get(globalService.getUrl() + paths.getTipoServicioList, config).then(function(response) {
				deferred.resolve(response.data);
			}).catch(function(response) {
				deferred.reject(response);
			});
			return deferred.promise;
		};


		factory.GetFechas = function(ClvUsuario, Clv_TipSer) {
			var deferred = $q.defer();
			var config = {headers: {'Authorization': $localStorage.currentUser.token}};
			var Parametros = {
				'ClvUsuario': ClvUsuario,
				'Clv_TipSer': Clv_TipSer
			};
			console.log(Parametros);
			$http.post(globalService.getUrl() + paths.getFechaList, JSON.stringify(Parametros), config).then(function(response) {
				deferred.resolve(response.data);
			}).catch(function(response) {
				deferred.reject(response);
			});

			return deferred.promise;
		};

		factory.GetDetalleCartera = function(servicio, fecha, TipoReporte) {
			var deferred = $q.defer();
			var DetalleCarteraEntity = {
				'Tipservicio': servicio,
				'Fecha': fecha,
				'TipoReporte': TipoReporte
			};

			var config = {
				headers: {
					'Authorization': $localStorage.currentUser.token,
				}
			};
			$http.post(globalService.getUrl() + paths.getDetalleCartera, JSON.stringify({
				'DetalleCartera': DetalleCarteraEntity
			}), config).then(function(response) {
				deferred.resolve(response.data);
			}).catch(function(response) {
				deferred.reject(response);
			});
			return deferred.promise;
		};

		factory.GetSp_MuestraDistribuidorCartera = function(Clv_TipSer, ClvUsuario) {
			var deferred = $q.defer();
			var Obj = {
				'Clv_TipSer': Clv_TipSer,
				'ClvUsuario': ClvUsuario
			};
			var config = {
				headers: {
					'Authorization': $localStorage.currentUser.token,
				}
			};
			$http.post(globalService.getUrl() + paths.GetSp_MuestraDistribuidorCartera, JSON.stringify(Obj), config).then(function(response) {
				deferred.resolve(response.data);
			}).catch(function(response) {
				deferred.reject(response);
			});
			return deferred.promise;
		};

		factory.GetSp_MuestraCompaniaCartera = function(ObjDistribuidor, Clv_TipSer) {
			var deferred = $q.defer();
			var Obj = {'ObjDistribuidor': ObjDistribuidor,
		                'Clv_TipSer': Clv_TipSer};
			var config = {
				headers: {
					'Authorization': $localStorage.currentUser.token,
				}
			};
			$http.post(globalService.getUrl() + paths.GetSp_MuestraCompaniaCartera, JSON.stringify(Obj), config).then(function(response) {
				deferred.resolve(response.data);
			}).catch(function(response) {
				deferred.reject(response);
			});
			return deferred.promise;
		};

		factory.GetSp_MuestraPeriodoCartera = function(ObjCompania) {
			var deferred = $q.defer();
			var Obj = {'ObjCompania': ObjCompania};
			var config = {
				headers: {
					'Authorization': $localStorage.currentUser.token,
				}
			};
			$http.post(globalService.getUrl() + paths.GetSp_MuestraPeriodoCartera, JSON.stringify(Obj), config).then(function(response) {
				deferred.resolve(response.data);
			}).catch(function(response) {
				deferred.reject(response);
			});
			return deferred.promise;
		};

		factory.GetGenera_CarteraDIG_PorPeriodo_ALL = function(ObjCartera, ObjDis, ObjCom, ObjPer) {
			var deferred = $q.defer();
			var Obj = {
				'ObjCartera': ObjCartera,
				'ObjDis': ObjDis,
				'ObjCom': ObjCom,
				'ObjPer': ObjPer
			  };
			var config = {
				headers: {
					'Authorization': $localStorage.currentUser.token,
				}
			};
			$http.post(globalService.getUrl() + paths.GetGenera_CarteraDIG_PorPeriodo_ALL, JSON.stringify(Obj), config).then(function(response) {
				deferred.resolve(response.data);
			}).catch(function(response) {
				deferred.reject(response);
			});
			return deferred.promise;
		};

		return factory;
	});
