'use strict';
angular.module('softvApp')
	.factory('mizarFactory', function($http, $q, globalService, $localStorage) {

        var factory = {};
		var paths = {
			GetGraba_Factura_Digital: '/FacturacionSoftv/GetGraba_Factura_Digital',
			GetGraba_Factura_Nota: '/FacturacionSoftv/GetGraba_Factura_Nota',
            GetImprimeFacturaFiscalFac: '/FacturacionSoftv/GetImprimeFacturaFiscalFac',			
			GetEnviaFacturaFiscalFac:'/FacturacionSoftv/GetEnviaFacturaFiscalFac',
			GetCancelacion_Factura_CFDMaestro:'/FacturacionSoftv/GetCancelacion_Factura_CFDMaestro'
//GetImprimeFacturaFiscalNotaMaestro:'/FacturacionSoftv/GetImprimeFacturaFiscalNotaMaestro',
		};


		factory.GetCancelacion_Factura_CFDMaestro = function(oClv_FacturaCFD,tipo) {
			var Parametros = {
				'oClv_FacturaCFD': oClv_FacturaCFD,
				'tipo':tipo
			};
			var deferred = $q.defer();
			var config = {
				headers: {
					'Authorization': $localStorage.currentUser.token,
				}
			};
			$http.post(globalService.getUrlMizar() + paths.GetCancelacion_Factura_CFDMaestro, JSON.stringify(
				Parametros
			), config).then(function(response) {
				deferred.resolve(response.data);
			}).catch(function(response) {
				deferred.reject(response);
			});
			return deferred.promise;
        };
		
		factory.GetEnviaFacturaFiscalFac = function(oClv_Factura) {
			var Parametros = {
				'oClv_Factura': oClv_Factura
			};
			var deferred = $q.defer();
			var config = {
				headers: {
					'Authorization': $localStorage.currentUser.token,
				}
			};
			$http.post(globalService.getUrlMizar() + paths.GetEnviaFacturaFiscalFac, JSON.stringify(
				Parametros
			), config).then(function(response) {
				deferred.resolve(response.data);
			}).catch(function(response) {
				deferred.reject(response);
			});
			return deferred.promise;
        };
        
        factory.GetGraba_Factura_Digital = function(oClv_Factura) {
			var Parametros = {
				'oClv_Factura': oClv_Factura
			};
			var deferred = $q.defer();
			var config = {
				headers: {
					'Authorization': $localStorage.currentUser.token,
				}
			};
			$http.post(globalService.getUrlMizar() + paths.GetGraba_Factura_Digital, JSON.stringify(
				Parametros
			), config).then(function(response) {
				deferred.resolve(response.data);
			}).catch(function(response) {
				deferred.reject(response);
			});
			return deferred.promise;
        };
        

        factory.GetGraba_Factura_Nota = function(oClv_Factura) {
			var Parametros = {
				'oClv_Factura': oClv_Factura
			};
			var deferred = $q.defer();
			var config = {
				headers: {
					'Authorization': $localStorage.currentUser.token,
				}
			};
			$http.post(globalService.getUrlMizar() + paths.GetGraba_Factura_Nota, JSON.stringify(
				Parametros
			), config).then(function(response) {
				deferred.resolve(response.data);
			}).catch(function(response) {
				deferred.reject(response);
			});
			return deferred.promise;
        };

        factory.GetImprimeFacturaFiscalFac = function(oClv_Factura,Tipo) {
			var Parametros = {
                'oClv_Factura': oClv_Factura,
                'Tipo':Tipo
			};
			var deferred = $q.defer();
			var config = {
				headers: {
					'Authorization': $localStorage.currentUser.token,
				}
			};
			$http.post(globalService.getUrlMizar() + paths.GetImprimeFacturaFiscalFac, JSON.stringify(
				Parametros
			), config).then(function(response) {
				deferred.resolve(response.data);
			}).catch(function(response) {
				deferred.reject(response);
			});
			return deferred.promise;
		};
        

        factory.GetImprimeFacturaFiscalNotaMaestro = function(oClv_Factura) {
			var Parametros = {
                'oClv_Factura': oClv_Factura                
			};
			var deferred = $q.defer();
			var config = {
				headers: {
					'Authorization': $localStorage.currentUser.token,
				}
			};
			$http.post(globalService.getUrlMizar() + paths.GetImprimeFacturaFiscalNotaMaestro, JSON.stringify(
				Parametros
			), config).then(function(response) {
				deferred.resolve(response.data);
			}).catch(function(response) {
				deferred.reject(response);
			});
			return deferred.promise;
		};
       
        return factory;
    });