'use strict';

angular
    .module('softvApp')
    .factory('ClienteServicioFactory', function($http, $q, globalService, $localStorage){

        var factory = {};
        var paths = {
            GetValidaTVDigCliente: '/ClientesServicio/GetValidaTVDigCliente',
            GetListServicioAdicTvDig: '/ContratacionServicio/GetListServicioAdicTvDig',
            GetAddPqueteAdic: '/ClientesServicio/GetAddPqueteAdic',
            GetRelTipoServClientePorContrato: '/ContratacionServicio/GetRelTipoServClientePorContrato',
            GetSaveCobroHoteles: '/ClientesServicio/GetSaveCobroHoteles',
            GetCobroHotelesByContratoClvUnicanet: '/ClientesServicio/GetCobroHotelesByContratoClvUnicanet',
            GetDeleteCobroHoteles: '/ClientesServicio/GetDeleteCobroHoteles'

        };

        factory.GetValidaTVDigCliente = function(Contrato){
            var deferred = $q.defer();
            var config = {headers: {'Authorization': $localStorage.currentUser.token}};
            var Parametros = {'Contrato': Contrato};
            $http.post(globalService.getUrl() + paths.GetValidaTVDigCliente, JSON.stringify(Parametros), config).then(function(response){
                deferred.resolve(response.data);
            }).catch(function(response){
                deferred.reject(response);
            });
            return deferred.promise;
        };

        factory.GetListServicioAdicTvDig = function(){
            var deferred = $q.defer();
            var config = {headers: {'Authorization': $localStorage.currentUser.token}};
            $http.get(globalService.getUrl() + paths.GetListServicioAdicTvDig, config).then(function(response){
                deferred.resolve(response.data);
            }).catch(function(response){
                deferred.reject(response);
            });
            return deferred.promise;
        };

        factory.GetAddPqueteAdic = function(ObjPqueteAdic){
            var deferred = $q.defer();
            var config = {headers: {'Authorization': $localStorage.currentUser.token}};
            var Parametros = {'ObjPqueteAdic':ObjPqueteAdic};
            $http.post(globalService.getUrl() + paths.GetAddPqueteAdic, JSON.stringify(Parametros), config).then(function(response){
                deferred.resolve(response.data);
            }).catch(function(response){
                deferred.reject(response);
            });
            return deferred.promise;
        };

        factory.GetRelTipoServClientePorContrato = function(Clv_TipSer, contrato){ 
            var deferred = $q.defer(); 
            var config = {headers: {'Authorization': $localStorage.currentUser.token}}; 
            var Parametros = { 
                'Clv_TipSer': Clv_TipSer, 
                'contrato': contrato 
            }; 
            console.log(Parametros); 
            $http.post(globalService.getUrl() + paths.GetRelTipoServClientePorContrato, JSON.stringify(Parametros), config).then(function(response){ 
                deferred.resolve(response.data); 
            }).catch(function(response){ 
                deferred.reject(response); 
            }); 
            return deferred.promise; 
        }; 

        factory.GetSaveCobroHoteles = function(ObjCobro){ 
            var deferred = $q.defer(); 
            var config = {headers: {'Authorization': $localStorage.currentUser.token}}; 
            var Parametros = {'ObjCobro': ObjCobro,};
            $http.post(globalService.getUrl() + paths.GetSaveCobroHoteles, JSON.stringify(Parametros), config).then(function(response){ 
                deferred.resolve(response.data); 
            }).catch(function(response){ 
                deferred.reject(response); 
            }); 
            return deferred.promise; 
        };

        factory.GetCobroHotelesByContratoClvUnicanet = function(ObjCobro){ 
            var deferred = $q.defer(); 
            var config = {headers: {'Authorization': $localStorage.currentUser.token}}; 
            var Parametros = {'ObjCobro': ObjCobro,};
            $http.post(globalService.getUrl() + paths.GetCobroHotelesByContratoClvUnicanet, JSON.stringify(Parametros), config).then(function(response){ 
                deferred.resolve(response.data); 
            }).catch(function(response){ 
                deferred.reject(response); 
            }); 
            return deferred.promise; 
        };

        factory.GetDeleteCobroHoteles = function(ObjCobro){ 
            var deferred = $q.defer(); 
            var config = {headers: {'Authorization': $localStorage.currentUser.token}}; 
            var Parametros = {'ObjCobro': ObjCobro,};
            $http.post(globalService.getUrl() + paths.GetDeleteCobroHoteles, JSON.stringify(Parametros), config).then(function(response){ 
                deferred.resolve(response.data); 
            }).catch(function(response){ 
                deferred.reject(response); 
            }); 
            return deferred.promise; 
        };

        return factory;

    });