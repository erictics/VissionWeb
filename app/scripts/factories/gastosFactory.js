'use strict';

angular

  .module('softvApp')
  .factory('GastosFactory', function ($http, $q, globalService, $localStorage) {

    var factory = {};
    var paths = {
      GetCategoriaGasto: '/Gastos/GetCategoriaGasto',
      GetClasificacionGasto:'/Gastos/GetClasificacionGasto',
      AddClasificacionGasto:'/Gastos/AddClasificacionGasto',
      UpdateClasificacionGasto:'/Gastos/UpdateClasificacionGasto',
      DeleteClasificacionGasto:'/Gastos/DeleteClasificacionGasto',
      AddCategoriaGasto:'/Gastos/AddCategoriaGasto',
      GetDepartamento: '/Gastos/GetDepartamento',
      UpdateCategoriaGasto: '/Gastos/UpdateCategoriaGasto',
      DeleteCategoriaGasto:'/Gastos/DeleteCategoriaGasto',
      AddDepartamento:'/Gastos/AddDepartamento',
      UpdateDepartamento:'/Gastos/UpdateDepartamento',
      DeleteDepartamento:'/Gastos/DeleteDepartamento',
      GetGastos:'/Gastos/GetGastos',
      GetFiltrosSolicitud:'/Gastos/GetFiltrosSolicitud',
      GetDetalleByIdSolicitud:'/Gastos/GetDetalleByIdSolicitud',
      GetSolicitudById:'/Gastos/GetSolicitudById',
      GetAutorizaConceptos:'/Gastos/GetAutorizaConceptos',
      GetAplicaConceptos:'/Gastos/GetAplicaConceptos',
      GetReporteGastoPendiente:'/Gastos/GetReporteGastoPendiente',
      GetReporteGastoAutorizados:'/Gastos/GetReporteGastoAutorizados',
      GetReporteGastoAplicados:'/Gastos/GetReporteGastoAplicados',
      GetReportePendientesAplicar:'/Reportes/GetReportePendientesAplicar',
      GetfiltrosGastos:'/Gastos/GetfiltrosGastos',
      UpdateSolicitud: '/Gastos/UpdateSolicitud',
      GetUpdateDetSolicitud: '/Gastos/GetUpdateDetSolicitud',
      GetAddSolicitud: '/Gastos/GetAddSolicitud',
      GetValidaReversionAplicacionGasto:'/Gastos/GetValidaReversionAplicacionGasto',
      GetFiltrosAutorizacionConceptos:'/Gastos/GetFiltrosAutorizacionConceptos'
    };

    factory.GetFiltrosAutorizacionConceptos = function (obj){
        var deferred = $q.defer();
                 
        var config = {headers: {'Authorization': $localStorage.currentUser.token}};      
        $http.post(globalService.getUrl() + paths.GetFiltrosAutorizacionConceptos,obj, config).then(function(response){
            deferred.resolve(response.data);
        }).catch(function(response){
            deferred.reject(response);
        });
        return deferred.promise;
    };

    factory.GetValidaReversionAplicacionGasto = function (IdGasto,IdUsuario,IdDetalle){
        var deferred = $q.defer();
        var params={
            IdGasto: IdGasto,
            IdUsuario: IdUsuario,
            IdDetalle:IdDetalle
        };          
        var config = {headers: {'Authorization': $localStorage.currentUser.token}};      
        $http.post(globalService.getUrl() + paths.GetValidaReversionAplicacionGasto,params, config).then(function(response){
            deferred.resolve(response.data);
        }).catch(function(response){
            deferred.reject(response);
        });
        return deferred.promise;
    };
    
    factory.GetReportePendientesAplicar = function (obj){
        var deferred = $q.defer();
        var params=obj;          
        var config = {headers: {'Authorization': $localStorage.currentUser.token}};      
        $http.post(globalService.getUrl() + paths.GetReportePendientesAplicar,params, config).then(function(response){
            deferred.resolve(response.data);
        }).catch(function(response){
            deferred.reject(response);
        });
        return deferred.promise;
    };

    factory.GetReporteGastoAplicados = function (obj){
        var deferred = $q.defer();
        var params=obj;          
        var config = {headers: {'Authorization': $localStorage.currentUser.token}};      
        $http.post(globalService.getUrl() + paths.GetReporteGastoAplicados,params, config).then(function(response){
            deferred.resolve(response.data);
        }).catch(function(response){
            deferred.reject(response);
        });
        return deferred.promise;
    };


    factory.GetReporteGastoAutorizados = function (obj){
        var deferred = $q.defer();
        var params=obj;
          
        var config = {headers: {'Authorization': $localStorage.currentUser.token}};      
        $http.post(globalService.getUrl() + paths.GetReporteGastoAutorizados,params, config).then(function(response){
            deferred.resolve(response.data);
        }).catch(function(response){
            deferred.reject(response);
        });
        return deferred.promise;
    };

    factory.GetReporteGastoPendiente = function (obj){
        var deferred = $q.defer();
        var params=obj;
          
        var config = {headers: {'Authorization': $localStorage.currentUser.token}};      
        $http.post(globalService.getUrl() + paths.GetReporteGastoPendiente,params, config).then(function(response){
            deferred.resolve(response.data);
        }).catch(function(response){
            deferred.reject(response);
        });
        return deferred.promise;
    };


    

    factory.GetAplicaConceptos = function (Idsolicitud,IdUsuario,conceptos){
        var deferred = $q.defer();
        var obj={
            'Idsolicitud': Idsolicitud,
            'IdUsuario':IdUsuario,   
            'conceptos':conceptos  
          };
          
        var config = {headers: {'Authorization': $localStorage.currentUser.token}};      
        $http.post(globalService.getUrl() + paths.GetAplicaConceptos,obj, config).then(function(response){
            deferred.resolve(response.data);
        }).catch(function(response){
            deferred.reject(response);
        });
        return deferred.promise;
    };

    factory.GetAutorizaConceptos = function (Idsolicitud,IdUsuario,conceptos){
        var deferred = $q.defer();
        var obj={
            'Idsolicitud': Idsolicitud,
            'IdUsuario':IdUsuario,   
            'conceptos':conceptos  
          };
          
        var config = {headers: {'Authorization': $localStorage.currentUser.token}};      
        $http.post(globalService.getUrl() + paths.GetAutorizaConceptos,obj, config).then(function(response){
            deferred.resolve(response.data);
        }).catch(function(response){
            deferred.reject(response);
        });
        return deferred.promise;
    };

    factory.GetSolicitudById = function (id){
        var deferred = $q.defer();
        var obj={
            'id': id         
          };
          
        var config = {headers: {'Authorization': $localStorage.currentUser.token}};      
        $http.post(globalService.getUrl() + paths.GetSolicitudById,obj, config).then(function(response){
            deferred.resolve(response.data);
        }).catch(function(response){
            deferred.reject(response);
        });
        return deferred.promise;
    };

    factory.GetSolicitudById = function (id){
        var deferred = $q.defer();
        var obj={
            'id': id         
          };
          
        var config = {headers: {'Authorization': $localStorage.currentUser.token}};      
        $http.post(globalService.getUrl() + paths.GetSolicitudById,obj, config).then(function(response){
            deferred.resolve(response.data);
        }).catch(function(response){
            deferred.reject(response);
        });
        return deferred.promise;
    };

    factory.GetDetalleByIdSolicitud = function (id){
        var deferred = $q.defer();
        var obj={
            'id': id         
          };
          
        var config = {headers: {'Authorization': $localStorage.currentUser.token}};      
        $http.post(globalService.getUrl() + paths.GetDetalleByIdSolicitud,obj, config).then(function(response){
            deferred.resolve(response.data);
        }).catch(function(response){
            deferred.reject(response);
        });
        return deferred.promise;
    };

    factory.GetFiltrosSolicitud = function (obj){
        var deferred = $q.defer();
        var obj={
            'solicitud':{
              'IdDpto':obj.IdDpto,
              'IdClasificacion': obj.IdClasificacion,
              'IdDistribuidor': obj.IdDistribuidor,
              'IdCompania': obj.IdCompania,
              'IdCd': obj.IdCd,
              'IdGasto': obj.IdGasto,
              'IdAutorizacion':obj.IdAutorizacion,
              'IdUsuario':$localStorage.currentUser.idUsuario,
              'IdSucursal': obj.IdSucursal
            }          
          };
        var config = {headers: {'Authorization': $localStorage.currentUser.token}};      
        $http.post(globalService.getUrl() + paths.GetFiltrosSolicitud,obj, config).then(function(response){
            deferred.resolve(response.data);
        }).catch(function(response){
            deferred.reject(response);
        });
        return deferred.promise;
    };


    factory.UpdateDepartamento = function (IdDpto,Descripcion){
        var deferred = $q.defer();
        var obj={
            'objDpto':{
              'IdDpto':IdDpto,
              'Descripcion': Descripcion
            }          
          };
          
        var config = {headers: {'Authorization': $localStorage.currentUser.token}};      
        $http.post(globalService.getUrl() + paths.UpdateDepartamento,obj, config).then(function(response){
            deferred.resolve(response.data);
        }).catch(function(response){
            deferred.reject(response);
        });
        return deferred.promise;
    };


    factory.AddDepartamento = function (Descripcion){
        var deferred = $q.defer();
        var obj={
            'objDpto':{
              'Descripcion': Descripcion
            }          
          };
          
        var config = {headers: {'Authorization': $localStorage.currentUser.token}};      
        $http.post(globalService.getUrl() + paths.AddDepartamento,obj, config).then(function(response){
            deferred.resolve(response.data);
        }).catch(function(response){
            deferred.reject(response);
        });
        return deferred.promise;
    };
    
    factory.UpdateCategoriaGasto = function (obj){
        var deferred = $q.defer();
        var obj={
            'objGastos':{
              'Clv_CateGasto':obj.Clv_CateGasto,
              'Descripcion': obj.Descripcion,
              'Clv_Clasificacion': obj.Clv_Clasificacion,
              'Importe': obj.Importe,
              'Fijo': obj.Fijo,
              'IdDpto': obj.IdDpto
            }
          };
          
        var config = {headers: {'Authorization': $localStorage.currentUser.token}};      
        $http.post(globalService.getUrl() + paths.UpdateCategoriaGasto,obj, config).then(function(response){
            deferred.resolve(response.data);
        }).catch(function(response){
            deferred.reject(response);
        });
        return deferred.promise;
    };


    factory.AddCategoriaGasto = function (obj){
        var deferred = $q.defer();
        var obj={
            'objGastos':{
              'Descripcion': obj.Descripcion,
              'Clv_Clasificacion': obj.Clv_Clasificacion,
              'Importe': obj.Importe,
              'Fijo': obj.Fijo,
              'IdDpto': obj.IdDpto
            }
          };
          
        var config = {headers: {'Authorization': $localStorage.currentUser.token}};      
        $http.post(globalService.getUrl() + paths.AddCategoriaGasto,obj, config).then(function(response){
            deferred.resolve(response.data);
        }).catch(function(response){
            deferred.reject(response);
        });
        return deferred.promise;
    };
  
    


    


    factory.GetGastos = function (){
        var deferred = $q.defer();
        var config = {headers: {'Authorization': $localStorage.currentUser.token}};      
        $http.get(globalService.getUrl() + paths.GetGastos, config).then(function(response){
            deferred.resolve(response.data);
        }).catch(function(response){
            deferred.reject(response);
        });
        return deferred.promise;
    };

    factory.GetDepartamento = function (){
        var deferred = $q.defer();
        var config = {headers: {'Authorization': $localStorage.currentUser.token}};      
        $http.get(globalService.getUrl() + paths.GetDepartamento, config).then(function(response){
            deferred.resolve(response.data);
        }).catch(function(response){
            deferred.reject(response);
        });
        return deferred.promise;
    };

    factory.GetCategoriaGasto = function (){
      var deferred = $q.defer();
      var config = {headers: {'Authorization': $localStorage.currentUser.token}};      
      $http.get(globalService.getUrl() + paths.GetCategoriaGasto, config).then(function(response){
          deferred.resolve(response.data);
      }).catch(function(response){
          deferred.reject(response);
      });
      return deferred.promise;
  };


    factory.GetClasificacionGasto = function (){
      var deferred = $q.defer();
      var config = {headers: {'Authorization': $localStorage.currentUser.token}};      
      $http.get(globalService.getUrl() + paths.GetClasificacionGasto, config).then(function(response){
          deferred.resolve(response.data);
      }).catch(function(response){
          deferred.reject(response);
      });
      return deferred.promise;
  };


  factory.AddClasificacionGasto = function (descripcion){    
    var deferred = $q.defer();
   var data= {
      'objGastos':{
      'Descripcion': descripcion
       }
    };
    
    var config = {headers: {'Authorization': $localStorage.currentUser.token}};      
    $http.post(globalService.getUrl() + paths.AddClasificacionGasto,JSON.stringify(data), config).then(function(response){
        deferred.resolve(response.data);
    }).catch(function(response){
        deferred.reject(response);
    });
    return deferred.promise;
};


factory.DeleteDepartamento = function (IdDpto){    
    var deferred = $q.defer();
   var data= {
    'IdDpto': IdDpto
    };
    
    var config = {headers: {'Authorization': $localStorage.currentUser.token}};      
    $http.post(globalService.getUrl() + paths.DeleteDepartamento,JSON.stringify(data), config).then(function(response){
        deferred.resolve(response.data);
    }).catch(function(response){
        deferred.reject(response);
    });
    return deferred.promise;
  };



factory.DeleteClasificacionGasto = function (Clv_Clasificacion){    
  var deferred = $q.defer();
 var data= {
  'Clv_Clasificacion': Clv_Clasificacion
  };
  
  var config = {headers: {'Authorization': $localStorage.currentUser.token}};      
  $http.post(globalService.getUrl() + paths.DeleteClasificacionGasto,JSON.stringify(data), config).then(function(response){
      deferred.resolve(response.data);
  }).catch(function(response){
      deferred.reject(response);
  });
  return deferred.promise;
};

factory.DeleteCategoriaGasto = function (Clv_CateGasto){    
    var deferred = $q.defer();
   var data= {
    'Clv_CateGasto': Clv_CateGasto
    };
    
    var config = {headers: {'Authorization': $localStorage.currentUser.token}};      
    $http.post(globalService.getUrl() + paths.DeleteCategoriaGasto,JSON.stringify(data), config).then(function(response){
        deferred.resolve(response.data);
    }).catch(function(response){
        deferred.reject(response);
    });
    return deferred.promise;
  };




factory.UpdateClasificacionGasto = function (Clv_Clasificacion,Descripcion){
  var deferred = $q.defer();
  var data={
    'objGastos':{
      'Clv_Clasificacion':Clv_Clasificacion,
      'Descripcion':Descripcion
  
    }
  };
  var config = {headers: {'Authorization': $localStorage.currentUser.token}};      
  $http.post(globalService.getUrl() + paths.UpdateClasificacionGasto,JSON.stringify(data), config).then(function(response){
      deferred.resolve(response.data);
  }).catch(function(response){
      deferred.reject(response);
  });
  return deferred.promise;
};

    factory.GetfiltrosGastos = function (ObjDetalle){
        var deferred = $q.defer();
        var ObjDetalle;
        var config = {headers: {'Authorization': $localStorage.currentUser.token}};      
        $http.post(globalService.getUrl() + paths.GetfiltrosGastos,JSON.stringify(ObjDetalle), config).then(function(response){
            deferred.resolve(response.data);
        }).catch(function(response){
            deferred.reject(response);
        });
        return deferred.promise;
    };

    factory.GetAddSolicitud = function (solicitud, detalle){
        var deferred = $q.defer();
        var obj = {
            'solicitud': solicitud,
            'detalle': detalle
        };
        var config = {headers: {'Authorization': $localStorage.currentUser.token}};      
        $http.post(globalService.getUrl() + paths.GetAddSolicitud,JSON.stringify(obj), config).then(function(response){
            deferred.resolve(response.data);
        }).catch(function(response){
            deferred.reject(response);
        });
        return deferred.promise;
    };

    factory.UpdateSolicitud = function (solicitud){
        var deferred = $q.defer();
        var obj = {
            'solicitud': solicitud
        };
        var config = {headers: {'Authorization': $localStorage.currentUser.token}};      
        $http.post(globalService.getUrl() + paths.UpdateSolicitud,JSON.stringify(obj), config).then(function(response){
            deferred.resolve(response.data);
        }).catch(function(response){
            deferred.reject(response);
        });
        return deferred.promise;
    };

    factory.GetUpdateDetSolicitud = function (detalle){
        var deferred = $q.defer();
        var obj = {
            'detalle': detalle
        };
        var config = {headers: {'Authorization': $localStorage.currentUser.token}};      
        $http.post(globalService.getUrl() + paths.GetUpdateDetSolicitud,JSON.stringify(obj), config).then(function(response){
            deferred.resolve(response.data);
        }).catch(function(response){
            deferred.reject(response);
        });
        return deferred.promise;
    };

    factory.GetSolicitudById = function (id){
        var deferred = $q.defer();
        var obj = {'id': id};
        var config = {headers: {'Authorization': $localStorage.currentUser.token}};      
        $http.post(globalService.getUrl() + paths.GetSolicitudById,JSON.stringify(obj), config).then(function(response){
            deferred.resolve(response.data);
        }).catch(function(response){
            deferred.reject(response);
        });
        return deferred.promise;
    };

    return factory;

  });