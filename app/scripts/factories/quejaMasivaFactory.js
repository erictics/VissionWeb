'use strict';
angular
  .module('softvApp')
  .factory('QuejaMasivaFactory', function ($http, $q, globalService, $localStorage) {

    var factory = {};
    var paths = {
        GetSoftvWeb_MQClientes: '/BusquedaQuejasMasivas/GetSoftvWeb_MQClientes',
        GetSoftvWeb_QMEstados: '/BusquedaQuejasMasivas/GetSoftvWeb_QMEstados',
        GetSoftvWeb_QMCiudades: '/BusquedaQuejasMasivas/GetSoftvWeb_QMCiudades',
        GetSoftvWeb_QMLocalidades: '/BusquedaQuejasMasivas/GetSoftvWeb_QMLocalidades',
        GEtSoftvWeb_QMColonias: '/BusquedaQuejasMasivas/GEtSoftvWeb_QMColonias',
        GetSoftvWeb_QMCalles: '/BusquedaQuejasMasivas/GetSoftvWeb_QMCalles',
        GetQMTecnicos: '/BusquedaQuejasMasivas/GetQMTecnicos',
        GetMUESTRATRABAJOSQUEJAS_MasivosList: '/MUESTRATRABAJOSQUEJAS/GetMUESTRATRABAJOSQUEJAS_MasivosList',
        GetAddQuejaMasiva: '/QuejasMasivas/GetAddQuejaMasiva',
        GetQuejasMasivasList: '/QuejasMasivas/GetQuejasMasivasList',
        UpdateQuejasMasivas: '/QuejasMasivas/UpdateQuejasMasivas',
        GetBusquedaQuejasMasivasList: '/BusquedaQuejasMasivas/GetBusquedaQuejasMasivasList',
        GetSoftvWeb_ValidaQMContrato: '/BusquedaQuejasMasivas/GetSoftvWeb_ValidaQMContrato',
        GetSoftvWeb_ValidaQMArea: '/BusquedaQuejasMasivas/GetSoftvWeb_ValidaQMArea',
        GetSoftvWeb_ListaQMClientes: '/QuejasMasivas/GetSoftvWeb_ListaQMClientes',
        GetTipoQM: '/QuejasMasivas/GetTipoQM',
        GetSoftvWeb_ListaQMEstados: '/QuejasMasivas/GetSoftvWeb_ListaQMEstados',
        GetSoftvWeb_ListaQMCiudades: '/QuejasMasivas/GetSoftvWeb_ListaQMCiudades',
        GetSoftvWeb_ListaQMLocalidades: '/QuejasMasivas/GetSoftvWeb_ListaQMLocalidades',
        GetSoftvWeb_ListaQMColonias: '/QuejasMasivas/GetSoftvWeb_ListaQMColonias',
        GetSoftvWeb_ListaQMCalles: '/QuejasMasivas/GetSoftvWeb_ListaQMCalles',
        GetSoftvWeb_UpdateContratoQM: '/QuejasMasivas/GetSoftvWeb_UpdateContratoQM',
        GetSoftvWeb_ListaQMEstadoDis: '/QuejasMasivas/GetSoftvWeb_ListaQMEstadoDis',
        GetSoftvWeb_ListaQMCiudadDis: '/QuejasMasivas/GetSoftvWeb_ListaQMCiudadDis',
        GetSoftvWeb_ListaQMLocalidadDis: '/QuejasMasivas/GetSoftvWeb_ListaQMLocalidadDis',
        GetSoftvWeb_ListaQMColoniaDis: '/QuejasMasivas/GetSoftvWeb_ListaQMColoniaDis',
        GetSoftvWeb_ListaQMCallesDis: '/QuejasMasivas/GetSoftvWeb_ListaQMCallesDis',
        GetSoftvWeb_UpdateEstadoQM: '/BusquedaQuejasMasivas/GetSoftvWeb_UpdateEstadoQM',
        GetSoftvWeb_UpdateCiudadQM: '/BusquedaQuejasMasivas/GetSoftvWeb_UpdateCiudadQM',
        GetSoftvWeb_UpdateLocalidadQM: '/BusquedaQuejasMasivas/GetSoftvWeb_UpdateLocalidadQM',
        GetSoftvWeb_UpdateColoniaQM: '/BusquedaQuejasMasivas/GetSoftvWeb_UpdateColoniaQM',
        GetSoftvWeb_UpdateCalleQM: '/BusquedaQuejasMasivas/GetSoftvWeb_UpdateCalleQM',
        GetSp_ReporteQuejaMasiva: '/Reportes/GetSp_ReporteQuejaMasiva',
        GetValidaServCliQM: '/QuejasMasivas/GetValidaServCliQM'
    };

    factory.GetSoftvWeb_MQClientes = function(ObjCliente) {
        var deferred = $q.defer(); 
        var config = {headers:{'Authorization': $localStorage.currentUser.token}};
        var Parametros = {'ObjCliente': ObjCliente};
        $http.post(globalService.getUrl() + paths.GetSoftvWeb_MQClientes, JSON.stringify(Parametros), config).then(function(response) { 
            deferred.resolve(response.data); 
        }).catch(function(response) { 
            deferred.reject(response.data); 
        });
        return deferred.promise; 
    };

    factory.GetSoftvWeb_QMEstados = function() {
        var deferred = $q.defer(); 
        var config = {headers:{'Authorization': $localStorage.currentUser.token}};
        var Parametros = {'Clave': $localStorage.currentUser.idUsuario};
        $http.post(globalService.getUrl() + paths.GetSoftvWeb_QMEstados, JSON.stringify(Parametros), config).then(function(response) { 
            deferred.resolve(response.data); 
        }).catch(function(response) { 
            deferred.reject(response.data); 
        });
        return deferred.promise; 
    };

    factory.GetSoftvWeb_QMCiudades = function(ListEstado) {
        var deferred = $q.defer(); 
        var config = {headers:{'Authorization': $localStorage.currentUser.token}};
        var Parametros = {
            'ListEstado': ListEstado,
            'Clave': $localStorage.currentUser.idUsuario
        };
        $http.post(globalService.getUrl() + paths.GetSoftvWeb_QMCiudades, JSON.stringify(Parametros), config).then(function(response) { 
            deferred.resolve(response.data); 
        }).catch(function(response) { 
            deferred.reject(response.data); 
        });
        return deferred.promise; 
    };

    factory.GetSoftvWeb_QMLocalidades = function(ListCiudad, ListEstado) {
        var deferred = $q.defer(); 
        var config = {headers:{'Authorization': $localStorage.currentUser.token}};
        var Parametros = {
            'ListCiudad': ListCiudad,
            'ListEstado': ListEstado,
            'Clave': $localStorage.currentUser.idUsuario
        };
        $http.post(globalService.getUrl() + paths.GetSoftvWeb_QMLocalidades, JSON.stringify(Parametros), config).then(function(response) { 
            deferred.resolve(response.data); 
        }).catch(function(response) { 
            deferred.reject(response.data); 
        });
        return deferred.promise; 
    };

    factory.GEtSoftvWeb_QMColonias = function(ListLocalidad, ListCiudad, ListEstado) {
        var deferred = $q.defer(); 
        var config = {headers:{'Authorization': $localStorage.currentUser.token}};
        var Parametros = {
            'ListLocalidad': ListLocalidad,
            'ListCiudad': ListCiudad,
            'ListEstado': ListEstado,
            'Clave': $localStorage.currentUser.idUsuario
        };
        $http.post(globalService.getUrl() + paths.GEtSoftvWeb_QMColonias, JSON.stringify(Parametros), config).then(function(response) { 
            deferred.resolve(response.data); 
        }).catch(function(response) { 
            deferred.reject(response.data); 
        });
        return deferred.promise; 
    };

    factory.GetSoftvWeb_QMCalles = function(ListColonia, ListLocalidad, ListCiudad, ListEstado) {
        var deferred = $q.defer(); 
        var config = {headers:{'Authorization': $localStorage.currentUser.token}};
        var Parametros = {
            'ListColonia': ListColonia,
            'ListLocalidad': ListLocalidad,
            'ListCiudad': ListCiudad,
            'ListEstado': ListEstado,
            'Clave': $localStorage.currentUser.idUsuario
        };
        $http.post(globalService.getUrl() + paths.GetSoftvWeb_QMCalles, JSON.stringify(Parametros), config).then(function(response) { 
            deferred.resolve(response.data); 
        }).catch(function(response) { 
            deferred.reject(response.data); 
        });
        return deferred.promise; 
    };

    factory.GetQMTecnicos = function() {
        var deferred = $q.defer(); 
        var config = {headers:{'Authorization': $localStorage.currentUser.token}};
        var Parametros = {'Clave': $localStorage.currentUser.idUsuario};
        $http.post(globalService.getUrl() + paths.GetQMTecnicos, JSON.stringify(Parametros), config).then(function(response) { 
            deferred.resolve(response.data); 
        }).catch(function(response) { 
            deferred.reject(response.data); 
        });
        return deferred.promise; 
    };

    factory.GetMUESTRATRABAJOSQUEJAS_MasivosList = function() {
        var deferred = $q.defer(); 
        var config = {headers:{'Authorization': $localStorage.currentUser.token}};
        $http.get(globalService.getUrl() + paths.GetMUESTRATRABAJOSQUEJAS_MasivosList, config).then(function(response) { 
            deferred.resolve(response.data); 
        }).catch(function(response) { 
            deferred.reject(response.data); 
        });
        return deferred.promise; 
    };

    factory.GetAddQuejaMasiva = function(ObjQM) {
        var deferred = $q.defer(); 
        var config = {headers:{'Authorization': $localStorage.currentUser.token}};
        var Parametros = ObjQM;
        $http.post(globalService.getUrl() + paths.GetAddQuejaMasiva, JSON.stringify(Parametros), config).then(function(response) { 
            deferred.resolve(response.data); 
        }).catch(function(response) { 
            deferred.reject(response.data); 
        });
        return deferred.promise; 
    };

    factory.GetQuejasMasivasList = function(Clv_Queja) {
        var deferred = $q.defer(); 
        var config = {headers:{'Authorization': $localStorage.currentUser.token}};
        var Parametros = {'Clv_Queja': Clv_Queja};
        $http.post(globalService.getUrl() + paths.GetQuejasMasivasList, JSON.stringify(Parametros), config).then(function(response) { 
            deferred.resolve(response.data); 
        }).catch(function(response) { 
            deferred.reject(response.data); 
        });
        return deferred.promise; 
    };

    factory.UpdateQuejasMasivas = function(objQuejasMasivas) {
        var deferred = $q.defer(); 
        var config = {headers:{'Authorization': $localStorage.currentUser.token}};
        var Parametros = {'objQuejasMasivas': objQuejasMasivas};
        $http.post(globalService.getUrl() + paths.UpdateQuejasMasivas, JSON.stringify(Parametros), config).then(function(response) { 
            deferred.resolve(response.data); 
        }).catch(function(response) { 
            deferred.reject(response.data); 
        });
        return deferred.promise; 
    };

    factory.GetBusquedaQuejasMasivasList = function(ObjQMList) {
        var deferred = $q.defer(); 
        var config = {headers:{'Authorization': $localStorage.currentUser.token}};
        var Parametros = ObjQMList;
        $http.post(globalService.getUrl() + paths.GetBusquedaQuejasMasivasList, JSON.stringify(Parametros), config).then(function(response) { 
            deferred.resolve(response.data); 
        }).catch(function(response) { 
            deferred.reject(response.data); 
        });
        return deferred.promise; 
    };

    factory.GetSoftvWeb_ValidaQMContrato = function(ListCliente) {
        var deferred = $q.defer(); 
        var config = {headers:{'Authorization': $localStorage.currentUser.token}};
        var Parametros = {ListCliente: ListCliente};
        $http.post(globalService.getUrl() + paths.GetSoftvWeb_ValidaQMContrato, JSON.stringify(Parametros), config).then(function(response) { 
            deferred.resolve(response.data); 
        }).catch(function(response) { 
            deferred.reject(response.data); 
        });
        return deferred.promise; 
    };

    factory.GetSoftvWeb_ValidaQMArea = function(ListCalles, ListColonia, ListLocalidad, ListCiudad, ListEstado) {
        var deferred = $q.defer(); 
        var config = {headers:{'Authorization': $localStorage.currentUser.token}};
        var Parametros = {
            'ListCalles': ListCalles,
            'ListColonia': ListColonia,
            'ListLocalidad': ListLocalidad,
            'ListCiudad': ListCiudad,
            'ListEstado': ListEstado,
            'Clave': $localStorage.currentUser.idUsuario
        };
        $http.post(globalService.getUrl() + paths.GetSoftvWeb_ValidaQMArea, JSON.stringify(Parametros), config).then(function(response) { 
            deferred.resolve(response.data); 
        }).catch(function(response) { 
            deferred.reject(response.data); 
        });
        return deferred.promise; 
    };

    factory.GetSoftvWeb_ListaQMClientes = function(Clv_Queja) {
        var deferred = $q.defer(); 
        var config = {headers:{'Authorization': $localStorage.currentUser.token}};
        var Parametros = {'Clv_Queja': Clv_Queja};
        $http.post(globalService.getUrl() + paths.GetSoftvWeb_ListaQMClientes, JSON.stringify(Parametros), config).then(function(response) { 
            deferred.resolve(response.data); 
        }).catch(function(response) { 
            deferred.reject(response.data); 
        });
        return deferred.promise; 
    };

    factory.GetTipoQM = function(Clv_Queja) {
        var deferred = $q.defer(); 
        var config = {headers:{'Authorization': $localStorage.currentUser.token}};
        var Parametros = {'Clv_Queja': Clv_Queja};
        $http.post(globalService.getUrl() + paths.GetTipoQM, JSON.stringify(Parametros), config).then(function(response) { 
            deferred.resolve(response.data); 
        }).catch(function(response) { 
            deferred.reject(response.data); 
        });
        return deferred.promise; 
    };

    factory.GetSoftvWeb_ListaQMEstados = function(Clv_Queja) {
        var deferred = $q.defer(); 
        var config = {headers:{'Authorization': $localStorage.currentUser.token}};
        var Parametros = {'Clv_Queja': Clv_Queja};
        $http.post(globalService.getUrl() + paths.GetSoftvWeb_ListaQMEstados, JSON.stringify(Parametros), config).then(function(response) { 
            deferred.resolve(response.data); 
        }).catch(function(response) { 
            deferred.reject(response.data); 
        });
        return deferred.promise; 
    };

    factory.GetSoftvWeb_ListaQMCiudades = function(Clv_Queja) {
        var deferred = $q.defer(); 
        var config = {headers:{'Authorization': $localStorage.currentUser.token}};
        var Parametros = {'Clv_Queja': Clv_Queja};
        $http.post(globalService.getUrl() + paths.GetSoftvWeb_ListaQMCiudades, JSON.stringify(Parametros), config).then(function(response) { 
            deferred.resolve(response.data); 
        }).catch(function(response) { 
            deferred.reject(response.data); 
        });
        return deferred.promise; 
    };

    factory.GetSoftvWeb_ListaQMLocalidades = function(Clv_Queja) {
        var deferred = $q.defer(); 
        var config = {headers:{'Authorization': $localStorage.currentUser.token}};
        var Parametros = {'Clv_Queja': Clv_Queja};
        $http.post(globalService.getUrl() + paths.GetSoftvWeb_ListaQMLocalidades, JSON.stringify(Parametros), config).then(function(response) { 
            deferred.resolve(response.data); 
        }).catch(function(response) { 
            deferred.reject(response.data); 
        });
        return deferred.promise; 
    };

    factory.GetSoftvWeb_ListaQMColonias = function(Clv_Queja) {
        var deferred = $q.defer(); 
        var config = {headers:{'Authorization': $localStorage.currentUser.token}};
        var Parametros = {'Clv_Queja': Clv_Queja};
        $http.post(globalService.getUrl() + paths.GetSoftvWeb_ListaQMColonias, JSON.stringify(Parametros), config).then(function(response) { 
            deferred.resolve(response.data); 
        }).catch(function(response) { 
            deferred.reject(response.data); 
        });
        return deferred.promise; 
    };

    factory.GetSoftvWeb_ListaQMCalles = function(Clv_Queja) {
        var deferred = $q.defer(); 
        var config = {headers:{'Authorization': $localStorage.currentUser.token}};
        var Parametros = {'Clv_Queja': Clv_Queja};
        $http.post(globalService.getUrl() + paths.GetSoftvWeb_ListaQMCalles, JSON.stringify(Parametros), config).then(function(response) { 
            deferred.resolve(response.data); 
        }).catch(function(response) { 
            deferred.reject(response.data); 
        });
        return deferred.promise; 
    };

    factory.GetSoftvWeb_UpdateContratoQM = function(ObjQMCliente) {
        var deferred = $q.defer(); 
        var config = {headers:{'Authorization': $localStorage.currentUser.token}};
        var Parametros = ObjQMCliente;
        $http.post(globalService.getUrl() + paths.GetSoftvWeb_UpdateContratoQM, JSON.stringify(Parametros), config).then(function(response) { 
            deferred.resolve(response.data); 
        }).catch(function(response) { 
            deferred.reject(response.data); 
        });
        return deferred.promise; 
    };

    factory.GetSoftvWeb_ListaQMEstadoDis = function(Obj) {
        var deferred = $q.defer(); 
        var config = {headers:{'Authorization': $localStorage.currentUser.token}};
        var Parametros = Obj;
        $http.post(globalService.getUrl() + paths.GetSoftvWeb_ListaQMEstadoDis, JSON.stringify(Parametros), config).then(function(response) { 
            deferred.resolve(response.data); 
        }).catch(function(response) { 
            deferred.reject(response.data); 
        });
        return deferred.promise; 
    };

    factory.GetSoftvWeb_ListaQMCiudadDis = function(Obj) {
        var deferred = $q.defer(); 
        var config = {headers:{'Authorization': $localStorage.currentUser.token}};
        var Parametros = Obj;
        $http.post(globalService.getUrl() + paths.GetSoftvWeb_ListaQMCiudadDis, JSON.stringify(Parametros), config).then(function(response) { 
            deferred.resolve(response.data); 
        }).catch(function(response) { 
            deferred.reject(response.data); 
        });
        return deferred.promise; 
    };

    factory.GetSoftvWeb_ListaQMLocalidadDis = function(Obj) {
        var deferred = $q.defer(); 
        var config = {headers:{'Authorization': $localStorage.currentUser.token}};
        var Parametros = Obj;
        $http.post(globalService.getUrl() + paths.GetSoftvWeb_ListaQMLocalidadDis, JSON.stringify(Parametros), config).then(function(response) { 
            deferred.resolve(response.data); 
        }).catch(function(response) { 
            deferred.reject(response.data); 
        });
        return deferred.promise; 
    };

    factory.GetSoftvWeb_ListaQMColoniaDis = function(Obj) {
        var deferred = $q.defer(); 
        var config = {headers:{'Authorization': $localStorage.currentUser.token}};
        var Parametros = Obj;
        $http.post(globalService.getUrl() + paths.GetSoftvWeb_ListaQMColoniaDis, JSON.stringify(Parametros), config).then(function(response) { 
            deferred.resolve(response.data); 
        }).catch(function(response) { 
            deferred.reject(response.data); 
        });
        return deferred.promise; 
    };

    factory.GetSoftvWeb_ListaQMCallesDis = function(Obj) {
        var deferred = $q.defer(); 
        var config = {headers:{'Authorization': $localStorage.currentUser.token}};
        var Parametros = Obj;
        $http.post(globalService.getUrl() + paths.GetSoftvWeb_ListaQMCallesDis, JSON.stringify(Parametros), config).then(function(response) { 
            deferred.resolve(response.data); 
        }).catch(function(response) { 
            deferred.reject(response.data); 
        });
        return deferred.promise; 
    };

    factory.GetSoftvWeb_UpdateEstadoQM = function(ObjAdd) {
        var deferred = $q.defer(); 
        var config = {headers:{'Authorization': $localStorage.currentUser.token}};
        var Parametros = ObjAdd;
        $http.post(globalService.getUrl() + paths.GetSoftvWeb_UpdateEstadoQM, JSON.stringify(Parametros), config).then(function(response) { 
            deferred.resolve(response.data); 
        }).catch(function(response) { 
            deferred.reject(response.data); 
        });
        return deferred.promise; 
    };

    factory.GetSoftvWeb_UpdateCiudadQM = function(ObjAdd) {
        var deferred = $q.defer(); 
        var config = {headers:{'Authorization': $localStorage.currentUser.token}};
        var Parametros = ObjAdd;
        $http.post(globalService.getUrl() + paths.GetSoftvWeb_UpdateCiudadQM, JSON.stringify(Parametros), config).then(function(response) { 
            deferred.resolve(response.data); 
        }).catch(function(response) { 
            deferred.reject(response.data); 
        });
        return deferred.promise; 
    };

    factory.GetSoftvWeb_UpdateLocalidadQM = function(ObjAdd) {
        var deferred = $q.defer(); 
        var config = {headers:{'Authorization': $localStorage.currentUser.token}};
        var Parametros = ObjAdd;
        $http.post(globalService.getUrl() + paths.GetSoftvWeb_UpdateLocalidadQM, JSON.stringify(Parametros), config).then(function(response) { 
            deferred.resolve(response.data); 
        }).catch(function(response) { 
            deferred.reject(response.data); 
        });
        return deferred.promise; 
    };

    factory.GetSoftvWeb_UpdateColoniaQM = function(ObjAdd) {
        var deferred = $q.defer(); 
        var config = {headers:{'Authorization': $localStorage.currentUser.token}};
        var Parametros = ObjAdd;
        $http.post(globalService.getUrl() + paths.GetSoftvWeb_UpdateColoniaQM, JSON.stringify(Parametros), config).then(function(response) { 
            deferred.resolve(response.data); 
        }).catch(function(response) { 
            deferred.reject(response.data); 
        });
        return deferred.promise; 
    };

    factory.GetSoftvWeb_UpdateCalleQM = function(ObjAdd) {
        var deferred = $q.defer(); 
        var config = {headers:{'Authorization': $localStorage.currentUser.token}};
        var Parametros = ObjAdd;
        $http.post(globalService.getUrl() + paths.GetSoftvWeb_UpdateCalleQM, JSON.stringify(Parametros), config).then(function(response) { 
            deferred.resolve(response.data); 
        }).catch(function(response) { 
            deferred.reject(response.data); 
        });
        return deferred.promise; 
    };

    factory.GetSp_ReporteQuejaMasiva = function(Clv_Queja) {
        var deferred = $q.defer(); 
        var config = {headers:{'Authorization': $localStorage.currentUser.token}};
        var Parametros = {'Clv_Queja': Clv_Queja};
        $http.post(globalService.getUrl() + paths.GetSp_ReporteQuejaMasiva, JSON.stringify(Parametros), config).then(function(response) { 
            deferred.resolve(response.data); 
        }).catch(function(response) { 
            deferred.reject(response.data); 
        });
        return deferred.promise; 
    };

    factory.GetValidaServCliQM = function(Contrato) {
        var deferred = $q.defer(); 
        var config = {headers:{'Authorization': $localStorage.currentUser.token}};
        var Parametros = {'Contrato': Contrato};
        $http.post(globalService.getUrl() + paths.GetValidaServCliQM, JSON.stringify(Parametros), config).then(function(response) { 
            deferred.resolve(response.data); 
        }).catch(function(response) { 
            deferred.reject(response.data); 
        });
        return deferred.promise; 
    };

    return factory;

  });