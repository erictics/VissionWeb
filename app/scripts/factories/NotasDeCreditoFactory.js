"use strict";
angular
  .module("softvApp")
  .factory("NotasDeCreditoFactory", function(
    $http,
    $q,
    $window,
    globalService,
    $localStorage    
  ) {
    var factory = {};

    var paths = {
      GetBUSCANOTASDECREDITO: "/NotaCredito/GetBUSCANOTASDECREDITO",
      GetStatusNotadeCredito: '/NotaCredito/GetStatusNotadeCredito',
      GetDetalle_NotasdeCredito: '/NotaCredito/GetDetalle_NotasdeCredito',
      GetDAME_FACTURASDECLIENTE: '/NotaCredito/GetDAME_FACTURASDECLIENTE',
      GetConsulta_NotaCredito:'/NotaCredito/GetConsulta_NotaCredito',
      GetObtieneDatosTicket:'/NotaCredito/GetObtieneDatosTicket',
      GetNueva_NotadeCredito:'/NotaCredito/GetNueva_NotadeCredito',
      GetGuarda_DetalleNotaweb: '/NotaCredito/GetGuarda_DetalleNotaweb',
      GetReportesNotasDeCredito: '/NotaCredito/GetReportesNotasDeCredito',
      GetBorrar_Session_Notas:'/NotaCredito/GetBorrar_Session_Notas',
      GetObtieneCajasSucursal:'/CatalogoCajas/GetObtieneCajasSucursal',
      GetobtenUsuariosSucursal:'/Usuario/GetobtenUsuariosSucursal',
      GetModifica_DetalleNotas:'/NotaCredito/GetModifica_DetalleNotas',
      GetDameMonto_NotadeCredito:'/NotaCredito/GetDameMonto_NotadeCredito',
      GetCANCELACIONFACTURAS_Notas:'/NotaCredito/GetCANCELACIONFACTURAS_Notas'
      //   DeleteBorraTablasArchivos: '/CargoAutomatico/DeleteBorraTablasArchivos'
    };


    factory.GetCANCELACIONFACTURAS_Notas = function(Clv_Factura,op) {      
      var deferred = $q.defer();
      var params={ 
        Clv_Factura:Clv_Factura,
        op:0
      };
      var config = {
        headers: {
          Authorization: $localStorage.currentUser.token
        }
      };
      $http.post( globalService.getUrl() + paths.GetCANCELACIONFACTURAS_Notas,JSON.stringify(params),config).then(function(response) {
          deferred.resolve(response.data);
        }).catch(function(response) {
          deferred.reject(response);
        });
      return deferred.promise;
    };

    factory.GetDameMonto_NotadeCredito = function(Clv_Nota,contrato) {      
      var deferred = $q.defer();
      var params={ 
        Clv_Nota:Clv_Nota,
        contrato:contrato       
      };
      var config = {
        headers: {
          Authorization: $localStorage.currentUser.token
        }
      };
      $http.post( globalService.getUrl() + paths.GetDameMonto_NotadeCredito,JSON.stringify(params),config).then(function(response) {
          deferred.resolve(response.data);
        }).catch(function(response) {
          deferred.reject(response);
        });
      return deferred.promise;
    };


    factory.GetModifica_DetalleNotas = function(Clv_detalle,Clv_factura,importe,secobra) {      
      var deferred = $q.defer();
      var params={ 
        Clv_detalle:Clv_detalle,
        Clv_factura:Clv_factura,
        secobra:secobra,
        importe:importe     
      };
      var config = {
        headers: {
          Authorization: $localStorage.currentUser.token
        }
      };
      $http.post( globalService.getUrl() + paths.GetModifica_DetalleNotas,JSON.stringify(params),config).then(function(response) {
          deferred.resolve(response.data);
        }).catch(function(response) {
          deferred.reject(response);
        });
      return deferred.promise;
    };


    factory.GetobtenUsuariosSucursal = function(clv_sucursal) {      
      var deferred = $q.defer();
      var params={ 
        clv_sucursal:clv_sucursal         
      };
      var config = {
        headers: {
          Authorization: $localStorage.currentUser.token
        }
      };
      $http.post( globalService.getUrl() + paths.GetobtenUsuariosSucursal,JSON.stringify(params),config).then(function(response) {
          deferred.resolve(response.data);
        }).catch(function(response) {
          deferred.reject(response);
        });
      return deferred.promise;
    };

    factory.GetObtieneCajasSucursal = function(Clv_sucursal) {      
      var deferred = $q.defer();
      var params={ 
        Clv_sucursal:Clv_sucursal         
      };
      var config = {
        headers: {
          Authorization: $localStorage.currentUser.token
        }
      };
      $http.post( globalService.getUrl() + paths.GetObtieneCajasSucursal,JSON.stringify(params),config).then(function(response) {
          deferred.resolve(response.data);
        }).catch(function(response) {
          deferred.reject(response);
        });
      return deferred.promise;
    };

    factory.GetBorrar_Session_Notas = function(Clv_factura,Clv_Nota) {      
      var deferred = $q.defer();
      var params={ 
        Clv_factura:Clv_factura,       
        Clv_Nota :Clv_Nota               
      };
      var config = {
        headers: {
          Authorization: $localStorage.currentUser.token
        }
      };
      $http.post( globalService.getUrl() + paths.GetBorrar_Session_Notas,JSON.stringify(params),config).then(function(response) {
          deferred.resolve(response.data);
        }).catch(function(response) {
          deferred.reject(response);
        });
      return deferred.promise;
    };

    factory.GetReportesNotasDeCredito = function(Clv_Nota) {      
      var deferred = $q.defer();
      var params={        
        Clv_Nota :Clv_Nota               
      };
      var config = {
        headers: {
          Authorization: $localStorage.currentUser.token
        }
      };
      $http.post( globalService.getUrl() + paths.GetReportesNotasDeCredito,JSON.stringify(params),config).then(function(response) {
          deferred.resolve(response.data);
        }).catch(function(response) {
          deferred.reject(response);
        });
      return deferred.promise;
    };

    factory.GetGuarda_DetalleNotaweb = function(Clv_Factura,Clv_Nota) {      
      var deferred = $q.defer();
      var params={        
          Clv_Factura :Clv_Factura,
          Clv_Nota:Clv_Nota               
      };
      var config = {
        headers: {
          Authorization: $localStorage.currentUser.token
        }
      };
      $http.post( globalService.getUrl() + paths.GetGuarda_DetalleNotaweb,JSON.stringify(params),config).then(function(response) {
          deferred.resolve(response.data);
        }).catch(function(response) {
          deferred.reject(response);
        });
      return deferred.promise;
    };


    factory.GetNueva_NotadeCredito = function(nota) {      
      var deferred = $q.defer();
      var params={
        'nota': nota         
      };
      var config = {
        headers: {
          Authorization: $localStorage.currentUser.token
        }
      };
      $http.post( globalService.getUrl() + paths.GetNueva_NotadeCredito,JSON.stringify(params),config).then(function(response) {
          deferred.resolve(response.data);
        }).catch(function(response) {
          deferred.reject(response);
        });
      return deferred.promise;
    };


    factory.GetObtieneDatosTicket = function(Clv_NotadeCredito) {      
      var deferred = $q.defer();
      var params={
        'Clv_NotadeCredito':Clv_NotadeCredito        
      };
      var config = {
        headers: {
          Authorization: $localStorage.currentUser.token
        }
      };
      $http.post( globalService.getUrl() + paths.GetObtieneDatosTicket,JSON.stringify(params),config).then(function(response) {
          deferred.resolve(response.data);
        }).catch(function(response) {
          deferred.reject(response);
        });
      return deferred.promise;
    };

   


    factory.GetConsulta_NotaCredito = function(Clv_NotadeCredito) {      
      var deferred = $q.defer();
      var params={
        'Clv_NotadeCredito':Clv_NotadeCredito        
      };
      var config = {
        headers: {
          Authorization: $localStorage.currentUser.token
        }
      };
      $http.post( globalService.getUrl() + paths.GetConsulta_NotaCredito,JSON.stringify(params),config).then(function(response) {
          deferred.resolve(response.data);
        }).catch(function(response) {
          deferred.reject(response);
        });
      return deferred.promise;
    };


    factory.GetDAME_FACTURASDECLIENTE = function(Factura, clv_nota) {      
      var deferred = $q.defer();
      var params={
        'contrato':Factura,
        'clv_nota':clv_nota
      };
      var config = {
        headers: {
          Authorization: $localStorage.currentUser.token
        }
      };
      $http.post( globalService.getUrl() + paths.GetDAME_FACTURASDECLIENTE,JSON.stringify(params),config).then(function(response) {
          deferred.resolve(response.data);
        }).catch(function(response) {
          deferred.reject(response);
        });
      return deferred.promise;
    };

    factory.GetDetalle_NotasdeCredito = function(Factura, Clv_NotadeCredito) {      
      var deferred = $q.defer();
      var params={
        'Factura':Factura,
        'Clv_NotadeCredito':Clv_NotadeCredito
      };
      var config = {
        headers: {
          Authorization: $localStorage.currentUser.token
        }
      };
      $http.post( globalService.getUrl() + paths.GetDetalle_NotasdeCredito,JSON.stringify(params),config).then(function(response) {
          deferred.resolve(response.data);
        }).catch(function(response) {
          deferred.reject(response);
        });
      return deferred.promise;
    };

   

    factory.GetStatusNotadeCredito = function() {      
      var deferred = $q.defer();
      var config = {
        headers: {
          Authorization: $localStorage.currentUser.token
        }
      };
      $http.get( globalService.getUrl() + paths.GetStatusNotadeCredito,config).then(function(response) {
          deferred.resolve(response.data);
        }).catch(function(response) {
          deferred.reject(response);
        });
      return deferred.promise;
    };

    

    factory.GetBUSCANOTASDECREDITO = function(params) {
      var Parametros = params;
       
      var deferred = $q.defer();
      var config = {
        headers: {
          Authorization: $localStorage.currentUser.token
        }
      };
      $http.post( globalService.getUrl() + paths.GetBUSCANOTASDECREDITO,JSON.stringify(Parametros),config).then(function(response) {
          deferred.resolve(response.data);
        }).catch(function(response) {
          deferred.reject(response);
        });
      return deferred.promise;
    };

    return factory;
  });


  