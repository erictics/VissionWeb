'use strict';
angular.module('softvApp')
  .factory('FacturacionGlobalFactory', function ($http, $q, globalService, $localStorage) {
    var factory = {};

    var paths = {
      GetMuestra_Distribuidor_RelUsuario: '/FacturaGloblal/GetMuestra_Distribuidor_RelUsuario',
      GetBuscaFacturasGlobales: '/FacturaGloblal/GetBuscaFacturasGlobales',
      GetMuestra_Tipo_Nota: '/FacturaGloblal/GetMuestra_Tipo_Nota',
      GetSP_DameImporteFacturaGlobalDistribuidor: '/FacturaGloblal/GetSP_DameImporteFacturaGlobalDistribuidor',
      GetDime_FacGlo_Ex: '/FacturaGloblal/GetDime_FacGlo_Ex',
      Getups_NUEVAFACTGLOBALporcompania: '/FacturaGloblal/Getups_NUEVAFACTGLOBALporcompania',
      GetUspDameClvCompañia: '/FacturaGloblal/GetUspDameClvCompañia',
      GetSP_DIMESIEXISTEFACTURADIGITAL: '/FacturaGloblal/GetSP_DIMESIEXISTEFACTURADIGITAL',
      GetDameFacDig_Parte_1_Global: '/FacturaGloblal/GetDameFacDig_Parte_1_Global',
      GetUsp_Ed_DameDatosFacDigGlobal: '/FacturaGloblal/GetUsp_Ed_DameDatosFacDigGlobal',
      GetCANCELAFACTGLOBAL: '/FacturaGloblal/GetCANCELAFACTGLOBAL',
      GetupsDameFacDig_Clv_FacturaCDF: '/FacturaGloblal/GetupsDameFacDig_Clv_FacturaCDF',
      GetValidaReFacturaGlobal: '/FacturaGloblal/GetValidaReFacturaGlobal',
      GetGraba_Factura_Digital_Global: '/FacturacionSoftv/GetGraba_Factura_Digital_Global',
      GetImprimeFacturaFiscalFac: '/FacturacionSoftv/GetImprimeFacturaFiscalFac'
    };

    factory.GetMuestra_Distribuidor_RelUsuario = function() { 
      var deferred = $q.defer(); 
      var config = {headers:{'Authorization': $localStorage.currentUser.token}};
      var Parametros = {ClvUsuario: $localStorage.currentUser.idUsuario};
      $http.post(globalService.getUrl() + paths.GetMuestra_Distribuidor_RelUsuario, JSON.stringify(Parametros), config).then(function(response) { 
        deferred.resolve(response.data); 
      }).catch(function(response) { 
        deferred.reject(response.data); 
      });
      return deferred.promise; 
    };

    factory.GetBuscaFacturasGlobales = function(ObjFacGlo) { 
      var deferred = $q.defer(); 
      var config = {headers:{'Authorization': $localStorage.currentUser.token}};
      var Parametros = {'ObjFacGlo': ObjFacGlo};
      $http.post(globalService.getUrl() + paths.GetBuscaFacturasGlobales, JSON.stringify(Parametros), config).then(function(response) { 
        deferred.resolve(response.data); 
      }).catch(function(response) { 
        deferred.reject(response.data); 
      });
      return deferred.promise; 
    };

    factory.GetMuestra_Tipo_Nota = function(ObjFacGlo) { 
      var deferred = $q.defer(); 
      var config = {headers:{'Authorization': $localStorage.currentUser.token}};
      var Parametros = {'ObjFacGlo': ObjFacGlo};
      $http.post(globalService.getUrl() + paths.GetMuestra_Tipo_Nota, JSON.stringify(Parametros), config).then(function(response) { 
        deferred.resolve(response.data); 
      }).catch(function(response) { 
        deferred.reject(response.data); 
      });
      return deferred.promise; 
    };
    
    factory.GetSP_DameImporteFacturaGlobalDistribuidor = function(ObjFacGlo) { 
      var deferred = $q.defer(); 
      var config = {headers:{'Authorization': $localStorage.currentUser.token}};
      var Parametros = {'ObjFacGlo': ObjFacGlo};
      $http.post(globalService.getUrl() + paths.GetSP_DameImporteFacturaGlobalDistribuidor, JSON.stringify(Parametros), config).then(function(response) { 
        deferred.resolve(response.data); 
      }).catch(function(response) { 
        deferred.reject(response.data); 
      });
      return deferred.promise; 
    };

    factory.GetDime_FacGlo_Ex = function(ObjFacGlo) { 
      var deferred = $q.defer(); 
      var config = {headers:{'Authorization': $localStorage.currentUser.token}};
      var Parametros = {'ObjFacGlo': ObjFacGlo};
      $http.post(globalService.getUrl() + paths.GetDime_FacGlo_Ex, JSON.stringify(Parametros), config).then(function(response) { 
        deferred.resolve(response.data); 
      }).catch(function(response) { 
        deferred.reject(response.data); 
      });
      return deferred.promise; 
    };

    factory.Getups_NUEVAFACTGLOBALporcompania = function(ObjFacGlo) { 
      var deferred = $q.defer(); 
      var config = {headers:{'Authorization': $localStorage.currentUser.token}};
      var Parametros = {'ObjFacGlo': ObjFacGlo};
      $http.post(globalService.getUrl() + paths.Getups_NUEVAFACTGLOBALporcompania, JSON.stringify(Parametros), config).then(function(response) { 
        deferred.resolve(response.data); 
      }).catch(function(response) { 
        deferred.reject(response.data); 
      });
      return deferred.promise; 
    };

    factory.GetUspDameClvCompañia = function() { 
      var deferred = $q.defer(); 
      var config = {headers:{'Authorization': $localStorage.currentUser.token}};
      $http.get(globalService.getUrl() + paths.GetUspDameClvCompañia, config).then(function(response) { 
        deferred.resolve(response.data); 
      }).catch(function(response) { 
        deferred.reject(response.data); 
      });
      return deferred.promise; 
    };

    factory.GetSP_DIMESIEXISTEFACTURADIGITAL = function(ObjFacGlo) { 
      var deferred = $q.defer(); 
      var config = {headers:{'Authorization': $localStorage.currentUser.token}};
      var Parametros = {'ObjFacGlo': ObjFacGlo};
      $http.post(globalService.getUrl() + paths.GetSP_DIMESIEXISTEFACTURADIGITAL, JSON.stringify(Parametros), config).then(function(response) { 
        deferred.resolve(response.data); 
      }).catch(function(response) { 
        deferred.reject(response.data); 
      });
      return deferred.promise; 
    };

    factory.GetDameFacDig_Parte_1_Global = function(ObjFacGlo) { 
      var deferred = $q.defer(); 
      var config = {headers:{'Authorization': $localStorage.currentUser.token}};
      var Parametros = {'ObjFacGlo': ObjFacGlo};
      $http.post(globalService.getUrl() + paths.GetDameFacDig_Parte_1_Global, JSON.stringify(Parametros), config).then(function(response) { 
        deferred.resolve(response.data); 
      }).catch(function(response) { 
        deferred.reject(response.data); 
      });
      return deferred.promise; 
    };

    factory.GetUsp_Ed_DameDatosFacDigGlobal = function(ObjFacGlo) { 
      var deferred = $q.defer(); 
      var config = {headers:{'Authorization': $localStorage.currentUser.token}};
      var Parametros = {'ObjFacGlo': ObjFacGlo};
      $http.post(globalService.getUrl() + paths.GetUsp_Ed_DameDatosFacDigGlobal, JSON.stringify(Parametros), config).then(function(response) { 
        deferred.resolve(response.data); 
      }).catch(function(response) { 
        deferred.reject(response.data); 
      });
      return deferred.promise; 
    };

    factory.GetCANCELAFACTGLOBAL = function(ObjFacGlo) { 
      var deferred = $q.defer(); 
      var config = {headers:{'Authorization': $localStorage.currentUser.token}};
      var Parametros = {'ObjFacGlo': ObjFacGlo};
      $http.post(globalService.getUrl() + paths.GetCANCELAFACTGLOBAL, JSON.stringify(Parametros), config).then(function(response) { 
        deferred.resolve(response.data); 
      }).catch(function(response) { 
        deferred.reject(response.data); 
      });
      return deferred.promise; 
    };

    factory.GetupsDameFacDig_Clv_FacturaCDF = function(ObjFacGlo) { 
      var deferred = $q.defer(); 
      var config = {headers:{'Authorization': $localStorage.currentUser.token}};
      var Parametros = {'ObjFacGlo': ObjFacGlo};
      $http.post(globalService.getUrl() + paths.GetupsDameFacDig_Clv_FacturaCDF, JSON.stringify(Parametros), config).then(function(response) { 
        deferred.resolve(response.data); 
      }).catch(function(response) { 
        deferred.reject(response.data); 
      });
      return deferred.promise; 
    };

    factory.GetValidaReFacturaGlobal = function(ObjFacGlo) { 
      var deferred = $q.defer(); 
      var config = {headers:{'Authorization': $localStorage.currentUser.token}};
      var Parametros = {'ObjFacGlo': ObjFacGlo};
      $http.post(globalService.getUrl() + paths.GetValidaReFacturaGlobal, JSON.stringify(Parametros), config).then(function(response) { 
        deferred.resolve(response.data); 
      }).catch(function(response) { 
        deferred.reject(response.data); 
      });
      return deferred.promise; 
    };

    factory.GetGraba_Factura_Digital_Global = function(oClv_Factura) { 
      var deferred = $q.defer(); 
      var config = {headers:{'Authorization': $localStorage.currentUser.token}};
      var Parametros = {oClv_Factura: oClv_Factura};
      $http.post(globalService.getUrlMizar() + paths.GetGraba_Factura_Digital_Global, JSON.stringify(Parametros), config).then(function(response) { 
        deferred.resolve(response.data); 
      }).catch(function(response) { 
        deferred.reject(response.data); 
      });
      return deferred.promise; 
    };

    factory.GetImprimeFacturaFiscalFac = function(ObjFac) { 
      var deferred = $q.defer(); 
      var config = {headers:{'Authorization': $localStorage.currentUser.token}};
      var Parametros = ObjFac;
      $http.post(globalService.getUrlMizar() + paths.GetImprimeFacturaFiscalFac, JSON.stringify(Parametros), config).then(function(response) { 
        deferred.resolve(response.data); 
      }).catch(function(response) { 
        deferred.reject(response.data); 
      });
      return deferred.promise; 
    };

    return factory;

  });
