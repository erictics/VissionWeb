'use strict';
angular
    .module('softvApp')
    .factory('EntregaEfectivoFactory', function ($http, $q, globalService, $localStorage) {

        var factory = {};
        var paths = {
            GetMuestra_Plaza_RelUsuario: '/NuevoPago/GetMuestra_Plaza_RelUsuario',
            GetMUESTRASucursalesPorDistribuidor: '/NuevoPago/GetMUESTRASucursalesPorDistribuidor',
            GetBuscaEntregaEfectivo: '/NuevoPago/GetBuscaEntregaEfectivo',
            GetBuscaEfectivoDisponiblePorSucursal: '/NuevoPago/GetBuscaEfectivoDisponiblePorSucursal',
            GetExisteEntregaDeEfectivo: '/NuevoPago/GetExisteEntregaDeEfectivo',
            GetNueEntregaEfectivo: '/NuevoPago/GetNueEntregaEfectivo',
            GetModEntregaEfectivo: '/NuevoPago/GetModEntregaEfectivo',
            GetConsultarEntregaDeEfectivo: '/NuevoPago/GetConsultarEntregaDeEfectivo',
            GetDocEntregaEfectivoCorp: '/Reportes/GetDocEntregaEfectivoCorp'
        };

        factory.GetMuestra_Plaza_RelUsuario = function (ClvUsuario){
            var deferred = $q.defer();
            var obj = {'ClvUsuario': ClvUsuario};
            var config = {headers: {'Authorization': $localStorage.currentUser.token}};      
            $http.post(globalService.getUrl() + paths.GetMuestra_Plaza_RelUsuario,JSON.stringify(obj), config).then(function(response){
                deferred.resolve(response.data);
            }).catch(function(response){
                deferred.reject(response);
            });
            return deferred.promise;
        };

        factory.GetMUESTRASucursalesPorDistribuidor = function (CLVPlaza){
            var deferred = $q.defer();
            var obj = {'CLVPlaza': CLVPlaza};
            var config = {headers: {'Authorization': $localStorage.currentUser.token}};      
            $http.post(globalService.getUrl() + paths.GetMUESTRASucursalesPorDistribuidor,JSON.stringify(obj), config).then(function(response){
                deferred.resolve(response.data);
            }).catch(function(response){
                deferred.reject(response);
            });
            return deferred.promise;
        };

        factory.GetBuscaEntregaEfectivo = function (ObjEntrega){
            var deferred = $q.defer();
            var obj = {'ObjEntrega': ObjEntrega};
            var config = {headers: {'Authorization': $localStorage.currentUser.token}};      
            $http.post(globalService.getUrl() + paths.GetBuscaEntregaEfectivo,JSON.stringify(obj), config).then(function(response){
                deferred.resolve(response.data);
            }).catch(function(response){
                deferred.reject(response);
            });
            return deferred.promise;
        };

        factory.GetBuscaEfectivoDisponiblePorSucursal = function (ObjEntrega){
            var deferred = $q.defer();
            var obj = ObjEntrega;
            var config = {headers: {'Authorization': $localStorage.currentUser.token}};      
            $http.post(globalService.getUrl() + paths.GetBuscaEfectivoDisponiblePorSucursal,JSON.stringify(obj), config).then(function(response){
                deferred.resolve(response.data);
            }).catch(function(response){
                deferred.reject(response);
            });
            return deferred.promise;
        };

        factory.GetExisteEntregaDeEfectivo = function (ObjEntrega){
            var deferred = $q.defer();
            var obj = {'ObjEntrega': ObjEntrega};
            var config = {headers: {'Authorization': $localStorage.currentUser.token}};      
            $http.post(globalService.getUrl() + paths.GetExisteEntregaDeEfectivo,JSON.stringify(obj), config).then(function(response){
                deferred.resolve(response.data);
            }).catch(function(response){
                deferred.reject(response);
            });
            return deferred.promise;
        };

        factory.GetNueEntregaEfectivo = function (ObjEntrega){
            var deferred = $q.defer();
            var obj = {'ObjEntrega': ObjEntrega};
            var config = {headers: {'Authorization': $localStorage.currentUser.token}};      
            $http.post(globalService.getUrl() + paths.GetNueEntregaEfectivo,JSON.stringify(obj), config).then(function(response){
                deferred.resolve(response.data);
            }).catch(function(response){
                deferred.reject(response);
            });
            return deferred.promise;
        };

        factory.GetModEntregaEfectivo = function (ObjEntrega){
            var deferred = $q.defer();
            var obj = {'ObjEntrega': ObjEntrega};
            var config = {headers: {'Authorization': $localStorage.currentUser.token}};      
            $http.post(globalService.getUrl() + paths.GetModEntregaEfectivo,JSON.stringify(obj), config).then(function(response){
                deferred.resolve(response.data);
            }).catch(function(response){
                deferred.reject(response);
            });
            return deferred.promise;
        };

        factory.GetConsultarEntregaDeEfectivo = function (ObjEntrega){
            var deferred = $q.defer();
            var obj = {'ObjEntrega': ObjEntrega};
            var config = {headers: {'Authorization': $localStorage.currentUser.token}};      
            $http.post(globalService.getUrl() + paths.GetConsultarEntregaDeEfectivo,JSON.stringify(obj), config).then(function(response){
                deferred.resolve(response.data);
            }).catch(function(response){
                deferred.reject(response);
            });
            return deferred.promise;
        };

        factory.GetDocEntregaEfectivoCorp = function (IdEntrega){
            var deferred = $q.defer();
            var obj = {'IdEntrega': IdEntrega};
            var config = {headers: {'Authorization': $localStorage.currentUser.token}};      
            $http.post(globalService.getUrl() + paths.GetDocEntregaEfectivoCorp,JSON.stringify(obj), config).then(function(response){
                deferred.resolve(response.data);
            }).catch(function(response){
                deferred.reject(response);
            });
            return deferred.promise;
        };

        return factory;
    });