'use strict';

angular

  .module('softvApp')
  .factory('ClientesFactory', function ($http, $q, globalService, $localStorage) {

    var factory = {};
    var paths = {
        GetMUESTRATIPOFACTURA_Historial: '/Cliente/GetMUESTRATIPOFACTURA_Historial',
        GetBuscaFacturasHistorial: '/Cliente/GetBuscaFacturasHistorial',
        GetBUSCAORDSER: '/Cliente/GetBUSCAORDSER',
        GetMuestraTipSerPrincipal2: '/Cliente/GetMuestraTipSerPrincipal2',
        GetBUSCAQUEJAS: '/Cliente/GetBUSCAQUEJAS',
        GetInsertaEntrecalles: '/CLIENTES_New/GetInsertaEntrecalles',
        GetSoftvWEb_DameEntrecalles: '/CLIENTES_New/GetSoftvWEb_DameEntrecalles',
        GetListaTbl_TipoIden: '/configuracion/GetListaTbl_TipoIden',
        GetAddRelClienteIDentificacion: '/CLIENTES_New/GetAddRelClienteIDentificacion',
        GetBuscarClienteIdentficacion: '/CLIENTES_New/GetBuscarClienteIdentficacion',
        GetSoftvweb_DameContratoAnt: '/CLIENTES_New/GetSoftvweb_DameContratoAnt',
        GetPlacaCliente:'/CLIENTES_New/GetPlacaCliente',
        GetConCPByColoniaLocalidadCiudad: '/CLIENTES_New/GetConCPByColoniaLocalidadCiudad',
        GetListaContratoSimilar: '/CLIENTES_New/GetListaContratoSimilar',
        GetAddBitacoraDireccionSimilar: '/CLIENTES_New/GetAddBitacoraDireccionSimilar',
        GetSP_ListaOrd_ClvSer_FecEje: '/CLIENTES_New/GetSP_ListaOrd_ClvSer_FecEje'
    };


    factory.GetPlacaCliente = function(contrato) {
        var deferred = $q.defer(); 
        var params={
        'contrato':contrato    
        };
        var config = {headers:{'Authorization': $localStorage.currentUser.token}};
        $http.post(globalService.getUrl() + paths.GetPlacaCliente,params ,config).then(function(response) { 
            deferred.resolve(response.data); 
        }).catch(function(response) { 
            deferred.reject(response.data); 
        });
        return deferred.promise; 
    };

    factory.GetMUESTRATIPOFACTURA_Historial = function() {
        var deferred = $q.defer(); 
        var config = {headers:{'Authorization': $localStorage.currentUser.token}};
        $http.get(globalService.getUrl() + paths.GetMUESTRATIPOFACTURA_Historial, config).then(function(response) { 
            deferred.resolve(response.data); 
        }).catch(function(response) { 
            deferred.reject(response.data); 
        });
        return deferred.promise; 
    };

    factory.GetBuscaFacturasHistorial = function(ObjFacHis) {
        var deferred = $q.defer(); 
        var config = {headers:{'Authorization': $localStorage.currentUser.token}};
        var Parametros = {'ObjFacHis': ObjFacHis};
        $http.post(globalService.getUrl() + paths.GetBuscaFacturasHistorial, JSON.stringify(Parametros), config).then(function(response) { 
            deferred.resolve(response.data); 
        }).catch(function(response) { 
            deferred.reject(response.data); 
        });
        return deferred.promise; 
    };

    factory.GetBUSCAORDSER = function(ObjOrdSer) {
        var deferred = $q.defer(); 
        var config = {headers:{'Authorization': $localStorage.currentUser.token}};
        var Parametros = {ObjOrdSer: ObjOrdSer};
        $http.post(globalService.getUrl() + paths.GetBUSCAORDSER, JSON.stringify(Parametros), config).then(function(response) { 
            deferred.resolve(response.data); 
        }).catch(function(response) { 
            deferred.reject(response.data); 
        });
        return deferred.promise; 
    };

    factory.GetMuestraTipSerPrincipal2 = function() {
        var deferred = $q.defer(); 
        var config = {headers:{'Authorization': $localStorage.currentUser.token}};
        $http.get(globalService.getUrl() + paths.GetMuestraTipSerPrincipal2, config).then(function(response) { 
            deferred.resolve(response.data); 
        }).catch(function(response) { 
            deferred.reject(response.data); 
        });
        return deferred.promise; 
    };

    factory.GetBUSCAQUEJAS = function(ObjQueja) {
        var deferred = $q.defer(); 
        var config = {headers:{'Authorization': $localStorage.currentUser.token}};
        var Parametros = {ObjQueja: ObjQueja};
        $http.post(globalService.getUrl() + paths.GetBUSCAQUEJAS, JSON.stringify(Parametros), config).then(function(response) { 
            deferred.resolve(response.data); 
        }).catch(function(response) { 
            deferred.reject(response.data); 
        });
        return deferred.promise; 
    };

    factory.GetInsertaEntrecalles = function(ObjEntrega) {
        var deferred = $q.defer(); 
        var config = {headers:{'Authorization': $localStorage.currentUser.token}};
        var Parametros = {ObjEntrega: ObjEntrega};
        $http.post(globalService.getUrl() + paths.GetInsertaEntrecalles, JSON.stringify(Parametros), config).then(function(response) { 
               deferred.resolve(response.data); 
        }).catch(function(response) { 
            deferred.reject(response.data); 
        });
        return deferred.promise; 
    };

    factory.GetListaTbl_TipoIden = function() {
        var deferred = $q.defer(); 
        var config = {headers:{'Authorization': $localStorage.currentUser.token}};
        $http.get(globalService.getUrl() + paths.GetListaTbl_TipoIden, config).then(function(response) { 

            deferred.resolve(response.data); 
        }).catch(function(response) { 
            deferred.reject(response.data); 
        });
        return deferred.promise; 
    };

    factory.GetSoftvWEb_DameEntrecalles = function(contrato) {
        var deferred = $q.defer(); 
        var config = {headers:{'Authorization': $localStorage.currentUser.token}};
        var Parametros = {contrato: contrato};
        $http.post(globalService.getUrl() + paths.GetSoftvWEb_DameEntrecalles, JSON.stringify(Parametros), config).then(function(response) {
              deferred.resolve(response.data); 
        }).catch(function(response) { 
            deferred.reject(response.data); 
        });
        return deferred.promise; 
    };

    factory.GetAddRelClienteIDentificacion = function(ObjIden) {
        var deferred = $q.defer(); 
        var config = {headers:{'Authorization': $localStorage.currentUser.token}};
        var Parametros = {ObjIden: ObjIden};
        $http.post(globalService.getUrl() + paths.GetAddRelClienteIDentificacion, JSON.stringify(Parametros), config).then(function(response) { 
            deferred.resolve(response.data); 
        }).catch(function(response) { 
            deferred.reject(response.data); 
        });
        return deferred.promise; 
    };

    factory.GetBuscarClienteIdentficacion = function(Contrato) {
        var deferred = $q.defer(); 
        var config = {headers:{'Authorization': $localStorage.currentUser.token}};
        var Parametros = {Contrato: Contrato};
        $http.post(globalService.getUrl() + paths.GetBuscarClienteIdentficacion, JSON.stringify(Parametros), config).then(function(response) { 

            deferred.resolve(response.data); 
        }).catch(function(response) { 
            deferred.reject(response.data); 
        });
        return deferred.promise; 
    };

    factory.GetSoftvweb_DameContratoAnt = function(Contrato) {
        var deferred = $q.defer(); 
        var config = {headers:{'Authorization': $localStorage.currentUser.token}};
        var Parametros = {Contrato: Contrato};
        $http.post(globalService.getUrl() + paths.GetSoftvweb_DameContratoAnt, JSON.stringify(Parametros), config).then(function(response) { 

            deferred.resolve(response.data); 
        }).catch(function(response) { 
            deferred.reject(response.data); 
        });
        return deferred.promise; 
    };

    factory.GetConCPByColoniaLocalidadCiudad = function(ObjCP) {
        var deferred = $q.defer(); 
        var config = {headers:{'Authorization': $localStorage.currentUser.token}};
        var Parametros = ObjCP;
        $http.post(globalService.getUrl() + paths.GetConCPByColoniaLocalidadCiudad, JSON.stringify(Parametros), config).then(function(response) { 

            deferred.resolve(response.data); 
        }).catch(function(response) { 
            deferred.reject(response.data); 
        });
        return deferred.promise; 
    };

    factory.GetListaContratoSimilar = function(ObjContratoSimilar) {
        var deferred = $q.defer(); 
        var config = {headers:{'Authorization': $localStorage.currentUser.token}};
        var Parametros = {ObjContratoSimilar: ObjContratoSimilar};
        $http.post(globalService.getUrl() + paths.GetListaContratoSimilar, JSON.stringify(Parametros), config).then(function(response) { 
            deferred.resolve(response.data); 
        }).catch(function(response) { 
            deferred.reject(response.data); 
        });
        return deferred.promise; 
    };

    factory.GetAddBitacoraDireccionSimilar = function(ObjBitacora) {
        var deferred = $q.defer(); 
        var config = {headers:{'Authorization': $localStorage.currentUser.token}};
        var Parametros = {ObjBitacora: ObjBitacora};
        $http.post(globalService.getUrl() + paths.GetAddBitacoraDireccionSimilar, JSON.stringify(Parametros), config).then(function(response) { 
            deferred.resolve(response.data); 
        }).catch(function(response) { 
            deferred.reject(response.data); 
        });
        return deferred.promise; 
    };


    return factory;

  });