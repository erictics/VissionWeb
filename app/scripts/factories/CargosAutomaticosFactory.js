'use strict';
angular.module('softvApp')
  .factory('CargosAutomaticosFactory', function ($http, $q, $window, globalService, $localStorage, PermPermissionStore, $location, $base64, ngNotify) {
    var factory = {};

    var paths = {
      DeleteBorraTablasArchivos: '/CargoAutomatico/DeleteBorraTablasArchivos',
      GetAllMUESTRAPERIODOS_Seleccionar: '/CargoAutomatico/GetAllMUESTRAPERIODOS_Seleccionar',
      GetDameDatosUsuario: '/CargoAutomatico/GetDameDatosUsuario',
      GetDameDatosGenerales: '/CargoAutomatico/GetDameDatosGenerales',
      GetConPreliminarBancosList: '/CargoAutomatico/GetConPreliminarBancosList',
      GetCONDETFACTURASBANCOSList: '/CargoAutomatico/GetCONDETFACTURASBANCOSList',
      AddGeneraBancos: '/CargoAutomatico/AddGeneraBancos',
      GetValidaAfectacionBancos_WEB: '/CargoAutomatico/GetValidaAfectacionBancos_WEB',
      UpdateGUARDAFACTURASBANCOS: '/CargoAutomatico/UpdateGUARDAFACTURASBANCOS',
      GetCANCELALISTADOPRELIMINAR_web: '/CargoAutomatico/GetCANCELALISTADOPRELIMINAR_web',
      UpdateMODDetFacturasBancos: '/CargoAutomatico/UpdateMODDetFacturasBancos',
      GetDameRangoFacturas: '/CargoAutomatico/GetDameRangoFacturas',
      GetTicket: '/CrearTicketTable/GetTicket',
      GetRepDetallePreliminar: '/Reportes/GetRepDetallePreliminar',
      GetConsulta_Grales_Prosa_bancomer: '/CargoAutomatico/GetConsulta_Grales_Prosa_bancomer',
      GetGuardaDocumentoTxt: '/CargoAutomatico/GetGuardaDocumentoTxt',
      GetDame_PreRecibo_oFactura: '/CargoAutomatico/GetDame_PreRecibo_oFactura'
    };


    factory.DeleteBorraTablasArchivos = function () {
      var deferred = $q.defer();
      var config = { headers: { 'Authorization': $localStorage.currentUser.token } };
      var Parametros = {};
      $http.get(globalService.getUrl() + paths.DeleteBorraTablasArchivos, config).then(function (response) {
        deferred.resolve(response.data);
      }).catch(function (response) {
        deferred.reject(response);
      });
      return deferred.promise;
    };


    factory.GetAllMUESTRAPERIODOS_Seleccionar = function () {
      var deferred = $q.defer();
      var config = { headers: { 'Authorization': $localStorage.currentUser.token } };
      var Parametros = { 'Op': 0 };
      $http.post(globalService.getUrl() + paths.GetAllMUESTRAPERIODOS_Seleccionar, JSON.stringify(Parametros), config).then(function (response) {
        deferred.resolve(response.data);
      }).catch(function (response) {
        deferred.reject(response);
      });
      return deferred.promise;
    };

    factory.GetDameDatosUsuario = function () {
      var deferred = $q.defer();
      var config = { headers: { 'Authorization': $localStorage.currentUser.token } };
      var Parametros = { 'Clv_Usuario': $localStorage.currentUser.usuario, 'Op': 0 };
      $http.post(globalService.getUrl() + paths.GetDameDatosUsuario, JSON.stringify(Parametros), config).then(function (response) {
        deferred.resolve(response.data);
      }).catch(function (response) {
        deferred.reject(response);
      });
      return deferred.promise;
    };

    factory.GetDameDatosGenerales = function () {
      var deferred = $q.defer();
      var config = { headers: { 'Authorization': $localStorage.currentUser.token } };
      var Parametros = { };
      $http.post(globalService.getUrl() + paths.GetDameDatosGenerales, JSON.stringify(Parametros), config).then(function (response) {
        deferred.resolve(response.data);
      }).catch(function (response) {
        deferred.reject(response);
      });
      return deferred.promise;
    };

    factory.GetConPreliminarBancosList = function (Clv_Periodo) {
      var deferred = $q.defer();
      var config = { headers: { 'Authorization': $localStorage.currentUser.token } };
      var Parametros = {'Clv_Periodo': Clv_Periodo,  'Op': 0 };
      $http.post(globalService.getUrl() + paths.GetConPreliminarBancosList, JSON.stringify(Parametros), config).then(function (response) {
        deferred.resolve(response.data);
      }).catch(function (response) {
        deferred.reject(response);
      });
      return deferred.promise;
    };
    
    factory.GetCONDETFACTURASBANCOSList = function (Clv_SessionBancos) {
      var deferred = $q.defer();
      var config = { headers: { 'Authorization': $localStorage.currentUser.token } };
      var Parametros = {'Clv_SessionBancos': Clv_SessionBancos,  'Op': 0 };
      $http.post(globalService.getUrl() + paths.GetCONDETFACTURASBANCOSList, JSON.stringify(Parametros), config).then(function (response) {
        deferred.resolve(response.data);
      }).catch(function (response) {
        deferred.reject(response);
      });
      return deferred.promise;
    };

    factory.AddGeneraBancos = function (Periodo, Cajera, Sucursal, Caja, Fecha) {
      var deferred = $q.defer();
      var config = { headers: { 'Authorization': $localStorage.currentUser.token } };
      var objCargoAutomatico = {'Clv_Periodo': Periodo,  'IdSistema': 'AG',  'Clv_Banco': 1, 'Op': 1 ,  'Cajera': Cajera,  'Sucursal': Sucursal, 'Caja': Caja, 'fechaFiltro': Fecha};
      var Parametros = {'objCargoAutomatico': objCargoAutomatico};
      $http.post(globalService.getUrl() + paths.AddGeneraBancos, JSON.stringify(Parametros), config).then(function (response) {
        deferred.resolve(response.data);
      }).catch(function (response) {
        deferred.reject(response);
      });
      return deferred.promise;
    };

    factory.GetValidaAfectacionBancos_WEB = function (Clv_SessionBancos) {
      var deferred = $q.defer();
      var config = { headers: { 'Authorization': $localStorage.currentUser.token } };
      var Parametros = { 'Clv_SessionBancos': Clv_SessionBancos };
      $http.post(globalService.getUrl() + paths.GetValidaAfectacionBancos_WEB, JSON.stringify(Parametros), config).then(function (response) {
        deferred.resolve(response.data);
      }).catch(function (response) {
        deferred.reject(response);
      });
      return deferred.promise;
    };

    factory.UpdateGUARDAFACTURASBANCOS = function (objCargoAutomatico) {
      var deferred = $q.defer();
      var config = { headers: { 'Authorization': $localStorage.currentUser.token } };
      var Parametros = { 'objCargoAutomatico': objCargoAutomatico };
      $http.post(globalService.getUrl() + paths.UpdateGUARDAFACTURASBANCOS, JSON.stringify(Parametros), config).then(function (response) {
        deferred.resolve(response.data);
      }).catch(function (response) {
        deferred.reject(response);
      });
      return deferred.promise;
    };

    factory.GetCANCELALISTADOPRELIMINAR_web = function (Clv_SessionBancos) {
      var deferred = $q.defer();
      var config = { headers: { 'Authorization': $localStorage.currentUser.token } };
      var Parametros = { 'Clv_SessionBancos': Clv_SessionBancos };
      $http.post(globalService.getUrl() + paths.GetCANCELALISTADOPRELIMINAR_web, JSON.stringify(Parametros), config).then(function (response) {
        deferred.resolve(response.data);
      }).catch(function (response) {
        deferred.reject(response);
      });
      return deferred.promise;
    };

    factory.UpdateMODDetFacturasBancos = function (objCargoAutomatico) {
      var deferred = $q.defer();
      var config = { headers: { 'Authorization': $localStorage.currentUser.token } };
      var Parametros = { 'objCargoAutomatico': objCargoAutomatico };
      $http.post(globalService.getUrl() + paths.UpdateMODDetFacturasBancos, JSON.stringify(Parametros), config).then(function (response) {
        deferred.resolve(response.data);
      }).catch(function (response) {
        deferred.reject(response);
      });
      return deferred.promise;
    };


    factory.GetDameRangoFacturas = function (Clv_SessionBancos) {
      var deferred = $q.defer();
      var config = { headers: { 'Authorization': $localStorage.currentUser.token } };
      var Parametros = { 'Clv_SessionBancos': Clv_SessionBancos };
      $http.post(globalService.getUrl() + paths.GetDameRangoFacturas, JSON.stringify(Parametros), config).then(function (response) {
        deferred.resolve(response.data);
      }).catch(function (response) {
        deferred.reject(response);
      });
      return deferred.promise;
    };

    factory.GetTicket = function (ClvFac) {
      var deferred = $q.defer();
      var config = { headers: { 'Authorization': $localStorage.currentUser.token } };
      var Parametros = { 'Clv_Factura': ClvFac };
      $http.post(globalService.getUrl() + paths.GetTicket, JSON.stringify(Parametros), config).then(function (response) {
        deferred.resolve(response.data);
      }).catch(function (response) {
        deferred.reject(response);
      });
      return deferred.promise;
    };

    factory.GetRepDetallePreliminar = function (Clv_SessionBancos) {
      var deferred = $q.defer();
      var config = { headers: { 'Authorization': $localStorage.currentUser.token } };
      var Parametros = { 'Clv_SessionBancos': Clv_SessionBancos };
      $http.post(globalService.getUrl() + paths.GetRepDetallePreliminar, JSON.stringify(Parametros), config).then(function (response) {
        deferred.resolve(response.data);
      }).catch(function (response) {
        deferred.reject(response);
      });
      return deferred.promise;
    };


    factory.GetConsulta_Grales_Prosa_bancomer = function (Clv_SessionBancos) {
      var deferred = $q.defer();
      var config = { headers: { 'Authorization': $localStorage.currentUser.token } };
      var Parametros = { 'Clv_SessionBancos': Clv_SessionBancos, 'Ruta':'' };
      $http.post(globalService.getUrl() + paths.GetConsulta_Grales_Prosa_bancomer, JSON.stringify(Parametros), config).then(function (response) {
        deferred.resolve(response.data);
      }).catch(function (response) {
        deferred.reject(response);
      });
      return deferred.promise;
    };

    factory.GetGuardaDocumentoTxt = function (EvidenciaFD) {
      var deferred = $q.defer();
      var config = { headers: { 'Authorization': $localStorage.currentUser.token, 'Content-Type': undefined } };
      $http.post(globalService.getUrl() + paths.GetGuardaDocumentoTxt, EvidenciaFD, config).then(function (response) {
        deferred.resolve(response.data);
      }).catch(function (response) {
        deferred.reject(response);
      });
      return deferred.promise;
    };

    factory.GetDame_PreRecibo_oFactura = function (ClvId) {
      var deferred = $q.defer();
      var config = { headers: { 'Authorization': $localStorage.currentUser.token } };
      var Parametros = {'Clv_Id': ClvId };
      $http.post(globalService.getUrl() + paths.GetDame_PreRecibo_oFactura, JSON.stringify(Parametros), config).then(function (response) {
        deferred.resolve(response.data);
      }).catch(function (response) {
        deferred.reject(response);
      });
      return deferred.promise;
    };


    return factory;
  });
