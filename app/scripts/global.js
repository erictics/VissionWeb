
'use strict';
angular
  .module('softvApp')
  .service('globalService', function () {
    var svc = {};
     
    svc.getUrl = function () {   
      return 'http://localhost:64481/SoftvWCFService.svc';
    };

    svc.getUrlReportes = function () {
     return 'http://localhost:64481';
    };

    svc.getUrlinfoSistema = function () {
     return 'http://192.168.50.100:8000/ConfigSistema/AppWCFService.svc';
    };

    svc.getUrllogos = function () {
      return 'http://192.168.50.100:8000/ConfigSistema/logos';     
    };

    svc.getUrlMizar = function () {   
      return 'http://localhost:64483/SoftvWCFService.svc';
    }; 

    svc.getUrlMizarReportes = function () {    
      return 'http://localhost:64483/';    
    }; 
    
    return svc;

  });