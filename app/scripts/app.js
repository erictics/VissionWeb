'use strict';
angular
  .module('softvApp', [
    'angular-loading-bar',
    'ngAnimate',
    'ui.router',
    'ui.bootstrap',
    'ngNotify',
    'ngStorage',
    'angularUtils.directives.dirPagination',
    'blockUI',
    'angularValidator',
    'permission', 'permission.ui',
    'ui.mask', 'dndLists',
    'smart-table',
    'ngSanitize',
    'ngCsv',
    'googlechart',
    'angucomplete-alt',
    'treeControl',
    'multipleSelect',
    'base64',
    'colorpicker.module',
    'moment-picker',
    'angularMoment',
    'angularFileUpload',
    'pageslide-directive',
    'ng-percent',
    'currencyFilter'
  ])
  .config(['$provide', '$urlRouterProvider', '$httpProvider', 'cfpLoadingBarProvider', '$qProvider', 'blockUIConfig', function ($provide, $urlRouterProvider, $httpProvider, cfpLoadingBarProvider, $qProvider, blockUIConfig) {
    $urlRouterProvider.otherwise(function ($injector) {
      var $state = $injector.get('$state');
      $state.go('auth');
    });
    /*  agcLibraryLoaderProvider.agcLibraryLoaderProvider.setLoader('gstatic');
      agcGstaticLoaderProvider
              .setVersion('45')
              .addPackage('map')
              .setOption('mapsApiKey', '[AIzaSyBo8Db0Dc56FBW_ZY4dVBlLAzhjQpgZpD0]');*/
    /*angular.lowercase = text => text.toLowerCase();*/
    cfpLoadingBarProvider.includeSpinner = false;
    $qProvider.errorOnUnhandledRejections(false);
    blockUIConfig.templateUrl = 'views/components/loading.html';


    // $provide.factory('myHttpInterceptor', function ($q, $localStorage, $injector) {

    //   return {

    //     request: function (config) {

    //       if ($localStorage.currentUser) {
    //           var moment = $injector.get('moment');
    //           var $uibModal = $injector.get('$uibModal');
    //           var $window = $injector.get('$window');
    //           //var Modal = $injector.get('$uibModal');
    //            var endTime = moment();              
    //           var seconds = endTime.diff($localStorage.currentUser.LastRequest, 'seconds');
    //           if (seconds >= 600) {
    //             delete $localStorage.currentUser;
    //             var modalInstance = $uibModal.open({
    //               animation: true,
    //               ariaLabelledBy: 'modal-title',
    //               ariaDescribedBy: 'modal-body',
    //               templateUrl: 'views/login/ModalSesionExpirada.html',
    //               controller: 'ModalSesionExpiradaCtrl',
    //               controllerAs: 'ctrl',
    //               backdrop: 'static',
    //               keyboard: false,
    //               class: 'modal-backdrop fade',
    //               size: 'md'
    //             });
    //             modalInstance.result.then(function (res) {
    //               $window.location.reload();
    //             });

    //           // $window.location.reload();
    //          }
    //         if( $localStorage.currentUser){
    //              $localStorage.currentUser.LastRequest = moment().format();
    //         }

            
    //       }
         
    //       return config;
    //     }
    //   };
    // });

    $provide.factory('ErrorHttpInterceptor', function ($q, $injector, $localStorage, $location) {
      function notifyError(rejection) {
        var notify = $injector.get('ngNotify');
        if (rejection.data === 'Acceso no autorizado, favor de validar autenticación') {
          delete $localStorage.currentUser;
          notify.set('Acceso no autorizado, por favor inicia sesión nuevamente.', {
            type: 'error',
            duration: 4000
          });
          $location.path('/auth/');
          return;
        }

        var content = '¡Se ha generado un error! \n' + rejection.data;
        notify.set(content, {
          type: 'error',
          duration: 4000
        });
      }

      return {
        requestError: function (rejection) {
          notifyError(rejection);
          return $q.reject(rejection);
        },
        responseError: function (rejection) {
          notifyError(rejection);
          sessionStorage.clear();
          return $q.reject(rejection);
        }
      };
    });
    $httpProvider.interceptors.push('ErrorHttpInterceptor');
    //$httpProvider.interceptors.push('myHttpInterceptor');
    $httpProvider.defaults.headers.post['Content-Type'] = 'application/json; charset=utf-8';
    delete $httpProvider.defaults.headers.common['X-Requested-With'];
  }])
  .constant('APP_CONFIG', window.appConfig)
  .run(['$rootScope', '$window', '$state', '$stateParams', '$localStorage', '$location', 'PermPermissionStore', 'PermRoleStore', 'inMenu', 'amMoment', function ($rootScope, $window, $state, $stateParams, $localStorage, $location, PermPermissionStore, PermRoleStore, inMenu, amMoment) {
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;

    amMoment.changeLocale('es');





    if ($localStorage.currentUser) {
      //$location.path('/home/');
      PermPermissionStore.definePermission('anonymous', function () {
        return false;
      });
      var permissions = inMenu.on();
      PermPermissionStore.defineManyPermissions(permissions, function () {
        return true;
      });
    } else {
      $location.path('/auth/');
      PermPermissionStore.definePermission('anonymous', function () {
        return true;
      });
    }
  }]);
