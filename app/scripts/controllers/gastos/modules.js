'use strict';

angular
    .module('softvApp')
    .config(function ($stateProvider) {
        var states = [
            {
                name: 'home.gastos',
                abstract: true,
                template: '<div ui-view></div>'  
            },
            {
                name: 'home.gastos.clasificaciongastos',
                data: {
                    pageTitle: 'SOFTV | CLASIFICACIÓN DE GASTOS',
                    permissions: {
                        only: ['clasificaciondegastosSelect'],
                        options: {
                          reload: false
                        }
                      }
                },
                url: '/gastos/clasificaciongastos',
                templateUrl: 'views/gastos/ClasificacionGastos.html',
                controller: 'ClasificacionGastosCtrl',
                controllerAs: '$ctrl'
            },
            {
                name: 'home.gastos.categoriagastos',
                data: {
                    pageTitle: 'SOFTV | CATEGORÍA DE GASTOS',
                    permissions: {
                        options: {
                            reload: false
                        }
                    }
                },
                url: '/gastos/categoriagastos',
                templateUrl: 'views/gastos/CategoriaGastos.html',
                controller: 'CategoriaGastosCtrl',
                controllerAs: '$ctrl'
            },
            {
                name: 'home.gastos.departamentos',
                data: {
                    pageTitle: 'SOFTV | DEPARTAMENTOS',
                    permissions: {
                        options: {
                            reload: false
                        }
                    }
                },
                url: '/gastos/departamentos',
                templateUrl: 'views/gastos/Departamentos.html',
                controller: 'DepartamentoCtrl',
                controllerAs: '$ctrl'
            },
            {
                name: 'home.gastos.gastos',
                data: {
                    pageTitle: 'SOFTV | SOLICITUD DE GASTOS',
                    permissions: {
                        options: {
                            reload: false
                        }
                    }
                },
                url: '/gastos/gastos',
                templateUrl: 'views/gastos/Gastos.html',
                controller: 'GastosCtrl',
                controllerAs: '$ctrl'
            },
            {
                name: 'home.gastos.gastosnuevo',
                data: {
                    pageTitle: 'SOFTV | NUEVA SOLICITUD DE GASTOS',
                    permissions: {
                        options: {
                            reload: false
                        }
                    }
                },
                url: '/gastos/gastosnuevo',
                templateUrl: 'views/gastos/GastosForm.html',
                controller: 'GastosAddCtrl',
                controllerAs: '$ctrl'
            },
            {
                name: 'home.gastos.gastoseditar',
                data: {
                    pageTitle: 'SOFTV | EDITAR SOLICITUD DE GASTOS',
                    permissions: {
                        options: {
                            reload: false
                        }
                    }
                },
                url: '/gastos/gastoseditar/:id',
                templateUrl: 'views/gastos/GastosForm.html',
                controller: 'GastosUpdateCtrl',
                controllerAs: '$ctrl'
            },
            {
                name: 'home.gastos.gastosconsultar',
                data: {
                    pageTitle: 'SOFTV | CONSULTAR SOLICITUD DE GASTOS',
                    permissions: {
                        options: {
                            reload: false
                        }
                    }
                },
                url: '/gastos/gastosconsultar/:id',
                templateUrl: 'views/gastos/GastosForm.html',
                controller: 'GastosViewCtrl',
                controllerAs: '$ctrl'
            },
            {
                name: 'home.gastos.reportesgastoaplicados',
                data: {
                    pageTitle: 'SOFTV | REPORTE GASTOS APLICADOS',
                    permissions: {
                        options: {
                            reload: false
                        }
                    }
                },
                url: '/gastos/reporte/gastosaplicados',
                templateUrl: 'views/gastos/gastosAplicados.html',
                controller: 'gastosAplicadosCtrl',
                controllerAs: '$ctrl'
            },
            {
                name: 'home.gastos.reportesgastopendiente',
                data: {
                    pageTitle: 'SOFTV | REPORTE GASTOS PENDIENTES',
                    permissions: {
                        options: {
                            reload: false
                        }
                    }
                },
                url: '/gastos/reporte/gastospendientes',
                templateUrl: 'views/gastos/gastosPendientes.html',
                controller: 'gastosPendientesCtrl',
                controllerAs: '$ctrl'
            },
            {
                name: 'home.gastos.reportesgastoautorizados',
                data: {
                    pageTitle: 'SOFTV | REPORTE GASTOS AUTORIZADOS',
                    permissions: {
                        options: {
                            reload: false
                        }
                    }
                },
                url: '/gastos/reporte/gastosautorizados',
                templateUrl: 'views/gastos/gastosAutorizados.html',
                controller: 'gastosAutorizadosCtrl',
                controllerAs: '$ctrl'
            },
            {
                name: 'home.gastos.reportesgastopendaplicar',
                data: {
                    pageTitle: 'SOFTV | REPORTE GASTOS PENDIENTES APLICAR',
                    permissions: {
                        options: {
                            reload: false
                        }
                    }
                },
                url: '/gastos/reporte/gastospendientesaplicar',
                templateUrl: 'views/gastos/gastosPendientesAplicar.html',
                controller: 'gastosPendientesAplicarCtrl',
                controllerAs: '$ctrl'
            },
            {
                name: 'home.gastos.autorizaciongastos',
                data: {
                    pageTitle: 'SOFTV | AUTORIZACION Y APLICACION DE GASTOS',
                    permissions: {
                        options: {
                            reload: false
                        }
                    }
                },
                url: '/gastos/autorizacion',
                templateUrl: 'views/gastos/autorizacionGasto.html',
                controller: 'autorizacionGastoCtrl',
                controllerAs: '$ctrl'
            },
            {
                name: 'home.gastos.autorizaciondetalle',
                data: {
                    pageTitle: 'SOFTV | AUTORIZACION Y APLICACION DE GASTOS',
                    permissions: {
                        options: {
                            reload: false
                        }
                    }
                },
                url: '/gastos/autorizacion/detalle/:id',
                templateUrl: 'views/gastos/autorizaDetalleGasto.html',
                controller: 'autorizaDetalleGastoCtrl',
                controllerAs: '$ctrl'
            },
            {
                name: 'home.gastos.aplicaciondetalle',
                data: {
                    pageTitle: 'SOFTV | AUTORIZACION Y APLICACION DE GASTOS',
                    permissions: {
                        options: {
                            reload: false
                        }
                    }
                },
                url: '/gastos/aplicacion/detalle/:id',
                templateUrl: 'views/gastos/aplicaDetalleGasto.html',
                controller: 'aplicaDetalleGastoCtrl',
                controllerAs: '$ctrl'
            }       

        ];
        states.forEach(function (state) {
            $stateProvider.state(state);
        });
    });