'use strict';
angular
    .module('softvApp')
    .controller('gastosPendientesCtrl', function(GastosFactory,globalService, distribuidorFactory, plazaFactory, CatalogosFactory,$sce){

        function initData(){
            GetDistribuidoresList();
            getClasificacion();
            GetDepartamento();
        }
        
        function GetDepartamento(){
            GastosFactory.GetDepartamento().then(function(res){
                console.log(res)
              vm.departamentos=res.GetDepartamentoResult;
            });            
        }

        function getClasificacion(){
            GastosFactory.GetClasificacionGasto().then(function(res){
                console.log(res)
              vm.clasificaciones=res.GetClasificacionGastoResult;
            });
        }

        function GetDistribuidoresList(){
            distribuidorFactory.Getplaza(0,'').then(function(data){
                vm.DistribuidorList = data.GetPlaza_DistribuidoresNewResult;
            });
            vm.PlazaList = null;
            vm.Estado = null;
            vm.CiudadList = null;
        }

        function GetPlazaList(){
            if(vm.Empresa != undefined){
                var ObjPlaza = {
                    'opcion': 3,
                    'razon': '',
                    'idcompania': vm.Empresa.Clv_Plaza
                };
                plazaFactory.GetBrwMuestraCompanias(ObjPlaza).then(function (result) {
                    vm.PlazaList =  result.GetBrwMuestraCompaniasResult;
                });
             }else{
                vm.PlazaList = null;
            }
            vm.EstadoList = null;
            vm.CiudadList = null;
        }

        function GetEstadoList(){
           if(vm.Region != undefined){
                CatalogosFactory.GetMuestraEstadosCompaniaList(vm.Region.id_compania).then(function(data){
                    vm.EstadoList = data.GetMuestraEstadosCompaniaListResult;
                });
             }else{
                vm.EstadoList = null;
            }
            vm.CiudadList = null;
        }

        function GetCiudadList(){
            if(vm.Estado != undefined){
                var RelEstMun = {
                    'clv_estado' : vm.Estado.Clv_Estado,
                    'idcompania' : vm.Region.id_compania
                };
                CatalogosFactory.GetMuestraCiudadesEstadoList(RelEstMun).then(function(data){
                    vm.CiudadList = data.GetMuestraCiudadesEstadoListResult;
                });
             }else{
                vm.CiudadMunicipioList = null;
            }
        }

        function generar(){
            var param={
                'reporte':{
                    'IdDistribuidor':(vm.Empresa)?vm.Empresa.Clv_Plaza:-1,
                    'IdCompania':(vm.Region)? vm.Region.id_compania: -1,
                    'IdCd':(vm.Ciudad)?vm.Ciudad.Clv_Ciudad:-1,
                    'IdDpto':( vm.departamento)? vm.departamento.IdDpto:-1,
                    'IdClasificacion':(vm.clasificacion)?vm.clasificacion.Clv_Clasificacion:-1,
                    'fechaInicio': document.getElementById('fechaInicio').value,
                    'fechaFin': document.getElementById('fechaFin').value
                }                
            }          

            GastosFactory.GetReporteGastoPendiente(param).then(function(result){
                vm.url = $sce.trustAsResourceUrl(globalService.getUrlReportes() + '/Reportes/' + result.GetReporteGastoPendienteResult);
              console.log(result.GetReporteGastoPendienteResult);
            });
            console.log(param);
        }

        var vm = this;
        vm.Icono = 'fa fa-plus';
        vm.Title = 'Nueva Solicitud de Gasto';
        vm.BtnCanTitulo ='Cancelar';
        vm.ShowSave = false;
        vm.GetPlazaList = GetPlazaList;
        vm.GetEstadoList = GetEstadoList;
        vm.GetCiudadList = GetCiudadList;
        vm.generar=generar;
        initData();
        $('#fechaInicio').datepicker();
        $('#fechaFin').datepicker();

    });