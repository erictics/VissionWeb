'use strict';
angular
    .module('softvApp')
    .controller('DepartamentoCtrl', function(GastosFactory, $localStorage, $scope, $uibModal){

        function initData(){
            vm.ViewList = true;
            GastosFactory.GetDepartamento().then(function(res){
                console.log(res);
                vm.departamentos=res.GetDepartamentoResult;
            });
        }

        function OpenDepartamentoAdd(){
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'views/gastos/DepartamentosForm.html',
                controller: 'DepartamentoAddCtrl',
                controllerAs: 'ctrl',
                backdrop: 'static',
                keyboard: false,
                class: 'modal-backdrop fade',
                size: 'sm'
            });
            modalInstance.result.then(function () {
                initData();
            });
        }        

        function OpenDepartamentoUpdate(obj){          
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'views/gastos/DepartamentosForm.html',
                controller: 'DepartamentoUpdateCtrl',
                controllerAs: 'ctrl',
                backdrop: 'static',
                keyboard: false,
                class: 'modal-backdrop fade',
                size: 'sm',
                resolve: {
                    obj: function () {
                        return obj;
                    }
                }
            });
            modalInstance.result.then(function () {
                initData();
            });
        }

        function OpenDepartamentoDelete(obj){
           
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'views/gastos/ModalDelete.html',
                controller: 'DepartamentoDeleteCtrl',
                controllerAs: 'ctrl',
                backdrop: 'static',
                keyboard: false,
                class: 'modal-backdrop fade',
                size: 'sm',
                resolve: {
                    ObjGasto: function () {
                        return obj;
                    }
                }
            });
            modalInstance.result.then(function () {
                initData();
            });
        }

        var vm = this;
        vm.OpenDepartamentoAdd = OpenDepartamentoAdd;
        vm.OpenDepartamentoUpdate = OpenDepartamentoUpdate;        
        vm.OpenDepartamentoDelete = OpenDepartamentoDelete;
        initData();

    });