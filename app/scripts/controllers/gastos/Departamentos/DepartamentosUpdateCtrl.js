'use strict';
angular
    .module('softvApp')
    .controller('DepartamentoUpdateCtrl', function(GastosFactory, $uibModalInstance, $uibModal, ngNotify, $state, obj, logFactory){

        function initData(){
           vm.IdDpto=obj.IdDpto;
           vm.Descripcion=obj.Descripcion;
        }

        function Close() {
            $uibModalInstance.close();
        }

        function SaveDepartamento(){            
            GastosFactory.UpdateDepartamento(vm.IdDpto,vm.Descripcion).then(function(){
                var log={
                    'Modulo':'home.gastos',
                    'Submodulo':'home.gastos.departamentos',
                    'Observaciones':'Se edito una area de departamento',
                    'Comando':JSON.stringify(vm.IdDpto,vm.Descripcion),
                    'Clv_afectada':vm.IdDpto,
                    'IdClassLog':2
                  };
                  logFactory.AddMovSist(log).then(function(result){ console.log('add'); });
                ngNotify.set('El area se editó correctamente','success');
                $uibModalInstance.close();
            });
        }

        var vm = this;
        vm.Icono = 'fa fa-pencil-square-o';
        vm.Titulo = 'Editar Area Administrativa';
        vm.BtnCanTitulo ='Cancelar';
        vm.ShowSave = false;
        vm.Close = Close;
        vm.SaveDepartamento=SaveDepartamento;
        initData();
        
    });