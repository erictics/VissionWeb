'use strict';
angular
    .module('softvApp')
    .controller('DepartamentoAddCtrl', function(GastosFactory, $uibModalInstance, $uibModal, ngNotify, $state, logFactory){
        
        function SaveDepartamento(){            
            GastosFactory.AddDepartamento(vm.Descripcion).then(function(){
                var log={
                    'Modulo':'home.gastos',
                    'Submodulo':'home.gastos.departamentos',
                    'Observaciones':'Se agrego una area de departamento',
                    'Comando':JSON.stringify(vm.Descripcion),
                    'Clv_afectada':0,
                    'IdClassLog':1
                  };
                  logFactory.AddMovSist(log).then(function(result){ console.log('add'); });
                ngNotify.set('El area se guardó correctamente','success');
                $uibModalInstance.close();
            });
        }

        function Close() {
            $uibModalInstance.close();
        }

        var vm = this;
        vm.Icono = 'fa fa-plus';
        vm.Titulo = 'Nuevo Area Administrativa';
        vm.BtnCanTitulo ='Cancelar';
        vm.ShowSave = false;
        vm.Close = Close;
        vm.SaveDepartamento=SaveDepartamento; 
              
    });