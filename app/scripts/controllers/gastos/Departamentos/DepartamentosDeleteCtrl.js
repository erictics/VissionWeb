'use strict';
angular
    .module('softvApp')
    .controller('DepartamentoDeleteCtrl', function(GastosFactory, $uibModalInstance, $uibModal, ngNotify, $state, ObjGasto, logFactory){

        function Close() {
            $uibModalInstance.close();
        }

        function Delete(){
            GastosFactory.DeleteDepartamento(ObjGasto.IdDpto).then(function(result){
                var log={
                    'Modulo':'home.gastos',
                    'Submodulo':'home.gastos.departamentos',
                    'Observaciones':'Se elimino una area de departamento',
                    'Comando':JSON.stringify(ObjGasto.IdDpto),
                    'Clv_afectada':ObjGasto.IdDpto,
                    'IdClassLog':3
                  };
                  logFactory.AddMovSist(log).then(function(result){ console.log('add'); });
                ngNotify.set('El area administrativa se ha eliminado correctamente','success');
                $uibModalInstance.close();
            });
        }
       
        
        var vm = this;
        vm.Titulo = 'Eliminar Area';
        vm.Tipo = 'Departamento';
        vm.Clave = ObjGasto.IdDpto;
        vm.Descripcion = ObjGasto.Descripcion;
        vm.Close = Close;
        vm.Delete=Delete;
        
    });