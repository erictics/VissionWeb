'use strict';
angular
    .module('softvApp')
    .controller('CategoriaGastosUpdateCtrl', function(GastosFactory, $uibModalInstance, $uibModal, ngNotify, $state, gasto, logFactory){

        function initData(){
        console.log(gasto);
        vm.id=gasto.Clv_CateGasto;
        vm.Descripcion=gasto.Descripcion;
        vm.Importe=gasto.Importe;
        vm.Fijo=(gasto.Fijo===1) ? 'S':'N';        

        GastosFactory.GetClasificacionGasto().then(function(result){           
            vm.clasificaciones=result.GetClasificacionGastoResult;
            vm.clasificaciones.forEach(function(element) {
                if(element.Clv_Clasificacion === gasto.Clv_Clasificacion){
                    vm.clasificacion=element;
                }                
            });
            GastosFactory.GetDepartamento().then(function(result){                   
                vm.departamentos=result.GetDepartamentoResult;
                vm.departamentos.forEach(function(element){
                if(element.IdDpto === gasto.IdDpto){
                    vm.departamento=element;
                }                    
                });
            });
           });
        }

        function SaveClasGasto(){           
            
        var data= {
            'Clv_CateGasto':vm.id,
            'Descripcion': vm.Descripcion,
            'Clv_Clasificacion': vm.clasificacion.Clv_Clasificacion,
            'Importe': vm.Importe,
            'Fijo': (vm.Fijo === 'S')?1:0,
            'IdDpto':  vm.departamento.IdDpto
            };           

            GastosFactory.UpdateCategoriaGasto(data).then(function(result){
                var log={
                    'Modulo':'home.gastos',
                    'Submodulo':'home.gastos.categoriagastos',
                    'Observaciones':'Se edito una categoria de gastos',
                    'Comando':JSON.stringify(data),
                    'Clv_afectada':vm.id,
                    'IdClassLog':2
                  };
                  logFactory.AddMovSist(log).then(function(result){ console.log('add'); });
            ngNotify.set('El gasto se editó correctamente','success');
            $uibModalInstance.close();
            });
        }

        function Close() {
            $uibModalInstance.close();
        }

        var vm = this;
        vm.Icono = 'fa fa-pencil-square-o';
        vm.Titulo = 'Editar Gasto';
        vm.BtnCanTitulo ='Cancelar';
        vm.ShowSave = false;
        vm.Close = Close;
        vm.SaveClasGasto=SaveClasGasto;
        initData();
        
    });