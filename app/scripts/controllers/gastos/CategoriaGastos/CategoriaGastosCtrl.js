'use strict';
angular
    .module('softvApp')
    .controller('CategoriaGastosCtrl', function(GastosFactory, $localStorage, $scope, $uibModal){

        function initData(){
            vm.ViewList = true;
            GastosFactory.GetCategoriaGasto().then(function(result){
                console.log(result)
              vm.categorias=result.GetCategoriaGastoResult;
            });
        }

        function OpenCatGastoAdd(){
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'views/gastos/CategoriaGastosForm.html',
                controller: 'CategoriaGastosAddCtrl',
                controllerAs: 'ctrl',
                backdrop: 'static',
                keyboard: false,
                class: 'modal-backdrop fade',
                size: 'md'
            });
            modalInstance.result.then(function () {
                initData()
            });
        }

        function OpenCatGastoUpdate(x){           
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'views/gastos/CategoriaGastosForm.html',
                controller: 'CategoriaGastosUpdateCtrl',
                controllerAs: 'ctrl',
                backdrop: 'static',
                keyboard: false,
                class: 'modal-backdrop fade',
                size: 'md',
                resolve: {
                    gasto: function () {
                        return x;
                    }
                }
            });
            modalInstance.result.then(function () {
                initData()
            });
        }

        function OpenCatGastoDelete(obj){
           
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'views/gastos/ModalDelete.html',
                controller: 'CategoriaGastosDeleteCtrl',
                controllerAs: 'ctrl',
                backdrop: 'static',
                keyboard: false,
                class: 'modal-backdrop fade',
                size: 'sm',
                resolve: {
                    ObjGasto: function () {
                        return obj;
                    }
                }
            });
            modalInstance.result.then(function () {
                initData();
            });
        }

        var vm = this;
        vm.OpenCatGastoAdd = OpenCatGastoAdd;
        vm.OpenCatGastoUpdate = OpenCatGastoUpdate;        
        vm.OpenCatGastoDelete = OpenCatGastoDelete;
        initData();

    });