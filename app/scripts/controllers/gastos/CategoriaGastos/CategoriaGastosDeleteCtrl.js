'use strict';
angular
    .module('softvApp')
    .controller('CategoriaGastosDeleteCtrl', function(GastosFactory, $uibModalInstance, $uibModal, ngNotify, $state, ObjGasto, logFactory){

         function Close() {
            $uibModalInstance.close();
        }

        function Delete(){
            GastosFactory.DeleteCategoriaGasto(ObjGasto.Clv_CateGasto).then(function(result){
                var log={
                    'Modulo':'home.gastos',
                    'Submodulo':'home.gastos.categoriagastos',
                    'Observaciones':'Se elimino una categoria de gastos',
                    'Comando':JSON.stringify(ObjGasto.Clv_CateGasto),
                    'Clv_afectada':ObjGasto.Clv_CateGasto,
                    'IdClassLog':3
                  };
                  logFactory.AddMovSist(log).then(function(result){ console.log('add'); });
                ngNotify.set('La clasificación se ha eliminado correctamente','success');
                $uibModalInstance.close();
            });
        }


        var vm = this;
        vm.Titulo = 'Eliminar Categoría de Gastos';
        vm.Tipo = 'Categoría de Gasto';
        vm.Clave = ObjGasto.Clv_CateGasto;
        vm.Descripcion = ObjGasto.Descripcion;
        vm.Close = Close;
        vm.Delete=Delete;
        console.log(ObjGasto);
        
    });