'use strict';
angular
    .module('softvApp')
    .controller('CategoriaGastosAddCtrl', function(GastosFactory, $uibModalInstance, $uibModal, ngNotify, $state, logFactory){

        function initData(){
            GastosFactory.GetClasificacionGasto().then(function(result){
             console.log(result);
             vm.clasificaciones=result.GetClasificacionGastoResult;
                GastosFactory.GetDepartamento().then(function(result){
                    console.log(result);
                    vm.departamentos=result.GetDepartamentoResult;
                });
            });
        }

        function Close() {
            $uibModalInstance.close();
        }

        function SaveClasGasto(){           

           var data= {
                'Descripcion': vm.Descripcion,
                'Clv_Clasificacion': vm.clasificacion.Clv_Clasificacion,
                'Importe': (vm.Importe)?vm.Importe:0,
                'Fijo': (vm.Fijo==='S')?1:0,
                'IdDpto':  vm.departamento.IdDpto
              };
              console.log(data);
              
              GastosFactory.AddCategoriaGasto(data).then(function(result){
                var log={
                    'Modulo':'home.gastos',
                    'Submodulo':'home.gastos.categoriagastos',
                    'Observaciones':'Se agrego una categoria de gastos',
                    'Comando':JSON.stringify(data),
                    'Clv_afectada':0,
                    'IdClassLog':1
                  };
                  logFactory.AddMovSist(log).then(function(result){ console.log('add'); });
                $uibModalInstance.close();
                ngNotify.set('El gasto se guardo correctamente','success');
                
              });
        }

        var vm = this;
        vm.Icono = 'fa fa-plus';
        vm.Titulo = 'Nuevo Gasto';
        vm.BtnCanTitulo ='Cancelar';
        vm.ShowSave = false;
        vm.Fijo = 'S';
        vm.Close = Close;
        vm.SaveClasGasto=SaveClasGasto;
        initData();
        
    });