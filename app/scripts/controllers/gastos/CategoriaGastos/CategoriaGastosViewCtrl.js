'use strict';
angular
    .module('softvApp')
    .controller('CategoriaGastosViewCtrl', function(GastosFactory, $uibModalInstance, $uibModal, ngNotify, $state, Clave){

        function initData(){
        }

        function Close() {
            $uibModalInstance.close();
        }

        var vm = this;
        vm.Icono = 'fa fa-eye';
        vm.Titulo = 'Consultar Categoría de Gastos';
        vm.BtnCanTitulo ='Salir';
        vm.ShowSave = true;
        vm.Close = Close;
        initData();
        
    });