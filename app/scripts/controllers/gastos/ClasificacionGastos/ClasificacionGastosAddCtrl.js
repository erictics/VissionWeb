'use strict';
angular
    .module('softvApp')
    .controller('ClasificacionGastosAddCtrl', function(GastosFactory, $uibModalInstance, $uibModal, ngNotify, $state, logFactory){
       
        function Close() {
            $uibModalInstance.close();
        }

        function SaveClasGasto(){
             GastosFactory.AddClasificacionGasto(vm.Descripcion).then(function(resp){
                var log={
                    'Modulo':'home.gastos',
                    'Submodulo':'home.gastos.clasificaciongastos',
                    'Observaciones':'Se agrego una clasificacion de gasto',
                    'Comando':JSON.stringify(vm.Descripcion),
                    'Clv_afectada':0,
                    'IdClassLog':1
                  };
                  logFactory.AddMovSist(log).then(function(result){ console.log('add'); });
                ngNotify.set('La clasificación se ha guardado correctamente','success');
                $uibModalInstance.close();
            });
        }

        var vm = this;
        vm.Icono = 'fa fa-plus';
        vm.Titulo = 'Nueva Clasificacón de Gastos';
        vm.BtnCanTitulo ='Cancelar';
        vm.ShowSave = false;
        vm.Close = Close;
        vm.SaveClasGasto=SaveClasGasto;
        
    });