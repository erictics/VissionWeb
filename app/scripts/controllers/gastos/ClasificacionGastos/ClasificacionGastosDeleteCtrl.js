'use strict';
angular
    .module('softvApp')
    .controller('ClasificacionGastosDeleteCtrl', function(GastosFactory, $uibModalInstance, $uibModal, ngNotify, $state, ObjGasto, logFactory){

       
        function Close() {
            $uibModalInstance.close();            
        }

        function Delete(){
            GastosFactory.DeleteClasificacionGasto(ObjGasto.Clv_Clasificacion).then(function(result){
                var log={
                    'Modulo':'home.gastos',
                    'Submodulo':'home.gastos.clasificaciongastos',
                    'Observaciones':'Se elimino una clasificacion de gasto',
                    'Comando':JSON.stringify(ObjGasto.Clv_Clasificacion),
                    'Clv_afectada':ObjGasto.Clv_Clasificacion,
                    'IdClassLog':3
                  };
                  logFactory.AddMovSist(log).then(function(result){ console.log('add'); });
                ngNotify.set('La clasificación se ha eliminado correctamente','success');
                $uibModalInstance.close();
            });
        }

        var vm = this;
        vm.Titulo = 'Eliminar Clasificación de Gastos';
        vm.Tipo = 'Clasificación de Gasto';
        vm.Clave = ObjGasto.Clv_Clasificacion;
        vm.Descripcion = ObjGasto.Descripcion;
        vm.Close = Close;
        vm.Delete=Delete;        
        
    });