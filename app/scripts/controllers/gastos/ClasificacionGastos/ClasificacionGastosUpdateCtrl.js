'use strict';
angular
    .module('softvApp')
    .controller('ClasificacionGastosUpdateCtrl', function(GastosFactory, $uibModalInstance, $uibModal, ngNotify, $state, gasto, logFactory){

        function initData(){
          console.log(gasto);
          vm.id=gasto.Clv_Clasificacion;
          vm.Descripcion=gasto.Descripcion;
        }

        function Close() {
            $uibModalInstance.close();
        }

        function SaveClasGasto(){
            GastosFactory.UpdateClasificacionGasto(vm.id,vm.Descripcion).then(function(resp){
                var log={
                    'Modulo':'home.gastos',
                    'Submodulo':'home.gastos.clasificaciongastos',
                    'Observaciones':'Se edito una clasificacion de gasto',
                    'Comando':JSON.stringify(vm.id,vm.Descripcion),
                    'Clv_afectada':vm.id,
                    'IdClassLog':2
                  };
                  logFactory.AddMovSist(log).then(function(result){ console.log('add'); });
                ngNotify.set('La clasificación se ha editado correctamente','success');
                $uibModalInstance.close();
            });
        }

        var vm = this;
        vm.Icono = 'fa fa-pencil-square-o';
        vm.Titulo = 'Editar Clasificacón de Gastos';
        vm.BtnCanTitulo ='Cancelar';
        vm.ShowSave = false;
        vm.Close = Close;
        vm.SaveClasGasto=SaveClasGasto;
        initData();
        
    });