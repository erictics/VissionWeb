'use strict';
angular
    .module('softvApp')
    .controller('ClasificacionGastosCtrl', function(GastosFactory, $uibModal){

        function initData(){
            vm.ViewList = true;
            GastosFactory.GetClasificacionGasto().then(function(resp){
            vm.clasificaciones= resp.GetClasificacionGastoResult;              
            });
        }

        function OpenClasGastoAdd(){
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'views/gastos/ClasificacionGastosForm.html',
                controller: 'ClasificacionGastosAddCtrl',
                controllerAs: 'ctrl',
                backdrop: 'static',
                keyboard: false,
                class: 'modal-backdrop fade',
                size: 'sm'
            });
            modalInstance.result.then(function () {
                initData();
            });
        }

        function OpenClasGastoUpdate(Clave){
            var Clave = Clave;
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'views/gastos/ClasificacionGastosForm.html',
                controller: 'ClasificacionGastosUpdateCtrl',
                controllerAs: 'ctrl',
                backdrop: 'static',
                keyboard: false,
                class: 'modal-backdrop fade',
                size: 'sm',
                resolve: {
                    gasto: function () {
                        return Clave;
                    }
                }
            });
            modalInstance.result.then(function () {
                initData();
            });
        }

        function OpenClasGastoDelete(x){
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'views/gastos/ModalDelete.html',
                controller: 'ClasificacionGastosDeleteCtrl',
                controllerAs: 'ctrl',
                backdrop: 'static',
                keyboard: false,
                class: 'modal-backdrop fade',
                size: 'sm',
                resolve: {
                    ObjGasto: function () {
                        return x;
                    }
                }
            });
            modalInstance.result.then(function () {
                initData();
            });
        }

        var vm = this;
        vm.OpenClasGastoAdd = OpenClasGastoAdd;
        vm.OpenClasGastoUpdate = OpenClasGastoUpdate;        
        vm.OpenClasGastoDelete = OpenClasGastoDelete;
        initData();

    });