'use strict';
angular
    .module('softvApp')
    .controller('GastosAddCtrl', function(GastosFactory, atencionFactory,distribuidorFactory, plazaFactory, CatalogosFactory, reportesVariosFactory, $uibModal, ngNotify, $state, $localStorage){

        function initData(){
            GetDistribuidoresList();
            GetDepartamentoList();
            GetClasGastoList();
        }


        function GetSucursales () {
            if(vm.Region){
                var Parametros = {
                    'Clv_Sucursal': 0,
                    'Nombre': '',
                    'OP': 2 ,
                    'idcompania': vm.Region.id_compania,
                    'clv_usuario': 0
                  };
                CatalogosFactory.GetSUCURSALES(Parametros).then(function(res){
                   vm.sucursales = res.GetSUCURSALESResult;
                });

            }

        }

        function GetDistribuidoresList(){
            reportesVariosFactory.mostrarDistribuidorByUsuario($localStorage.currentUser.idUsuario).then(function(result) {
                vm.DistribuidorList = result.GetDistribuidorByUsuarioResult;
            });
            vm.PlazaList = null;
            vm.Estado = null;
            vm.CiudadList = null;
        }

        function GetPlazaList(){
            if(vm.Distribuidor != undefined){
                var distribuidores = [];
                distribuidores.push(vm.Distribuidor);
                if (distribuidores.length > 0) {
                    reportesVariosFactory.mostrarPlazaByDistribuidor($localStorage.currentUser.idUsuario,distribuidores).then(function(result) {
                        vm.PlazaList = result.GetPlazasByDistribuidorResult;
                    });
                }
             }else{
                vm.PlazaList = null;
            }
            vm.EstadoList = null;
            vm.CiudadList = null;
        }

        function GetEstadoList(){
           if(vm.Region != undefined){
                CatalogosFactory.GetMuestraEstadosCompaniaList(vm.Region.id_compania).then(function(data){
                    vm.EstadoList = data.GetMuestraEstadosCompaniaListResult;
                    GetSucursales();
                });
             }else{
                vm.EstadoList = null;
            }
            vm.CiudadList = null;
        }

        function GetCiudadList(){
            if(vm.Estado != undefined){
                var RelEstMun = {
                    'clv_estado' : vm.Estado.Clv_Estado,
                    'idcompania' : vm.Region.id_compania
                };
                CatalogosFactory.GetMuestraCiudadesEstadoList(RelEstMun).then(function(data){
                    vm.CiudadList = data.GetMuestraCiudadesEstadoListResult;
                });
             }else{
                vm.CiudadMunicipioList = null;
            }
        }

        function GetDepartamentoList(){
            GastosFactory.GetDepartamento().then(function(data){
                vm.DepartamentoList = data.GetDepartamentoResult;
            }); 
        }

        function GetClasGastoList(){
            GastosFactory.GetClasificacionGasto().then(function(data){
                vm.ClasGastoList = data.GetClasificacionGastoResult;
            }); 
        }

        function GetGastosList(){
            vm.AddAll = (vm.Fijo == 'S')? true:false;
            vm.GastosList = null;
            if(vm.Fijo != undefined && vm.Departamento != undefined && vm.ClasGasto != undefined){
                vm.GastosIncList = (vm.IdDpto == vm.Departamento.IdDpto &&  vm.Clv_Clasificacion == vm.ClasGasto.Clv_Clasificacion)? vm.GastosIncList:[];
                var ObjDetalle = {
                    'fijo': (vm.Fijo == 'S')? 1:-1,
                    'Iddepartamento': vm.Departamento.IdDpto,
                    'Idclasificacion': vm.ClasGasto.Clv_Clasificacion,
                    
                };
                vm.IdDpto = vm.Departamento.IdDpto;
                vm.Clv_Clasificacion = vm.ClasGasto.Clv_Clasificacion;
                GastosFactory.GetfiltrosGastos(ObjDetalle).then(function(data){
                    console.log(data);
                    var GastosList = data.filtrosGastosResult;
                    if(vm.Fijo == 'S'){
                        vm.GastosList = [];
                        GastosList.forEach(function(element, key) {
                            if(vm.GastosIncList.length > 0){
                                if (ExisteGastosIncList(element) == true){
                                    var ObjDetalle = {
                                        'Clv_CateGasto': element.Clv_CateGasto,
                                        'Importe': element.Importe,
                                        'Descripcion': element.Descripcion,
                                        'Fijo': element.Fijo,
                                        'Status': 'P',
                                        'Aplicada': 0
                                    };
                                    vm.GastosIncList.push(ObjDetalle);
                                }
                             }else{
                                var ObjDetalle = {
                                    'Clv_CateGasto': element.Clv_CateGasto,
                                    'Importe': element.Importe,
                                    'Descripcion': element.Descripcion,
                                    'Fijo': element.Fijo,
                                    'Status': 'P',
                                    'Aplicada': 0
                                };
                                vm.GastosIncList.push(ObjDetalle);
                            }
                        });
                     }else{
                        vm.GastosList = GastosList;
                        vm.GastosIncList.forEach(function(element, key){
                            vm.GastosList.forEach(function(e, k){
                                if(element.Clv_CateGasto == e.Clv_CateGasto){
                                    vm.GastosList.splice(k, 1);
                                }
                            });
                        });
                    }
                });
             }else{
                vm.GastosList = null;
                vm.GastosIncList = [];
            }
        }

        function AddGastoSolicitud(ObjGasto, Status){
            if(Status === 1){
                var ObjDetalle = {
                    'Clv_CateGasto': ObjGasto.Clv_CateGasto,
                    'Importe': ObjGasto.Importe,
                    'ImporteO': ObjGasto.Importe,
                    'Descripcion': ObjGasto.Descripcion,
                    'Fijo': ObjGasto.Fijo,
                    'Status': 'P',
                    'Aplicada': 0,
                    'observaciones': ObjGasto.observaciones
                };
                vm.GastosIncList.push(ObjDetalle);
                vm.GastosList.forEach(function(element, key){
                    if(element.Clv_CateGasto == ObjGasto.Clv_CateGasto){
                        vm.GastosList.splice(key, 1);
                    }
                });
             }else if(Status == 0){
                vm.GastosIncList.forEach(function(element, key){
                    if(element.Clv_CateGasto == ObjGasto.Clv_CateGasto){
                        vm.GastosIncList.splice(key, 1);
                    }
                });
                var ObjDetalle = {
                    'Clv_CateGasto': ObjGasto.Clv_CateGasto,
                    'Importe': (vm.Fijo == 'S')? ObjGasto.Importe:ObjGasto.ImporteO,
                    'Descripcion': ObjGasto.Descripcion,
                    'Fijo': ObjGasto.Fijo,
                };
                vm.GastosList.push(ObjDetalle);
            }
        }
        
        function AddAllC(){
            if(vm.AddAll == true){
            vm.GastosList.forEach(function(element, key){
                var ObjDetalle = {
                    'Clv_CateGasto': element.Clv_CateGasto,
                    'Importe': element.Importe,
                    'ImporteO': element.Importe,
                    'Descripcion': element.Descripcion,
                    'Fijo': element.Fijo,
                    'Status': 'P',
                    'Aplicada': 0
                };
                vm.GastosIncList.push(ObjDetalle);
            });
            vm.GastosList = [];
             }else{
                vm.GastosIncList.forEach(function(element, key){
                    var ObjDetalle = {
                        'Clv_CateGasto': element.Clv_CateGasto,
                        'Importe': element.ImporteO,
                        'Descripcion': element.Descripcion,
                        'Fijo': element.Fijo
                    };
                    vm.GastosList.push(ObjDetalle);
                });
                vm.GastosIncList = [];
            }
        }

        function ExisteGastosIncList(ObjGasto){
            var Cont = 0;
            vm.GastosIncList.forEach(function(element, key){
                if(element.Clv_CateGasto == ObjGasto.Clv_CateGasto){
                    Cont ++;
                }
            });
            return (Cont > 0)? false:true
        }

        function SaveSolicitud(){
            var detalle = [];
            if(vm.GastosIncList != null && vm.GastosIncList != undefined){
                vm.GastosIncList.forEach(function(element, key) {
                    var ObjDetalle = {
                        'Clv_CateGasto': element.Clv_CateGasto,
                        'Importe': element.Importe,
                        'Status': 'P',
                        'Aplicada': 0,
                        'observaciones':element.observaciones
                    }
                    detalle.push(ObjDetalle);
                });
               console.log(detalle)
               
                if(detalle.length > 0){
                    var solicitud = {
                        'DescripcionGasto': vm.Descripcion,
                        'IdUsuario': $localStorage.currentUser.idUsuario,
                        'IdDistribuidor': vm.Distribuidor.Clv_Plaza,
                        'IdCompania': vm.Region.id_compania,
                        'IdEdo': vm.Estado.Clv_Estado,
                        'IdCd': vm.Ciudad.Clv_Ciudad,
                        'IdDpto': vm.Departamento.IdDpto,
                        'IdClasificacion': vm.ClasGasto.Clv_Clasificacion,
                        'Fijo': (vm.Fijo == 'S')? 1:0,
                        'IdSucursal': vm.Sucursal.Clv_Sucursal
                    };
                    GastosFactory.GetAddSolicitud(solicitud, detalle).then(function(data){
                        if(data.AddSolicitudResult > 0){
                            ngNotify.set('CORRECTO, Se guardó la solicitud.', 'success');
                            $state.go('home.gastos.gastos');
                         }else{
                            ngNotify.set('ERROR, Al guardar la solicitud.', 'warn');
                        }
                    });
                 }else{
                    ngNotify.set('ERROR, No ha seleccionado algun concepto.', 'warn');
                }
             }else{
                ngNotify.set('ERROR, No ha seleccionado algun concepto.', 'warn');
            }
        }

        var vm = this;
        vm.Title = 'Nueva Solicitud de Gasto';
        vm.BtnCanTitulo ='Cancelar';
        vm.Fijo = 'S';
        vm.AddAll = true;
        vm.ShowSave1 = false;
        vm.ShowSave2 = false;
        vm.ShowSave3 = false;
        vm.ShowSave = false;
        vm.GastosList = [];
        vm.GastosIncList = [];
        vm.IdDpto = 0;
        vm.Clv_Clasificacion = 0;
        vm.GetPlazaList = GetPlazaList;
        vm.GetEstadoList = GetEstadoList;
        vm.GetCiudadList = GetCiudadList;
        vm.GetGastosList = GetGastosList;
        vm.AddGastoSolicitud = AddGastoSolicitud;
        vm.AddAllC = AddAllC;
        vm.SaveSolicitud = SaveSolicitud;
        initData();
        
    });