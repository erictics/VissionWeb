'use strict';
angular
  .module('softvApp')
  .controller('autorizacionGastoCtrl', function(
    GastosFactory,reportesVariosFactory,$localStorage, $uibModal, ngNotify,  $filter, CatalogosFactory ) {
    function initData() {     
      obtieneClasificaciones();       
      reportesVariosFactory.mostrarDistribuidorByUsuario($localStorage.currentUser.idUsuario)
        .then(function(result) {
          vm.empresas = result.GetDistribuidorByUsuarioResult;                   
        });
    }

    function obtieneClasificaciones(){
      GastosFactory.GetClasificacionGasto().then(function(res){
        vm.clasificaciones = res.GetClasificacionGastoResult;       
      });
    }

    function obtienePlazas() {
      var distribuidores = [];
      vm.regiones=[];
      if(vm.empresa){
        distribuidores.push(vm.empresa);
      }
      
      if (distribuidores.length > 0) {
        reportesVariosFactory
          .mostrarPlazaByDistribuidor($localStorage.currentUser.idUsuario,  distribuidores).then(function(result) {
            vm.regiones = result.GetPlazasByDistribuidorResult;   
                     
          });
      }
    }

    function ObtieneSucursales(){
      var Parametros = {
        'Clv_Sucursal': 0,
        'Nombre': '',
        'OP': 2 ,
        'idcompania': vm.region.id_compania,
        'clv_usuario': 0
      };
    CatalogosFactory.GetSUCURSALES(Parametros).then(function(res){
       vm.sucursales = res.GetSUCURSALESResult;
    });
    }


    function getSucursales(){
      ObtieneSucursales();
      GetConceptos();
    }
  
    function GetConceptos(){
      var params={
        idcompania: (vm.region) ? vm.region.id_compania:0,
        fechaInicio: (vm.fechaInicio)?$filter('date')(vm.fechaInicio, 'yyyy/MM/dd'):'',
        fechaFin: (vm.fechaFin)? $filter('date')(vm.fechaFin, 'yyyy/MM/dd') :'',
        clasificacion:(vm.clasificacion) ? vm.clasificacion.Clv_Clasificacion : 0,
        fijo:(vm.tipo) ? vm.tipo:-1,
        clv_usuario:$localStorage.currentUser.usuario,
        IdSucursal: (vm.sucursal)? vm.sucursal.Clv_Sucursal:-1
      };
      GastosFactory.GetFiltrosAutorizacionConceptos(params).then(function(res){
        vm.conceptos = res.GetFiltrosAutorizacionConceptosResult;
        vm.conceptos.forEach(function(i){
          if(estaSeleccionado(i.IdDetalle)){
            i.selected = true;
          }
        });
         console.log(res);
      });
    }

  

   function autConceptos(item){
     
      if(!estaSeleccionado(item.IdDetalle)){
        vm.conceptosAutorizados.push(item);
      }
      else{
        if(item.selected === false){
          eliminaConcepto(item.IdDetalle);
        }
      }
                
    }

    function eliminaConcepto(IdDetalle){
      vm.conceptosAutorizados.forEach(function(element,index) {
        if (element.IdDetalle ===IdDetalle) {
          vm.conceptosAutorizados.splice(index,1);
          GetConceptos();
        }       
      });
    }

    function estaSeleccionado(IdDetalle){
      var count=0;
      vm.conceptosAutorizados.forEach(function(element) {
        if (element.IdDetalle ===IdDetalle) {
          count=count + 1;
        }       
      });

      return (count > 0 ) ? true: false;
    }


    function autorizar() {
      var modalInstance = $uibModal.open({
        animation: true,
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: 'views/gastos/ModalPreguntaGasto.html',
        controller: 'ModalPreguntaGastoCtrl',
        controllerAs: 'ctrl',
        backdrop: 'static',
        keyboard: false,
        class: 'modal-backdrop fade',
        size: 'sm',
        resolve: {
            pregunta: function () {
                return '¿Estas seguro de autorizar estos Gastos ?';
            }
        }
    });
    modalInstance.result.then(function () {    
      GastosFactory.GetAutorizaConceptos(0, $localStorage.currentUser.idUsuario, vm.conceptosAutorizados )
      .then(function(result) {
        vm.conceptosAutorizados = [];
        ngNotify.set('Los conceptos se autorizaron  correctamente',{type: 'success', sticky: true, button: true});       
        GetConceptos();
      });
    });

     
    }

    var vm = this;
    initData();   
    vm.obtienePlazas = obtienePlazas;
    vm.GetConceptos = GetConceptos; 
    vm.autorizar =autorizar;  
    vm.getSucursales=getSucursales;
    vm.autConceptos=autConceptos;
    vm.conceptosAutorizados = [];
    vm.eliminaConcepto=eliminaConcepto;
    vm.usuarioCorporativo=$localStorage.currentUser.usuarioCorporativo;
  });
