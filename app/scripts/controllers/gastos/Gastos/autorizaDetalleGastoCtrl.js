'use strict';
angular
  .module('softvApp')
  .controller('autorizaDetalleGastoCtrl', function(
    GastosFactory,
    reportesVariosFactory,
    $localStorage,
    $uibModal,
    ngNotify,
    $state,
    $stateParams
  ) {

   function autorizarConceptos(){
    vm.cantauto = 0;
    vm.montoauto = 0;    
    if(vm.checkAutoriza){
      vm.detalle.forEach(function(element) {
        if(element.Editable=== true){
          element.Status= "A";
        }        
          vm.cantauto = vm.cantauto + 1;        
      });
     }else{
      vm.detalle.forEach(function(element) {
        if(element.Editable=== true){
          element.Status = "P";
        }
      });
    }
    calculaMonto();
    }

    function initData() {
      GastosFactory.GetSolicitudById($stateParams.id).then(function(res) {
        vm.gasto = res.GetSolicitudByIdResult;
        GastosFactory.GetDetalleByIdSolicitud($stateParams.id).then(function(
          res
        ) {
          vm.detalle = res.GetDetalleByIdSolicitudResult;
          calculaMonto();
        });
      });
    }

    function calculaMonto() {
      vm.cantauto = 0;
      vm.montoauto = 0;
      vm.detalle.forEach(function(element) {
        if (element.Status === 'A') {
          vm.montoauto = vm.montoauto + element.Importe;
          vm.cantauto = vm.cantauto + 1;
        }
      });
    }

    function guardar() {
      var conceptos = [];
      vm.detalle.forEach(function(item) {
        if (item.Status === 'A' && item.Editable === true) {
          conceptos.push(item);
        }
      });
      GastosFactory.GetAutorizaConceptos(
        vm.gasto.IdGasto,
        $localStorage.currentUser.idUsuario,
        conceptos
      ).then(function(result) {
        ngNotify.set(
          'La solicitud se autorizó correctamente, Autorización #' +
            result.GetAutorizaConceptosResult,
          {
            type: 'success',
            sticky: true,
            button: true
          }
        );
        $state.go('home.gastos.autorizaciongastos');
        //result.GetAutorizaConceptosResult;
        
      });
    }

    var vm = this;
    initData();
    vm.calculaMonto = calculaMonto;
    vm.guardar = guardar;
    vm.autorizarConceptos=autorizarConceptos;

  });
