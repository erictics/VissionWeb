'use strict';
angular
    .module('softvApp')
    .controller('GastosViewCtrl', function(GastosFactory, distribuidorFactory, plazaFactory, CatalogosFactory, $uibModal, ngNotify, $stateParams, $state, $localStorage){

        function initData(){
            GetSolicitud();
            GetDetalleList();
            vm.Fijo = 'N';
        }

        function GetSolicitud(){
            var obj = {
                'IdGasto': parseInt($stateParams.id)      
            };
            GastosFactory.GetSolicitudById(parseInt($stateParams.id)).then(function(data){
                if(data.GetSolicitudByIdResult.IdGasto > 0){
                    var Solicitud = data.GetSolicitudByIdResult;
                    vm.IdSolicitud = Solicitud.IdGasto;
                    vm.Descripcion = Solicitud.DescripcionGasto;
                    vm.Fijo = (Solicitud.Fijo == true)? 'S':'N';
                    vm.IdDpto = Solicitud.IdDpto;
                    vm.IdClas = Solicitud.IdClasificacion;
                    vm.IdF = (Solicitud.Fijo == true)? 'S':'N';
                    vm.IdSucursal=Solicitud.IdSucursal;
                    GetDistribuidoresList(Solicitud.IdDistribuidor);
                    GetPlazaList(Solicitud.IdDistribuidor, Solicitud.IdCompania);
                    GetEstadoList(Solicitud.IdCompania, Solicitud.IdEdo);
                    GetCiudadList(Solicitud.IdCompania, Solicitud.IdEdo, Solicitud.IdCd);
                    GetDepartamentoList(Solicitud.IdDpto);
                    GetClasGastoList(Solicitud.IdClasificacion);
                    GetDetalleList();
                 }else{
                    ngNotify.set('ERROR, No se enctro la solicitud.', 'warn');
                    $state.go('home.gastos.gastos');
                }
            });
        }


        function GetSucursales(id){

            var Parametros = {
                'Clv_Sucursal': 0,
                'Nombre': '',
                'OP': 2 ,
                'idcompania': id,
                'clv_usuario': 0
                };

            CatalogosFactory.GetSUCURSALES(Parametros).then(function(res){
                vm.sucursales = res.GetSUCURSALESResult;
                vm.sucursales.forEach(function(item){
                    if(item.Clv_Sucursal === vm.IdSucursal ){
                        vm.Sucursal= item;
                    }
                });
            });

        }

        function GetDistribuidoresList(Id){
            distribuidorFactory.Getplaza(0,'').then(function(data){
                vm.DistribuidorList = data.GetPlaza_DistribuidoresNewResult;
                if(Id != undefined){
                    vm.DistribuidorList.forEach(function(element, key) {
                        if(element.Clv_Plaza == Id){
                            vm.Distribuidor = vm.DistribuidorList[key];
                        }
                    });
                }
            });
            vm.PlazaList = null;
            vm.Estado = null;
            vm.CiudadList = null;
        }

        function GetPlazaList(IdDi, ClvP){
            if(vm.Distribuidor != undefined || (IdDi != undefined && ClvP != undefined)){
                var ObjPlaza = {
                    'opcion': 3,
                    'razon': '',
                    'idcompania': (vm.Distribuidor != undefined)? vm.Distribuidor.Clv_Plaza:IdDi
                };
                plazaFactory.GetBrwMuestraCompanias(ObjPlaza).then(function (result) {
                    vm.PlazaList =  result.GetBrwMuestraCompaniasResult;
                    if(ClvP != undefined){
                        vm.PlazaList.forEach(function(element, key){
                            if(element.id_compania == ClvP){
                                vm.Region = vm.PlazaList[key];
                                GetSucursales(vm.Region.id_compania);
                            }
                        });
                    }
                });
             }else{
                vm.PlazaList = null;
            }
            vm.EstadoList = null;
            vm.CiudadList = null;
        }

        function GetEstadoList(ClvP, ClvE){
            if(vm.Region != undefined || (ClvP != undefined && ClvE != undefined)){
                var idcompania = (vm.Region != undefined)? vm.Region.id_compania:ClvP;
                CatalogosFactory.GetMuestraEstadosCompaniaList(idcompania).then(function(data){
                    vm.EstadoList = data.GetMuestraEstadosCompaniaListResult;
                    if(ClvE != undefined){
                        vm.EstadoList.forEach(function(element, key){
                            if(element.Clv_Estado == ClvE){
                                vm.Estado = vm.EstadoList[key];
                            }
                        });
                    }
                });
             }else{
                vm.EstadoList = null;
            }
            vm.CiudadList = null;
        }

        function GetCiudadList(ClvP, ClvE, IdCd){
            if(vm.Estado != undefined || (ClvP != undefined && ClvE != undefined && IdCd != undefined)){
                var RelEstMun = {
                    'clv_estado' : (vm.Estado != undefined)? vm.Estado.Clv_Estado:ClvE,
                    'idcompania' : (vm.Estado != undefined)? vm.Region.id_compania:ClvP
                };
                CatalogosFactory.GetMuestraCiudadesEstadoList(RelEstMun).then(function(data){
                    vm.CiudadList = data.GetMuestraCiudadesEstadoListResult;
                    if(IdCd != undefined){
                        vm.CiudadList.forEach(function(element, key){
                            if(element.Clv_Ciudad == IdCd){
                                vm.Ciudad = vm.CiudadList[key];
                            }
                        });
                    }
                });
             }else{
                vm.CiudadMunicipioList = null;
            }
        }

        function GetDepartamentoList(IdDpto){
            GastosFactory.GetDepartamento().then(function(data){
                vm.DepartamentoList = data.GetDepartamentoResult;
                if(IdDpto != undefined){
                    vm.DepartamentoList.forEach(function(element, key){
                        if(element.IdDpto == IdDpto){
                            vm.Departamento = vm.DepartamentoList[key];
                        }
                    });
                }
            }); 
        }

        function GetClasGastoList(IdClas){
            GastosFactory.GetClasificacionGasto().then(function(data){
                vm.ClasGastoList = data.GetClasificacionGastoResult;
                if(IdClas != undefined){
                    vm.ClasGastoList.forEach(function(element, key){
                        if(element.Clv_Clasificacion == IdClas){
                            vm.ClasGasto = vm.ClasGastoList[key];
                        }
                    });
                }
            }); 
        }

        function GetDetalleList(){
            GastosFactory.GetDetalleByIdSolicitud(vm.IdSolicitud).then(function(data){
                vm.DetalleList = data.GetDetalleByIdSolicitudResult;
                vm.DetalleList.forEach(function(element, key){
                    element.op = 2;
                    vm.GastosIncList.push(element);
                    if(element.Status == 'A' || element.Aplicada == true){
                        vm.ShowSave2 = true;
                    }
                    if(element.Aplicada == true){
                        vm.ShowSave3 = true;
                    }
                })
                GetGastosListI(0);
            });
        }

        function GetGastosListI(){
            /*vm.GastosList == null;*/
            if(vm.IdDpto > 0 && vm.IdClas > 0){
                var ObjDetalle = {
                    'fijo': (vm.Fijo == 'S')? 1:-1,
                    'Iddepartamento': vm.IdDpto, 
                    'Idclasificacion': vm.IdClas
                }
                GastosFactory.GetfiltrosGastos(ObjDetalle).then(function(data){
                    var GastosList = data.filtrosGastosResult;
                    GastosList.forEach(function(element, key){
                        GastDis.push(element);
                    });
                    if(vm.DetalleList.length > 0){
                        vm.DetalleList.forEach(function(de, dk){
                            var Clv_CateGasto = de.Clv_CateGasto;
                            GastDis.forEach(function(ge, gk){
                                if(ge.Clv_CateGasto == Clv_CateGasto){
                                    GastDis.splice(gk, 1);
                                }
                            });
                        });
                        vm.GastosList = GastDis;
                    }
                });
            }/*else{
                vm.GastosList = null;
                vm.GastosIncList = [];
            }*/
        }

        function GetGastosList(){
            vm.GastosList = null;
            vm.GastosIncList = [];
            if(vm.Fijo != undefined && vm.Departamento != undefined && vm.ClasGasto != undefined){
                console.log(vm.DetalleList);
                if(vm.IdDpto == vm.Departamento.IdDpto && vm.IdClas == vm.ClasGasto.Clv_Clasificacion && vm.IdF == vm.Fijo){
                    vm.DetalleList.forEach(function(element){
                        if(element.IdDetalle > 0){
                            element.op = vm.Fijo2;
                            vm.GastosIncList.push(element);
                        }
                    });
                    console.log(vm.DetalleList);
                    GastosFactory.GetfiltrosGastos(ObjDetalle).then(function(data){
                        var GastosList = data.filtrosGastosResult;
                        GastosList.forEach(function(element, key){
                            GastDis.push(element);
                        });
                        if(vm.DetalleList.length > 0){
                            vm.DetalleList.forEach(function(de, dk){
                                var Clv_CateGasto = de.Clv_CateGasto;
                                GastDis.forEach(function(ge, gk){
                                    if(ge.Clv_CateGasto == Clv_CateGasto){
                                        GastDis.splice(gk, 1);
                                    }
                                });
                            });
                            vm.GastosList = GastDis;
                        }
                    });
                 }else{
                    console.log('3 n');
                    vm.DetalleList.forEach(function(element){
                        if(element.IdDetalle > 0){
                            element.op = 3;
                        }
                    });
                    console.log(vm.DetalleList);
                    var ObjDetalle = {
                        'fijo': (vm.Fijo == 'S')? 1:-1,
                        'Iddepartamento': vm.Departamento.IdDpto, 
                        'Idclasificacion': vm.ClasGasto.Clv_Clasificacion
                    };
                    GastosFactory.GetfiltrosGastos(ObjDetalle).then(function(data){
                        var GastosList = data.filtrosGastosResult;
                        if(vm.Fijo == 'S'){
                            console.log('y');
                            vm.GastosList = [];
                            GastosList.forEach(function(element){
                                var obj = {
                                    'Aplicada': false,
                                    'Clv_CateGasto': element.Clv_CateGasto,
                                    'Descripcion': element.Descripcion,
                                    'IdDetalle': 0,
                                    'IdGasto': vm.IdSolicitud,
                                    'Importe': element.Importe,
                                    'ImporteO': element.Importe,
                                    'Status': 'P',
                                    'op': 1
                                };
                                vm.GastosIncList.push(obj);
                                vm.DetalleList.push(obj);
                            });
                         }else{
                            console.log('n');
                            vm.GastosList = GastosList;
                        }
                    });
                }
             }else{
                onsole.log('2 n');
                vm.GastosList = null;
                vm.GastosIncList = [];
            }
        }

        function Close() {
            $uibModalInstance.close();
        }

        var vm = this;
        vm.Title = 'Consultar Solicitud de Gasto';
        vm.BtnCanTitulo ='Salir';
        vm.ShowSave1 = true;
        vm.ShowSave2 = true;
        vm.ShowSave3 = true;
        vm.ShowSave = true;
        vm.IdDpto = 0;
        vm.IdClas = 0;
        vm.DetalleList = [];
        vm.GastosIncList = [];
        var GastosList = [];
        var GastDis = [];
        vm.GetPlazaList = GetPlazaList;
        vm.GetEstadoList = GetEstadoList;
        vm.GetCiudadList = GetCiudadList;
        vm.GetGastosList = GetGastosList;
        vm.opSI = 0; 
        vm.Close = Close;
        initData();
        
    });