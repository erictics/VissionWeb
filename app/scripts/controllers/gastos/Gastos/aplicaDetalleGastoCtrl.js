'use strict';
angular
  .module('softvApp')
  .controller('aplicaDetalleGastoCtrl', function(
    GastosFactory,
    reportesVariosFactory,
    $localStorage,
    $uibModal,
    ngNotify,
    $state,
    $stateParams
  ) {

   function autorizarConceptos(){
    vm.cantauto = 0;
    vm.montoauto = 0;    
    if(vm.checkAutoriza){
      vm.detalle.forEach(function(element) {
          if(element.Editable_aplicar===true){
            element.Aplicada= true;
          }
          
          vm.montoauto = vm.montoauto + element.Importe;
          vm.cantauto = vm.cantauto + 1;        
      });
    }else{
      vm.detalle.forEach(function(element) {
        if(element.Editable_aplicar===true){
          element.Aplicada= false;
        }       
      });
    }
    } 

    function initData() {
      GastosFactory.GetSolicitudById($stateParams.id).then(function(res) {
        vm.gasto = res.GetSolicitudByIdResult;
         
        if(vm.gasto.IdUsuario !== $localStorage.currentUser.idUsuario){
          ngNotify.set('No tienes permiso para  aplicar esta solicitud','warn');
          $state.go('home.gastos.gastos');
        }

        GastosFactory.GetDetalleByIdSolicitud($stateParams.id).then(function(
          res
        ) {
          vm.detalle = res.GetDetalleByIdSolicitudResult;
          console.log(vm.detalle);
          calculaMonto();
        });
      });
    }

    function calculaMonto() {
      vm.cantauto = 0;
      vm.montoauto = 0;
      vm.detalle.forEach(function(element) {
        if (element.Aplicada) {
          vm.montoauto = vm.montoauto + element.Importe;
          vm.cantauto = vm.cantauto + 1;
        }
      });
    }

    function guardar() {
      var conceptos = [];
      vm.detalle.forEach(function(item) {
        if (item.Aplicada === true && item.Editable_aplicar=== true) {
          conceptos.push(item);
        }
      });
      GastosFactory.GetAplicaConceptos(
        vm.gasto.IdGasto,
        $localStorage.currentUser.idUsuario,
        conceptos
      ).then(function(result) {
        ngNotify.set('Los conceptos se aplicaron correctamente');
        $state.go('home.gastos.gastos');
        //result.GetAutorizaConceptosResult;
        
      });
    }

    function revertir(item){
      console.log(vm.gasto.IdGasto);
      console.log($localStorage.currentUser.idUsuario);
      console.log(item.IdDetalle);
      GastosFactory.GetValidaReversionAplicacionGasto(vm.gasto.IdGasto, $localStorage.currentUser.idUsuario, item.IdDetalle).then(function(res){
      if(res.GetValidaReversionAplicacionGastoResult.error > 0){
        ngNotify.set(res.GetValidaReversionAplicacionGastoResult.mensaje,'warn');
      }
      else{
        ngNotify.set('Se revirtió la aplicacion del concepto correctamenete','success');
        
        GastosFactory.GetDetalleByIdSolicitud($stateParams.id).then(function( res ) {
          vm.detalle = res.GetDetalleByIdSolicitudResult;          
          calculaMonto();
        });
      }
      });
    }

    var vm = this;
    initData();
    vm.calculaMonto = calculaMonto;
    vm.guardar = guardar;
    vm.autorizarConceptos=autorizarConceptos;
    vm.revertir=revertir;

  });
