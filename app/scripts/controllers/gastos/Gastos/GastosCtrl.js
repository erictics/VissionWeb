'use strict';
angular
    .module('softvApp')
    .controller('GastosCtrl', function(
        GastosFactory,
        reportesVariosFactory,
        $localStorage,
        $uibModal,
        ngNotify,
        CatalogosFactory,
        $state){

        function initData() {
            //buscar();
            GetDepartamentos();
            reportesVariosFactory
              .mostrarDistribuidorByUsuario($localStorage.currentUser.idUsuario)
              .then(function(result) {
                vm.empresas = result.GetDistribuidorByUsuarioResult;
              });
          }


          function ObtieneSucursales(){
            var Parametros = {
              'Clv_Sucursal': 0,
              'Nombre': '',
              'OP': 2 ,
              'idcompania': vm.region.id_compania,
              'clv_usuario': 0
            };
          CatalogosFactory.GetSUCURSALES(Parametros).then(function(res){
             vm.sucursales = res.GetSUCURSALESResult;
          });
          }
      
          function obtienePlazas() {
            var distribuidores = [];
            vm.regiones=[];
            if(vm.empresa){
              distribuidores.push(vm.empresa);
            }
            if (distribuidores.length > 0) {
              reportesVariosFactory
                .mostrarPlazaByDistribuidor(
                  $localStorage.currentUser.idUsuario,
                  distribuidores
                )
                .then(function(result) {
                  vm.regiones = result.GetPlazasByDistribuidorResult;
                  
                  buscar();
                });
            }
          }
      
          function obtieneEstados() {
            var plazas = [];
            vm.estados=[];
            if(vm.region){
              plazas.push(vm.region);
            }
            if (plazas.length > 0) {
              reportesVariosFactory
                .mostrarEstadoByPlaza(plazas)
                .then(function(result) {
                  vm.estados = result.GetEstadosByplazaResult;
                  ObtieneSucursales();
                  buscar();
                });
            }
          }
      
          function obtieneCiudades() {
            var plazas = [];
            var estados = [];
            vm.ciudades=[]; 
            plazas.push(vm.region);
            estados.push(vm.estado);
            reportesVariosFactory
              .mostrarCiudad(plazas, estados)
              .then(function(result) {
                vm.ciudades = result.GetCiudadesBy_PlazasEstadoResult;
                buscar();
              });
          }
      
          function GetDepartamentos(){
              GastosFactory.GetDepartamento().then(function(result){
                  vm.departamentos=result.GetDepartamentoResult;
              });
          }
      
          function buscar() {
            var obj = {
              IdDpto: (vm.departamento)?vm.departamento.IdDpto:-1,
              IdClasificacion: -1,
              IdDistribuidor: vm.empresa ? vm.empresa.Clv_Plaza : -1,
              IdCompania: (vm.region) ? vm.region.id_compania : -1,
              IdCd: (vm.ciudad)? vm.ciudad.Clv_Ciudad:-1,
              IdGasto: (vm.solicitud)?vm.solicitud:-1,
              IdAutorizacion: (vm.autorizacion)?vm.autorizacion:-1,
              IdSucursal:(vm.sucursal)?vm.sucursal.Clv_Sucursal:-1
            };
            GastosFactory.GetFiltrosSolicitud(obj).then(function(result) {        
              vm.solicitudes = result.GetFiltrosSolicitudResult;
            });
          }
      
          var vm = this;
          initData();
          vm.buscar = buscar;
          vm.obtienePlazas = obtienePlazas;
          vm.obtieneEstados = obtieneEstados;
          vm.obtieneCiudades = obtieneCiudades;

    });