'use strict';
angular
    .module('softvApp')
    .controller('GastosUpdateCtrl', function(GastosFactory, distribuidorFactory, plazaFactory, CatalogosFactory, $uibModal, ngNotify, $stateParams, $state, $localStorage){

        function initData(){
            GetSolicitud();
            vm.Fijo = 'N';
        }

        function GetSolicitud(){
            var obj = {
                'IdGasto': parseInt($stateParams.id)      
            };
            GastosFactory.GetSolicitudById(parseInt($stateParams.id)).then(function(data){
                if(data.GetSolicitudByIdResult.IdGasto > 0){
                    var Solicitud = data.GetSolicitudByIdResult;
                    vm.IdSolicitud = Solicitud.IdGasto;
                    vm.Descripcion = Solicitud.DescripcionGasto;
                    vm.Fijo = (Solicitud.Fijo == true)? 'S':'N';
                    vm.IdDpto = Solicitud.IdDpto;
                    vm.IdClas = Solicitud.IdClasificacion;
                    vm.IdDptoS = Solicitud.IdDpto;
                    vm.IdClasS = Solicitud.IdClasificacion;
                    vm.IdF = (Solicitud.Fijo === true)? 'S':'N';
                    vm.IdSucursal = Solicitud.IdSucursal;
                    GetDistribuidoresList(Solicitud.IdDistribuidor);
                    GetPlazaList(Solicitud.IdDistribuidor, Solicitud.IdCompania);
                    GetEstadoList(Solicitud.IdCompania, Solicitud.IdEdo);
                    GetCiudadList(Solicitud.IdCompania, Solicitud.IdEdo, Solicitud.IdCd);
                    GetDepartamentoList(Solicitud.IdDpto);
                    GetClasGastoList(Solicitud.IdClasificacion);
                    GetDetalleList();

                 }else{
                    ngNotify.set('ERROR, No se enctro la solicitud.', 'warn');
                    $state.go('home.gastos.gastos');
                }
            });
        }

        function GetDistribuidoresList(Id){
            distribuidorFactory.Getplaza(0,'').then(function(data){
                vm.DistribuidorList = data.GetPlaza_DistribuidoresNewResult;
                if(Id != undefined){
                    vm.DistribuidorList.forEach(function(element, key) {
                        if(element.Clv_Plaza == Id){
                            vm.Distribuidor = vm.DistribuidorList[key];
                        }
                    });
                }
            });
            vm.PlazaList = null;
            vm.Estado = null;
            vm.CiudadList = null;
        }

        function GetSucursales(id){

            var Parametros = {
                'Clv_Sucursal': 0,
                'Nombre': '',
                'OP': 2 ,
                'idcompania': id,
                'clv_usuario': 0
              };

            CatalogosFactory.GetSUCURSALES(Parametros).then(function(res){
               vm.sucursales = res.GetSUCURSALESResult;
               vm.sucursales.forEach(function(item){
                    if(item.Clv_Sucursal === vm.IdSucursal ){
                        vm.Sucursal= item;
                    }
               });
            });

        }

        function GetPlazaList(IdDi, ClvP){
            if(vm.Distribuidor != undefined || (IdDi != undefined && ClvP != undefined)){
                var ObjPlaza = {
                    'opcion': 3,
                    'razon': '',
                    'idcompania': (vm.Distribuidor != undefined)? vm.Distribuidor.Clv_Plaza:IdDi
                };
                plazaFactory.GetBrwMuestraCompanias(ObjPlaza).then(function (result) {
                    vm.PlazaList =  result.GetBrwMuestraCompaniasResult;
                    if(ClvP != undefined){
                        vm.PlazaList.forEach(function(element, key){
                            if(element.id_compania == ClvP){
                                vm.Region = vm.PlazaList[key];
                                GetSucursales(vm.Region.id_compania);
                            }
                        });
                    }
                });
             }else{
                vm.PlazaList = null;
            }
            vm.EstadoList = null;
            vm.CiudadList = null;
        }

        function GetEstadoList(ClvP, ClvE){
            if(vm.Region != undefined || (ClvP != undefined && ClvE != undefined)){
                var idcompania = (vm.Region != undefined)? vm.Region.id_compania:ClvP;
                CatalogosFactory.GetMuestraEstadosCompaniaList(idcompania).then(function(data){
                    vm.EstadoList = data.GetMuestraEstadosCompaniaListResult;
                    if(ClvE != undefined){
                        vm.EstadoList.forEach(function(element, key){
                            if(element.Clv_Estado == ClvE){
                                vm.Estado = vm.EstadoList[key];
                            }
                        });
                    }
                });
             }else{
                vm.EstadoList = null;
            }
            vm.CiudadList = null;
        }

        function GetCiudadList(ClvP, ClvE, IdCd){
            if(vm.Estado != undefined || (ClvP != undefined && ClvE != undefined && IdCd != undefined)){
                var RelEstMun = {
                    'clv_estado' : (vm.Estado != undefined)? vm.Estado.Clv_Estado:ClvE,
                    'idcompania' : (vm.Estado != undefined)? vm.Region.id_compania:ClvP
                };
                CatalogosFactory.GetMuestraCiudadesEstadoList(RelEstMun).then(function(data){
                    vm.CiudadList = data.GetMuestraCiudadesEstadoListResult;
                    if(IdCd != undefined){
                        vm.CiudadList.forEach(function(element, key){
                            if(element.Clv_Ciudad == IdCd){
                                vm.Ciudad = vm.CiudadList[key];
                            }
                        });
                    }
                });
             }else{
                vm.CiudadMunicipioList = null;
            }
        }

        function GetDepartamentoList(IdDpto){
            GastosFactory.GetDepartamento().then(function(data){
                vm.DepartamentoList = data.GetDepartamentoResult;
                if(IdDpto != undefined){
                    vm.DepartamentoList.forEach(function(element, key){
                        if(element.IdDpto == IdDpto){
                            vm.Departamento = vm.DepartamentoList[key];
                        }
                    });
                }
            }); 
        }

        function GetClasGastoList(IdClas){
            GastosFactory.GetClasificacionGasto().then(function(data){
                vm.ClasGastoList = data.GetClasificacionGastoResult;
                if(IdClas != undefined){
                    vm.ClasGastoList.forEach(function(element, key){
                        if(element.Clv_Clasificacion == IdClas){
                            vm.ClasGasto = vm.ClasGastoList[key];
                        }
                    });
                }
            }); 
        }

        function GetDetalleList(){
            GastosFactory.GetDetalleByIdSolicitud(vm.IdSolicitud).then(function(data){
                vm.DetalleList = data.GetDetalleByIdSolicitudResult;
                vm.DetalleList.forEach(function(element, key){
                    element.op = 2;
                    vm.GastosIncList.push(element);
                    if(element.Status == 'A' || element.Aplicada == true){
                        vm.ShowSave2 = true;
                    }
                    if(element.Aplicada == true){
                        vm.ShowSave3 = true;
                    }
                })
                GetGastosListI(0);
            });
        }

        function GetGastosListI(){
            if(vm.IdDpto > 0 && vm.IdClas > 0){
                var ObjDetalle = {
                    'fijo': (vm.Fijo == 'S')? 1:-1,
                    'Iddepartamento': vm.IdDpto, 
                    'Idclasificacion': vm.IdClas
                }
                GastosFactory.GetfiltrosGastos(ObjDetalle).then(function(data){
                    var GastosList = data.filtrosGastosResult;
                    GastosList.forEach(function(element, key){
                        GastDis.push(element);
                    });
                    if(vm.DetalleList.length > 0){
                        vm.DetalleList.forEach(function(de, dk){
                            var Clv_CateGasto = de.Clv_CateGasto;
                            GastDis.forEach(function(ge, gk){
                                if(ge.Clv_CateGasto == Clv_CateGasto){
                                    GastDis.splice(gk, 1);
                                }
                            });
                        });
                        vm.GastosList = GastDis;
                    }
                });
            }
        }

        function GetGastosList(){
            vm.GastosList = null;
            vm.GastosIncList = [];
            if(vm.Fijo != undefined && vm.Departamento != undefined && vm.ClasGasto != undefined){
                if(vm.IdDpto == vm.Departamento.IdDpto && vm.IdClas == vm.ClasGasto.Clv_Clasificacion && vm.IdF == vm.Fijo){
                    vm.DetalleList.forEach(function(element){
                        if(element.IdDetalle > 0){
                            element.op = 2;
                            vm.GastosIncList.push(element);
                        }
                    });
                    GastosFactory.GetfiltrosGastos(ObjDetalle).then(function(data){
                        var GastosList = data.filtrosGastosResult;
                        GastosList.forEach(function(element, key){
                            GastDis.push(element);
                        });
                        if(vm.DetalleList.length > 0){
                            vm.DetalleList.forEach(function(de, dk){
                                var Clv_CateGasto = de.Clv_CateGasto;
                                GastDis.forEach(function(ge, gk){
                                    if(ge.Clv_CateGasto == Clv_CateGasto){
                                        GastDis.splice(gk, 1);
                                    }
                                });
                            });
                            vm.GastosList = GastDis;
                        }
                    });
                 }else{
                    vm.DetalleList.forEach(function(element){
                        if(element.IdDetalle > 0){
                            element.op = 3;
                        }
                    });
                    var ObjDetalle = {
                        'fijo': (vm.Fijo == 'S')? 1:-1,
                        'Iddepartamento': vm.Departamento.IdDpto, 
                        'Idclasificacion': vm.ClasGasto.Clv_Clasificacion
                    };
                    GastosFactory.GetfiltrosGastos(ObjDetalle).then(function(data){
                        var GastosList = data.filtrosGastosResult;
                        if(vm.Fijo == 'S'){
                            vm.GastosList = [];
                            GastosList.forEach(function(element){
                                var obj = {
                                    'Aplicada': false,
                                    'Clv_CateGasto': element.Clv_CateGasto,
                                    'Descripcion': element.Descripcion,
                                    'IdDetalle': 0,
                                    'IdGasto': vm.IdSolicitud,
                                    'Importe': element.Importe,
                                    'ImporteO': element.Importe,
                                    'Fijo': element.Fijo,
                                    'Status': 'P',
                                    'op': 1
                                };
                                vm.GastosIncList.push(obj);
                            });
                         }else{
                            vm.GastosList = GastosList;
                            if(vm.IdDptoS == vm.Departamento.IdDpto && vm.IdClasS == vm.ClasGasto.Clv_Clasificacion){
                                vm.DetalleList.forEach(function(element, key){
                                    vm.GastosList.forEach(function(e, k){
                                        if(element.Clv_CateGasto == e.Clv_CateGasto){
                                            vm.GastosList.splice(k, 1);
                                        }
                                    });
                                });
                            }
                            if(vm.IdDptoS == vm.Departamento.IdDpto && vm.IdClasS == vm.ClasGasto.Clv_Clasificacion){
                                vm.DetalleList.forEach(function(element){
                                    if(element.IdDetalle > 0){
                                        element.op = 2;
                                        vm.GastosIncList.push(element);
                                    }
                                });
                            }
                            vm.IdDptoS = vm.Departamento.IdDpto;
                            vm.IdClasS = vm.ClasGasto.Clv_Clasificacion;
                        }
                    });
                }
             }else{
                vm.GastosList = null;
                vm.GastosIncList = [];
            }
        }
        
        function AddGastoSolicitud(ObjGasto, Status){
            if(Status == 1){
                var obj = {
                    'Aplicada': false,
                    'Clv_CateGasto': ObjGasto.Clv_CateGasto,
                    'Descripcion': ObjGasto.Descripcion,
                    'IdDetalle': 0,
                    'IdGasto': vm.IdSolicitud,
                    'Importe': ObjGasto.Importe,
                    'ImporteO': ObjGasto.Importe,
                    'Status': 'P',
                    'Fijo': ObjGasto.Fijo,
                    'op': (ExisteGastosIncList(ObjGasto) == false)? 2:1
                };
                vm.GastosList.forEach(function(element, key){
                    if(element.Clv_CateGasto == ObjGasto.Clv_CateGasto){
                        vm.GastosList.splice(key, 1);
                    }
                });
                vm.GastosIncList.push(obj);
                if(ExisteGastosIncList(ObjGasto) == false){
                    vm.DetalleList.forEach(function(element, key){
                        if(element.Clv_CateGasto == ObjGasto.Clv_CateGasto){
                            element.op = 2;
                        }
                    });
                }
             }else{
                if(ObjGasto.Status != 'A' && ObjGasto.Aplicada != true){
                    vm.GastosIncList.forEach(function(element, key){
                        if(element.Clv_CateGasto == ObjGasto.Clv_CateGasto){
                            vm.GastosIncList.splice(key, 1);
                        }
                    });
                    if(ObjGasto.IdDetalle != null){
                        vm.DetalleList.forEach(function(element, key){
                            if(element.Clv_CateGasto == ObjGasto.Clv_CateGasto){
                            element.op = 3;
                            }
                        });
                        var obj = {
                            'Clv_CateGasto': ObjGasto.Clv_CateGasto,
                            'Descripcion': ObjGasto.Descripcion,
                            'Importe': ObjGasto.ImporteO,
                            'Fijo': ObjGasto.Fijo
                        };
                        vm.GastosList.push(obj);
                    }else{
                        var obj = {
                            'Clv_CateGasto': ObjGasto.Clv_CateGasto,
                            'Descripcion': ObjGasto.Descripcion,
                            'Importe': ObjGasto.ImporteO,
                            'Fijo': ObjGasto.Fijo
                        };
                        vm.GastosList.push(obj);
                    }
                 }else{
                    ngNotify.set('ERROR, No se puede remover un Concepto que se a Autorizado/Aplicado.', 'warn');
                }
            }
        }

        function AddAllC(){
            if(vm.AddAll == true){
                vm.GastosList.forEach(function(element){
                    var obj = {
                        'Aplicada': false,
                        'Clv_CateGasto': element.Clv_CateGasto,
                        'Descripcion': element.Descripcion,
                        'IdDetalle': 0,
                        'IdGasto': vm.IdSolicitud,
                        'Importe': element.Importe,
                        'ImporteO': element.Importe,
                        'Status': 'P',
                        'Fijo': element.Fijo,
                        'op': (ExisteGastosIncList(element) == false)? 2:1
                    };
                    vm.GastosIncList.push(obj);
                    if(ExisteGastosIncList(element) == false){
                        vm.DetalleList.forEach(function(e, k){
                            if(e.Clv_CateGasto == element.Clv_CateGasto){
                                e.op = 2;
                            }
                        });
                    }
                });
                vm.GastosList = [];
             }else{
                vm.GastosIncList.forEach(function(element){
                    if(element.IdDetalle != null){
                        vm.DetalleList.forEach(function(e, k){
                            if(e.Clv_CateGasto == element.Clv_CateGasto){
                                e.op = 3;
                            }
                        });
                        var obj = {
                            'Clv_CateGasto': element.Clv_CateGasto,
                            'Descripcion': element.Descripcion,
                            'Importe': element.ImporteO,
                            'Fijo': element.Fijo,
                        };
                        vm.GastosList.push(obj);
                     }else{
                        var obj = {
                            'Clv_CateGasto': element.Clv_CateGasto,
                            'Descripcion': element.Descripcion,
                            'Importe': element.ImporteO,
                            'Fijo': element.Fijo,
                        };
                        vm.GastosList.push(obj);
                    }
                });
                vm.GastosIncList = [];
            }
        }

        function ExisteGastosIncList(ObjGasto){
            var Cont = 0;
            vm.DetalleList.forEach(function(element, key){
                if(element.Clv_CateGasto == ObjGasto.Clv_CateGasto){
                    Cont ++;
                }
            });
            return (Cont > 0)? false:true
        }

        function SaveSolicitud(){
            vm.Save = false;
            if(vm.DetalleList.length > 0){
                var solicitud = {
                    'IdGasto': vm.IdSolicitud,
                    'DescripcionGasto': vm.Descripcion,
                    'IdUsuario': $localStorage.currentUser.idUsuario,
                    'IdDistribuidor': vm.Distribuidor.Clv_Plaza,
                    'IdCompania': vm.Region.id_compania,
                    'IdEdo': vm.Estado.Clv_Estado,
                    'IdCd': vm.Ciudad.Clv_Ciudad,
                    'IdDpto': vm.Departamento.IdDpto,
                    'IdClasificacion': vm.ClasGasto.Clv_Clasificacion,
                    'Fijo': (vm.Fijo == 'S')? 1:0
                };
                var DetalleList = [];
                vm.DetalleList.forEach(function(element){
                    var obj = {
                        'IdGasto': element.IdGasto,
                        'IdDetalle': element.IdDetalle,
                        'Clv_CateGasto': element.Clv_CateGasto,
                        'Importe': element.Importe,
                        'Status': element.Status,
                        'Aplicada': element.Aplicada,
                        'op': element.op,
                        'observaciones':element.observaciones
                    };
                    DetalleList.push(obj);
                });
                vm.GastosIncList.forEach(function(element){
                    if(element.op == 1){
                        var obj = {
                            'IdGasto': element.IdGasto,
                            'IdDetalle': element.IdDetalle,
                            'Clv_CateGasto': element.Clv_CateGasto,
                            'Importe': element.Importe,
                            'Status': element.Status,
                            'Aplicada': element.Aplicada,
                            'op': element.op,
                            'observaciones':element.observaciones
                        };
                        DetalleList.push(obj);
                     }else if(element.op == 2){
                        DetalleList.forEach(function(e, k){
                            if(e.Clv_CateGasto == element.Clv_CateGasto){
                                e.Importe = element.Importe;
                            }
                        });
                    }
                });

                DetalleList.forEach(function(element, ok){
                    if(element.op != 3){
                        vm.Save = true;
                    }
                });
                if(vm.Save == true){
                    GastosFactory.UpdateSolicitud(solicitud).then(function(data){
                        GastosFactory.GetUpdateDetSolicitud(DetalleList).then(function(data){
                            $state.go('home.gastos.gastos');
                            ngNotify.set('CORRECTO, Se guardó la solicitud.', 'success');
                        });
                    });
                 }else{
                    ngNotify.set('ERROR, No ha seleccionado algun concepto.', 'warn');
                }
             }else{
                ngNotify.set('ERROR, No ha seleccionado algun concepto.', 'warn');
            }
        }

        function Close() {
            $uibModalInstance.close();
        }

        var vm = this;
        vm.Title = 'Editar Solicitud de Gasto';
        vm.BtnCanTitulo ='Cancelar';
        vm.ShowSave1 = true;
        vm.ShowSave2 = false;
        vm.ShowSave3 = false;
        vm.ShowSave = false;
        vm.IdDpto = 0;
        vm.IdClas = 0;
        vm.IdDptoS = 0;
        vm.IdClasS = 0;
        vm.DetalleList = [];
        vm.GastosIncList = [];
        var GastosList = [];
        var GastDis = [];
        vm.GetPlazaList = GetPlazaList;
        vm.GetEstadoList = GetEstadoList;
        vm.GetCiudadList = GetCiudadList;
        vm.GetGastosList = GetGastosList;
        vm.AddGastoSolicitud = AddGastoSolicitud;
        vm.AddAllC = AddAllC;
        vm.SaveSolicitud = SaveSolicitud;
        vm.opSI = 0; 
        vm.Close = Close;
        initData();
        
    });