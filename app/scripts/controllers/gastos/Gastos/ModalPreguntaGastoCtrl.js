'use strict';
angular
	.module('softvApp')
    .controller('ModalPreguntaGastoCtrl', 
    function($uibModalInstance, pregunta) {
     	
        function ok () {
            $uibModalInstance.close(1);
        }
		function cancel() {
			$uibModalInstance.dismiss('cancel');
		}

        var vm = this;
        vm.pregunta = pregunta;
        vm.cancel = cancel;
        vm.ok = ok;
	});
