"use strict";
angular
  .module("softvApp")
  .controller("LoginCtrl", function(
    $stateParams,
    generalesSistemaFactory,
    authFactory,
    $location,
    $localStorage,
    $window,
    ngNotify,
    globalService
  ) {
    function initData() {
      if ($localStorage.currentUser) {
        $location.path("/home/dashboard");
      }
      getlogos();
    }

    function getlogos(){
      generalesSistemaFactory.Getlogos().then(function(result) {
        vm.logo1 = globalService.getUrllogos() + "/" + result.GetlogosResult[0].Valor;
        vm.logo2 = globalService.getUrllogos() + "/" + result.GetlogosResult[1].Valor;
        vm.logo3 = globalService.getUrllogos() + "/" + result.GetlogosResult[2].Valor;
      });
    }

    function login() {
      authFactory.login(vm.user, vm.password).then(function(data) {
        if (data) {
          $localStorage.currentUser.logoBrand=vm.logo1;
          $localStorage.currentUser.logoHome= vm.logo3;
           $window.location.reload();
         /* authFactory
            .obtenNombreComputadora()
            .then(function(result) {
              console.log(result);
            
              var maquina=result.split('|')[0];
              var ip=result.split('|')[1];
              $localStorage.currentUser.maquina = maquina;
              $localStorage.currentUser.ipmaquina = ip;
              authFactory
                .obtensucursalIp(
                  $localStorage.currentUser.token,
                  $localStorage.currentUser.maquina
                )
                .then(function(response) {
                  console.log(response);
                  $localStorage.currentUser.sucursal = response.IdSucursal;
                  $localStorage.currentUser.IdCaja = response.IdCaja;
                  $localStorage.currentUser.CajaNombre = response.Caja;
                  $localStorage.currentUser.SucursalNombre = response.Sucursal;
                  $window.location.reload();
                });
            })
            .catch(function(result) {
              $window.location.reload();
            });*/
        } else {
          ngNotify.set("Datos de acceso erróneos", "error");
        }
      });
    }

    var vm = this;
    initData();
    vm.login = login;
  });
