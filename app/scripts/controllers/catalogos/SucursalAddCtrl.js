'use strict';

angular
  .module('softvApp')
  .controller('SucursalAddCtrl', function (CatalogosFactory, ngNotify, $rootScope, $state, atencionFactory, logFactory) {

    function initData() {
      atencionFactory.getPlazas()
        .then(function (data) {
          vm.plazas = data.GetMuestra_Compania_RelUsuarioListResult;
        });
    }

    function SaveSucursal() {
      var SUCURSALESobj = {};
      SUCURSALESobj.Clv_Sucursal = 0;
      SUCURSALESobj.Nombre = vm.descripcion;
      SUCURSALESobj.IP = vm.ip;
      SUCURSALESobj.Impresora = vm.impresoraff;
      SUCURSALESobj.Clv_Equivalente = '';
      SUCURSALESobj.Serie = vm.serie;
      SUCURSALESobj.UltimoFolioUsado = vm.folio;
      SUCURSALESobj.idcompania = (vm.plazas === undefined) ? 0 : vm.plaza.id_compania;
      SUCURSALESobj.Matriz = (vm.Matriz === true) ? 1 : 0;
      SUCURSALESobj.Impresora_contratos = vm.contratos;
      SUCURSALESobj.Impresora_tarjetas = vm.tarjetas;
      SUCURSALESobj.Impresora_Tickets = vm.tickets;
      SUCURSALESobj.Calle = vm.calle;
      SUCURSALESobj.numeros = vm.numero;
      SUCURSALESobj.Colonia = vm.colonia;
      SUCURSALESobj.CP = vm.cp;
      SUCURSALESobj.Municipio = vm.municipio;
      SUCURSALESobj.Ciudad = vm.ciudad;
      SUCURSALESobj.Telefono = vm.telefono;
      SUCURSALESobj.Horario = vm.atencion;
      SUCURSALESobj.Referencia = vm.referencia;
      SUCURSALESobj.Contacto = vm.contacto;
      SUCURSALESobj.Email = vm.email;
      SUCURSALESobj.numeroint = vm.numeroInt;
      CatalogosFactory.AddSucursal(SUCURSALESobj).then(function (data) {
        if (data.AddSUCURSALESResult > 0) {
          var log={
            'Modulo':'home.catalogos',
            'Submodulo':'home.catalogos.sucursal_nueva',
            'Observaciones':'Se agrego una nueva Sucursal ',
            'Comando':JSON.stringify(SUCURSALESobj),
            'Clv_afectada':0,
            'IdClassLog':1
          };
          logFactory.AddMovSist(log).then(function(result){ console.log('add'); });

          ngNotify.set('Se añadió una sucursal correctamente.', 'success');
          $state.go('home.catalogos.sucursales');
         } else {
          ngNotify.set('Sucedió  un error  al añadir una nueva Sucursal.', 'warn');
        }
      });
    }

    var vm = this;
    vm.Titulo = 'Sucursal Nueva';
    vm.SaveSucursal = SaveSucursal;
    vm.blockreturn = true;
    initData();

  });