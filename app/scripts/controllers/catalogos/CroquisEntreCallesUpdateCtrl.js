'use strict';
angular
    .module('softvApp')
    .controller('CroquisEntreCallesUpdateCtrl', function(CatalogosFactory, ClientesFactory, $uibModalInstance, $uibModal, ngNotify, $state, $localStorage, globalService, $sce, ObjCli){

        function initData(){
            GetCalleList();
        }

        function GetCalleList(){
            CatalogosFactory.GetMuestraCalleColoniaList(ObjCli.ClvColonia).then(function(data){
                vm.CalleList = data.GetMuestraCalleColoniaListResult;
                vm.CalleListN = data.GetMuestraCalleColoniaListResult;
                vm.CalleListS = data.GetMuestraCalleColoniaListResult;
                vm.CalleListE = data.GetMuestraCalleColoniaListResult;
                vm.CalleListO = data.GetMuestraCalleColoniaListResult;
                
                if(ObjCli.Clv_Calle == 0){
                    if(ObjCli.Obj == null){
                        GetEntreCalles();
                    }else{
                        if(ObjCli.Obj.Casa == 'N'){
                            vm.CalleP = ObjCli.Obj.CalleNorte;
                        }else if(ObjCli.Obj.Casa == 'S'){
                            vm.CalleP = ObjCli.Obj.CalleSur;
                        }else if(ObjCli.Obj.Casa == 'E'){
                            vm.CalleP = ObjCli.Obj.CalleEste;
                        }else if(ObjCli.Obj.Casa == 'O'){
                            vm.CalleP = ObjCli.Obj.CalleOeste;
                        }
                        vm.Orientacion = ObjCli.Obj.Casa;
                        vm.OrientacionP = ObjCli.Obj.Casa;
                        vm.Referencias = ObjCli.Obj.referencia;
                        vm.CalleList.forEach(function(element, key) {
                            if(element.Clv_Calle == ObjCli.Obj.CalleNorte){
                                vm.Norte = vm.CalleList[key];
                            }
                            if(element.Clv_Calle == ObjCli.Obj.CalleSur){
                                vm.Sur = vm.CalleList[key];
                            }
                            if(element.Clv_Calle == ObjCli.Obj.CalleEste){
                                vm.Este = vm.CalleList[key];
                            }
                            if(element.Clv_Calle == ObjCli.Obj.CalleOeste){
                                vm.Oeste = vm.CalleList[key];
                            }
                        });
                    }
                }else{
                    vm.Orientacion = 'N';
                    vm.OrientacionP = 'N';
                    vm.CalleP = ObjCli.Clv_Calle;
                    vm.CalleList.forEach(function(element, key){
                        if(element.Clv_Calle == vm.CalleP){
                            vm.Norte = vm.CalleList[key];
                        }
                    });
                }
            });
        }

        function GetEntreCalles(){
            ClientesFactory.GetSoftvWEb_DameEntrecalles(ObjCli.IdContrato).then(function(data){
                var EntrCall = data.GetSoftvWEb_DameEntrecallesResult;
                var Norte = EntrCall.CalleNorte;
                var Sur = EntrCall.CalleSur;
                var Este = EntrCall.CalleEste;
                var Oeste = EntrCall.CalleOeste;
                vm.Orientacion = EntrCall.Casa;
                vm.OrientacionP = EntrCall.Casa;
                vm.Referencias = EntrCall.referencia;

                if(EntrCall.Casa == 'N'){
                    vm.CalleP =  Norte;
                }else if(EntrCall.Casa == 'S'){
                    vm.CalleP = Sur;
                }else if(EntrCall.Casa == 'E'){
                    vm.CalleP = Este;
                }else if(EntrCall.Casa == 'O'){
                    vm.CalleP = Oeste;
                }
                vm.CalleList.forEach(function(element, key) {
                    if(element.Clv_Calle == Norte){
                        vm.Norte = vm.CalleList[key];
                    }
                    if(element.Clv_Calle == Sur){
                        vm.Sur = vm.CalleList[key];
                    }
                    if(element.Clv_Calle == Este){
                        vm.Este = vm.CalleList[key];
                    }
                    if(element.Clv_Calle == Oeste){
                        vm.Oeste = vm.CalleList[key];
                    }
                });
            });
        }

        function Save(){
            if(ValidaOrientacion() == true){
                vm.obj = {
                    'CalleNorte': (vm.Norte != undefined && vm.Norte != null)? vm.Norte.Clv_Calle:0,
                    'CalleSur': (vm.Sur != undefined && vm.Sur != null)? vm.Sur.Clv_Calle:0,
                    'CalleEste': (vm.Este != undefined && vm.Este != null)? vm.Este.Clv_Calle:0,
                    'CalleOeste': (vm.Oeste != undefined && vm.Oeste != null)?vm.Oeste.Clv_Calle:0,
                    'Casa': vm.Orientacion,
                    'referencia': vm.Referencias,
                    'entrecalles': GetNombreCalles(),
                    'callep': GetPosicion()
                };
                Cancel();
            }else{
                ngNotify.set('ERROR, Las Calles que ingresó deben ser perpendiculares.', 'warn');
            }
        }

        function GetNombreCalles(){
            var N = (vm.Norte != undefined && vm.Norte != null)? vm.Norte.Nombre + ', ':'';
            var S = (vm.Sur != undefined && vm.Sur != null)? vm.Sur.Nombre + ', ':'';
            var E = (vm.Este != undefined && vm.Este != null)? vm.Este.Nombre + ', ':'';
            var O = (vm.Oeste != undefined && vm.Oeste != null)? vm.Oeste.Nombre:'';
            return N + S + E + O
        }

        function GetPosicion(){
            var IdP = 0;
            if(vm.Orientacion == 'N'){
                IdP = vm.Norte.Clv_Calle;
            }
            if(vm.Orientacion == 'S'){
                IdP = vm.Sur.Clv_Calle;
            }
            if(vm.Orientacion == 'E'){
                IdP = vm.Este.Clv_Calle;
            }
            if(vm.Orientacion == 'O'){
                IdP = vm.Oeste.Clv_Calle;
            }
            return IdP
        }

        function SetPosicion(){
            if((vm.OrientacionP == 'N' && vm.Norte != undefined && vm.Norte != null)){
                vm.CalleListN = null;
                vm.CalleListN = vm.CalleList;
                Reset();
            }

            if((vm.OrientacionP == 'S' && vm.Sur != undefined && vm.Sur != null)){
                vm.CalleListS = null;
                vm.CalleListS = vm.CalleList;
                Reset();
            }

            if((vm.OrientacionP == 'E' && vm.Este != undefined && vm.Este != null)){
                vm.CalleListE = null;
                vm.CalleListE = vm.CalleList;
                Reset();
            }

            if((vm.OrientacionP == 'O' && vm.Oeste != undefined && vm.Oeste != null)){
                vm.CalleListO = null;
                vm.CalleListO = vm.CalleList;
                Reset();
            }
        }

        function Reset(){
            if(vm.Orientacion == 'N'){
                vm.CalleList.forEach(function(element, key){
                    if(element.Clv_Calle == vm.CalleP){
                        vm.Norte = vm.CalleList[key];
                    }
                });
            }

            if(vm.Orientacion == 'S'){
                vm.CalleList.forEach(function(element, key){
                    if(element.Clv_Calle == vm.CalleP){
                        vm.Sur = vm.CalleList[key];
                    }
                });
            }

            if(vm.Orientacion == 'E'){
                vm.CalleList.forEach(function(element, key){
                    if(element.Clv_Calle == vm.CalleP){
                        vm.Este = vm.CalleList[key];
                    }
                });
            }

            if(vm.Orientacion == 'O'){
                vm.CalleList.forEach(function(element, key){
                    if(element.Clv_Calle == vm.CalleP){
                        vm.Oeste = vm.CalleList[key];
                    }
                });
            }

            vm.OrientacionP = vm.Orientacion;
        }
        
        function ValidaOrientacion(){
            if(
               ((vm.Orientacion == 'N') && ((vm.Norte != undefined && vm.Norte != null)) && ((vm.Este != undefined && vm.Este != null) || (vm.Oeste != undefined && vm.Oeste != null))) 
            || ((vm.Orientacion == 'O') && ((vm.Norte != undefined && vm.Norte != null) || (vm.Sur != undefined && vm.Sur != null)) && ((vm.Oeste != undefined && vm.Oeste != null)))
            || ((vm.Orientacion == 'S') && ((vm.Sur != undefined && vm.Sur != null)) && ((vm.Este != undefined && vm.Este != null) || (vm.Oeste != undefined && vm.Oeste != null))) 
            || ((vm.Orientacion == 'E') && ((vm.Norte != undefined && vm.Norte != null) || (vm.Sur != undefined && vm.Sur != null)) && ((vm.Este != undefined && vm.Este != null)))
            ){
                return true
            }else{
                return false
            }
        }

        function Close() {
            vm.obj = {
                'CalleNorte': ObjCli.Obj.CalleNorte,
                'CalleSur': ObjCli.Obj.CalleSur,
                'CalleEste': ObjCli.Obj.CalleEste,
                'CalleOeste': ObjCli.Obj.CalleOeste,
                'Casa': ObjCli.Obj.Casa,
                'referencia': ObjCli.Obj.referencia,
                'entrecalles': ObjCli.entrecalles,
            };
            Cancel();
        }

        function Cancel(){
            $uibModalInstance.close(vm.obj);
        }

        var vm = this;
        vm.Orientacion = 'N';
        vm.obj = null;
        var FileCorquis = globalService.getUrlReportes() + '/Pictures/' + 'Croquis.jpg';
        vm.FileCorquis = $sce.trustAsResourceUrl(FileCorquis);
        var FileHouse = globalService.getUrlReportes() + '/Pictures/' + 'logoCasa.jpg';
        vm.FileHouse = $sce.trustAsResourceUrl(FileHouse);
        vm.View = false;
        vm.SetPosicion = SetPosicion;
        vm.Save = Save;
        vm.Close = Close;
        initData();
    });