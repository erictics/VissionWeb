'use strict';

angular
    .module('softvApp')
    .controller('ClienteEditarCtrl', function(CatalogosFactory,cajasFactory, ordenesFactory, DocVendedorClienteFactory, ClientesFactory, ngNotify, $uibModal, $state, $stateParams, $rootScope, $localStorage,reportesFactory, $sce, globalService){

        function initData() {

            CatalogosFactory.GetStatusNet().then(function(data){
                vm.StatusServicioList = data.GetStatusNetResult;
            });

            CatalogosFactory.GetStatusCableModem().then(function(data){
                vm.StatusAparatoList = data.GetStatusCableModemResult;
            });

            CatalogosFactory.GetMuestraPromotoresNet().then(function(data){
                vm.VendedorList = data.GetMuestraPromotoresNetResult;
            });

            CatalogosFactory.GetInfoTvs().then(function(data){
                vm.TvConPago = data.GetInfoTvsResult.TvConPago;
                vm.TvSinPago = data.GetInfoTvsResult.TvSinPago;
            });

            CatalogosFactory.GetConsultaClientesList(vm.IdContrato).then(function(data){
                if(data.GetConsultaClientesListResult.length > 0){
                    dameInformacionCliente();
                    CatalogosFactory.GetPlazaList($localStorage.currentUser.idUsuario).then(function(data){
                        vm.PlazaList = data.GetPlazaListResult;
                        CatalogosFactory.GetTipoClienteList_WebSoftvnew().then(function(data){
                            vm.TipoCobroList = data.GetTipoClienteList_WebSoftvnewResult;
                            CatalogosFactory.GetBancoList().then(function(data){
                                vm.BancoList = data.GetBancoListResult;
                                CatalogosFactory.GetMUESTRATIPOSDECUENTAList().then(function(data){
                                    vm.TipoCuentaList = data.GetMUESTRATIPOSDECUENTAListResult;
                                    GetDatosClientes(vm.IdContrato);
                                    GetDatosFiscal(vm.IdContrato);
                                    GetDatosBancario(vm.IdContrato);
                                    GetReferenciasPersonales(vm.IdContrato);
                                    GetNotas(vm.IdContrato);
                                    GetServicios(vm.IdContrato);
                                    GetDocumentos();
                                    GetTipoTicketList();
                                    GetTipoSerList();
                                    GetTapNap();
                                    GetEntreCalles();
                                    ClientesFactory.GetListaTbl_TipoIden().then(function(data){
                                        vm.IdentificacionList = data.GetListaTbl_TipoIdenResult;
                                        GetIdentificacion();
                                    });
                                    GetContratoAnterior();
                                    GetPlacaCliente();
                                });
                            });
                        });
                    });
                }else{
                    ngNotify.set('ERROR, No se encontró el contrato seleccionado.', 'warn');
                    $state.go('home.catalogos.clientes');
                }
            });
        }


        function GetPlacaCliente(){
            ClientesFactory.GetPlacaCliente(vm.IdContrato).then(function(res){
              vm.numPlaca=res.GetPlacaClienteResult;
            });
        }

        function GetReporteContrato (){
            reportesFactory.GetReporteContrato(vm.IdContrato).then(function(res){
                vm.urlcontrato = $sce.trustAsResourceUrl(globalService.getUrlReportes() + '/Reportes/' + res.GetReporteContratoResult);
            });
        }
    
        function dameInformacionCliente (){
            cajasFactory.dameInformacionCliente(vm.IdContrato).then(function(data) {
                vm.informacionCliente = data.GetInformacionClientePeriodosListResult[0];
            });
        }


        function GetDatosClientes(IdContratoCliente){
            CatalogosFactory.GetConsultaClientesList(IdContratoCliente).then(function(data){
                var DatosCliente = data.GetConsultaClientesListResult[0];
                vm.CONTRATO = DatosCliente.CONTRATO;
                vm.IdCliente = DatosCliente.ContratoCom;
                vm.IdPlaza = DatosCliente.IdCompania;
                vm.IdPeriodo = DatosCliente.Clv_Periodo;
                vm.IdTipoCliente = DatosCliente.TipoCliente;
                vm.Nombre = DatosCliente.Nombre;
                vm.NombreAdi = DatosCliente.SegundoNombre;
                vm.PrimerApe = DatosCliente.Apellido_Paterno;
                vm.SegundoApe = DatosCliente.Apellido_Materno;
                vm.ClaveElector = DatosCliente.ClaveElector;
                vm.Telefono = DatosCliente.TELEFONO;
                vm.Celular = DatosCliente.CELULAR;
                vm.Email = DatosCliente.Email;
                vm.EsPersonaFisica = DatosCliente.EsFisica;
                vm.FechaNac = toDate(DatosCliente.FechaNacimiento);
                vm.IdEstado = DatosCliente.Clv_Estado;
                vm.IdMunicipio = DatosCliente.Clv_Ciudad;
                vm.IdLocalidad = DatosCliente.Clv_Localidad;
                vm.IdColonia = DatosCliente.Clv_Colonia;
                vm.IdCalle = DatosCliente.Clv_Calle;
                vm.EntCalles = DatosCliente.ENTRECALLES;
                vm.NumExt = DatosCliente.NUMERO;
                vm.NumInt = DatosCliente.NumInt;
                vm.CodigoPos = DatosCliente.CP;
                vm.Clv_Periodo=DatosCliente.Clv_Periodo;
                vm.NombreCompleto = GetNombre(DatosCliente);
                for (var b = 0; b < vm.TipoCobroList.length; b++) {
                    if(vm.TipoCobroList[b].CLV_TIPOCLIENTE == vm.IdTipoCliente) {
                        vm.TipoCobro = vm.TipoCobroList[b];
                        vm.NombreTipoCobro = vm.TipoCobroList[b].DESCRIPCION;
                    }
                }
                if(vm.EsPersonaFisica == true){
                    vm.TipoPersona = "F";
                }else if(vm.EsPersonaFisica == false){
                    vm.TipoPersona = "M";
                }
                for(var b = 0; b < vm.PlazaList.length; b++){
                    if(vm.PlazaList[b].id_compania == vm.IdPlaza){
                        vm.Plaza = vm.PlazaList[b];
                        vm.NombrePlaza = vm.PlazaList[b].razon_social;
                    }
                }
                CatalogosFactory.GetMuestraEstadosCompaniaList(vm.IdPlaza).then(function(data){
                    vm.EstadoList = data.GetMuestraEstadosCompaniaListResult;
                    for (var b = 0; b < vm.EstadoList.length; b++) {
                        if (vm.EstadoList[b].Clv_Estado == vm.IdEstado) {
                            vm.Estado = vm.EstadoList[b];
                            vm.NombreEstado = vm.EstadoList[b].Nombre;
                            GetCiudadMunicipio(vm.IdMunicipio);
                            GetOrdSerList(300);
                        }
                    }
                });

            });
        }

        function GetCiudadMunicipio(IdMunicipio){
            if(vm.Estado != undefined){
                var RelEstMun = {
                    'clv_estado' : vm.Estado.Clv_Estado,
                    'idcompania' : vm.Plaza.id_compania
                };
                CatalogosFactory.GetMuestraCiudadesEstadoList(RelEstMun).then(function(data){
                    vm.CiudadMunicipioList = data.GetMuestraCiudadesEstadoListResult;
                    if(IdMunicipio != undefined){
                        for (var b = 0; b < vm.CiudadMunicipioList.length; b++) {
                            if (vm.CiudadMunicipioList[b].Clv_Ciudad == IdMunicipio) {
                                vm.CiuMun = vm.CiudadMunicipioList[b];
                                vm.NombreCiuMun = vm.CiudadMunicipioList[b].Nombre;
                                GetLocalidad(vm.IdLocalidad);
                            }
                        }
                    }else{
                        vm.Obj = null;
                        vm.EntCalles = null;
                    }
                });
            }else{
                vm.CiudadMunicipioList = null;
                vm.Obj = null;
                vm.EntCalles = null;
            }
            vm.LocalidadList = null;
            vm.ColoniaList = null;
            vm.CalleList = null;
        }

        function GetLocalidad(IdLocalidad){
            if(vm.CiuMun != undefined){
                CatalogosFactory.GetMuestraLocalidadCiudadList(vm.CiuMun.Clv_Ciudad).then(function(data){
                    vm.LocalidadList = data.GetMuestraLocalidadCiudadListResult;
                    if(IdLocalidad != undefined){
                        for (var b = 0; b < vm.LocalidadList.length; b++) {
                            if (vm.LocalidadList[b].Clv_Localidad == vm.IdLocalidad) {
                                vm.Localidad = vm.LocalidadList[b];
                                vm.NombreLocalidad = vm.LocalidadList[b].NOMBRE;
                                GetColonia(vm.IdColonia);
                            }
                        }
                    }else{
                        vm.Obj = null;
                        vm.EntCalles = null;
                    }
                });
            }else{
                vm.LocalidadList = null;
                vm.Obj = null;
                vm.EntCalles = null;
            }
            vm.ColoniaList = null;
            vm.CalleList = null;
        }
        
        function GetColonia(IdColonia){
            if(vm.Localidad != undefined){
                CatalogosFactory.GetMuestraColoniaLocalidadList(vm.Localidad.Clv_Localidad).then(function(data){
                    vm.ColoniaList = data.GetMuestraColoniaLocalidadListResult;
                    if(IdColonia != undefined){
                        for (var b = 0; b < vm.ColoniaList.length; b++) {
                            if (vm.ColoniaList[b].CLV_COLONIA == IdColonia) {
                                vm.Colonia = vm.ColoniaList[b];
                                vm.NombreColonia = vm.ColoniaList[b].Nombre;
                                GetCalle(vm.IdCalle);
                            }
                        }
                    }else{
                        vm.Obj = null;
                        vm.EntCalles = null;
                    }
                });
            }else{
                vm.ColoniaList = null;
                vm.Obj = null;
                vm.EntCalles = null;
            }
            vm.CalleList = null;
        }

        function GetCalle(IdCalle){
            if(vm.Colonia != undefined){
                var ValidCall = {
                    'IdCompania': vm.Plaza.id_compania, 
                    'Clv_Estado': vm.Estado.Clv_Estado,
                    'Clv_Ciudad': vm.CiuMun.Clv_Ciudad,
                    'Clv_Localidad': vm.Localidad.Clv_Localidad,
                    'Clv_Colonia': vm.Colonia.CLV_COLONIA
                };
                CatalogosFactory.GetValidaColServCli(ValidCall).then(function(data){
                    if(data.GetValidaColServCliResult.Resultado == false){
                        ngNotify.set('NOTA, la colonia que seleccionó aún no tiene servicios definidos.', 'warn');
                    }
                });
                CatalogosFactory.GetMuestraCalleColoniaList(vm.Colonia.CLV_COLONIA).then(function(data){
                    vm.CalleList = data.GetMuestraCalleColoniaListResult;
                    if(IdCalle != undefined){
                        for (var b = 0; b < vm.CalleList.length; b++) {
                            if (vm.CalleList[b].Clv_Calle == IdCalle) {
                                vm.Calle = vm.CalleList[b];
                                vm.NombreCalle = vm.CalleList[b].Nombre;
                            }
                        }
                    }else{
                        vm.Obj = null;
                        vm.EntCalles = null;
                    }
                });
                var ObjCP = {
                    'Clv_Colonia': vm.Colonia.CLV_COLONIA,
                    'Clv_Localidad': vm.Localidad.Clv_Localidad,
                    'Clv_Ciudad': vm.CiuMun.Clv_Ciudad
                };
                ClientesFactory.GetConCPByColoniaLocalidadCiudad(ObjCP).then(function(data){
                    vm.CodigoPos = data.GetConCPByColoniaLocalidadCiudadResult.CodigoPostal;
                });
            }else{
                vm.CalleList = null;
                vm.CodigoPos = null;
                vm.Obj = null;
                vm.EntCalles = null;
            }
            vm.CalleList = null;
        }

        function GetEntreCalles(){
            ClientesFactory.GetSoftvWEb_DameEntrecalles(vm.IdContrato).then(function(data){
                vm.Obj = data.GetSoftvWEb_DameEntrecallesResult;
            });
        }

        function ResetEC(){
            vm.Obj = null;
            vm.EntCalles = null;
        }

        function OpenEntreCalles(){
            var ObjCli = {
                'IdContrato': vm.IdContrato,
                'ClvColonia': vm.Colonia.CLV_COLONIA,
                'Clv_Calle': (vm.EntCalles != null)? 0:vm.Calle.Clv_Calle,
                'Obj': vm.Obj,
                'entrecalles': vm.EntCalles
            };
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'views/catalogos/CroquisEntreCallesForm.html',
                controller: 'CroquisEntreCallesUpdateCtrl',
                controllerAs: 'ctrl',
                backdrop: 'static',
                keyboard: false,
                class: 'modal-backdrop fade',
                size: 'lg',
                resolve: {
                    ObjCli: function () {
                        return ObjCli;
                    }
                }
            });
            modalInstance.result.then(function (Obj) {
                vm.Obj = Obj;
                vm.EntCalles = vm.Obj.entrecalles;
                vm.CalleList.forEach(function(element, key){
                    if(element.Clv_Calle == vm.Obj.callep){
                        vm.Calle = vm.CalleList[key];
                    }
                });
            });
        }

        function GetIdentificacion(){
            ClientesFactory.GetBuscarClienteIdentficacion(vm.IdContrato).then(function(data){
                var Iden = data.GetBuscarClienteIdentficacionResult;
                var Idid = Iden.Id;
                vm.ClaveElector = Iden.Descripcion;
                vm.IdentificacionList.forEach(function(element, key){
                    if(element.Id == Idid){
                        vm.Identidicacion = vm.IdentificacionList[key];
                    }
                });
            });
        }

        function GetContratoAnterior(){
            ClientesFactory.GetSoftvweb_DameContratoAnt(vm.IdContrato).then(function(data){
                vm.ContratoAnterior = data.GetSoftvweb_DameContratoAntResult.ContratoAnterior;
                vm.ShowContAnt = data.GetSoftvweb_DameContratoAntResult.Existe;
            });
        }

        function AddDatosPersonales(){
            var FechaNacD = vm.FechaNac.getDate();
            var FechaNacM = vm.FechaNac.getMonth() + 1;
            var FechaNacY = vm.FechaNac.getFullYear();
            var objCLIENTES_New = {
                'CONTRATO': vm.IdContrato,
                'Nombre': vm.Nombre, 
                'SegundoNombre': vm.NombreAdi,
                'Apellido_Paterno': vm.PrimerApe,
                'Apellido_Materno': vm.SegundoApe,
                'FechaNacimiento': FechaNacD + '/' + FechaNacM + '/' + FechaNacY,
                'EsFisica': (vm.TipoPersona == 'F') ? 1 : 0,
                'TELEFONO': vm.Telefono, 
                'CELULAR': vm.Celular, 
                'Email': vm.Email, 
                'ClaveElector': vm.ClaveElector, 
                'IdCompania': vm.Plaza.id_compania, 
                'Clv_Estado': vm.Estado.Clv_Estado, 
                'Clv_Ciudad': vm.CiuMun.Clv_Ciudad, 
                'Clv_Localidad': vm.Localidad.Clv_Localidad, 
                'Clv_Colonia': vm.Colonia.CLV_COLONIA, 
                'Clv_Calle': vm.Calle.Clv_Calle, 
                'ENTRECALLES': vm.EntCalles,
                'NUMERO': vm.NumExt, 
                'NumInt': vm.NumInt, 
                'Codigo': vm.CodigoPos, 
                'IdUsuario': $localStorage.currentUser.idUsuario,
                'TipoCliente': vm.TipoCobro.CLV_TIPOCLIENTE
            };
            CatalogosFactory.UpdateCLIENTES_New(objCLIENTES_New).then(function(data){
                if(data.UpdateCLIENTES_NewResult == -1){
                    var ObjEntrega = {
                        'contrato': vm.IdContrato,
                        'CalleNorte': vm.Obj.CalleNorte,
                        'CalleSur': vm.Obj.CalleSur,
                        'CalleEste': vm.Obj.CalleEste,
                        'CalleOeste': vm.Obj.CalleOeste,
                        'Casa': vm.Obj.Casa,
                        'referencia': vm.Obj.referencia
                    };
                    ClientesFactory.GetInsertaEntrecalles(ObjEntrega).then(function(data){
                        var ObjIden = {
                            'Id': vm.Identidicacion.Id,
                            'Contrato': vm.IdContrato,
                            'Descripcion': vm.ClaveElector,
                            'Op': 1,
                        };
                        ClientesFactory.GetAddRelClienteIDentificacion(ObjIden).then(function(data){
                            ngNotify.set('CORRECTO, se guardaron datos personales.', 'success');
                            SaveMovimientoSistema('Se editó cliente', objCLIENTES_New, 2);
                        });
                    });
                }else{
                    ngNotify.set('ERROR, al guardar datos personales.', 'warn');
                }
            });
        }

        function GetDateToday() {
            var F = new Date();
            var D = F.getDate();
            var M = F.getMonth();
            var Y = F.getFullYear();
            var ToDay = new Date(Y, M, D);
            return ToDay;
        }

        function toDate(dateStr) {
            var parts = dateStr.split("/");
            return new Date(parts[2], parts[1] - 1, parts[0]);
        }

        function JToDate(Fecha){
            var D = Fecha.getDate();
            var M = Fecha.getMonth() + 1;
            var FD = (String(D).length == 1)? '0'+D : D;
            var FM = (String(M).length == 1)? '0'+M : M;
            var FY = Fecha.getFullYear();
            var FDate =  String(FD) + '/' + String(FM) + '/' + String(FY);
            return FDate;
        }

        function ToDateF(date) {
            var part = date.split(" ");
            return part[0];
        }

        function ValidateFechaVen(dateStr) {
            if(dateStr != undefined){
                if(dateStr.length == 4){
                    var P1 = String(dateStr[0]) + String(dateStr[1]);
                    var P2 = String(dateStr[2]) + String(dateStr[3]);
                    if(parseInt(P1) <= 12 && parseInt(P1) > 0 && parseInt(P2) >= 17){
                        return true;
                    }else{
                        return false;
                    }
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }

        function GetDatosFiscal(IdContrato){
            CatalogosFactory.GetDatosFiscalesList(IdContrato).then(function(data){
                var DatosFiscales = data.GetDatosFiscalesListResult[0];
                vm.RazonSoc = DatosFiscales.RAZON_SOCIAL;
                vm.RFC = DatosFiscales.RFC;
                vm.CURP = DatosFiscales.CURP;
                vm.CalleDF = DatosFiscales.CALLE_RS;
                vm.NumExtDF = DatosFiscales.NUMERO_RS;
                vm.EntCallesDF = DatosFiscales.ENTRECALLES;
                vm.Pais = DatosFiscales.PAIS;
                vm.EstadoDF = DatosFiscales.ESTADO_RS;
                vm.CiuMunDF = DatosFiscales.CIUDAD_RS;
                vm.ColoniaDF = DatosFiscales.COLONIA_RS;
                vm.CodigoPosDF = DatosFiscales.CP_RS;
                vm.TelefonoDF = DatosFiscales.TELEFONO_RS;
                vm.Fax = DatosFiscales.FAX_RS;
                vm.EmailDF = DatosFiscales.Email;
            });
        }

        function GetDatosBancario(IdContrato){
            CatalogosFactory.GetRELCLIBANCOList(IdContrato).then(function(data){
                var DatosBancariosL = data.GetRELCLIBANCOListResult;
                if(DatosBancariosL.length > 0){
                    vm.UpdateDB = true;
                }else{
                    vm.UpdateDB = false;
                }
                var DatosBancarios = DatosBancariosL[0];
                var P1 = String(DatosBancarios.VENCIMIENTO[0]) + String(DatosBancarios.VENCIMIENTO[1]);
                var P2 = String(DatosBancarios.VENCIMIENTO[3]) + String(DatosBancarios.VENCIMIENTO[4]);
                var FechaVen = String(P1) + String(P2);
                vm.IdBanco = DatosBancarios.CLV_BANCO;
                vm.Titular = DatosBancarios.NOMBRE;
                vm.NumTarjeta = DatosBancarios.CUENTA_BANCO;
                vm.CodigoSeg = DatosBancarios.CODIGOSEGURIDAD;
                vm.NumTarjetaC = DatosBancarios.CUENTA_BANCO;
                vm.CodigoSegC = DatosBancarios.CODIGOSEGURIDAD;
                vm.FechaVen = FechaVen;
                vm.NombreTipoCuenta = DatosBancarios.TIPO_CUENTA;
                for(var b = 0; b < vm.TipoCuentaList.length; b++){
                    if(vm.TipoCuentaList[b].NOMBRE == vm.NombreTipoCuenta){
                        vm.TipoCuenta = vm.TipoCuentaList[b];
                    }
                }
                for (var b = 0; b < vm.BancoList.length; b++) {
                    if (vm.BancoList[b].IdBanco == vm.IdBanco) {
                        vm.Banco = vm.BancoList[b];
                    }
                }
            });
        }

        function GetReferenciasPersonales(IdContrato){
            var ObjRef = {
                'contrato': IdContrato,
                'tipo': 'C'
            };
            CatalogosFactory.GettblReferenciasClietesList(ObjRef).then(function(data){
                vm.RefPerList = data.GettblReferenciasClietesListResult;
                if (vm.RefPerList.length == 0) {
					vm.SinRegistros = true;
					vm.ConRegistros = false;
				} else {
					vm.SinRegistros = false;
					vm.ConRegistros = true;
				}
            });
        }

        function GetNotas(IdContrato){
            CatalogosFactory.GetDeepRELCLIENTEOBS(IdContrato).then(function(data){
                var DataObser = data.GetDeepRELCLIENTEOBSResult;
                if(DataObser.Obs != null){
                    vm.UpdateObs = true;
                    vm.Observaciones = DataObser.Obs;
                }else{
                    vm.UpdateObs = false;
                    vm.Observaciones = null;
                }
            });
            CatalogosFactory.GetDeepRoboDeSeñal_New(IdContrato).then(function(data){
                var DataNota = data.GetDeepRoboDeSeñal_NewResult;
                if(DataNota != null){
                    vm.Notas = DataNota.Descripcion;
                    vm.UpdateNota = true;
                }else{
                    vm.Notas = null;
                    vm.UpdateNota = false;
                }
            });
        }

        function AddDatosFiscales(){
            if(vm.IdContrato != undefined){
                var objDatosFiscales = {
                    'Contrato': vm.IdContrato,
                    'RAZON_SOCIAL' : vm.RazonSoc,
                    'RFC' : vm.RFC,
                    'CURP' : vm.CURP,
                    'PAIS' : vm.Pais,
                    'ESTADO_RS' : vm.EstadoDF,
                    'CIUDAD_RS' : vm.CiuMunDF,
                    'COLONIA_RS' : vm.ColoniaDF,
                    'CP_RS' : vm.CodigoPosDF,
                    'CALLE_RS' : vm.CalleDF,
                    'NUMERO_RS' : vm.NumExtDF,
                    'ENTRECALLES' : vm.EntCallesDF,
                    'TELEFONO_RS' : vm.TelefonoDF,
                    'FAX_RS' : vm.Fax,
                    'Email' : vm.EmailDF
                };
                CatalogosFactory.AddDatosFiscales(objDatosFiscales).then(function(data){
                    var DatosFiscales = data.AddDatosFiscalesResult;
                    if(DatosFiscales == -1){
                        ngNotify.set('CORRECTO, se guardaron datos fiscales.', 'success');
                        GetDatosFiscal(vm.IdContrato);
                        SaveMovimientoSistema('Se Agregó datos fiscales a cliente', objDatosFiscales, 6);
                    }else{
                        ngNotify.set('ERROR, al guardar datos fiscales.', 'warn');
                    }
                });
            }else{
                ngNotify.set('Aun no se han registrado los datos personales.', 'warn');
            }
        }

        function AddDatosBancarios(){
            if(vm.IdContrato != undefined){
                var FechaVen = String(vm.FechaVen[0]) + String(vm.FechaVen[1]) + '/' + String(vm.FechaVen[2]) + String(vm.FechaVen[3]);
                var objRELCLIBANCO = {
                    'Contrato': vm.IdContrato,
                    'CLV_BANCO': vm.Banco.IdBanco,
                    'CUENTA_BANCO': vm.NumTarjeta,
                    'TIPO_CUENTA': vm.TipoCuenta.NOMBRE,
                    'VENCIMIENTO': FechaVen,
                    'CODIGOSEGURIDAD': vm.CodigoSeg,
                    'NOMBRE': vm.Titular
                };
                if(vm.UpdateDB == false){
                    CatalogosFactory.AddRELCLIBANCO(objRELCLIBANCO).then(function(data){
                        if(data.AddRELCLIBANCOResult == 1){
                            ngNotify.set('CORRECTO, se guardaron datos bancarios.', 'success');
                            SaveMovimientoSistema('Se Agregó Datos Bancarios a Cliente', objRELCLIBANCO, 7);
                            GetDatosBancario(vm.IdContrato);
                        }else{
                            ngNotify.set('ERROR, al guardar datos bancarios.', 'warn');
                        }
                    });
                }else if(vm.UpdateDB == true){
                    CatalogosFactory.UpdateRELCLIBANCO(objRELCLIBANCO).then(function(data){
                        if(data.UpdateRELCLIBANCOResult == 1){
                            ngNotify.set('CORRECTO, se guardaron datos bancarios.', 'success');
                            SaveMovimientoSistema('Se Agregó Datos Bancarios a Cliente', objRELCLIBANCO, 7);
                            GetDatosBancario(vm.IdContrato);
                        }else{
                            ngNotify.set('ERROR, al guardar datos bancarios.', 'warn');
                        }
                    });
                }
            }else{
                ngNotify.set('Aun no se han registrado los datos personales.', 'warn');
            }
        }

        function OpenAddRefPersonal(){
            var IdContrato = vm.IdContrato;
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'views/catalogos/ModalEditarRefCliente.html',
                controller: 'ModalAddRefClienteCtrl',
                controllerAs: 'ctrl',
                backdrop: 'static',
                keyboard: false,
                class: 'modal-backdrop fade',
                size: 'sm',
                resolve: {
                    IdContrato: function () {
                        return IdContrato;
                    }
                }
            });
            modalInstance.result.then(function () {
                GetReferenciasPersonales(vm.IdContrato);
            });
        }

        function OpenEditRefPersonal(ObjRefCliente){
            var ObjRefCliente = ObjRefCliente;
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'views/catalogos/ModalEditarRefCliente.html',
                controller: 'ModalEditarRefClienteCtrl',
                controllerAs: 'ctrl',
                backdrop: 'static',
                keyboard: false,
                class: 'modal-backdrop fade',
                size: 'sm',
                resolve: {
                    ObjRefCliente: function () {
                        return ObjRefCliente;
                    }
                }
            });
            modalInstance.result.then(function () {
                GetReferenciasPersonales(vm.IdContrato);
            });
        }

        function OpenDeleteRefPersonal(ObjRefCliente){
            var ObjRefCliente = ObjRefCliente;
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'views/catalogos/ModalEliminarRefCliente.html',
                controller: 'ModalEliminarRefClienteCtrl',
                controllerAs: 'ctrl',
                backdrop: 'static',
                keyboard: false,
                class: 'modal-backdrop fade',
                size: 'sm',
                resolve: {
                    ObjRefCliente: function () {
                        return ObjRefCliente;
                    }
                }
            });
            modalInstance.result.then(function () {
                GetReferenciasPersonales(vm.IdContrato);
            });
        }

        function AddObservaciones(){
            if(vm.UpdateObs == false){
                var objRELCLIENTEOBS = {
                    'Contrato': vm.IdContrato,
                    'Obs': vm.Observaciones
                };
                CatalogosFactory.AddRELCLIENTEOBS(objRELCLIENTEOBS).then(function(data){
                    ngNotify.set('CORRECTO, se guardó las observaciones.', 'success');
                    SaveMovimientoSistema('Se Agregó Nota de Observaciones a Cliente', objRELCLIENTEOBS, 8);
                    GetNotas(vm.IdContrato);
                });
            }else{
                var objRELCLIENTEOBS = {
                    'Contrato': vm.IdContrato,
                    'Obs': vm.Observaciones
                };
                CatalogosFactory.UpdateRELCLIENTEOBS(objRELCLIENTEOBS).then(function(data){
                    ngNotify.set('CORRECTO, se guardó las observaciones.', 'success');
                    SaveMovimientoSistema('Se Agrego Nota de Observaciones a Cliente', objRELCLIENTEOBS, 8);
                    GetNotas(vm.IdContrato);
                });
            }
        }

        function AddNotas(){
            if(vm.UpdateNota == false){
                var objRoboDeSeñal_New = {
                    'Contrato': vm.IdContrato,
                    'Descripcion': vm.Notas
                };
                CatalogosFactory.AddRoboDeSeñal_New(objRoboDeSeñal_New).then(function(data){
                    ngNotify.set('CORRECTO, se guardó las notas.', 'success');
                    SaveMovimientoSistema('Se Agregó Nota de Robo de Señal a Cliente', objRoboDeSeñal_New, 9);
                    GetNotas(vm.IdContrato);
                });
            }else{
                var objRoboDeSeñal_New = {
                    'Contrato': vm.IdContrato,
                    'Descripcion': vm.Notas
                };
                CatalogosFactory.UpdateRoboDeSeñal_New(objRoboDeSeñal_New).then(function(data){
                    ngNotify.set('CORRECTO, se guardó las notas.', 'success');
                    SaveMovimientoSistema('Se Agregó Nota de Robo de Señal a Cliente', objRoboDeSeñal_New, 9);
                    GetNotas(vm.IdContrato);
                });
            }
        }

        function GetServicios(IdContrato){
            CatalogosFactory.GetMuestraArbolServicios_ClientesList(IdContrato).then(function(data){
                vm.ServicioList = data.GetMuestraArbolServicios_ClientesListResult;
                vm.expandedNodes=[];
                angular.forEach(vm.ServicioList, function(value, key) {
                    vm.expandedNodes.push(value);
                });
                vm.ServicioList.forEach(function(element){
                    if(element.idMedio == 1){
                        vm.SaveTap = true;
                        GetTap();
                    }

                    if(element.idMedio == 2){
                        vm.SaveNap = true;
                        GetNap();
                    }
                });
                vm.ShowServicios = (vm.ServicioList.length > 0)? true : false;
                var CS = vm.ServicioList.length;
                var CA = 0;
                for(var i = 0; vm.ServicioList.length > i; i++){
                    CA = CA + vm.ServicioList[i].children.length;
                }
                vm.CT = CS + CA;
                vm.ShowServiciosE = true;
                if(vm.CT >= 13){
                    vm.ShowServiciosEN = 0;
                }else{
                    vm.ShowServiciosEN = (vm.CT > 0)? 13 - vm.CT:13;
                }
                DetalleConcepto(vm.ServicioList[0]);
            });
        }

        function DetalleConcepto(ObjConcepto){
            if(ObjConcepto.Tipo == 'S' || ObjConcepto.Tipo == 'P'){
                vm.ServicioDisCtrl = (ObjConcepto.status == 'B' || ObjConcepto.status == 'F')? true:false;
                vm.ConceptoTipo = ObjConcepto.Tipo;
                vm.DivServicio = true;
                vm.DivAparato = false;
                vm.ShowServiciosE = false;
                vm.TBtnSaveSP = (ObjConcepto.Tipo == 'S')? 'Guardar Detalle del Servicio':'Guardar Detalle del Paquete';
                vm.TBtnDeleteSP = (ObjConcepto.Tipo == 'S')? 'Eliminar Servicio':'Eliminar Paquete';
                var Clv_UnicaNet = ObjConcepto.Clv_UnicaNet;
                var IdMedio = ObjConcepto.idMedio;
                vm.NombreServicio = ObjConcepto.Nombre;
                vm.DetalleServicio = ObjConcepto.Detalle;
                CatalogosFactory.GetClientesServicioList(Clv_UnicaNet).then(function(data){
                    if($stateParams.token){
                        
                        if(atob($stateParams.token) === $localStorage.currentUser.usuario ){    
                           vm.token = true;                       
                            //document.getElementById("InpSinPago").disabled = false;
                           // document.getElementById("InpConPago").disabled = false;
                        }
                    }else{
                        vm.token = vm.soycontrolTotal;
                    }
                    var ServicioResult = data.GetClientesServicioListResult[0];
                    vm.Clv_UnicaNet = ServicioResult.Clv_UnicaNet;
                    vm.Clv_Servicio = ServicioResult.Clv_Servicio;
                    vm.Factura = ServicioResult.factura;
                    vm.ObservacionesServicio = ServicioResult.Obs;
                    vm.UltimoMesServicio = ServicioResult.ultimo_mes;
                    vm.UltimoAnioServicio = ServicioResult.ultimo_anio;
                    vm.FechaContratacion = (ServicioResult.fecha_solicitud != null)? toDate(ServicioResult.fecha_solicitud) : null;
                    vm.FechaContratacionP = (ServicioResult.fecha_solicitud != null)? toDate(ServicioResult.fecha_solicitud) : null;
                    vm.FechaInstalacion = (ServicioResult.fecha_instalacio != null)? toDate(ServicioResult.fecha_instalacio) : null;
                    vm.FechaSuspencion = (ServicioResult.fecha_suspension != null)? toDate(ServicioResult.fecha_suspension) : null;
                    vm.FechaBaja = (ServicioResult.fecha_baja != null)? toDate(ServicioResult.fecha_baja) : null;
                    vm.FechaFueraArea = (ServicioResult.fecha_Fuera_Area != null)? toDate(ServicioResult.fecha_Fuera_Area) : null;
                    vm.FechaUltimoPago = (ServicioResult.FECHA_ULT_PAGO != null)? toDate(ServicioResult.FECHA_ULT_PAGO) : null;
                    vm.PrimerMen = (ServicioResult.PrimerMensualidad == true)? 'Y' : 'N';
                    //vm.PrimerMen =(vm.token)?'Y':'N';
                    vm.Cortesia = (ServicioResult.Cortesia == 1)? 'Y' : 'N';
                    vm.Clv_usuarioCapturo = ServicioResult.Clv_usuarioCapturo;
                    vm.AdicServicio = ServicioResult.Adic;
                    vm.CLV_MOTCANServicio = ServicioResult.CLV_MOTCAN;
                    vm.Clv_PromocionServicio = ServicioResult.Clv_Promocion;
                    vm.EmailServicio = ServicioResult.Email;
                    vm.GENERAOSINSTAServicio = ServicioResult.GENERAOSINSTA;
                    vm.IdMedioServicio = ServicioResult.IdMedio;
                    vm.TVCONPAGOServicio = ServicioResult.TVCONPAGO;
                    vm.TVSINPAGOServicio = ServicioResult.TVSINPAGO;
                    vm.facturaAntServicio = ServicioResult.facturaAnt;
                    vm.primerMesAntServicio = ServicioResult.primerMesAnt;
                    vm.statusAntServicio = ServicioResult.statusAnt;
                    vm.ClvStatus = ServicioResult.status;
                    var Vendedor = ServicioResult.Clv_Vendedor;
                    if(Vendedor > 0){
                        for(var i = 0; vm.VendedorList.length > i; i ++){
                            if(vm.VendedorList[i].Clv_Vendedor == Vendedor){
                                vm.Vendedor = vm.VendedorList[i];
                            }
                        }
                    }else{
                        vm.Vendedor = undefined;
                    }
                    for(var i = 0; vm.StatusServicioList.length > i; i ++){
                        if(vm.StatusServicioList[i].Clv_StatusNet == vm.ClvStatus){
                            vm.StatusServicio = vm.StatusServicioList[i];
                        }
                    }
                    CatalogosFactory.GetDeepServicios_New(vm.Clv_Servicio).then(function(data){
                        var Clv_TipSer = data.GetDeepServicios_NewResult.Clv_TipSer;
                        vm.Clv_TipSer = Clv_TipSer;
                        vm.ShowTipServ1 = (data.GetDeepServicios_NewResult.Clv_TipSer == 1)? true : false;
                        vm.ShowBtnAddPaq = (Clv_TipSer == 3)? true : false;
                        var ObjUsuario = {
                            'CLV_UNICANET': vm.Clv_UnicaNet,
                            'tipo_serv': Clv_TipSer
                        };
                        CatalogosFactory.GetMuestra_Usuarios(ObjUsuario).then(function(data){
                            vm.UsuarioList = data.GetMuestra_UsuariosResult;
                            for(var i = 0; vm.UsuarioList.length > i; i ++){
                                if(vm.UsuarioList[i].Clave == vm.Clv_usuarioCapturo){
                                    vm.Usuario = vm.UsuarioList[i];
                                    CatalogosFactory.GetDeepMuestraMedios_New(IdMedio).then(function(data){
                                        var MedioResult = data.GetDeepMuestraMedios_NewResult
                                        vm.Medio = (MedioResult != null)? MedioResult.Descripcion : '';
                                        GetDescuentoServicio(Clv_TipSer);
                                    });
                                }
                            }
                        });
                    });
                });                
            }else if(ObjConcepto.Tipo == 'A'){
                vm.DivServicio = false;
                vm.DivAparato = true;
                vm.ShowServiciosE = true;
                var CT2 = (vm.CT >= 6)?  vm.CT:6;
                if(CT2 >= 13){
                    vm.ShowServiciosEN = 0;
                }else{
                    vm.ShowServiciosEN = (CT2 > 0)? 13 - CT2:13;
                }
                var ContratoNet = ObjConcepto.ContratoNet;
                vm.NombreAparato = ObjConcepto.Nombre;
                vm.DetalleAparato = ObjConcepto.Detalle;
                CatalogosFactory.GetClientesAparatoList(ContratoNet).then(function(data){
                    var AparatoResult = data.GetClientesAparatoListResult[0];
                    vm.ContratoNet = AparatoResult.ContratoNet;
                    vm.Clv_CableModem = AparatoResult.Clv_CableModem;
                    vm.ObservacionesAparatos = AparatoResult.Obs;
                    vm.FechaActivacionAparato = (AparatoResult.Fecha_Activacion != null)? toDate(AparatoResult.Fecha_Activacion) : null;
                    vm.FechaSuspencionAparato = (AparatoResult.Fecha_Suspension != null)? toDate(AparatoResult.Fecha_Suspension) : null;
                    vm.FechaBajaAparato = (AparatoResult.Fecha_Baja != null)? toDate(AparatoResult.Fecha_Baja) : null;
                    vm.Fecha_Traspaso = (AparatoResult.Fecha_Traspaso != null)? toDate(AparatoResult.Fecha_Traspaso) : null;
                    vm.SeRenta = (AparatoResult.SeRenta == true)? 'Y' : 'N';
                    vm.Clv_UsuarioAparato = AparatoResult.Clv_Usuario;
                    vm.NoCajaAparato = AparatoResult.NoCaja;
                    vm.Tipo_CablemodemAparato = AparatoResult.Tipo_Cablemodem;
                    vm.no_extensionesAparato = AparatoResult.no_extensiones;
                    vm.ventacablemodem1Aparato = AparatoResult.ventacablemodem1;
                    vm.ventacablemodem2Aparato = AparatoResult.ventacablemodem2;
                    vm.StatusA = AparatoResult.Status;
                    for(var i = 0; vm.StatusAparatoList.length > i; i ++){
                        if(vm.StatusAparatoList[i].Clv_StatusNet == vm.StatusA){
                            vm.StatusAparato = vm.StatusAparatoList[i];
                        }
                    }
                    CatalogosFactory.GetModeloAparato(vm.Clv_CableModem).then(function(data){
                        vm.ModeloAparato = data.GetModeloAparatoResult.Nombre;
                    });
                });
            }
        }

        function UpdateServicioCliente(){
            var objClientesServicio = {
                'Clv_UnicaNet': vm.Clv_UnicaNet,
                'Contrato': vm.IdContrato,
                'Clv_Servicio': vm.Clv_Servicio,
                'status': vm.StatusServicio.Clv_StatusNet,
                'fecha_solicitud': (vm.FechaContratacion != null)? JToDate(vm.FechaContratacion) :'01/01/1900',
                'fecha_instalacio': (vm.FechaInstalacion != null)? JToDate(vm.FechaInstalacion) : '01/01/1900',
                'fecha_suspension': (vm.FechaSuspencion != null)? JToDate(vm.FechaSuspencion) : '01/01/1900',
                'fecha_baja': (vm.FechaBaja != null)? JToDate(vm.FechaBaja) : '01/01/1900',
                'fecha_Fuera_Area': (vm.FechaFueraArea != null)? JToDate(vm.FechaFueraArea) : '01/01/1900',
                'FECHA_ULT_PAGO': (vm.FechaUltimoPago != null)? JToDate(vm.FechaUltimoPago) : '01/01/1900',
                'PrimerMensualidad': (vm.PrimerMen == 'Y')? 1:0,
                'ultimo_mes': vm.UltimoMesServicio,
                'ultimo_anio': vm.UltimoAnioServicio,
                'primerMesAnt': vm.primerMesAntServicio,
                'statusAnt': vm.statusAntServicio,
                'facturaAnt': vm.facturaAntServicio,
                'GENERAOSINSTA': vm.GENERAOSINSTAServicio,
                'factura': vm.Factura,
                'Clv_Vendedor': (vm.Vendedor != undefined)? vm.Vendedor.Clv_Vendedor:0,
                'Clv_Promocion': vm.Clv_PromocionServicio,
                'Email': vm.EmailServicio,
                'Obs': vm.ObservacionesServicio,
                'CLV_MOTCAN': vm.CLV_MOTCANServicio,
                'Cortesia': (vm.Cortesia == 'Y')? 1:0,
                'Adic': vm.AdicServicio,
                'TVSINPAGO': vm.TVSINPAGOServicio,
                'TVCONPAGO':  vm.TVCONPAGOServicio,
                'IdMedio': vm.IdMedioServicio,
                'Clv_usuarioCapturo': (vm.Usuario != undefined)? vm.Usuario.Clave : vm.Clv_usuarioCapturo
            };
            CatalogosFactory.UpdateClientesServicio(objClientesServicio).then(function(data){
                var ObjConcepto = {
                    'Clv_UnicaNet': vm.Clv_UnicaNet,
                    'Nombre': vm.NombreServicio,
                    'Detalle': vm.DetalleServicio,
                    'Tipo': 'S',
                    'idMedio': vm.IdMedioServicio
                };
                if(data.UpdateClientesServicioResult == -1){
                    ngNotify.set('CORRECTO, se guardó detalle del servicio.', 'success');
                    SaveMovimientoSistema('Se Edito Servico a Cliente', objClientesServicio, 10);
                    DetalleConcepto(ObjConcepto);
                    GetServicios(vm.IdContrato);
                }else{
                    ngNotify.set('ERROR, al guardar detalle del servicio.', 'warn');
                    DetalleConcepto(ObjConcepto);
                    GetServicios(vm.IdContrato);
                }
                SaveTapNap();
            });
        }

        function UpdateAparatoCliente(){
            var objClientesAparato = {
                'ContratoNet': vm.ContratoNet,
                'Status': vm.StatusAparato.Clv_StatusNet,
                'Clv_CableModem': vm.Clv_CableModem,
                'Clv_Usuario': vm.Clv_UsuarioAparato,
                'Fecha_Activacion': (vm.FechaActivacionAparato != null)? JToDate(vm.FechaActivacionAparato) : '01/01/1900',
                'Fecha_Suspension': (vm.FechaSuspencionAparato != null)? JToDate(vm.FechaSuspencionAparato):'01/01/1900',
                'Fecha_Baja': (vm.FechaBajaAparato != null)? JToDate(vm.FechaBajaAparato):'01/01/1900',
                'Fecha_Traspaso': (vm.Fecha_Traspaso != null)? JToDate(vm.Fecha_Traspaso):'01/01/1900',
                'Obs': vm.ObservacionesAparatos,
                'SeRenta': (vm.SeRenta == 'Y')? 1:0,
                'no_extensiones': vm.no_extensionesAparato,
                'NoCaja': vm.NoCajaAparato,
                'ventacablemodem1': vm.ventacablemodem1Aparato,
                'ventacablemodem2': vm.ventacablemodem2Aparato,
                'Tipo_Cablemodem': vm.Tipo_CablemodemAparato
            };
            CatalogosFactory.UpdateClientesAparato(objClientesAparato).then(function(data){
                var ObjConcepto = {
                    'ContratoNet': vm.ContratoNet,
                    'Nombre': vm.NombreAparato,
                    'Detalle': vm.DetalleAparato,
                    'Tipo': 'A'
                };
                if(data.UpdateClientesAparatoResult == -1){
                    ngNotify.set('CORRECTO, se guardó detalle del aparato.', 'success');
                    SaveMovimientoSistema('Se Modificó el Detalle del Aparato a Cliente', objClientesAparato, 11);
                    DetalleConcepto(ObjConcepto);
                    GetServicios(vm.IdContrato);
                }else{
                    ngNotify.set('ERROR, al guardar detalle del aparato.', 'warn');
                    DetalleConcepto(ObjConcepto);
                    GetServicios(vm.IdContrato);
                }
                SaveTapNap();
            });
        }

        $rootScope.$on('LoadServicioCliente', function(e, IdContrato){
            GetServicios(IdContrato);
        });

        function OpenAddServicioCliente(){
            var ValidCall = {
                'IdCompania': vm.Plaza.id_compania, 
                'Clv_Estado': vm.Estado.Clv_Estado,
                'Clv_Ciudad': vm.CiuMun.Clv_Ciudad,
                'Clv_Localidad': vm.Localidad.Clv_Localidad,
                'Clv_Colonia': vm.Colonia.CLV_COLONIA
            };
            CatalogosFactory.GetValidaColServCli(ValidCall).then(function(data){
                if(data.GetValidaColServCliResult.Resultado == false){
                    ngNotify.set('NOTA, la colonia que seleccionó aún no tiene servicios definidos.', 'warn');
                }else{
                    var IdContrato = vm.IdContrato;
                    var modalInstance = $uibModal.open({
                        animation: true,
                        ariaLabelledBy: 'modal-title',
                        ariaDescribedBy: 'modal-body', 
                        templateUrl: 'views/catalogos/ModalServicioClienteForm.html',
                        controller: 'ModalServicioClienteAddCtrl',
                        controllerAs: 'ctrl',
                        backdrop: 'static',
                        keyboard: false,
                        class: 'modal-backdrop fade',
                        size: 'sm',
                        resolve: {
                            IdContrato: function () {
                                return IdContrato;
                            }
                        }
                    });
                }
            });
        }

        function DeleteServicioCliente(Clv_UnicaNet, ConceptoTipo){
            var Clv_UnicaNetD = (Clv_UnicaNet != null && Clv_UnicaNet != undefined)? Clv_UnicaNet:vm.Clv_UnicaNet;
            var ObjServDel = {
                'Clv_UnicaNetD': Clv_UnicaNetD,
                'ConceptoTipo': (ConceptoTipo != null && ConceptoTipo != undefined)? ConceptoTipo:vm.ConceptoTipo
            }
            CatalogosFactory.GetValidaPapoClienteServicio(Clv_UnicaNetD).then(function(data){
                var ToDay = GetDateToday();
                if(data.GetValidaPapoClienteServicioResult == 0 && vm.FechaContratacionP.getTime() == ToDay.getTime()){
                    OpenDeleteServicioCliente(ObjServDel);
                }else{
                    var MSJ = (vm.ConceptoTipo == 'S')? 'ERROR, solo se puede eliminar un servicio el mismo día que se contrató y/o que tenga ningún pago realizado.':'ERROR, solo se puede eliminar un paquete el mismo día que se contrató y/o que tenga ningún pago realizado.'
                    ngNotify.set(MSJ, 'warn');
                }
            });
        }

        function OpenDeleteServicioCliente(ObjServDel){
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body', 
                templateUrl: 'views/catalogos/ModalServicioClienteDelete.html',
                controller: 'ModalServicioClienteDeleteCtrl',
                controllerAs: 'ctrl',
                backdrop: 'static',
                keyboard: false,
                class: 'modal-backdrop fade',
                size: 'sm',
                resolve: {
                    ObjServDel: function () {
                        return ObjServDel;
                    }
                }
            });
            modalInstance.result.then(function () {
                vm.DivServicio = false;
                vm.DivAparato = false;
                GetServicios(vm.IdContrato);
            });
        }

        function OpenAddPaqueteAdic(Clv_UnicaNet){
            var ObjPaqAdic = {
                'Clv_UnicaNet': (Clv_UnicaNet != null && Clv_UnicaNet != undefined)? Clv_UnicaNet:vm.Clv_UnicaNet,
                'IdContrato': vm.IdContrato
            };
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body', 
                templateUrl: 'views/catalogos/ModalPaqueteAdicForm.html',
                controller: 'ModalPaqueteAdicAddCtrl',
                controllerAs: 'ctrl',
                backdrop: 'static',
                keyboard: false,
                class: 'modal-backdrop fade',
                size: 'sm',
                resolve: {
                    ObjPaqAdic: function () {
                        return ObjPaqAdic;
                    }
                }
            });
            modalInstance.result.then(function (IdContrato) {
                GetServicios(IdContrato);
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        }

        $rootScope.$on('LoadDescuentoServicio', function(e, Clv_TipSer){
            GetDescuentoServicio(Clv_TipSer);
        });

        function GetDescuentoServicio(Clv_TipSer){
            var ObjRelDescuento = {
                "Clv_UnicaNet": vm.Clv_UnicaNet,
                "Clv_TipSer": Clv_TipSer
            };
            CatalogosFactory.GetConRelCteDescuento(ObjRelDescuento).then(function(data){
                var DescuentoServicio = data.GetConRelCteDescuentoResult;
                if(DescuentoServicio.Clv_TipServ != null && DescuentoServicio.Clv_UnicaNet != null){
                    vm.DescuentoServicio = DescuentoServicio.Descuento;
                    vm.ConDescuento = true;
                    vm.SinDescuento = false;
                }else{
                    vm.DescuentoServicio = 0;
                    vm.SinDescuento = true;
                    vm.ConDescuento = false;
                }
            });
        }

        function AddDescuentoServicio(){
            var Clv_UnicaNet = vm.Clv_UnicaNet;
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body', 
                templateUrl: 'views/catalogos/ModalDescuentoServicioForm.html',
                controller: 'ModalDescuentoServicioCtrl',
                controllerAs: 'ctrl',
                backdrop: 'static',
                keyboard: false,
                class: 'modal-backdrop fade',
                size: 'sm',
                resolve: {
                    Clv_UnicaNet: function () {
                        return Clv_UnicaNet;
                    }
                }
            });
        }

        function GetTapNap(){
            if(vm.SaveTap == true){
                GetTap();
            }

            if(vm.SaveNap == true){
                GetNap();
            }
        }

        function GetTap(){
            vm.TapList = [];
            ordenesFactory.GetConTapCliente(vm.IdContrato).then(function(data){
                var IdTap = (data.GetConTapClienteResult.IdTap != null)? data.GetConTapClienteResult.IdTap:0;
                ordenesFactory.GetObtieneTap(vm.IdContrato).then(function(data){
                    vm.TapList = data.GetObtieneTapResult;
                    vm.TapList.forEach(function(element, key){
                        if(element.IdTap == IdTap){
                            vm.Tap = vm.TapList[key];
                        }
                    });
                });
            });
        }

        function GetNap(){
            vm.NapList = [];
            ordenesFactory.GetTraerNap(vm.IdContrato).then(function(data){
                var IdNap = (data.GetTraerNapResult.IdTap != null)? data.GetTraerNapResult.IdTap:0;
                ordenesFactory.GetObtieneNap(vm.IdContrato).then(function(data){
                    vm.NapList = data.GetObtieneNapResult;
                    vm.NapList.forEach(function(element, key){
                        if(element.IdTap == IdNap){
                            vm.Nap = vm.NapList[key];
                        }
                    });
                });
            });
        }

        function SaveTapNap(){
            if(vm.SaveTap == true && vm.Tap.IdTap != 0){
                SetSaveTap();
            }
            if(vm.SaveNap == true && vm.Nap.IdTap != 0){
                SetSaveNap();
            }
        }

        function SetSaveTap(){
            var ObjTap = {
                'CONTRATO': vm.IdContrato,
                'TAP': vm.Tap.Clavetecnica,
                'idtap': vm.Tap.IdTap
            };
            ordenesFactory.GetGuardarRelClienteTap(ObjTap).then(function(data){
                SaveMovimientoSistema('Se Modificó Tap a Cliente', ObjTap, 12);
                GetTapNap();
            });
        }

        function SetSaveNap(){
            var ObjNap = {
                'CONTRATO': vm.IdContrato,
                'TAP': vm.Nap.Clavetecnica,
                'IDTAP': vm.Nap.IdTap
            };
            ordenesFactory.GetGuardarRelClienteNap(ObjNap).then(function(data){
                SaveMovimientoSistema('Se Modificó Nap a Cliente', ObjNap, 13);
                GetTapNap();
            });
        }

        function OpenCobros(){
            var ObjCobro = {
                'Clv_UnicaNet': vm.Clv_UnicaNet,
                'Clv_TipSer': vm.Clv_TipSer,
                'Clv_Servicio': vm.Clv_Servicio,
                'IdContrato': vm.IdContrato,
                'Edit': true
            };
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body', 
                templateUrl: 'views/catalogos/ModalCobroForm.html',
                controller: 'ModalCobroCtrl',
                controllerAs: 'ctrl',
                backdrop: 'static',
                keyboard: false,
                class: 'modal-backdrop fade',
                size: 'sm',
                resolve: {
                    ObjCobro: function () {
                        return ObjCobro;
                    }
                }
            });
        }

        function GetDocumentos(){
            DocVendedorClienteFactory.GetValidaPerfilActivarChecksDocumentos(vm.tipoUsuario).then(function(data){
                vm.DisCbxDocumneto = (data.GetValidaPerfilActivarChecksDocumentosResult.desactivar == 1)? true:false;
                DocVendedorClienteFactory.GetDameDocumentos(vm.IdContrato).then(function(data){
                    vm.DocumentoList = data.GetDameDocumentosResult;
                    GetDocumentosCliente();
                });
            });
        }

        function GetDocumentosCliente(){
            DocVendedorClienteFactory.GetDameOpcionesDocumentos(vm.IdContrato).then(function(data){
                var OpcionDoc = data.GetDameOpcionesDocumentosResult;
                vm.Revisado = (OpcionDoc.cbRevisado == 1)? true:false;
                vm.Recibido = (OpcionDoc.cbRecibido == 1)? true:false;
                DocVendedorClienteFactory.GetDameDocumentosContrato(vm.IdContrato).then(function(data){
                    vm.DocumentoClienteList = data.GetDameDocumentosContratoResult;
                    vm.ViewDocClienteList = (vm.DocumentoClienteList.length > 0)? true:false;
                });
            });
        }

        function SaveDocumentoCliente(){
            vm.FileName = null;
            vm.TituloDoc = null;
            if(vm.Evidencia.type == "application/pdf"){
                if(vm.Evidencia.size <= 1000000){
                    var EvidenciaFD = new FormData();
                    EvidenciaFD.append('file', vm.Evidencia); 
                    EvidenciaFD.append('IdDocumento', vm.Documento.IdDocumento);
                    EvidenciaFD.append('contrato', vm.IdContrato);
                    DocVendedorClienteFactory.GetGuardaDocumentoPDF(EvidenciaFD).then(function(data){
                        ngNotify.set('CORRECTO, Correcto se guardó el documento para el cliente.', 'success');
                        GetDocumentosCliente();
                        ResetEvidencia();
                        SaveMovimientoSistema('Se agregó documento: ' + vm.Documento.Documento + ', a cliente', '', 14);
                    });
                }else{
                    ngNotify.set('ERROR, el tamaño del archivo es invalido.', 'warn');
                }
            }else{
                ngNotify.set('ERROR, Formato invalido', 'warn');
            }
        }

        function GetDocumentoCliente(ObjDoc){
            var ObjDocumento = {
                'IdDocumento': ObjDoc.IdDocumento, 
                'contrato': vm.IdContrato
            };
            DocVendedorClienteFactory.GetDimeTipoDocumento(ObjDocumento).then(function(data){
                DocVendedorClienteFactory.GetDocumentoClienteWeb(ObjDocumento).then(function(data){
                    var Name = data.GetDocumentoClienteWebResult;
                    var FileName = globalService.getUrlReportes() + '/Images/' + Name;
                    vm.FileName = $sce.trustAsResourceUrl(FileName);
                    vm.TituloDoc = ObjDoc.Documento;
                });
            });
        }

        function SetRevisado(){
            var ObjRevisado = {
                'cbRevisado': (vm.Revisado == true)? 1:0,
                'contrato': vm.IdContrato,
                'idUsuario': $localStorage.currentUser.idUsuario
            };
            DocVendedorClienteFactory.GetModificaRevisado(ObjRevisado).then(function(data){
                GetDocumentosCliente();
            });
        }

        function SetRecibido(){
            var ObjRecibido = {
                'cbRecibido': (vm.Recibido == true)? 1:0,
                'contrato': vm.IdContrato,
                'idUsuario': $localStorage.currentUser.idUsuario
            };
            DocVendedorClienteFactory.GetModificaRecibido(ObjRecibido).then(function(data){
                GetDocumentosCliente();
            });
        }

        function GetNombre(Cli){
            var NC = '';
            if(Cli.SegundoNombre != null && Cli.Apellido_Materno != null){
                NC = Cli.Nombre + ' ' + Cli.SegundoNombre + ' ' + Cli.Apellido_Paterno + ' ' + Cli.Apellido_Materno;
            }else if(Cli.SegundoNombre == null && Cli.Apellido_Materno == null){
                NC = Cli.Nombre + ' ' + Cli.Apellido_Paterno;
            }else if(Cli.SegundoNombre != null && Cli.Apellido_Materno == null){
                NC = Cli.Nombre + ' ' + Cli.SegundoNombre + ' ' + Cli.Apellido_Paterno;
            }else if(Cli.SegundoNombre == null && Cli.Apellido_Materno != null){
                NC = Cli.Nombre + ' ' + Cli.Apellido_Paterno + ' ' + Cli.Apellido_Materno;
            }
            return NC;
        }

        function GetNumber(num){
            var res = [];
            for (var i = 0; i < num; i++) {
                res.push(i);
            }
            return res;
        }

        function ValidateStauts(){
            if(vm.StatusServicio.Clv_StatusNet != vm.ClvStatus){
                vm.DisFI = (vm.StatusServicio.Clv_StatusNet == 'I')? false : true;
                vm.DisFS = (vm.StatusServicio.Clv_StatusNet == 'S')? false : true;
                vm.DisFB = (vm.StatusServicio.Clv_StatusNet == 'B')? false : true;
                vm.DisFFA = (vm.StatusServicio.Clv_StatusNet == 'F')? false : true;
                
            }else{
                vm.DisFI = true;
                vm.DisFS = true;
                vm.DisFB = true;
                vm.DisFFA = true;
            }
        }

        function ValidateStautsAparato(){
            if(vm.StatusAparato.Clv_StatusNet != vm.StatusA){
                vm.DisFA_A = (vm.StatusAparato.Clv_StatusNet == 'I')? false : true;
                vm.DisFB_A = (vm.StatusAparato.Clv_StatusNet == 'B')? false : true;
            }else{
                vm.DisFA_A = true;
                vm.DisFB_A = true;
            }
        }

        function ResetEvidencia(){
            vm.Evidencia = null;
            vm.File = null;
            vm.TouchFile = false;
            angular.element("input[type='file']").val(null);
        }
        
        function SetTouch(){
            vm.TouchFile = true;
        }

        function SaveMovimientoSistema(Observaciones, Comando, IdClassLog){
            var objMovSist = {
                'Clv_usuario': $localStorage.currentUser.idUsuario, 
                'Modulo': 'home.catalogos', 
                'Submodulo': 'home.catalogos.clientes', 
                'Observaciones': Observaciones, 
                'Usuario': $localStorage.currentUser.usuario, 
                'Comando': (Comando != '')? JSON.stringify(Comando):'', 
                'Clv_afectada': vm.IdContrato,
                'IdClassLog': IdClassLog
            };
            CatalogosFactory.AddMovSist(objMovSist).then(function(data){
            });
        }

        function GetTipoTicketList(){
            ClientesFactory.GetMUESTRATIPOFACTURA_Historial().then(function(data){
                vm.TipoTicketList = data.GetMUESTRATIPOFACTURA_HistorialResult;
                vm.TipoTicket = vm.TipoTicketList[0];
                GetHistorialFacturaList('tipo');
            });
        }

        function GetHistorialFacturaList(op){
            if((op == 'tipo' && vm.TipoTicket != undefined) || (op == 'seriefolio' && (vm.Serie != undefined || vm.Folio != undefined)) || (op == 'fecha' && vm.FechaTicket != undefined)){
                var ObjFacHis = {
                    'op': op,
                    'Serie': (vm.Serie != undefined)? vm.Serie:'',
                    'Folio': (vm.Folio != undefined)? vm.Folio:0,
                    'Fecha': (vm.FechaTicket != undefined)? JToDate(vm.FechaTicket):'01/01/1900',
                    'Tipo': vm.TipoTicket.CLAVE,
                    'ContratoO': vm.CONTRATO
                };
                ClientesFactory.GetBuscaFacturasHistorial(ObjFacHis).then(function(data){
                    vm.FacturasHistorialList = data.GetBuscaFacturasHistorialResult;
                    vm.ViewFacturasHistorialList = (vm.FacturasHistorialList.length > 0)? true:false;
                    vm.FacturasHistorialList.forEach(function(value, key) {
                        value.FechaF = ToDateF(value.Fecha);
                        value.Import = GetDecimalN(value.Importe);
                    });
                    vm.Folio = null;
                    vm.Serie = null;
                    vm.FechaTicket = null;
                });
            }
        }

        function GetOrdSerList(OP){
            var ObjOrdSer = {
                'Clv_TipSer': 0,
                'Clv_Orden': 0,
                'ContratoO': vm.CONTRATO,
                'Nombre': vm.StatusOrdSer,
                'CALLE': '',
                'NUMERO': '',
                'OP': OP,
                'procauto': 0,
                'UsuarioEjecuto':'',
                'Concepto': '',
                'Fec_Eje':''
            };
            ClientesFactory.GetBUSCAORDSER(ObjOrdSer).then(function(data){
                console.log(data);
                vm.OrdSerList = data.GetBUSCAORDSERResult;
                vm.ShowOrdSerList = (vm.OrdSerList.length > 0)? true:false;
            });   
        }

        function GetTipoSerList(){
            ClientesFactory.GetMuestraTipSerPrincipal2().then(function(data){
                vm.TipoSerList = data.GetMuestraTipSerPrincipal2Result;
                vm.TipoSerR = vm.TipoSerList[0];
                GetReporteList();
            });
        }

        function GetReporteList(){
            if(vm.TipoSerR != undefined){
                var ObjQueja = {
                    'Clv_TipSer': vm.TipoSerR.Clv_TipSer,
                    'Clv_Queja': 0,
                    'ContratoO': vm.CONTRATO,
                    'Nombre': vm.StatusRep,
                    'CALLE': '',
                    'NUMERO': '',
                    'OP': 99,
                    'UsuarioEjecuto': '',
                    'Concepto': '',
                    'Fecha_Ejecucion': ''
                };
                ClientesFactory.GetBUSCAQUEJAS(ObjQueja).then(function(data){
                    console.log(data);
                    vm.ReporteList = data.GetBUSCAQUEJASResult;
                    vm.ViewReporteList = (vm.ReporteList.length > 0)? true:false;
                });
            }
        }

        function GetDecimalN(num) {
            return (num / 100).toFixed(2);
        }

        function ImprimeOrden(clv_orden) {
            var modalInstance = $uibModal.open({
              animation: true,
              ariaLabelledBy: "modal-title",
              ariaDescribedBy: "modal-body",
              templateUrl: "views/procesos/modalReporteOrdSer.html",
              controller: "modalReporteOrdeSerCtrl",
              controllerAs: "ctrl",
              backdrop: "static",
              keyboard: false,
              class: "modal-backdrop fade",
              size: "lg",
              resolve: {
                clv_orden: function() {
                  return clv_orden;
                }
              }
            });
          }

          function singleQueja(clave) {
			vm.animationsEnabled = true;
			var modalInstance = $uibModal.open({
				animation: vm.animationsEnabled,
				ariaLabelledBy: 'modal-title',
				ariaDescribedBy: 'modal-body',
				templateUrl: 'views/facturacion/modalSingleQueja.html',
				controller: 'ModalSingleQuejaCtrl',
				controllerAs: 'ctrl',
				backdrop: 'static',
				keyboard: false,
				size: 'lg',
				windowClass: 'app-modal-window',
				resolve: {
					clave: function() {
						return clave;
					}
				}
			});
		}

        function dameFactura(factura) {
			var modalInstance = $uibModal.open({
				animation: true,
				ariaLabelledBy: 'modal-title',
				ariaDescribedBy: 'modal-body',
				templateUrl: 'views/facturacion/modalSingleTicket.html',
				controller: 'ModalSingleTicketCtrl',
				controllerAs: 'ctrl',
				backdrop: 'static',
				keyboard: false,
				size: 'sm',
				resolve: {
					factura: function() {
						return factura;
					},
					imprimir: function() {
						return false;
					}
				}
			});
        }
        
        function MuestraAgenda(Clv_Orden) {
            var options = {
                'clv_queja_orden': Clv_Orden,
                'opcion': 1
            };
            var modalInstance = $uibModal.open({
              animation: vm.animationsEnabled,
              ariaLabelledBy: 'modal-title',
              ariaDescribedBy: 'modal-body',
              templateUrl: 'views/procesos/ModalAgendaQueja.html',
              controller: 'ModalAgendaQuejaCtrl',
              controllerAs: 'ctrl',
              backdrop: 'static',
              keyboard: false,
              size: 'sm',
              resolve: {
                options: function () {
                  return options;
                }
              }
            });
          }
        
        var vm = this;
        vm.soycontrolTotal=($localStorage.currentUser.tipoUsuario==40)? true:false;
        vm.IdContrato = $stateParams.id;
        vm.token=$stateParams.token;
        vm.Title = 'Editar Cliente - ';
        vm.SetForm = 1;
        vm.ShowAccord = true;
        vm.BlockInput = true;
        vm.DisableInput = false;
        vm.DivServicio = false;
        vm.DivAparato = false;
        vm.ShowServicios = false;
        vm.SinDescuento = true;
        vm.ConDescuento = false;
        vm.DisFC = true;
        vm.DisFI = true;
        vm.DisFS = true;
        vm.DisFB = true;
        vm.DisFFA = true;
        vm.DisFUP = true;
        vm.DisFA_A = true;
        vm.DisFB_A = true;
        vm.ShowTipServ1 = false;
        vm.View = false;
        vm.TouchFile = false;
        vm.TBtnSaveSP = '';
        vm.TBtnDeleteSP = '';
        vm.tipoUsuario = $localStorage.currentUser.tipoUsuario
        vm.clv_usuario = $localStorage.currentUser
        vm.ShowBtnAddPaq = false;
        vm.SaveNap = false;
        vm.SaveTap = false;
        vm.maskOptions = {
            maskDefinitions:{'A': /[a-zA-Z]/, '9': /[0-9]/, '*': /[a-zA-Z0-9]/},
            clearOnBlur: false,
            clearOnBlurPlaceholder: true,
            eventsToHandle:['input', 'keyup', 'click'],
            addDefaultPlaceholder: true,
            escChar: '\\',
            allowInvalidValue: false
        };
        vm.StatusOrdSer = 'P';
        vm.StatusRep = 'P';
        vm.OPOS = 2;
        vm.AddDatosPersonales = AddDatosPersonales;
        vm.GetCiudadMunicipio = GetCiudadMunicipio;
        vm.GetLocalidad = GetLocalidad;
        vm.GetColonia = GetColonia;
        vm.GetCalle = GetCalle;
        vm.ValidateFechaVen = ValidateFechaVen;
        vm.AddDatosFiscales = AddDatosFiscales;
        vm.AddDatosBancarios = AddDatosBancarios;
        vm.OpenAddRefPersonal = OpenAddRefPersonal;
        vm.OpenEditRefPersonal = OpenEditRefPersonal;
        vm.OpenDeleteRefPersonal = OpenDeleteRefPersonal;
        vm.AddObservaciones = AddObservaciones;
        vm.AddNotas = AddNotas;
        vm.DetalleConcepto = DetalleConcepto;
        vm.OpenAddServicioCliente = OpenAddServicioCliente;
        vm.UpdateServicioCliente = UpdateServicioCliente;
        vm.UpdateAparatoCliente = UpdateAparatoCliente;
        vm.AddDescuentoServicio = AddDescuentoServicio;
        vm.GetNumber = GetNumber;
        vm.ValidateStauts = ValidateStauts;
        vm.ValidateStautsAparato = ValidateStautsAparato;
        vm.ResetEvidencia = ResetEvidencia;
        vm.SetTouch = SetTouch;
        vm.SaveDocumentoCliente = SaveDocumentoCliente;
        vm.SetRevisado = SetRevisado;
        vm.SetRecibido = SetRecibido;
        vm.GetDocumentoCliente = GetDocumentoCliente;
        vm.DeleteServicioCliente = DeleteServicioCliente;
        vm.OpenAddPaqueteAdic = OpenAddPaqueteAdic;
        vm.GetHistorialFacturaList = GetHistorialFacturaList;
        vm.GetOrdSerList = GetOrdSerList;
        vm.GetReporteList = GetReporteList;
        vm.OpenCobros = OpenCobros;
        initData();
        vm.dameFactura=dameFactura;
        vm.GetReporteContrato=GetReporteContrato;
        vm.ImprimeOrden=ImprimeOrden;
        vm.MuestraAgenda=MuestraAgenda;
        vm.OpenEntreCalles = OpenEntreCalles;
        vm.ResetEC = ResetEC;
        vm.ShowContAnt = false;
        vm.nuevocliente =false;
        vm.singleQueja=singleQueja;
        vm.showOptions=true;
        vm.token = false;
        vm.DisTipoSuscriptor = (vm.soycontrolTotal == true)? false:true;
    });