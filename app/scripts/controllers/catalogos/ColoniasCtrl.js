'use strict';

angular
    .module('softvApp')
    .controller('ColoniasCtrl', function($uibModal, CatalogosFactory, $localStorage){
        
        function initData(){
            GetColoniaList();
        }

        function GetColoniaList(){
            var ObjColList = {
                'IdCompania': $localStorage.currentUser.idUsuario
            };
            CatalogosFactory.GetColonias_NewList(ObjColList).then(function(data){
                vm.ColoniaList = data.GetColonias_NewListResult;
                if (vm.ColoniaList.length == 0) {
					vm.SinRegistros = true;
					vm.ConRegistros = false;
				} else {
					vm.SinRegistros = false;
					vm.ConRegistros = true;
				}
            });
        }

        var vm = this;
        initData();
        
    });