'use strict';

angular
    .module('softvApp')
    .controller('ModalCalleFormAddCtrl', function(CatalogosFactory, $uibModal, $uibModalInstance, ngNotify, $state,logFactory){

        function initData(){
            CatalogosFactory.GetMuestraEstados_RelColList().then(function(data){
                vm.EstadoList = data.GetMuestraEstados_RelColListResult;
            });
        }

        function SaveCalle(){
            var objValidaNombreCalle = {
                'nombre': vm.Calle,
                'clv_calle': 0
            };
            CatalogosFactory.AddValidaNombreCalle(objValidaNombreCalle).then(function(data){
                if(data.AddValidaNombreCalleResult == 0){
                    var objCalles_New = {
                        'NOMBRE': vm.Calle
                    };
                    CatalogosFactory.AddCalles_New(objCalles_New).then(function(data){
                        vm.Clv_Calle = data.AddCalles_NewResult;
                        if(vm.Clv_Calle > 0){
                            LogSistema(objCalles_New, 'Se registró nueva calle', 1);
                            ngNotify.set('CORRECTO, Se añadió una Calle Nueva, ahora puedes comenzar a agregar Relaciones', 'success');
                            vm.Disable = false;
                            vm.DisableAdd = true;
                            GetRelCalle();
                        }else{
                            ngNotify.set('ERROR, Al añadir una Calle Nueva.', 'warn');
                        }
                    });
                }else if(data.AddValidaNombreCalleResult == 1){
                    ngNotify.set('ERROR, Ya existe una Calle con el mismo nombre.', 'warn');
                }
            });
        }

        function GetRelCalle(){
            CatalogosFactory.GetRelColoniasCalles_NewList(vm.Clv_Calle).then(function(data){
                vm.RelCalleList = data.GetRelColoniasCalles_NewListResult;
            });
        }

        function AddRelCalle(){
            var objRelColoniasCalles_New = {
                'clv_estado': vm.Estado.Clv_Estado,
                'clv_ciudad': vm.Municipio.Clv_Ciudad,
                'clv_localidad': vm.Localidad.Clv_Localidad,
                'clv_colonia': vm.Colonia.CLV_COLONIA,
                'clv_calle': vm.Clv_Calle
            };
            CatalogosFactory.AddRelColoniasCalles_New(objRelColoniasCalles_New).then(function(data){
                if(data.AddRelColoniasCalles_NewResult == 0){
                    ngNotify.set('CORRECTO, Se guardó la Relación con la Colonia.', 'success');
                    GetRelCalle();
                    ClearInput();
                    LogSistema(objRelColoniasCalles_New, 'Se agregó relación calle - colonia', 37);
                }else{
                    ngNotify.set('ERROR, Al guardar la Relación con la Colonia, posiblemente esta relación ya existe para esta calle.', 'warn');
                    GetRelCalle();
                    ClearInput();
                }
            });
        }

        function DeleteRelCalle(RelCalle){
            var ObjRelCalle = {
                'clv_Calle': vm.Clv_Calle,
                'clv_colonia': RelCalle.clv_colonia
            };
            CatalogosFactory.GetValidaEliminarRelColoniaCalle(ObjRelCalle).then(function(data){
                if(data.GetValidaEliminarRelColoniaCalleResult.Msj == null){
                    var ObjRelCalleD = {
                        'clv_estado': RelCalle.clv_estado,
                        'clv_ciudad': RelCalle.clv_ciudad,
                        'clv_localidad': RelCalle.clv_localidad,
                        'clv_colonia': RelCalle.clv_colonia,
                        'clv_calle': vm.Clv_Calle
                    };
                    CatalogosFactory.DeleteRelColoniasCalles_New(ObjRelCalleD).then(function(data){
                        if(data.DeleteRelColoniasCalles_NewResult == -1){
                            ngNotify.set('CORRECTO, Se eliminó la Relación con la Colonia.', 'success');
                            LogSistema(ObjRelCalleD, 'Se eliminó relación calle - colonia', 38);
                            GetRelCalle();
                        }else{
                            ngNotify.set('ERROR, Al eliminar la Relación con la Colonia.', 'warn');
                            GetRelCalle();
                        }
                    });
                }else{
                    ngNotify.set('ERROR, Al eliminar la Relación con la Colonia, posiblemente puede estar relacionada con uno o varios clientes.', 'warn');
                }
            });
        }

        function GetCiudadList(){
            if(vm.Estado != undefined){  
                CatalogosFactory.GetMuestraCdsEdo_RelColoniaList(vm.Estado.Clv_Estado).then(function(data){
                    vm.CiudadList = data.GetMuestraCdsEdo_RelColoniaListResult;
                });
            }else{
                vm.CiudadList = '';
            }
            vm.LocalidadList = '';
        }
        
        function GetLocalidadList(){
            if(vm.Municipio != undefined){
                CatalogosFactory.GetMuestraLocalidades_CalleList(vm.Municipio.Clv_Ciudad).then(function(data){
                    vm.LocalidadList = data.GetMuestraLocalidades_CalleListResult;
                    vm.ColoniaList = null;
                });
            }else{
                vm.LocalidadList = null;
                vm.ColoniaList = null;
            }
        }
        
        function GetColoniaList(){
             if(vm.Localidad != undefined){
                CatalogosFactory.GetMuestraColonias_CalleList(vm.Localidad.Clv_Localidad).then(function(data){
                    vm.ColoniaList = data.GetMuestraColonias_CalleListResult;
                });
            }else{
                vm.ColoniaList = null;
            }
        }

        function ClearInput(){
            vm.Estado = undefined;
            vm.CiudadList = null;
            vm.LocalidadList = null;
            vm.ColoniaList = null;
        }

        function LogSistema(Comando, Obs, IdClassLog){
            var log={
                'Modulo':'home.catalogos',
                'Submodulo':'home.catalogos.calles',
                'Observaciones': Obs,
                'Comando': JSON.stringify(Comando),
                'Clv_afectada': vm.Clv_Calle,
                'IdClassLog':IdClassLog
            };
            logFactory.AddMovSist(log).then(function(result){ console.log('add'); });
        }

        function cancel() {
            $uibModalInstance.close();
        }

        var vm = this;
        vm.Titulo = 'Nueva Calle';
        vm.Icono = 'fa fa-plus';
        vm.Disable = true;
        vm.DisableAdd = false;
        vm.View = false;
        vm.SaveCalle = SaveCalle;
        vm.GetCiudadList = GetCiudadList;
        vm.GetColoniaList = GetColoniaList;
        vm.GetLocalidadList = GetLocalidadList;
        vm.AddRelCalle = AddRelCalle;
        vm.DeleteRelCalle = DeleteRelCalle;
        vm.cancel = cancel;
        initData();

    });