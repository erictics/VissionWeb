'use strict';

angular
    .module('softvApp')
    .controller('ModalCalleFormUpdateCtrl', function(CatalogosFactory, logFactory, $uibModalInstance, ngNotify, $state, IdCalle){

        function initData(){
            CatalogosFactory.GetMuestraEstados_RelColList().then(function(data){
                vm.EstadoList = data.GetMuestraEstados_RelColListResult;
            });
            GetCalle();
        }

        function GetCalle(){
            CatalogosFactory.GetDeepCalles_New(IdCalle).then(function(data){
                var Calle = data.GetDeepCalles_NewResult;
                vm.IdCalle = Calle.Clv_Calle;
                vm.Calle = Calle.NOMBRE;
                vm.Titulo = 'Editar Registro - '+vm.Calle;
                GetRelCalle();
            });
        }

        function GetCiudadList(){
            if(vm.Estado != undefined){  
                CatalogosFactory.GetMuestraCdsEdo_RelColoniaList(vm.Estado.Clv_Estado).then(function(data){
                    vm.CiudadList = data.GetMuestraCdsEdo_RelColoniaListResult;
                });
            }else{
                vm.CiudadList = '';
            }
            vm.LocalidadList = '';
        }
        
        function GetLocalidadList(){
            if(vm.Municipio != undefined){
                CatalogosFactory.GetMuestraLocalidades_CalleList(vm.Municipio.Clv_Ciudad).then(function(data){
                    vm.LocalidadList = data.GetMuestraLocalidades_CalleListResult;
                    vm.ColoniaList = null;
                });
            }else{
                vm.LocalidadList = null;
                vm.ColoniaList = null;
            }
        }
        
        function GetColoniaList(){
             if(vm.Localidad != undefined){
                CatalogosFactory.GetMuestraColonias_CalleList(vm.Localidad.Clv_Localidad).then(function(data){
                    vm.ColoniaList = data.GetMuestraColonias_CalleListResult;
                });
            }else{
                vm.ColoniaList = null;
            }
        }

        function GetRelCalle(){
            CatalogosFactory.GetRelColoniasCalles_NewList(vm.IdCalle).then(function(data){
                vm.RelCalleList = data.GetRelColoniasCalles_NewListResult;
            });
        }

        function AddRelCalle(){
            var objRelColoniasCalles_New = {
                'clv_estado': vm.Estado.Clv_Estado,
                'clv_ciudad': vm.Municipio.Clv_Ciudad,
                'clv_localidad': vm.Localidad.Clv_Localidad,
                'clv_colonia': vm.Colonia.CLV_COLONIA,
                'clv_calle': vm.IdCalle
            };
            CatalogosFactory.AddRelColoniasCalles_New(objRelColoniasCalles_New).then(function(data){
                if(data.AddRelColoniasCalles_NewResult == 0){
                    ngNotify.set('CORRECTO, Se guardó la Relación con la Colonia.', 'success');
                    var log={
                        'Modulo':'home.catalogos',
                        'Submodulo':'home.catalogos.calles',
                        'Observaciones':'Se agrego relación  calle-colonia ',
                        'Comando':JSON.stringify(objRelColoniasCalles_New),
                        'Clv_afectada':vm.IdCalle,
                        'IdClassLog':37
                    };        
                    logFactory.AddMovSist(log).then(function(result){ console.log('add'); });
                    GetRelCalle();
                    ClearInput();
                }else{
                    ngNotify.set('ERROR, Al guardar la Relación con la Colonia, posiblemente esta relación ya existe para esta calle.', 'warn');
                    GetRelCalle();
                    ClearInput();
                }
            });
        }

        function DeleteRelCalle(RelCalle){
            var ObjRelCalle = {
                'clv_Calle': vm.IdCalle,
                'clv_colonia': RelCalle.clv_colonia
            };
            CatalogosFactory.GetValidaEliminarRelColoniaCalle(ObjRelCalle).then(function(data){
                if(data.GetValidaEliminarRelColoniaCalleResult.Msj == null){
                    var ObjRelCalleD = {
                        'clv_estado': RelCalle.clv_estado,
                        'clv_ciudad': RelCalle.clv_ciudad,
                        'clv_localidad': RelCalle.clv_localidad,
                        'clv_colonia': RelCalle.clv_colonia,
                        'clv_calle': vm.IdCalle
                    };
                    CatalogosFactory.DeleteRelColoniasCalles_New(ObjRelCalleD).then(function(data){
                        if(data.DeleteRelColoniasCalles_NewResult == -1){
                            ngNotify.set('CORRECTO, Se Eliminó la Relación con la Colonia.', 'success');
                            var log={
                                'Modulo':'home.catalogos',
                                'Submodulo':'home.catalogos.calles',
                                'Observaciones':'Se eliminó relación  calle-colonia ',
                                'Comando':JSON.stringify(ObjRelCalleD),
                                'Clv_afectada':vm.IdCalle,
                                'IdClassLog': 38
                            };        
                            logFactory.AddMovSist(log).then(function(result){ console.log('add'); });
                            GetRelCalle();
                        }else{
                            ngNotify.set('ERROR, Al eliminar la Relación con la Colonia.', 'warn');
                            GetRelCalle();
                        }
                    });
                }else{
                    ngNotify.set('ERROR, Al eliminar la Relación con la Colonia, posiblemente puede estar relacionada con uno o varios clientes.', 'warn');
                }
            });
        }

        function SaveCalle(){
            var objValidaNombreCalle = {
                'nombre': vm.Calle,
                'clv_calle': vm.IdCalle
            };
            CatalogosFactory.AddValidaNombreCalle(objValidaNombreCalle).then(function(data){
                if(data.AddValidaNombreCalleResult == 0){
                    var objCalles_New = {
                        'Clv_Calle': vm.IdCalle,
                        'NOMBRE': vm.Calle
                    };
                    CatalogosFactory.UpdateCalles_New(objCalles_New).then(function(data){
                        if(data.UpdateCalles_NewResult = -1){
                            var log={
                                'Modulo':'home.catalogos',
                                'Submodulo':'home.catalogos.calles',
                                'Observaciones':'Se modificó calle',
                                'Comando':JSON.stringify(objCalles_New),
                                'Clv_afectada':vm.IdCalle,
                                'IdClassLog':2
                            };        
                            logFactory.AddMovSist(log).then(function(result){
                                ngNotify.set('CORRECTO, Se guardó la Calle.', 'success');
                                GetCalle();
                                cancel();
                            });

                            
                        }else{
                            ngNotify.set('ERROR, Al guardar la Calle.', 'warn');
                            GetCalle();
                            
                        }
                    });
                }else if(data.AddValidaNombreCalleResult == 1){
                    ngNotify.set('ERROR, Ya existe una Calle con el mismo nombre.', 'warn');
                }
                
            });
        }

        function ClearInput(){
            vm.Estado = undefined;
            vm.CiudadList = null;
            vm.LocalidadList = null;
            vm.ColoniaList = null;
        }

        function cancel() {
            $uibModalInstance.close();
        }

        var vm = this;
        
        vm.Icono = 'fa fa-pencil-square-o';
        vm.Show = false;
        vm.View = false;
        vm.GetCiudadList = GetCiudadList;
        vm.GetLocalidadList = GetLocalidadList;
        vm.GetColoniaList = GetColoniaList;
        vm.AddRelCalle = AddRelCalle;
        vm.DeleteRelCalle = DeleteRelCalle;
        vm.SaveCalle = SaveCalle;
        vm.cancel = cancel;
        initData();

    });