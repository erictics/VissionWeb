'use strict';
angular
  .module('softvApp')
  .controller('CajaEditarCtrl', function (CatalogosFactory, atencionFactory, ngNotify, $state, $stateParams, logFactory) {

    function init() {
      CatalogosFactory.GetDeepCatalogoCajas($stateParams.id)
        .then(function (data) { 
          var res = data.GetDeepCatalogoCajasResult;
          vm.Clave = res.Clave;
          vm.descripcion = res.Descripcion;
          vm.ticket = res.ImprimeTickets;
          vm.ip = res.IpMaquina;
          vm.idcompania = res.idcompania;
          vm.Clv_sucursal = res.Clv_sucursal;
          vm.impresoratermica = (res.impresoratermica === 1) ? true : false;
          atencionFactory.getPlazas().then(function (data) {            
            vm.plazas = data.GetMuestra_Compania_RelUsuarioListResult;
            vm.plazas.forEach(function (item) {
              if (item.id_compania === vm.idcompania) {
                vm.plaza = item;
                GetSucursales( vm.Clv_sucursal);
              }
            });
          });
        });
      GetIPList();
    }

    function GetIPList(){
      var Parametros = {
        'Clave': 0,
        'Descripcion': '',
        'OP': 2,
        'idcompania': 0
      };
      CatalogosFactory.GetCatalogoCajasList(Parametros).then(function (data) {
        vm.CajaList = data.GetCatalogoCajasListResult;
      });
    }

    function ValidaIPMaquina(){
      var result = true
      if(vm.CajaList != undefined && vm.ip != undefined){
        for(var i = 0; vm.CajaList.length > i; i++){
          if(vm.CajaList[i].IpMaquina.toUpperCase() == vm.ip.toUpperCase() && vm.CajaList[i].Clave != vm.Clave){
            result = false
            break;
          }
        }
        return result;
      }
    }

    function GetSucursales( sucursal) {
      CatalogosFactory.GetMUESTRASUCURSALES2(vm.plaza.id_compania)
        .then(function (result) {
          vm.sucursales = result.GetMUESTRASUCURSALES2Result;
          if (sucursal > 0) {
            vm.sucursales.forEach(function (item) {
              if (item.Clv_Sucursal === sucursal) {
                vm.sucursal = item;
              }
            });
          }         
        });
    }

    function SaveCaja() {
      var ObjIp = {
        'IpMAquina': vm.ip,
        'Clave': vm.Clave,
        'Op': 2
      };
      CatalogosFactory.GetValidaIPMaquina(ObjIp).then(function(data){
        if(data.GetValidaIPMaquinaResult.Resultado == true){
          var Parametros = {
            'Clave': vm.IdCaja,
            'Clv_sucursal': vm.sucursal.Clv_Sucursal,
            'IpMaquina': vm.ip,
            'Descripcion': vm.descripcion,
            'ImprimeTickets': vm.ticket,
            'facnormal': (vm.factura === 'n') ? true : false,
            'facticket': (vm.factura === 't') ? true : false,
            'impresoratermica': (vm.termica===true)? 1:0
          };
          CatalogosFactory.UpdateCatalogoCajas(Parametros)
            .then(function (result) {    
              var log={
                'Modulo':'home.catalogos',
                'Submodulo':'home.catalogos.editarcaja',
                'Observaciones':'Se edito una Caja ',
                'Comando':JSON.stringify(Parametros),
                'Clv_afectada':vm.IdCaja,
                'IdClassLog':2
              };
              logFactory.AddMovSist(log).then(function(result){ console.log('add'); });

              ngNotify.set('La caja se ha editado guardado correctamente', 'success');
              $state.go('home.catalogos.cajas');
             });
            }else{
              ngNotify.set('Error, La Ip de la Maquina que ingresó, ya existe', 'warn');
           }
       });
      /**/
    }

    var vm = this;
    vm.IdCaja = $stateParams.id;
    vm.Titulo = 'Editar Caja #' + $stateParams.id;
    vm.block = false;
    init();
    vm.ValidaIPMaquina = ValidaIPMaquina;
    vm.GetSucursales=GetSucursales;
    vm.SaveCaja=SaveCaja;
  });
