'use strict';

angular
    .module('softvApp')
    .controller('ColoniaFormAddCtrl', function(CatalogosFactory, ngNotify, $state, $uibModal, logFactory){

        function initData(){
            CatalogosFactory.GetTipo_Colonias1_NewList().then(function(data){
                vm.TipoColoniaList = data.GetTipo_Colonias1_NewListResult;
            });
            CatalogosFactory.GetMuestraEstados_RelColList().then(function(data){
                vm.EstadoList = data.GetMuestraEstados_RelColListResult;
            });
        }

        function SaveColonia(){
            var objValidaNombreColonia = {
                'nombre': vm.Colonia,
                'mismoNombre': 0,
                'clv_colonia': 0
            };
            CatalogosFactory.AddValidaNombreColonia(objValidaNombreColonia).then(function(data){

                if(data.AddValidaNombreColoniaResult == 0){
                    var objColonias_New = {
                        'Clv_Tipo': (vm.TipoColonia != undefined || vm.TipoColonia != null)? vm.TipoColonia.Clave:0,
                        'FechaEntrega': (vm.FechaEntrega != null)? vm.FechaEntrega:'',
                        'Nombre': vm.Colonia,
                        'Op': 0
                    };
                    CatalogosFactory.AddColonias_New(objColonias_New).then(function(data){
                        vm.Clv_Colonia = data.AddColonias_NewResult;
                        if(vm.Clv_Colonia > 0){
                            ngNotify.set('CORRECTO, Se añadió una Colonia Nueva.', 'success');
                            vm.Disable = false;
                            vm.SaveBtnS = true;
                            LogSistema(objColonias_New, 'Se agregó una colonia nueva', 1);
                        }else{
                            ngNotify.set('ERROR, Al añadir una Colonia Nueva.', 'warn');
                        }
                    });
                }else if(data.AddValidaNombreColoniaResult == 1){
                    ngNotify.set('ERROR, Ya existe una Colonia con el mismo nombre.', 'warn');
                }
            });
        }

        function GetCiudadList(){
            if(vm.Estado != undefined){  
                CatalogosFactory.GetMuestraCdsEdo_RelColoniaList(vm.Estado.Clv_Estado).then(function(data){
                    vm.CiudadList = data.GetMuestraCdsEdo_RelColoniaListResult;
                });
            }else{
                vm.CiudadList = '';
            }
            vm.LocalidadList = '';
        }

        function GetLocalidadList(){
            if(vm.Ciudad != undefined){
                var ObjLocalidadList = {
                    'clv_colonia': vm.Clv_Colonia,
                    'clv_ciudad': vm.Ciudad.Clv_Ciudad
                };
                CatalogosFactory.GetMuestraLoc_RelColoniaList(ObjLocalidadList).then(function(data){
                    vm.LocalidadList = data.GetMuestraLoc_RelColoniaListResult;
                });
            }else{
                vm.LocalidadList = '';
            }
        }

        function GetRelLocColList(){
            CatalogosFactory.GetInsertaRelColoniaLocalidadList(vm.Clv_Colonia).then(function(data){
                vm.RelColLocList = data.GetInsertaRelColoniaLocalidadListResult;
            });
        }

        function AddRelEstCiuLocCol(){
            var objInsertaRelColoniaLocalidad = {
                'Clv_Colonia': vm.Clv_Colonia,
                'Clv_Localidad': vm.Localidad.Clv_Localidad,
                'Clv_Ciudad': vm.Ciudad.Clv_Ciudad,
                'CodigoPostal': vm.CPRel
            };
            CatalogosFactory.AddInsertaRelColoniaLocalidad(objInsertaRelColoniaLocalidad).then(function(data){
                if(data.AddInsertaRelColoniaLocalidadResult == -1){
                    ngNotify.set('CORRECTO, Se agregó la Relación.', 'success');
                    GetRelLocColList();
                    GetCiudadList();
                    LogSistema(objInsertaRelColoniaLocalidad, 'Se agregó relación localidad-colonia', 31);
                    vm.ObjRelCol = null;
                    vm.ShowRel = false;
                    vm.CPRel = '';
                }else{
                    ngNotify.set('ERROR, Al agregar la Relación.', 'warn');
                    GetRelLocColList();
                    vm.CPRel = '';
                    vm.ObjRelCol = null;
                    vm.ShowRel = false;
                }
            });
        }

        function DeleteRelEstCiuLocCol(ObjRelLocCol){
            var objValidaCVELOCCOL = {
                'clv_localidad': ObjRelLocCol.Clv_Localidad,
                'clv_colonia': ObjRelLocCol.Clv_Colonia
            };
            CatalogosFactory.AddValidaCVELOCCOL(objValidaCVELOCCOL).then(function(data){
                if(data.AddValidaCVELOCCOLResult == 1){
                    var ObjRelLocColD = {
                        'Clv_Colonia': ObjRelLocCol.Clv_Colonia,
                        'Clv_Localidad': ObjRelLocCol.Clv_Localidad,
                        'Clv_Ciudad': ObjRelLocCol.Clv_Ciudad,
                        'CodigoPostal': ObjRelLocCol.CodigoPostal
                    };
                    CatalogosFactory.DeleteInsertaRelColoniaLocalidad(ObjRelLocCol).then(function(data){
                        if(data.DeleteInsertaRelColoniaLocalidadResult == -1){
                            ngNotify.set('CORRECTO, Se eliminó la Relación.', 'success');
                            GetRelLocColList();
                            GetCiudadList();
                            LogSistema(ObjRelLocCol, 'Se eliminó relación localidad-colonia', 32);
                            vm.CPRel = '';
                            vm.ObjRelCol = null;
                            vm.ShowRel = false;
                        }else{
                            ngNotify.set('ERROR, Al eliminar la Relación.', 'warn');
                            GetRelLocColList();
                            vm.CPRel = '';
                            vm.ObjRelCol = null;
                            vm.ShowRel = false;
                        }
                    });
                }else if(data.AddValidaCVELOCCOLResult == 0){
                    ngNotify.set('ERROR, Al eliminar la Relación, posiblemente puede estar relacionada con uno o varios clientes.', 'warn');
                }
            });
        }

        function OpenRelServicios(ObjRelColonia){
            var ObjRelColonia = ObjRelColonia;
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'views/catalogos/ModalRelColoniaServicios.html',
                controller: 'ModalRelColoniaServiciosCtrl',
                controllerAs: 'ctrl',
                backdrop: 'static',
                keyboard: false,
                class: 'modal-backdrop fade',
                size: 'lg',
                resolve: {
                    ObjRelColonia: function () {
                        return ObjRelColonia;
                    }
                }
            });
        }
        function LogSistema(Comando, Obs, IdClassLog){
            var Obj = {
                'Modulo': 'home.catalogos',
                'Submodulo': 'home.catalogos.colonias',
                'Observaciones': Obs,
                'Comando': JSON.stringify(Comando),
                'Clv_afectada': 0,
                'IdClassLog': IdClassLog
            };
            logFactory.AddMovSist(Obj).then(function(data){});
        }

        var vm = this;
        vm.Titulo = 'Nueva Colonia';
        vm.Disable = true;
        vm.View = false;
        vm.SaveBtnS = true; 
        vm.SaveColonia = SaveColonia;
        vm.GetCiudadList = GetCiudadList;
        vm.GetLocalidadList = GetLocalidadList;
        vm.AddRelEstCiuLocCol = AddRelEstCiuLocCol;
        vm.DeleteRelEstCiuLocCol = DeleteRelEstCiuLocCol;
        vm.OpenRelServicios = OpenRelServicios;
        initData();

    });