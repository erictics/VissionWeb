'use strict';

angular
    .module('softvApp')
    .controller('ModalBancoFormAddCtrl', function(CatalogosFactory, $uibModal, $uibModalInstance, ngNotify, $state, logFactory){
    
        function SaveBanco(){
             var objBanco = {
                'Nombre': vm.Banco,
                'ClaveRel': '',
                'ClaveTxt': vm.Clave
             };
             CatalogosFactory.AddBanco(objBanco).then(function(data){
                if(data.AddBancoResult > 0){
                    var Obj = {
                        'Modulo': 'home.catalogos',
                        'Submodulo': 'home.catalogos.bancos',
                        'Observaciones': 'Se agregó un banco nuevo',
                        'Comando': JSON.stringify(objBanco),
                        'Clv_afectada': 0,
                        'IdClassLog':1
                    };
                    logFactory.AddMovSist(Obj).then(function(data){
                        ngNotify.set('CORRECTO, se añadió un banco nuevo.', 'success');
                        cancel();
                    });
                }else{
                    ngNotify.set('ERROR, al añadir un banco nuevo.', 'warn');
                }
             });
        }

        function cancel() {
            $uibModalInstance.close();
        }

        var vm = this;
        vm.Titulo = 'Nuevo Banco';
        vm.Icono = 'fa fa-plus';
        vm.InpDes = false;
        vm.SaveBanco = SaveBanco;
        vm.cancel = cancel;

    });