'use strict';

angular
    .module('softvApp')
    .controller('ModalCobroCtrl', function(ClienteServicioFactory, $localStorage, $uibModalInstance, $uibModal, ngNotify, $state, logFactory, ObjCobro){

        function initData(){
            GetCobros();
        }

        function GetCobros(){
            var Obj = {
                'Contrato': ObjCobro.IdContrato,
                'Clv_UnicaNet': ObjCobro.Clv_UnicaNet
            };
            ClienteServicioFactory.GetCobroHotelesByContratoClvUnicanet(Obj).then(function(data){
                var CobroResult = data.GetCobroHotelesByContratoClvUnicanetResult;
                if(CobroResult.Clv_UnicaNet != null){
                    vm.Contratacion = CobroResult.Contratacion;
                    vm.Mensualidad = CobroResult.Mensualidad;
                    vm.Renta = CobroResult.Renta;
                    vm.BtnDelete = true;
                }
            });
        }

        function Save(){
            if(vm.Mensualidad > 0 || vm.Contratacion > 0 || vm.Renta > 0){
                var Obj = {
                    'Clv_servicio': ObjCobro.Clv_Servicio,
                    'Clv_tipser': ObjCobro.Clv_TipSer,
                    'Contratacion': (vm.Contratacion != undefined && vm.Contratacion != null && vm.Contratacion != '')? vm.Contratacion:0,
                    'Mensualidad': (vm.Mensualidad != undefined && vm.Mensualidad != null && vm.Mensualidad != '')?vm.Mensualidad:0,
                    'Contrato': ObjCobro.IdContrato,
                    'Clv_UnicaNet': ObjCobro.Clv_UnicaNet,
                    'Renta': (vm.InpRenta == true && vm.Renta != undefined && vm.Renta != null && vm.Renta != '')? vm.Renta:0
                };
                ClienteServicioFactory.GetSaveCobroHoteles(Obj).then(function(data){
                    if(data.GetSaveCobroHotelesResult.result != 3){
                        ngNotify.set('CORRECTO, Se Guardó Cobros a Hoteles.', 'success');
                        cancel();
                    }else{
                        ngNotify.set('ERROR, No se puede asignar Cobros de Hoteles, el tipo de suscriptor no concuerda.', 'warn');
                        cancel();
                    }
                });
            }else{
                ngNotify.set('ERROR, ingrese alguno de los conceptos.', 'warn');
            }
        }

        function Delete(){
            var Obj = {
                'Contrato': ObjCobro.IdContrato,
                'Clv_UnicaNet': ObjCobro.Clv_UnicaNet
            };
            ClienteServicioFactory.GetDeleteCobroHoteles(Obj).then(function(data){
                ngNotify.set('CORRECTO, Se Eliminó Cobros a Hoteles.', 'success');
                cancel();
            })
        }

        function cancel() {
            $uibModalInstance.close();
        }

        var vm = this;
        vm.Edit = ObjCobro.Edit;
        vm.InpRenta = (ObjCobro.Clv_TipSer == 2 || ObjCobro.Clv_TipSer == 3)? true:false;
        vm.BtnDelete = false;
        vm.Contratacion = 0;
        vm.Mensualidad = 0;
        vm.Renta = 0;
        vm.Save = Save;
        vm.Delete = Delete;
        vm.cancel = cancel;
        initData();
    });