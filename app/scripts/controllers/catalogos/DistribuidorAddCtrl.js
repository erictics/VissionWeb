'use strict';

angular
  .module('softvApp')
  .controller('DistribuidorAddCtrl', function (CatalogosFactory, distribuidorFactory, ngNotify, logFactory, $state) {

    function SaveDistribuidor() {      
      var Parametros = {
        'Clv_Plaza': 0,
        'Nombre': vm.Nombre,
        'RFC': vm.RFC,
        'Calle': vm.Calle,
        'NumEx': vm.NumExt,
        'NumIn': vm.NumInt,
        'Colonia': vm.Colonia,
        'CP': vm.CP,
        'Localidad': vm.Localidad,
        'Estado': vm.Estado,
        'EntreCalles': vm.Calles,
        'Telefono': vm.Telefono,
        'Fax': '',
        'Email': vm.Email,
        'Municipio': vm.Municipio,
        'Pais': vm.Pais,
        'lada1': '',
        'lada2': '',
        'Telefono2': vm.Telefono2,
        'NombreContacto': vm.nombrecont,
        'TiposDistribuidor': 0,
        'TelefonoContacto': vm.telefonocont,
        'celularContacto': vm.celularcont,
        'emailContacto': vm.emailcont,
        'responsablecomercial': vm.respcomcont,
        'responsableOperaciones': vm.respopcont,
        'responsableAtencion': vm.resatencont,
        'Nombrecomercial': vm.NombreDC,
        'Callecomercial': vm.CalleDC,
        'NumIntComercial': vm.NumeroInDC,
        'NumExtcomercial': vm.NumeroEXDC,
        'CPcomercial': vm.CPDC,
        'ColoniaComercial': vm.ColoniaDC,
        'EntrecallesComercial': '',
        'LocalidadComercial': vm.LocalidadDC,
        'municipioComercial': vm.MunicipoDC,
        'estadoComercial': vm.EstadoDC
      };
      distribuidorFactory.AddPlaza_DistribuidoresNew(Parametros).then(function (data){
        var Obj = {
          'Modulo': 'home.catalogos',
          'Submodulo': 'home.catalogos.distribuidores',
          'Observaciones': 'Se agregó una empresa nueva',
          'Comando': JSON.stringify(Parametros),
          'Clv_afectada': 0,
          'IdClassLog': 1
        };
        logFactory.AddMovSist(Obj).then(function(data){
          ngNotify.set('Se ha guardado  la empresa correctamente', 'success');          
          $state.go('home.catalogos.distribuidores');
        });
      });

    }

    function DupliForm(){
      vm.NombreDC = (vm.DPForm == true)? vm.Nombre:null;
      vm.CalleDC = (vm.DPForm == true)? vm.Calle:null;
      vm.NumeroInDC = (vm.DPForm == true)? vm.NumInt:null;
      vm.NumeroEXDC = (vm.DPForm == true)? vm.NumExt:null;
      vm.CPDC = (vm.DPForm == true)? vm.CP:null;
      vm.ColoniaDC = (vm.DPForm == true)? vm.Colonia:null;
      vm.LocalidadDC = (vm.DPForm == true)? vm.Localidad:null;
      vm.MunicipoDC = (vm.DPForm == true)? vm.Municipio:null;
      vm.EstadoDC = (vm.DPForm == true)? vm.Estado:null;
    }

    var vm = this;
    vm.Titulo = 'Nueva Empresa';
    vm.Icono = 'fa fa-plus';
    vm.block = false;
    vm.Submit = false;
    vm.ValidateRFC = /^[A-Z]{4}\d{6}[A-Z]{3}$|^[A-Z]{4}\d{6}\d{3}$|^[A-Z]{4}\d{6}[A-Z]{2}\d{1}$|^[A-Z]{4}\d{6}[A-Z]{1}\d{2}$|^[A-Z]{4}\d{6}\d{2}[A-Z]{1}$|^[A-Z]{4}\d{6}\d{1}[A-Z]{2}$|^[A-Z]{4}\d{6}\d{1}[A-Z]{1}\d{1}$|^[A-Z]{4}\d{6}[A-Z]{1}\d{1}[A-Z]{1}$/;
    vm.SaveDistribuidor = SaveDistribuidor;
    vm.maskOptions = {
      maskDefinitions:{'A': /[A-Z]/, '9': /[0-9]/, '*': /[A-Z0-9]/},
      clearOnBlur: false,
      eventsToHandle:['input', 'keyup', 'click']
    };
    vm.DupliForm = DupliForm;
    vm.ShowDPD = true;

  });