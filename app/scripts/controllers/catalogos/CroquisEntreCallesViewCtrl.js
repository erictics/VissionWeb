'use strict';
angular
    .module('softvApp')
    .controller('CroquisEntreCallesViewCtrl', function(CatalogosFactory, ClientesFactory, $uibModalInstance, $uibModal, ngNotify, $state, $localStorage, globalService, $sce, ObjCli){

        function initData(){
            GetCalleList();
        }

        function GetCalleList(){
            CatalogosFactory.GetMuestraCalleColoniaList(ObjCli.ClvColonia).then(function(data){
                vm.CalleList = data.GetMuestraCalleColoniaListResult;
                vm.CalleListN = data.GetMuestraCalleColoniaListResult;
                vm.CalleListS = data.GetMuestraCalleColoniaListResult;
                vm.CalleListE = data.GetMuestraCalleColoniaListResult;
                vm.CalleListO = data.GetMuestraCalleColoniaListResult;
                GetEntreCalles();
            });
        }

        function GetEntreCalles(){
            ClientesFactory.GetSoftvWEb_DameEntrecalles(ObjCli.IdContrato).then(function(data){
                var EntrCall = data.GetSoftvWEb_DameEntrecallesResult;
                var Norte = EntrCall.CalleNorte;
                var Sur = EntrCall.CalleSur;
                var Este = EntrCall.CalleEste;
                var Oeste = EntrCall.CalleOeste;
                vm.Orientacion = EntrCall.Casa;
                vm.OrientacionP = EntrCall.Casa;
                vm.Referencias = EntrCall.referencia;

                if(EntrCall.Casa == 'N'){
                    vm.CalleP =  Norte;
                }else if(EntrCall.Casa == 'S'){
                    vm.CalleP = Sur;
                }else if(EntrCall.Casa == 'E'){
                    vm.CalleP = Este;
                }else if(EntrCall.Casa == 'O'){
                    vm.CalleP = Oeste;
                }
                vm.CalleList.forEach(function(element, key) {
                    if(element.Clv_Calle == Norte){
                        vm.Norte = vm.CalleList[key];
                    }
                    if(element.Clv_Calle == Sur){
                        vm.Sur = vm.CalleList[key];
                    }
                    if(element.Clv_Calle == Este){
                        vm.Este = vm.CalleList[key];
                    }
                    if(element.Clv_Calle == Oeste){
                        vm.Oeste = vm.CalleList[key];
                    }
                });
            });
        }

        function Close() {
            $uibModalInstance.close();
        }

        var vm = this;
        var FileCorquis = globalService.getUrlReportes() + '/Pictures/' + 'Croquis.jpg';
        vm.FileCorquis = $sce.trustAsResourceUrl(FileCorquis);
        var FileHouse = globalService.getUrlReportes() + '/Pictures/' + 'logoCasa.jpg';
        vm.FileHouse = $sce.trustAsResourceUrl(FileHouse);
        vm.Close = Close;
        vm.View = true;
        initData();
    });