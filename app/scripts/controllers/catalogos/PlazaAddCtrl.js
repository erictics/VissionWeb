'use strict';
angular
  .module('softvApp')
  .controller('PlazaAddCtrl', function (CatalogosFactory, ngNotify, $rootScope, $state, distribuidorFactory, plazaFactory, logFactory) {

    function initData() {
      vm.Titulo = 'Nueva Región';
      distribuidorFactory.Getplaza(0, '')
        .then(function (data) {
          vm.distribuidores = data.GetPlaza_DistribuidoresNewResult;
          plazaFactory.GetMuestraEstadosFrmCompania(0)
            .then(function (data) {
              vm.estados = data.GetMuestraEstadosFrmCompaniaResult;
              vm.estadoselect = vm.estados[1];
              ObtenCiudades();
            });
        });
    }

    function muestraRelacion() {
      plazaFactory.GetAgregaEliminaRelCompaniaCiudad2(3, vm.Clv_plaza, 0, 0).then(function (res) {
        vm.relaciones = res.GetAgregaEliminaRelCompaniaCiudad2Result;
      });
    }

    function ObtenCiudades() {
      if(vm.estado2select.Clv_Estado != undefined){
        plazaFactory.GetMuestra_Ciudad_RelCompania(vm.Clv_plaza, vm.estado2select.Clv_Estado).then(function (data) {
          vm.ciudades = data.GetMuestra_Ciudad_RelCompaniaResult;
        });
      }else{
        vm.ciudades = null;
      }
    }

    function agregaRelacion() {
      plazaFactory.GetAgregaEliminaRelCompaniaCiudad2(1, vm.Clv_plaza, vm.CiudadPla.Clv_Ciudad, vm.estado2select.Clv_Estado).then(function (res) {
          muestraRelacion();
          var Obj = {
            'Clv_plaza': vm.Clv_plaza,
            'Clv_Ciudad': vm.CiudadPla.Clv_Ciudad,
            'Clv_Estado': vm.estado2select.Clv_Estado
          };
          LogSistema(Obj, 'Se agregó una relación Cuidad a la Región', 15);
        });
    }

    function EliminaRelacion(obj) {
      plazaFactory.GetAgregaEliminaRelCompaniaCiudad2(2, vm.Clv_plaza, obj.Clv_Ciudad, obj.Clv_Estado).then(function (res) {
          muestraRelacion();
          var Obj = {
            'Clv_plaza': vm.Clv_plaza,
            'Clv_Ciudad': obj.Clv_Ciudad,
            'Clv_Estado': obj.Clv_Estado
          };
          LogSistema(Obj, 'Se eliminó una relación Cuidad a la Región', 16);
        });
    }

    function SavePlaza() {
      vm.detplaza.Clv_plaza = vm.distribuidor.Clv_Plaza;
      vm.detplaza.Estado=vm.EstadoRegion.Nombre;
      plazaFactory.AddPlaza(vm.detplaza).then(function (result) {
          vm.Clv_plaza=result.AddPlazaResult;
          vm.block = false;
          vm.ShowBtnSave = false;
          LogSistema(vm.detplaza, 'Se agregó una Región nueva', 1);
          ngNotify.set('La región se ha guardado correctamente, ahora puede asignar relaciones con estados y ciudades', 'success');
        });
    }

    function LogSistema(Comando, Obs, IdClassLog){
      var Obj = {
        'Modulo': 'home.catalogos',
        'Submodulo': 'home.catalogos.plazas',
        'Observaciones': Obs,
        'Comando': JSON.stringify(Comando),
        'Clv_afectada': 0,
        'IdClassLog': IdClassLog
      };
      logFactory.AddMovSist(Obj).then(function(data){});
    }

    function igualarDomicilio(){
     
      if(vm.igualar){
        vm.detplaza.CalleAlmacen=vm.detplaza.Calle;
        vm.detplaza.CPAlmacen=vm.detplaza.CP;
        vm.detplaza.ColoniaAlmacen=vm.detplaza.Colonia;
        vm.detplaza.LocalidadAlmacen=vm.detplaza.Localidad;
        vm.detplaza.MunicipioAlmacen =vm.detplaza.Municipio;
        vm.detplaza.NumIntAlmacen=vm.detplaza.NumExt;
        if(vm.EstadoRegion){
          vm.estados.forEach(function(element) {       
            if(vm.EstadoRegion.Nombre==element.Nombre){                      
              vm.estadoselect=element;
            }        
          });
        }
      }
             
    }

    var vm = this;
    vm.detplaza = {};
    vm.block = false;
    vm.ShowBtnSave = true;
    vm.SavePlaza = SavePlaza;
    vm.ObtenCiudades = ObtenCiudades;
    vm.agregaRelacion = agregaRelacion;
    vm.EliminaRelacion = EliminaRelacion;
    vm.igualarDomicilio=igualarDomicilio;
    vm.igualar=false;
    vm.Clv_plaza=0;
    initData();

  });
