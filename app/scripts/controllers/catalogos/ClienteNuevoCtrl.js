'use strict';

angular
    .module('softvApp')
    .controller('ClienteNuevoCtrl', function(CatalogosFactory, ClientesFactory, ngNotify, $uibModal, $rootScope, $state, $localStorage){

        function initData(){
            CatalogosFactory.GetPlazaList($localStorage.currentUser.idUsuario).then(function(data){
                vm.PlazaList = data.GetPlazaListResult;
            });

            CatalogosFactory.GetTipoClienteList_WebSoftvnew().then(function(data){
                vm.TipoCobroList = data.GetTipoClienteList_WebSoftvnewResult;
                vm.TipoCobro = vm.TipoCobroList[0];
            });

            CatalogosFactory.GetBancoList().then(function(data){
                vm.BancoList = data.GetBancoListResult;
            });

            ClientesFactory.GetListaTbl_TipoIden().then(function(data){
                vm.IdentificacionList = data.GetListaTbl_TipoIdenResult;
            });
        }

        function AddDatosPersonales(){
            var ObjContratoSimilar = {
                'Nombre': vm.Nombre,
                'SegundoNombre': vm.NombreAdi,
                'Apellido_Paterno': vm.PrimerApe,
                'Apellido_Materno': vm.SegundoApe,
                'IdCompania': vm.Plaza.id_compania,
                'Clv_Estado': vm.Estado.Clv_Estado,
                'Clv_Ciudad': vm.CiuMun.Clv_Ciudad,
                'Clv_Localidad': vm.Localidad.Clv_Localidad,
                'Clv_Colonia': vm.Colonia.CLV_COLONIA,
                'Clv_Calle': vm.Calle.Clv_Calle,
                'NUMERO': vm.NumExt,
                'Num_Int': vm.NumInt,
                'IdIdentificacion': vm.Identidicacion.Id,
                'Descripcion': vm.ClaveElector,
            };
            ClientesFactory.GetListaContratoSimilar(ObjContratoSimilar).then(function(data){
                vm.SimilitudList = data.GetListaContratoSimilarResult;
                if(vm.SimilitudList.length > 0){
                    OpenContratoSimilutud();
                }else{
                    Save();
                }
            });
        }

        function OpenContratoSimilutud(){
            var SimilitudList = vm.SimilitudList;
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'views/catalogos/ModalContratoSimilar.html',
                controller: 'ModalContratoSimilarCtrl',
                controllerAs: 'ctrl',
                backdrop: 'static',
                keyboard: false,
                class: 'modal-backdrop fade',
                size: 'lg',
                resolve: {
                    SimilitudList: function () {
                        return SimilitudList;
                    }
                }
            });
            modalInstance.result.then(function (Result) {
                if(Result == true){
                    Save();
                }
            });
        }

        function Save(){
            var FechaNacD = vm.FechaNac.getDate();
            var FechaNacM = vm.FechaNac.getMonth() + 1;
            var FechaNacY = vm.FechaNac.getFullYear();
            var ObjCliente = {
                'Nombre': vm.Nombre, 
                'SegundoNombre': vm.NombreAdi,
                'Apellido_Paterno': vm.PrimerApe,
                'Apellido_Materno': vm.SegundoApe,
                'FechaNacimiento': FechaNacD + '/' + FechaNacM + '/' + FechaNacY,
                'EsFisica': 1,
                'TELEFONO': vm.Telefono, 
                'CELULAR': vm.Celular, 
                'Email': vm.Email,
                'IdCompania': vm.Plaza.id_compania, 
                'Clv_Estado': vm.Estado.Clv_Estado, 
                'Clv_Ciudad': vm.CiuMun.Clv_Ciudad, 
                'Clv_Localidad': vm.Localidad.Clv_Localidad, 
                'Clv_Colonia': vm.Colonia.CLV_COLONIA, 
                'Clv_Calle': vm.Calle.Clv_Calle, 
                'ENTRECALLES': vm.EntCalles,
                'NUMERO': vm.NumExt, 
                'NumInt': vm.NumInt, 
                'CodigoPostal': vm.CodigoPos, 
                'IdUsuario': $localStorage.currentUser.idUsuario,
                'TipoCliente': vm.TipoCobro.CLV_TIPOCLIENTE
            };
            CatalogosFactory.GetCLIENTES_NewList(ObjCliente).then(function(data){
                vm.Cliente = data.GetCLIENTES_NewListResult;
                if(vm.Cliente.length == 1){
                    var IdCliente = vm.Cliente[0].CONTRATO;
                    var ObjEntrega = {
                        'contrato': IdCliente,
                        'CalleNorte': vm.Obj.CalleNorte,
                        'CalleSur': vm.Obj.CalleSur,
                        'CalleEste': vm.Obj.CalleEste,
                        'CalleOeste': vm.Obj.CalleOeste,
                        'Casa': vm.Obj.Casa,
                        'referencia': vm.Obj.referencia
                    };
                    ClientesFactory.GetInsertaEntrecalles(ObjEntrega).then(function(data){
                        var ObjIden = {
                            'Id': vm.Identidicacion.Id,
                            'Contrato': IdCliente,
                            'Descripcion': vm.ClaveElector,
                            'Op': 1,
                        };
                        ClientesFactory.GetAddRelClienteIDentificacion(ObjIden).then(function(data){
                            var ObjCli = {
                                'Cliente': ObjCliente,
                                'Identificacion': ObjIden,
                                'EntreCalles': ObjEntrega
                            };
                            var objMovSist = {
                                'Clv_usuario': $localStorage.currentUser.idUsuario, 
                                'Modulo': 'home.catalogos', 
                                'Submodulo': 'home.catalogos.clientes', 
                                'Observaciones': 'Se registró nuevo cliente', 
                                'Usuario': $localStorage.currentUser.usuario, 
                                'Comando': JSON.stringify(ObjCliente), 
                                'Clv_afectada': IdCliente,
                                'IdClassLog': 1
                            };
                            CatalogosFactory.AddMovSist(objMovSist).then(function(data){
                                var objMovSist = {
                                    'Clv_usuario': $localStorage.currentUser.idUsuario, 
                                    'Modulo': 'home.catalogos', 
                                    'Submodulo': 'home.catalogos.clientes', 
                                    'Observaciones': 'Se registró Identificacion a nuevo cliente', 
                                    'Usuario': $localStorage.currentUser.usuario, 
                                    'Comando': JSON.stringify(ObjIden), 
                                    'Clv_afectada': IdCliente,
                                    'IdClassLog': 4
                                };
                                CatalogosFactory.AddMovSist(objMovSist).then(function(data){
                                    var objMovSist = {
                                        'Clv_usuario': $localStorage.currentUser.idUsuario, 
                                        'Modulo': 'home.catalogos', 
                                        'Submodulo': 'home.catalogos.clientes', 
                                        'Observaciones': 'Se registró entre calles a nuevo cliente', 
                                        'Usuario': $localStorage.currentUser.usuario, 
                                        'Comando': JSON.stringify(ObjEntrega), 
                                        'Clv_afectada': IdCliente,
                                        'IdClassLog': 5
                                    };
                                    var ObjBitacora = [];
                                    vm.SimilitudList.forEach(function(element){
                                        if(element.NombreSimilar == 1){
                                            var objN = {
                                                'Contrato': IdCliente,
                                                'Coincidencia': 'Nombre',
                                                'ContratoSimilar': element.CONTRATO
                                            };
                                            ObjBitacora.push(objN);
                                        }

                                        if(element.DomicilioSimilar == 1){
                                            var objD = {
                                                'Contrato': IdCliente,
                                                'Coincidencia': 'Domicilio',
                                                'ContratoSimilar': element.CONTRATO
                                            };
                                            ObjBitacora.push(objD);
                                        }

                                        if(element.IdentificacionSimilar == 1){
                                            var objI = {
                                                'Contrato': IdCliente,
                                                'Coincidencia': 'Identificación',
                                                'ContratoSimilar': element.CONTRATO
                                            };
                                            ObjBitacora.push(objI);
                                        }
                                    });
                                    ClientesFactory.GetAddBitacoraDireccionSimilar(ObjBitacora).then(function(data){
                                        CatalogosFactory.AddMovSist(objMovSist).then(function(data){
                                            ngNotify.set('CORRECTO, se añadió un cliente nuevo.', 'success');
                                            var token= btoa($localStorage.currentUser.usuario);
                                            $state.go('home.catalogos.cliente_editar', { id:IdCliente,token: token });

                                        });
                                    });
                                });
                            });
                        });
                    });
                }else{
                    ngNotify.set('ERROR, al añadir un cliente nuevo.', 'warn');
                    $state.go('home.catalogos.clientes');
                }
            });
            
        }

        function GetEstado(){
            if(vm.Plaza != undefined){
                CatalogosFactory.GetMuestraEstadosCompaniaList(vm.Plaza.id_compania).then(function(data){
                    vm.EstadoList = data.GetMuestraEstadosCompaniaListResult;
                });
            }else{
                vm.EstadoList = null;
            }
            vm.CiudadMunicipioList = null;
            vm.LocalidadList = null;
            vm.ColoniaList = null;
            vm.CalleList = null;
            vm.Obj = null;
            vm.EntCalles = null;
        }

        function GetCiudadMunicipio(){
            if(vm.Estado != undefined){
                var RelEstMun = {
                    'clv_estado' : vm.Estado.Clv_Estado,
                    'idcompania' : vm.Plaza.id_compania
                };
                CatalogosFactory.GetMuestraCiudadesEstadoList(RelEstMun).then(function(data){
                    vm.CiudadMunicipioList = data.GetMuestraCiudadesEstadoListResult;
                });
            }else{
                vm.CiudadMunicipioList = null;
            }
            vm.LocalidadList = null;
            vm.ColoniaList = null;
            vm.CalleList = null;
            vm.Obj = null;
            vm.EntCalles = null;
        }

        function GetLocalidad(){
            if(vm.CiuMun != undefined){
                CatalogosFactory.GetMuestraLocalidadCiudadList(vm.CiuMun.Clv_Ciudad).then(function(data){
                    vm.LocalidadList = data.GetMuestraLocalidadCiudadListResult;
                });
            }else{
                vm.LocalidadList = null;
            }
            vm.ColoniaList = null;
            vm.CalleList = null;
            vm.Obj = null;
            vm.EntCalles = null;
        }

        function GetColonia(){
            if(vm.Localidad != undefined){
                CatalogosFactory.GetMuestraColoniaLocalidadList(vm.Localidad.Clv_Localidad).then(function(data){
                    vm.ColoniaList = data.GetMuestraColoniaLocalidadListResult;
                });
            }else{
                vm.ColoniaList = null;
            }
            vm.CalleList = null;
            vm.Obj = null;
            vm.EntCalles = null;
        }

        function GetCalle(){
            if(vm.Colonia != undefined){
                var ValidCall = {
                    'IdCompania': vm.Plaza.id_compania, 
                    'Clv_Estado': vm.Estado.Clv_Estado,
                    'Clv_Ciudad': vm.CiuMun.Clv_Ciudad,
                    'Clv_Localidad': vm.Localidad.Clv_Localidad,
                    'Clv_Colonia': vm.Colonia.CLV_COLONIA
                };
                CatalogosFactory.GetValidaColServCli(ValidCall).then(function(data){
                    if(data.GetValidaColServCliResult.Resultado == false){
                        ngNotify.set('NOTA, la colonia que seleccionó aún no tiene servicios definidos.', 'warn');
                    }
                });
                var ObjCP = {
                    'Clv_Colonia': vm.Colonia.CLV_COLONIA,
                    'Clv_Localidad': vm.Localidad.Clv_Localidad,
                    'Clv_Ciudad': vm.CiuMun.Clv_Ciudad
                };
                ClientesFactory.GetConCPByColoniaLocalidadCiudad(ObjCP).then(function(data){
                    vm.CodigoPos = data.GetConCPByColoniaLocalidadCiudadResult.CodigoPostal;
                });
                CatalogosFactory.GetMuestraCalleColoniaList(vm.Colonia.CLV_COLONIA).then(function(data){
                    vm.CalleList = data.GetMuestraCalleColoniaListResult;
                });
            }else{
                vm.CalleList = null;
                vm.CodigoPos = null;
            }
        }

        function ResetEC(){
            vm.Obj = null;
            vm.EntCalles = null;
        }

        function OpenEntreCalles() {
            var ClvColonia = vm.Colonia.CLV_COLONIA;
            var Clv_Calle = vm.Calle.Clv_Calle;
            var Obj = vm.Obj;
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'views/catalogos/CroquisEntreCallesForm.html',
                controller: 'CroquisEntreCallesAddCtrl',
                controllerAs: 'ctrl',
                backdrop: 'static',
                keyboard: false,
                class: 'modal-backdrop fade',
                size: 'lg',
                resolve: {
                    ClvColonia: function () {
                        return ClvColonia;
                    },
                    Clv_Calle: function () {
                        return Clv_Calle;
                    },
                    Obj: function () {
                        return Obj;
                    }
                }
            });
            modalInstance.result.then(function (Obj) {
                if(Obj != null){
                    vm.Obj = Obj;
                    vm.EntCalles = vm.Obj.entrecalles;
                }
                
                vm.CalleList.forEach(function(element, key){
                    if(element.Clv_Calle == vm.Obj.callep){
                        vm.Calle = vm.CalleList[key];
                    }
                });
            });
        }

        function GetNumber(num){
            var res = [];
            for (var i = 0; i < num; i++) {
                res.push(i);
            }
            return res;
        }

        var vm = this;
        vm.ShowAccord = false;
        vm.BlockInput = false;
        vm.DisableInput = true;
        vm.ShowServiciosE = 8;
        vm.SetForm = 1;
        vm.TipoPersona = "F";
        vm.Title = 'Cliente Nuevo';
        vm.View = false;
        vm.maskOptions = {
            maskDefinitions:{'A': /[a-zA-Z]/, '9': /[0-9]/, '*': /[a-zA-Z0-9]/},
            clearOnBlur: false,
            clearOnBlurPlaceholder: true,
            eventsToHandle:['input', 'keyup', 'click'],
            addDefaultPlaceholder: true,
            escChar: '\\',
            allowInvalidValue: false
        };
        /*vm.maskOptions = {
            maskDefinitions:{'A': /[A-Z]/, '9': /[0-9]/, '*': /[A-Z0-9]/},
            clearOnBlur: false,
            eventsToHandle:['input', 'keyup', 'click']
        };*/
        vm.AddDatosPersonales = AddDatosPersonales;
        vm.GetEstado = GetEstado;
        vm.GetCiudadMunicipio = GetCiudadMunicipio;
        vm.GetLocalidad = GetLocalidad;
        vm.GetColonia = GetColonia;
        vm.GetCalle = GetCalle;
        vm.GetNumber = GetNumber;
        vm.Obj = null;
        vm.OpenEntreCalles = OpenEntreCalles;
        vm.ResetEC = ResetEC;
        vm.ShowServiciosEN = true; 
        vm.ShowServiciosEN = 17;
        vm.showOptions=false;
        vm.ContratoAnterior = false;
        vm.PrimerMen='Y';
        vm.DisTipoSuscriptor = false;
        initData();
        
    });