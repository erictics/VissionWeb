'use strict';
angular
  .module('softvApp')
  .controller('PlazaUpdateCtrl', function (plazaFactory, ngNotify, $rootScope, $state, $stateParams, distribuidorFactory, logFactory) {

    function initData() {
      distribuidorFactory.Getplaza(0, '')
        .then(function (data) {
          vm.distribuidores = data.GetPlaza_DistribuidoresNewResult;
          plazaFactory.GetObtendatosPlaza($stateParams.id)
            .then(function (data) {
              vm.detplaza = data.GetObtendatosPlazaResult;
              vm.distribuidores.forEach(function(item){
                if(item.Clv_Plaza===parseInt(vm.detplaza.Clv_plaza)){
                   vm.distribuidor=item;
                }
              });
              vm.Titulo = 'Editar Región - ' + vm.detplaza.Razonsocial;
              plazaFactory.GetMuestraEstadosFrmCompania($stateParams.id)
                .then(function (data) {
                  vm.estados = data.GetMuestraEstadosFrmCompaniaResult;
                  console.log(vm.estados)
                  vm.estados.forEach(function(element) {              
                    
                    if(element.Nombre==vm.detplaza.Estado){                      
                      vm.EstadoRegion=element;
                    }
                    
                  }); 
                   
                  vm.estadoselect = vm.estados[1];
                  vm.estado2select = vm.estados[1]
                  muestraRelacion();
                  ObtenCiudades();
                });
            });
        });
    }

    function muestraRelacion() {
      plazaFactory.GetAgregaEliminaRelCompaniaCiudad2(3, $stateParams.id, 0, 0).then(function (res) {
        vm.relaciones = res.GetAgregaEliminaRelCompaniaCiudad2Result;       
      });
    }

    function ObtenCiudades() {
      if(vm.estado2select != null){
        plazaFactory.GetMuestra_Ciudad_RelCompania($stateParams.id, vm.estado2select.Clv_Estado).then(function (data) {        
          vm.ciudades = data.GetMuestra_Ciudad_RelCompaniaResult;         
        });
      }else{
        vm.ciudades = null;
      }
    }

    function agregaRelacion() {
      plazaFactory.GetAgregaEliminaRelCompaniaCiudad2(1, $stateParams.id, vm.CiudadPla.Clv_Ciudad, vm.estado2select.Clv_Estado)
        .then(function (res) {
          muestraRelacion();
          var Obj = {
            'Clv_plaza': $stateParams.id,
            'Clv_Ciudad': vm.CiudadPla.Clv_Ciudad,
            'Clv_Estado': vm.estado2select.Clv_Estado
          };
          LogSistema(Obj, 'Se agregó una relación Cuidad a la Región', 15);
        });
    }

    function EliminaRelacion(obj) {
      plazaFactory.GetAgregaEliminaRelCompaniaCiudad2(2, $stateParams.id, obj.Clv_Ciudad, obj.Clv_Estado).then(function (res) {         
          muestraRelacion();
          var Obj = {
            'Clv_plaza': $stateParams.id,
            'Clv_Ciudad': obj.Clv_Ciudad,
            'Clv_Estado': obj.Clv_Estado
          };
          LogSistema(Obj, 'Se eliminó una relación Cuidad a la Región', 16);
        });
    }

    function SavePlaza() {
      console.log(vm.EstadoRegion)
      vm.detplaza.Clv_plaza =  vm.distribuidor.Clv_Plaza;
      vm.detplaza.Estado=vm.EstadoRegion.Nombre;
      plazaFactory.EditPlaza(vm.detplaza).then(function (result) {
        $state.go('home.catalogos.plazas');
        ngNotify.set('La región se ha editado correctamente ', 'success');
        LogSistema(vm.detplaza, 'Se modificó la Región', 2);
      });
    }

    function igualarDomicilio(){
      if(vm.igualar){
        vm.detplaza.CalleAlmacen=vm.detplaza.Calle;
        vm.detplaza.CPAlmacen=vm.detplaza.CP;
        vm.detplaza.ColoniaAlmacen=vm.detplaza.Colonia;
        vm.detplaza.LocalidadAlmacen=vm.detplaza.Localidad;
        vm.detplaza.MunicipioAlmacen =vm.detplaza.Municipio;
        vm.detplaza.NumIntAlmacen=vm.detplaza.NumExt;
        if(vm.EstadoRegion){
          vm.estados.forEach(function(element) {       
            if(vm.EstadoRegion.Nombre==element.Nombre){                      
              vm.estadoselect=element;
            }        
          });
        }
      }     
    }

    function LogSistema(Comando, Obs, IdClassLog){
      var Obj = {
        'Modulo': 'home.catalogos',
        'Submodulo': 'home.catalogos.plazas',
        'Observaciones': Obs,
        'Comando': JSON.stringify(Comando),
        'Clv_afectada': vm.detplaza.Clv_plaza,
        'IdClassLog': IdClassLog
      };
      logFactory.AddMovSist(Obj).then(function(data){});
    }

    var vm = this;
    vm.ObtenCiudades = ObtenCiudades;
    vm.agregaRelacion = agregaRelacion;
    vm.EliminaRelacion = EliminaRelacion;    
    vm.SavePlaza = SavePlaza;
    vm.Clv_plaza=$stateParams.id;
    vm.block = false;
    vm.igualarDomicilio=igualarDomicilio;
    vm.igualar=false;
    vm.ShowBtnSave = true;
    initData();

  });
