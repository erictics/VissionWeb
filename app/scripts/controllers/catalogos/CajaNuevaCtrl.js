'use strict';
angular
  .module('softvApp')
  .controller('CajaNuevaCtrl', function (CatalogosFactory, atencionFactory, ngNotify, $state, logFactory) {

    function init() {
      atencionFactory.getPlazas().then(function (data) {
        vm.plazas = data.GetMuestra_Compania_RelUsuarioListResult;
        vm.plaza = vm.plazas[0];
        GetSucursales(vm.plaza.id_compania);
      });
      GetIPList();
    }

    function GetSucursales(sucursal) {
      CatalogosFactory.GetMUESTRASUCURSALES2(vm.plaza.id_compania)
        .then(function (result) {
          vm.sucursales = result.GetMUESTRASUCURSALES2Result;
          if (sucursal > 0) {
            vm.sucursales.forEach(function (item) {
              if (item.Clv_Sucursal === sucursal) {
                vm.sucursal = item;
              }
            });
          }
        });
    }

    function GetIPList(){
      var Parametros = {
        'Clave': 0,
        'Descripcion': '',
        'OP': 2,
        'idcompania': 0
      };
      CatalogosFactory.GetCatalogoCajasList(Parametros).then(function (data) {
        vm.CajaList = data.GetCatalogoCajasListResult;
      });
    }

    function SaveCaja() {
      var ObjIp = {
        'IpMAquina': vm.ip,
        'Clave': 0,
        'Op': 1
      };
      CatalogosFactory.GetValidaIPMaquina(ObjIp).then(function(data){
        if(data.GetValidaIPMaquinaResult.Resultado == true){
          var Parametros = {
            'Clave': 0,
            'Clv_sucursal': vm.sucursal.Clv_Sucursal,
            'IpMaquina': vm.ip,
            'Descripcion': vm.descripcion,
            'ImprimeTickets': vm.ticket,
            'facnormal': (vm.factura === 'n') ? true : false,
            'facticket': (vm.factura === 't') ? true : false,
            'impresoratermica': (vm.termica===true)? 1:0
          };
          CatalogosFactory.AddCatalogoCajas(Parametros)
            .then(function (result) {
              var log={
                'Modulo':'home.catalogos',
                'Submodulo':'home.catalogos.nuevacaja',
                'Observaciones':'Se agrego una Caja ',
                'Comando':JSON.stringify(Parametros),
                'Clv_afectada':0,
                'IdClassLog':1
              };
              logFactory.AddMovSist(log).then(function(result){ console.log('add'); });

              ngNotify.set('La caja se ha guardado correctamente', 'success');
              $state.go('home.catalogos.cajas');
            });
         }else{
          ngNotify.set('Error, La IP de la Maquina que ingresó, ya existe', 'warn');
        }
      });
    }

    function ValidaIPMaquina(){
      var result = true
      if(vm.CajaList != undefined && vm.ip != undefined){
        for(var i = 0; vm.CajaList.length > i; i++){
          if(vm.CajaList[i].IpMaquina.toUpperCase() == vm.ip.toUpperCase()){
            result = false
            break;
          }
        }
        return result;
      }
    }

    var vm = this;
    vm.Titulo = 'Nueva Caja';
    init();
    vm.GetSucursales = GetSucursales;
    vm.ValidaIPMaquina = ValidaIPMaquina;
    vm.SaveCaja = SaveCaja;
    vm.block=false;

  });