'use strict';

angular
    .module('softvApp')
    .controller('ModalContratoSimilarCtrl', function(ClientesFactory, $localStorage, $uibModalInstance, $uibModal, ngNotify, $state, logFactory, SimilitudList){

        function initData(){
            console.log(SimilitudList);
            vm.SimilitudList = SimilitudList;
        }

        function SaveCliente(){
            vm.Result = true;
            cancel();
        }

        function cancel() {
            $uibModalInstance.close(vm.Result);
        }

        var vm = this;
        vm.Result = false;
        vm.SaveCliente = SaveCliente;
        vm.cancel = cancel;
        initData();
        
    });