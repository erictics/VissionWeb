'use strict';

angular
    .module('softvApp')
    .controller('ModalConceptoDeleteCtrl', function($uibModalInstance, $uibModal, CatalogosFactory, $state, $rootScope, ngNotify, ObjConcepto, logFactory){
    
        function cancel() {
            $uibModalInstance.dismiss('cancel');
        }

        function DeleteConcepto(){
            CatalogosFactory.GetDeepValidaEliminaClvLlave(vm.CLV_LLAVE).then(function(data){
                var ObjConcepto = {
                    'CLV_LLAVE':vm.CLV_LLAVE,
                    'Clv_TipoCliente': vm.CLV_TIPOCLIENTE
                };
                CatalogosFactory.DeleteREL_TARIFADOS_SERVICIOS_New(ObjConcepto).then(function(data){
                    if(data.DeleteREL_TARIFADOS_SERVICIOS_NewResult > 0){
                        var Obj = {
                            'Modulo': 'home.catalogos',
                            'Submodulo': 'home.catalogos.servicios',
                            'Observaciones': 'Se eliminó el concepto a servicio',
                            'Comando': JSON.stringify(ObjConcepto),
                            'Clv_afectada': 0,
                            'IdClassLog': 22
                        };
                        logFactory.AddMovSist(Obj).then(function(data){});
                        
                        ngNotify.set('CORRECTO, se eliminó el concepto a servicio.', 'success');
                        $rootScope.$emit('LoadConceptos', vm.Clv_Servicio);
                        cancel();
                    }else{
                        ngNotify.set('ERROR, al eliminar el concepto a servicio.', 'warn');
                        $rootScope.$emit('LoadRefPersonal', vm.IdContrato);
                        cancel();
                    }
                });
            });
        }

        var vm = this;
        vm.cancel = cancel;
        vm.DeleteConcepto = DeleteConcepto;
        vm.CONCEPTO = ObjConcepto.ObjConcepto.CONCEPTO;
        vm.CLV_LLAVE = ObjConcepto.ObjConcepto.CLV_LLAVE;
        vm.CLV_TIPOCLIENTE = ObjConcepto.CLV_TIPOCLIENTE;
    });