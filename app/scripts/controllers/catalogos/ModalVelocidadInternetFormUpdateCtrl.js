'use strict';

angular
    .module('softvApp')
    .controller('ModalVelocidadInternetFormUpdateCtrl', function(CatalogosFactory, $uibModal, $uibModalInstance, ngNotify, $state, id, logFactory){

        function initData(){
            CatalogosFactory.GetDeeptbl_politicasFibra(id).then(function(data){
                var VelocidadInternet = data.GetDeeptbl_politicasFibraResult;
                vm.ClaveEquivalente = VelocidadInternet.Clv_equivalente;
                vm.VelocidadSubida = parseInt(VelocidadInternet.VelSub);
                vm.VelocidadBajada = parseInt(VelocidadInternet.VelBaj);
                var ClvUnidadMedidaSubida = VelocidadInternet.UnidadSub;
                var ClvUnidadMedidaBajada = VelocidadInternet.UnidadBaj;
                CatalogosFactory.GetDameUnidadesMedidasDeVelocidadList().then(function(data){
                    vm.UnidadMedidaList = data.GetDameUnidadesMedidasDeVelocidadListResult;
                    for(var i = 0; vm.UnidadMedidaList.length > i; i ++){
                        if(vm.UnidadMedidaList[i].Clave == ClvUnidadMedidaSubida){
                            vm.UnidadMedidaSubida = vm.UnidadMedidaList[i];
                        }
                    }
                    for(var i = 0; vm.UnidadMedidaList.length > i; i ++){
                        if(vm.UnidadMedidaList[i].Clave == ClvUnidadMedidaBajada){
                            vm.UnidadMedidaBajada = vm.UnidadMedidaList[i];
                        }
                    }
                });
            });
        }

        function SaveVelocidadInternet(){
            var ObjVelocidad = {
                'id': id, 
                'Clv_equivalente': vm.ClaveEquivalente, 
                'VelSub': vm.VelocidadSubida, 
                'VelBaj': vm.VelocidadBajada, 
                'UnidadSub': vm.UnidadMedidaSubida.Clave, 
                'UnidadBaj': vm.UnidadMedidaBajada.Clave
            };
            CatalogosFactory.GetSp_guardaPolitica(ObjVelocidad).then(function(data){
                console.log(data);
                var Msj = data.GetSp_guardaPoliticaResult[0].Msj;
                if(Msj == 'Politica cambiada correctamente'){
                    var Obj = {
                        'Modulo': 'home.catalogos',
                        'Submodulo': 'home.catalogos.VelocidadInternet',
                        'Observaciones': 'Se modificó una velocidad de internet',
                        'Comando': JSON.stringify(ObjVelocidad),
                        'Clv_afectada': 0,
                        'IdClassLog':2
                    };
                    logFactory.AddMovSist(Obj).then(function(data){
                        ngNotify.set(Msj + '.', 'success');
                        cancel();
                    });
                }else{
                    ngNotify.set(Msj + '.', 'warn');
                    cancel();
                }
            });
        }

        function cancel() {
            $uibModalInstance.close();
        }

        var vm = this;
        vm.Titulo = 'Editar Velocidad de Internet';
        vm.Icono = 'fa fa-pencil-square-o';
        vm.BlockInput = true;
        vm.View = false;
        vm.SaveVelocidadInternet = SaveVelocidadInternet;
        vm.cancel = cancel;
        vm.guardar = true;
        initData();

    });