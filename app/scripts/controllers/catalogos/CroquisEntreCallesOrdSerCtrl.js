'use strict';
angular
    .module('softvApp')
    .controller('CroquisEntreCallesOrdSerCtrl', function(CatalogosFactory, ClientesFactory, $uibModalInstance, $uibModal, ngNotify, $state, $localStorage, globalService, $sce, Obj){

        function initData(){
            GetCalleList();
        }

        function GetCalleList(){
            CatalogosFactory.GetMuestraCalleColoniaList(Obj.ClvColonia).then(function(data){
                vm.CalleList = data.GetMuestraCalleColoniaListResult;
                vm.CalleListN = data.GetMuestraCalleColoniaListResult;
                vm.CalleListS = data.GetMuestraCalleColoniaListResult;
                vm.CalleListE = data.GetMuestraCalleColoniaListResult;
                vm.CalleListO = data.GetMuestraCalleColoniaListResult;
                vm.CalleP = Obj.Clv_calle;
                if(Obj.Obj != null){
                    vm.Orientacion = Obj.Obj.Casa;
                    vm.OrientacionP = Obj.Obj.Casa;
                    vm.CalleList.forEach(function(element, key) {
                        vm.Referencias = Obj.Obj.referencia;
                        if(element.Clv_Calle == Obj.Obj.CalleNorte){
                            vm.Norte = vm.CalleList[key];
                        }
                        if(element.Clv_Calle == Obj.Obj.CalleSur){
                            vm.Sur = vm.CalleList[key];
                        }
                        if(element.Clv_Calle == Obj.Obj.CalleEste){
                            vm.Este = vm.CalleList[key];
                        }
                        if(element.Clv_Calle == Obj.Obj.CalleOeste){
                            vm.Oeste = vm.CalleList[key];
                        }
                    });
                }else{
                    vm.CalleList.forEach(function(element, key){
                        if(element.Clv_Calle == vm.CalleP){
                            vm.Norte = vm.CalleList[key];
                        }
                    });
                }
            });
        }

        function Save(){
            if(ValidaOrientacion() == true){
                vm.obj = {
                    'CalleNorte': (vm.Norte != undefined && vm.Norte != null)? vm.Norte.Clv_Calle:0,
                    'CalleSur': (vm.Sur != undefined && vm.Sur != null)? vm.Sur.Clv_Calle:0,
                    'CalleEste': (vm.Este != undefined && vm.Este != null)? vm.Este.Clv_Calle:0,
                    'CalleOeste': (vm.Oeste != undefined && vm.Oeste != null)?vm.Oeste.Clv_Calle:0,
                    'Casa': vm.Orientacion,
                    'referencia': vm.Referencias,
                    'entrecalles': GetNombreCalles(),
                    'callep': GetPosicion()
                };
                Close();
            }else{
                ngNotify.set('ERROR, Las Calles que ingresó deben ser perpendiculares.', 'warn');
            }
        }

        function GetNombreCalles(){
            var N = (vm.Norte != undefined && vm.Norte != null)? vm.Norte.Nombre + ', ':'';
            var S = (vm.Sur != undefined && vm.Sur != null)? vm.Sur.Nombre + ', ':'';
            var E = (vm.Este != undefined && vm.Este != null)? vm.Este.Nombre + ', ':'';
            var O = (vm.Oeste != undefined && vm.Oeste != null)? vm.Oeste.Nombre:'';
            return N + S + E + O
        }

        function GetPosicion(){
            var IdP = 0;
            if(vm.Orientacion == 'N'){
                IdP = vm.Norte.Clv_Calle;
            }
            if(vm.Orientacion == 'S'){
                IdP = vm.Sur.Clv_Calle;
            }
            if(vm.Orientacion == 'E'){
                IdP = vm.Este.Clv_Calle;
            }
            if(vm.Orientacion == 'O'){
                IdP = vm.Oeste.Clv_Calle;
            }
            return IdP
        }

        function ValidaOrientacion(){
            if(
                ((vm.Orientacion == 'N') && ((vm.Norte != undefined && vm.Norte != null)) && ((vm.Este != undefined && vm.Este != null) || (vm.Oeste != undefined && vm.Oeste != null))) 
             || ((vm.Orientacion == 'O') && ((vm.Norte != undefined && vm.Norte != null) || (vm.Sur != undefined && vm.Sur != null)) && ((vm.Oeste != undefined && vm.Oeste != null)))
             || ((vm.Orientacion == 'S') && ((vm.Sur != undefined && vm.Sur != null)) && ((vm.Este != undefined && vm.Este != null) || (vm.Oeste != undefined && vm.Oeste != null))) 
             || ((vm.Orientacion == 'E') && ((vm.Norte != undefined && vm.Norte != null) || (vm.Sur != undefined && vm.Sur != null)) && ((vm.Este != undefined && vm.Este != null)))
             ){
                 return true
             }else{
                 return false
             }
        }

        function SetPosicion(){
            if((vm.OrientacionP == 'N' && vm.Norte != undefined && vm.Norte != null)){
                vm.CalleListN = null;
                vm.CalleListN = vm.CalleList;
                Reset();
            }

            if((vm.OrientacionP == 'S' && vm.Sur != undefined && vm.Sur != null)){
                vm.CalleListS = null;
                vm.CalleListS = vm.CalleList;
                Reset();
            }

            if((vm.OrientacionP == 'E' && vm.Este != undefined && vm.Este != null)){
                vm.CalleListE = null;
                vm.CalleListE = vm.CalleList;
                Reset();
            }

            if((vm.OrientacionP == 'O' && vm.Oeste != undefined && vm.Oeste != null)){
                vm.CalleListO = null;
                vm.CalleListO = vm.CalleList;
                Reset();
            }
        }

        function Reset(){
            if(vm.Orientacion == 'N'){
                vm.CalleList.forEach(function(element, key){
                    if(element.Clv_Calle == vm.CalleP){
                        vm.Norte = vm.CalleList[key];
                    }
                });
            }

            if(vm.Orientacion == 'S'){
                vm.CalleList.forEach(function(element, key){
                    if(element.Clv_Calle == vm.CalleP){
                        vm.Sur = vm.CalleList[key];
                    }
                });
            }

            if(vm.Orientacion == 'E'){
                vm.CalleList.forEach(function(element, key){
                    if(element.Clv_Calle == vm.CalleP){
                        vm.Este = vm.CalleList[key];
                    }
                });
            }

            if(vm.Orientacion == 'O'){
                vm.CalleList.forEach(function(element, key){
                    if(element.Clv_Calle == vm.CalleP){
                        vm.Oeste = vm.CalleList[key];
                    }
                });
            }

            vm.OrientacionP = vm.Orientacion;
        }

        function Close() {
            $uibModalInstance.close(vm.obj);
        }
        

        var vm = this;
        vm.Orientacion = 'N';
        vm.OrientacionP = 'N';
        vm.obj = null;
        var FileCorquis = globalService.getUrlReportes() + '/Pictures/' + 'Croquis.jpg';
        vm.FileCorquis = $sce.trustAsResourceUrl(FileCorquis);
        var FileHouse = globalService.getUrlReportes() + '/Pictures/' + 'logoCasa.jpg';
        vm.FileHouse = $sce.trustAsResourceUrl(FileHouse);
        vm.View = Obj.Detalle;
        vm.SetPosicion = SetPosicion;
        vm.Save = Save;
        vm.Close = Close;
        initData();

    });
