'use strict';
angular
  .module('softvApp')
  .controller('PlazasCtrl', function (plazaFactory, ngNotify, $rootScope, $state, $localStorage, distribuidorFactory) {

    function initData() {
      distribuidorFactory.Getplaza(0,'')
        .then(function (data) {
          vm.distribuidores=data.GetPlaza_DistribuidoresNewResult;
          obtenPlazas(0);
        });
    }

    function obtenPlazas(op) {
      var ObjPlaza = {
        'opcion': (op == 3 && (vm.distribuidor == undefined || vm.distribuidor == null))? 0:op,
        'razon': '',
        'idcompania': ((op == 3 && (vm.distribuidor == undefined || vm.distribuidor == null)) || op == 0)? 0:vm.distribuidor.Clv_Plaza
      };
      plazaFactory.GetBrwMuestraCompanias(ObjPlaza).then(function (result) {
        vm.plazas =  result.GetBrwMuestraCompaniasResult;
      });
    }

    var vm = this;
    vm.obtenPlazas = obtenPlazas;
    initData();

  });
