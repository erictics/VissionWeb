'use strict';
angular
	.module('softvApp')
	.controller('HomeCtrl', function ($localStorage, $location, $window,moment, $state, generalesSistemaFactory, globalService, $uibModal) {
		function initialData() {
			if ($localStorage.currentUser) {
				//TimeElapsed();
				vm.menus = $localStorage.currentUser.Menu;
				vm.usuario = $localStorage.currentUser.usuario;
				vm.caja = $localStorage.currentUser.CajaNombre;
				vm.sucursal = $localStorage.currentUser.SucursalNombre;
				generalesSistemaFactory.Getlogos().then(function (result) {
					vm.logo1 = globalService.getUrllogos() + "/" + result.GetlogosResult[0].Valor;

				});
				vm.colordefinido = '#305b8c';

				/*
				setInterval(function () {
					
					TimeElapsed()
				}, 3000);*/

			} else {
				location.href === '/auth/';
			}
		}

		function logout() {
			delete $localStorage.currentUser;
			$window.location.reload();
		}
/*
		function TimeElapsed(){
			
			var endTime = moment();
            console.log( $localStorage.currentUser.LastRequest);
           console.log( endTime);
		   var seconds= endTime.diff($localStorage.currentUser.LastRequest,'seconds');
			
			console.log(seconds + " seconds");
			if (seconds >= 600) {
				//logout();
				delete $localStorage.currentUser;
				var modalInstance = $uibModal.open({
                  animation: true,
                  ariaLabelledBy: 'modal-title',
                  ariaDescribedBy: 'modal-body',
                  templateUrl: 'views/login/ModalSesionExpirada.html',
                  controller: 'ModalSesionExpiradaCtrl',
                  controllerAs: 'ctrl',
                  backdrop: 'static',
                  keyboard: false,
                  class: 'modal-backdrop fade',
                  size: 'md'
                });
                modalInstance.result.then(function (res) {
					$window.location.reload();
                });
			}
		}*/

		function changepassword() {
			$state.go('home.configuracion.changepassword');
		}

		

		var vm = this;
		vm.logout = logout;
		vm.changepassword = changepassword;
		initialData();

	});
