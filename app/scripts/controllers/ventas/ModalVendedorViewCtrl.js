'use strict';

angular
    .module('softvApp')
    .controller('ModalVendedorViewCtrl', function(VentasFactory, $localStorage, $uibModalInstance, $uibModal, ngNotify, $state, $rootScope, Clv_Vendedor){
        
        function initData(){
            VentasFactory.GetMuestra_PlazasPorUsuarioList($localStorage.currentUser.idUsuario).then(function(data){
                vm.DistribuidorList = data.GetMuestra_PlazasPorUsuarioListResult;
                GetVendedor();
            });
        }

        function GetVendedor(){
            VentasFactory.GetDeepVendedores(Clv_Vendedor).then(function(data){
                var Vendedor = data.GetDeepVendedoresResult;
                vm.Clv_Vendedor = Vendedor.Clv_Vendedor;
                vm.Nombre = Vendedor.Nombre;
                vm.Domicilio = Vendedor.Domicilio;
                vm.Colonia = Vendedor.Colonia;
                vm.FechaIngreso = GetDate(Vendedor.FechaIngreso);
                vm.FechaSalida = (Vendedor.FechaSalida != "01/01/1900" && Vendedor.FechaSalida != null)? GetDate(Vendedor.FechaSalida): null;
                vm.ActivoP = Vendedor.Activo;
                vm.Activo = Vendedor.Activo;
                vm.Capacitacion = Vendedor.Capacitacion;
                var Distribuidor = Vendedor.idcompania;
                for(var i = 0; vm.DistribuidorList.length > i; i++){
                    if(vm.DistribuidorList[i].Clv_Plaza == Distribuidor){
                        vm.Distribuidor = vm.DistribuidorList[i];
                    }
                }
            });
        }

        function SaveVendedor(){
            var objVendedores = {
                'Clv_Vendedor': vm.Clv_Vendedor,
                'Nombre': vm.Nombre,
                'Domicilio': vm.Domicilio,
                'Colonia': vm.Colonia,
                'FechaIngreso': SaveDate(vm.FechaIngreso),
                'FechaSalida': ValidaFechaSalida(),
                'Activo': vm.Activo,
                'Clv_TipPro': 0,
                'Clv_Grupo': 0,
                'idcompania': vm.Distribuidor.Clv_Plaza,
                'Capacitacion': vm.Capacitacion,
                'ClvUsuario': $localStorage.currentUser.idUsuario
            };
            VentasFactory.UpdateVendedores(objVendedores).then(function(data){
                ngNotify.set('CORRECTO, Se guardó el Vendedor.', 'success');
                $rootScope.$emit('LoadVendedorList');
                cancel();
            });
        }

        function GetDate(date) {
            var Parts = date.split("/");
            return new Date(Parts[2], Parts[1] - 1, Parts[0]);
        }
        
        function cancel() {
            $uibModalInstance.dismiss('cancel');
        }

        var vm = this;
        vm.Titulo = 'Detalle Vendedor - ' + Clv_Vendedor;
        vm.Icono = 'fa fa-eye';
        vm.View = true;
        vm.DisAdd = false;
        vm.cancel = cancel;
        initData();

    });