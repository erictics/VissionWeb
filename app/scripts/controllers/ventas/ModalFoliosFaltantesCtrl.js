'use strict';

angular
    .module('softvApp')
    .controller('ModalFoliosFaltantesCtrl', function(SeriesFactory, $localStorage, $uibModalInstance, $uibModal, ngNotify, $state, $rootScope){
        
        function initData(){
            var ObjVendedorList = {
                'ClvUsuario': $localStorage.currentUser.idUsuario, 
                'Op': 0
            };
            SeriesFactory.GetVendedores_dosList(ObjVendedorList).then(function(data){
                vm.VendedorList = data.GetVendedores_dosListResult;
                vm.Vendedor = vm.VendedorList[0];
                GetSerieList();
            });
        }

        function GetSerieList(){
            if(vm.Vendedor != undefined){
                var ObjSerieList = {
                    'ClvVendedor': vm.Vendedor.Clv_Vendedor, 
                    'Contrato': 0
                };
                SeriesFactory.GetUltimo_SERIEYFOLIOList(ObjSerieList).then(function(data){
                    vm.SerieList = data.GetUltimo_SERIEYFOLIOListResult;
                    vm.Serie = vm.SerieList[0];
                    /*GetFolioDisponible();*/
                });
            }
        }
        
        function FoliosFaltantes(){
            SeriesFactory.GetReporte_Folios5(vm.Vendedor.Clv_Vendedor, vm.Serie.SERIE).then(function(data){
                console.log(data);
                vm.Name = data.GetReporte_Folios5Result;
                ngNotify.set('CORRECTO, se imprimieron los folios.', 'success');
                cancel();
            });
        }

        function cancel() {
            $uibModalInstance.close(vm.Name);
        }

        var vm = this;
        vm.View = false;
        vm.GetSerieList = GetSerieList;
        vm.FoliosFaltantes = FoliosFaltantes;
        vm.cancel = cancel;
        initData();

    });