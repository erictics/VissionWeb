'use strict';

angular
    .module('softvApp')
    .controller('RentaAparatosCtrl', function (CatalogosFactory, $uibModal,ngNotify) {
     
        var vm =this;
    function init(){
        CatalogosFactory.GetMuestraTipoServicioPagare(2).then(function(result){
            vm.servicios = result.GetMuestraTipoServicioPagareResult;
            vm.servicio = vm.servicios[0]
            buscar();
                console.log(result.GetMuestraTipoServicioPagareResult);
            
        });           
    }

    function buscar(){
        var tipser = (vm.servicio)?vm.servicio.Clv_TipSer:0;
        CatalogosFactory.GetCostoArticuloPagare(1,tipser,0).then(function(res){
            console.log(res);
            vm.lista = res.GetCostoArticuloPagareResult;
       });
    }

    function elimina(item){
        CatalogosFactory.GetEliCostosPagare(item.Id).then(function(res){
            ngNotify.set('Se eliminó correctamente','error');
            buscar();
        });
    }
    
    init();
    vm.buscar=buscar;
    vm.elimina=elimina;

    });