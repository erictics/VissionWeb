angular
.module('softvApp')
.controller('ModalMotivoReimpFactDetalleCtrl', function (CatalogosFactory, $uibModalInstance, ngNotify, $state, Clv_motivo) {
 
 function initData(){
    var ObjMotivo = {
      'Clv_Motivo':  Clv_motivo,
      'Descripcion': '',
      'Bandera': 1,    
      'op': 0
    };
    CatalogosFactory.GetBuscaMotivosFacturaCancelada(ObjMotivo).then(function(data){
      var Motivo = data.GetBuscaMotivosFacturaCanceladaResult[0];
      vm.Descripcion = Motivo.Descripcion;
      vm.Clave = Motivo.Clv_motivo;
    });
  }

  function SaveMotivo(){
      var ObjMotivo = {
          'Clv_Motivo': vm.Clave,
          'Bandera': 1 , 
          'Descripcion': vm.Descripcion
      };
      CatalogosFactory.GetMODMOTIVOSFACTURACANCELACION(ObjMotivo).then(function(data){
          if(data.GetMODMOTIVOSFACTURACANCELACIONResult == 1){    
              ngNotify.set('CORRECTO, Se guardó un Motivo de Reimpresión de Factura.', 'success');
              $state.reload('home.motivos.ReimpresionFactura');
              cancel();
          }else{
              ngNotify.set('ERROR, Al guardar un motivo de Reimpresión de Factura.', 'warn');
          }
      });
  }

  function cancel() {
    $uibModalInstance.dismiss('cancel');
  }

  var vm = this;
  vm.Titulo = 'Editar Motivo de Reimpresión';
  vm.Icono = 'fa fa-pencil-square-o';
  vm.SaveMotivo = SaveMotivo;
  vm.cancel = cancel;
  vm.blockForm = true;
  vm.blocksave = true;
  vm.blockdelete = true;
  initData();
});