angular
.module('softvApp')
.controller('ModalMotivoAddCtrl', function(CatalogosFactory, $uibModalInstance, ngNotify, logFactory, $state){

    function SaveMotivo(){
        var ObjMotivo = {
            'op':0 ,
            'MOTCAN': vm.Descripcion
        };
        CatalogosFactory.GetNUEMotivoCancelacion(ObjMotivo).then(function(data){
            if(data.GetNUEMotivoCancelacionResult == 1){
                var log={
                    'Modulo':'home.motivos',
                    'Submodulo':'home.motivos.MotivosDeCancelacion',
                    'Observaciones':'Se agregó un motivo de cancelación ',
                    'Comando':JSON.stringify(ObjMotivo),
                    'Clv_afectada':0,
                    'IdClassLog':1
                };
                logFactory.AddMovSist(log).then(function(result){ console.log('add'); });

                ngNotify.set('CORRECTO, Se añadió un Motivo de Cancelación Nuevo.', 'success');
                $state.reload('home.motivos.MotivosDeCancelacion');
                cancel();
             }else{
                ngNotify.set('ERROR, Al añadir un Motivo de Cancelación Nuevo.', 'warn');
            }
        });
    }

    function cancel() {
        $uibModalInstance.close();
    }

var vm = this;
vm.Titulo = 'Nuevo Motivo de Cancelación';
vm.Icono = 'fa fa-plus';
vm.Clave = 0;
vm.Disable = true;
vm.blockdelete = true;
vm.SaveMotivo = SaveMotivo;
vm.cancel = cancel;
});