'use strict';
angular
.module('softvApp')
.controller('ModalFormProblemasCtrl', function(areaTecnicaFactory, $uibModalInstance, ngNotify,logFactory, $state,opAccion,item){

    function init(){
        vm.clave=0;
        //nuevo
        if(opAccion === 1){           
            vm.Titulo = 'Nueva clasificación de problema';
           
        }
        //editar
        if(opAccion === 2){
            
            vm.Titulo = 'Editar clasificación de problema';
            console.log(item);
            vm.clave=item.clvProblema;
            vm.clasificacion = item.descripcion;
            vm.activo=item.activo;

        }
    }

    function Save(){        
     
        if(opAccion === 1){ 
            areaTecnicaFactory.GetuspInsertaTblClasificacionProblemas(vm.clave,vm.clasificacion,vm.activo,opAccion)
            .then(function(result){
                var log={
                    'Modulo':'home.areatecnica',
                    'Submodulo':'home.areatecnica.clasificacionproblemas',
                    'Observaciones':'Se agrego una clasificacion de problemas ',
                    'Comando':JSON.stringify(vm.clave,vm.clasificacion,vm.activo),
                    'Clv_afectada':0,
                    'IdClassLog':1
                };
                logFactory.AddMovSist(log).then(function(result){ console.log('add'); });

                ngNotify.set('La clasificación se guardó correctamente','success');
                $uibModalInstance.close();
            });  
        }
        if(opAccion === 2){  
            
         areaTecnicaFactory.GetuspInsertaTblClasificacionProblemas(vm.clave,vm.clasificacion,vm.activo,opAccion)
         .then(function(result){
            var log={
                'Modulo':'home.areatecnica',
                'Submodulo':'home.areatecnica.clasificacionproblemas',
                'Observaciones':'Se edito una clasificacion de problemas ',
                'Comando':JSON.stringify(vm.clave,vm.clasificacion,vm.activo),
                'Clv_afectada':vm.clave,
                'IdClassLog':2
            };
            logFactory.AddMovSist(log).then(function(result){ console.log('add'); });
            
            ngNotify.set('La clasificación se editó correctamente','success');
            $uibModalInstance.close();
         });
        }
    }

    function cancel() {
        $uibModalInstance.close();
    }

var vm = this;
vm.clave=0;
vm.Save = Save;
vm.cancel = cancel;
init();

});