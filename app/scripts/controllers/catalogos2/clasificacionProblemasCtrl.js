'use strict'
angular
.module('softvApp')
.controller('clasificacionProblemasCtrl', function(areaTecnicaFactory,$uibModal, ngNotify, $state){

    function GetList(clvProblema,descripcion,opBusqueda){
        areaTecnicaFactory.GetuspConsultaTblClasificacionProblemas(clvProblema,descripcion,opBusqueda)
        .then(function(result){
            vm.lista=result.GetuspConsultaTblClasificacionProblemasResult;
          console.log(result);
        });
    }

    function OpenAdd(opAccion,item){
        var modalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'views/catalogos/modalFormProblemas.html',
            controller: 'ModalFormProblemasCtrl',
            controllerAs: 'ctrl',
            backdrop: 'static',
            keyboard: false,
            class: 'modal-backdrop fade',
            size: 'md',
            resolve: {
                opAccion: function () {
                    return opAccion;
                },
                item: function () {
                    return item;
                }
            }
        });
        modalInstance.result.then(function () {
            GetList(0,'',1);
        }, function () {});
    }

    function init(){
        GetList(0,'',1);
    }

    var vm=this;
    vm.OpenAdd=OpenAdd;
    init();
});