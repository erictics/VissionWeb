'use strict';

angular
    .module('softvApp')
    .controller('RentaAparatosNuevoCtrl', function (CatalogosFactory,$stateParams,$uibModal,ngNotify) {
     
   var vm =this;
    function init(){
        CatalogosFactory.GetMuestraTipoServicioPagare(2).then(function(result){
            vm.servicios = result.GetMuestraTipoServicioPagareResult;
            vm.servicio = vm.servicios[0]  
            CatalogosFactory.GetConsultaEquipoMarcado(vm.servicio.Clv_TipSer,vm.paquete.Clv_Servicio,vm.IdCostoAp)
            .then(function(res){
             vm.articulos=res.GetConsultaEquipoMarcadoResult;
                console.log(res);
                GetMuestraServiciosRentas();
            });         
           
        });           
    }

    function GetMuestraServiciosRentas(){
        CatalogosFactory.GetMuestraServiciosRentas(vm.servicio.Clv_TipSer).then(function(res){
            vm.paquetes = res.GetMuestraServiciosRentasResult;
            
          console.log(res);
        });
    }

    function muestraAparatos(){
        CatalogosFactory.GetMuestraMarcasAparatosPagareParaList(vm.servicio.Clv_TipSer,vm.paquete.Clv_Servicio,0).then(function(res){
           vm.articulos = res.GetMuestraMarcasAparatosPagareParaListResult;
            console.log(res.GetMuestraMarcasAparatosPagareParaListResult);
        });
    }

    function guardaRelacion(){

        var obj = {
            'ID':0,
            'TIPSER':vm.servicio.Clv_TipSer,
            'OPCION':'N',
            'Clv_Servicio':vm.paquete.Clv_Servicio
        };        
        CatalogosFactory.GetSP_GuardaCostosPagareNew(obj).then(function(res){           
            if(res.GetSP_GuardaCostosPagareNewResult.MSJ.length > 0){
                ngNotify.set(res.GetSP_GuardaCostosPagareNewResult.MSJ,'warn');
            } else {
                vm.IdCostoAp=res.GetSP_GuardaCostosPagareNewResult.IdCostoAp;
                CatalogosFactory.GetSP_LimpiaInsertaRel_CostoArticuloPagareConAparatos(vm.IdCostoAp)
                .then(function(result) {
                   var lista =[];
                   vm.articulos.forEach(function(item) {
                       if (item.Bnd) {
                        var _aparato = {
                            NoArticulo: item.NoArticulo,
                            IdCostoAP : vm.IdCostoAp
                           };
                           lista.push(_aparato);
                       }
                   });                    
                   CatalogosFactory.GetSP_InsertaRel_CostoArticuloPagareConAparatos(lista).then(function(res){
                           console.log(res);
                           
                           CatalogosFactory.GetConsultaEquipoMarcado(vm.servicio.Clv_TipSer,vm.paquete.Clv_Servicio,vm.IdCostoAp)
                           .then(function(res){
                            vm.articulos=res.GetConsultaEquipoMarcadoResult;
                               console.log(res);
                           });
                   });
                });
            }          
        });
    }

    function agregaVigencia() {
      var obj = {
          'IdCostoAP': vm.IdCostoAp,
          'DIA_INICIAL': vm.diaInicial,
          'DIA_FINAL': vm.diaFinal,
          'Periodo_Inicial': vm.anioInicial +''+ vm.mesInicial +'01',
          'Periodo_Final': vm.anioFinal +''+ vm.mesFinal +'01',
          'Vigente': true,
          'CostoArticulo': vm.costoEquipo,
          'RentaPrincipal': vm.rentaPrincipal,
          'RentaAdicional': 0,
          'RentaAdicional2': 0,
          'RentaAdicional3': 0
       };


    //    declare @p7 varchar(400)
    //    set @p7=''
    //    exec Sp_ValidaVigencias @DiaIni=1,@DiaFin=25,@FechaIni='2018-05-29 00:00:00',@FechaFin='2018-12-21 00:00:00',@IdCostoAP=313,@IdRelVigencia=496,@Msj=@p7 output
    //    select @p7

/* 
    declare @p14 varchar(400)
    set @p14=''
    exec SP_ModificarRel_CostoArticuloPagareConVigencias @IdCostoAP=313,@IdRelVigencia=496,@DIA_INICIAL=1,@DIA_FINAL=25,@Periodo_Inicial='2018-05-29 00:00:00',@Periodo_Final='2018-12-21 00:00:00',@Vigente=1,@CostoArticulo=15000.0000,@RentaPrincipal=499.0000,@RentaAdicional=0.0000,@idcompania=1,@RentaAdicional2=0.0000,@RentaAdicional3=0.0000,@Msj=@p14 output
    select @p14 */

       var obj2 = {
        'DIA_INICIAL': vm.diaInicial,
        'DIA_FINAL': vm.diaFinal,
        'Periodo_Inicial': vm.anioInicial +''+ vm.mesInicial +'01',
        'Periodo_Final': vm.anioFinal +''+ vm.mesFinal +'01',
        'IdCostoAP': vm.IdCostoAp,
        'IdRelVigencia': 0
       };
       console.log(obj);
       console.log(obj2);
      
       CatalogosFactory.GetSp_ValidaVigencias(obj2).then(function(res){
           console.log(res);
         if(res.GetSp_ValidaVigenciasResult !== "") {
           ngNotify.set(res.GetSp_ValidaVigenciasResult ,'warn');
           }else{

            CatalogosFactory.GetSP_InsertaRel_CostoArticuloPagareConVigencias(obj).then(function(res){
                console.log(res);
                getVigencias();
                     
                 });

         }

       

       });

      
    }

    function getVigencias () {
        CatalogosFactory.GetSP_ConsultaRel_CostoArticuloPagareConVigencia(vm.IdCostoAp).then(function(response){
            vm.vigencias = response.GetSP_ConsultaRel_CostoArticuloPagareConVigenciaResult;
         });
    }

    function eliminaVigencia(item){

        CatalogosFactory.GetSP_EliminarRel_CostoArticuloPagareConVigenciasIndividual(item.IdCostoAP,item.IdRelVigencia).then(function(res){
           ngNotify.set('La vigencia se eliminó correctamente' , 'success');
            getVigencias();
        });

    }

    init();
    vm.GetMuestraServiciosRentas=GetMuestraServiciosRentas;
    vm.muestraAparatos=muestraAparatos;
    vm.guardaRelacion=guardaRelacion;
    vm.agregaVigencia=agregaVigencia;
    vm.eliminaVigencia=eliminaVigencia;
    vm.IdCostoAp = 0;

    });