angular
.module('softvApp')
.controller('ModalMotivoReimpDeleteCtrl', function(CatalogosFactory, $uibModalInstance, ngNotify, $state, Clv_motivo,logFactory){

    function initData(){
      var ObjMotivo = {
        'Clv_Motivo':  Clv_motivo,
        'Descripcion': '',
        'Bandera': 1,    
        'op': 0
      };
      CatalogosFactory.GetBuscaMotivosFacturaCancelada(ObjMotivo).then(function(data){
        var Motivo = data.GetBuscaMotivosFacturaCanceladaResult[0];
        vm.Descripcion = Motivo.Descripcion;
        vm.Clave = Motivo.Clv_motivo;
      });
    }
    
    function DeleteMotivoR(){
        CatalogosFactory.GetBORMOTIVOSFACTURACANCELACION(vm.Clave).then(function(data){
            if(data.GetBORMOTIVOSFACTURACANCELACIONResult == 1){  
                var log={
                    'Modulo':'home.motivos',
                    'Submodulo':'home.motivos.ReimpresionFactura',
                    'Observaciones':'Se eliminó un motivo de reimpresión de factura',
                    'Comando':JSON.stringify(vm.Clave),
                    'Clv_afectada': vm.Clave,
                    'IdClassLog':3
                };
                logFactory.AddMovSist(log).then(function(result){ console.log('add'); });  

                ngNotify.set('CORRECTO, Se eliminó un Motivo de Reimpresión de Factura.', 'success');
                cancel();
            }else{
                ngNotify.set('ERROR, Al eliminar un Motivo de Reimpresión de Factura.', 'warn');
            }
        });
    }

    function cancel() {
        $uibModalInstance.close();
    }

    var vm = this;
    vm.Titulo = 'Eliminar Motivo de Reimpresion';
    vm.Icono = 'fa fa-less';
    vm.DeleteMotivoR = DeleteMotivoR;
    vm.cancel = cancel;
    initData();
});