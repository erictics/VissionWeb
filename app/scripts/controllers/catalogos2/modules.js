'use strict';

angular
    .module('softvApp')
    .config(function ($stateProvider) {
        var states = [{
            name: 'home.motivos',
            abstract: true,
            template: '<div ui-view></div>'
          },
          {
            name: 'home.motivos.MotivosDeCancelacion',
            data: {
              pageTitle: 'SOFTV | MOTIVOS DE CANCELACION',
              permissions: {
                only: ['motivosdecancelacionSelect'],
                options: {
                    reload: false
                }
            }
            },
                url: '/MotivosDeCancelacion',
                templateUrl: 'views/catalogos/MotivosDeCancelacion.html',
                controller: 'MotivosDeCancelacionCtrl',
                controllerAs: '$ctrl'
            },
            {
                name: 'home.motivos.CancelacionFactura',
                data: {
                  pageTitle: 'SOFTV | MOTIVOS DE CANCELACION DE FACTURAS',
                  permissions: {
                    only: ['motivoscancelacionfacturasSelect'],
                    options: {
                        reload: false
                    }
                }
                },
                    url: '/MotivosDeCancelacionFactura',
                    templateUrl: 'views/catalogos/MotivosCancelacionFactura.html',
                    controller: 'MotivosDeCancelacionFacturaCtrl',
                    controllerAs: '$ctrl'
                },
                {
                    name: 'home.motivos.ReimpresionFactura',
                    data: {
                      pageTitle: 'SOFTV | MOTIVOS DE REIMPRESION DE FACTURAS',
                      permissions: {
                        only: ['motivosreimpresionfacturaSelect'],
                        options: {
                            reload: false
                        }
                    }
                    },
                        url: '/MotivosDeReimpresionFactura',
                        templateUrl: 'views/catalogos/MotivosReImpresionFactura.html',
                        controller: 'MotivosReImpresionFacturaCtrl',
                        controllerAs: '$ctrl'
             },             
            {
                name: 'home.areatecnica.clasificacionproblemas',
                data: {
                  pageTitle: 'SOFTV | CLASIFICACION DE PROBLEMAS',
                  permissions: {
                    only: ['motivosreimpresionfacturaSelect'],
                    options: {
                        reload: false
                    }
                }
                },
                    url: '/clasificacionproblemas',
                    templateUrl: 'views/catalogos/clasificacionProblemas.html',
                    controller: 'clasificacionProblemasCtrl',
                    controllerAs: '$ctrl'
            },
            {
                name: 'home.catalogos.aparatosrenta',
                data: {
                    pageTitle: 'SOFTV | RENTA DE APARATOS',
                    permissions: {
                        only: ['motivosreimpresionfacturaSelect'],
                        options: {
                            reload: false
                        }
                    }
                },
                    url: '/aparatosrenta',
                    templateUrl: 'views/catalogos/RentaAparatos.html',
                    controller: 'RentaAparatosCtrl',
                    controllerAs: '$ctrl'
               },
               {
                name: 'home.catalogos.aparatosrentanuevo',
                data: {
                    pageTitle: 'SOFTV | RENTA DE APARATOS',
                    permissions: {
                        only: ['motivosreimpresionfacturaSelect'],
                        options: {
                            reload: false
                        }
                    }
                },
                    url: '/aparatosrenta/nuevo',
                    templateUrl: 'views/catalogos/RentaAparatosNuevo.html',
                    controller: 'RentaAparatosNuevoCtrl',
                    controllerAs: '$ctrl'
               },
               {
                name: 'home.catalogos.aparatosrentaedita',
                data: {
                    pageTitle: 'SOFTV | RENTA DE APARATOS',
                    permissions: {
                        only: ['motivosreimpresionfacturaSelect'],
                        options: {
                            reload: false
                        }
                    }
                },
                    url: '/aparatosrenta/edita/:id',
                    templateUrl: 'views/catalogos/RentaAparatosNuevo.html',
                    controller: 'RentaAparatosEditaCtrl',
                    controllerAs: '$ctrl'
               }           
        ];
        states.forEach(function (state) {
            $stateProvider.state(state);
        });
    });