"use strict";
angular
  .module("softvApp")
  .controller("ReportesVarios2Ctrl", function(
    $state,
    reportesFactory,
    reportesVariosFactory,
    $filter,
    globalService,
    $sce,
    $localStorage,
    CatalogosFactory,
    $timeout ,
    $window
  ) {

     this.$onInit = function() {
      reportesVariosFactory.mostrarTipServ().then(function(data) {
        vm.tipoServicios = data.GetTipServicioListResult;
        vm.ServicioDigitalInternet = data.GetTipServicioListResult[0];
      });
      llenarMes(); 
      llenarOpMes();
      //anioActual(); 
    };

    function GetReport() {
      if (vm.ordenRadio ==="1")
      {
        vm.OpOrdenar = 1
      }else if (vm.ordenRadio ==="2")
      {
        vm.OpOrdenar = 2
      }
       
      if ( vm.Reporte === "DESCONECTADOS" ||  vm.Reporte === "SUSPENDIDOS" ||  vm.Reporte === "PORPAGAR") {
        var estatus = "";
        estatus =  vm.Reporte === "DESCONECTADOS" ? "D" : estatus;
        estatus =  vm.Reporte === "SUSPENDIDOS" ? "S" : estatus;
        estatus =  vm.Reporte === "PORPAGAR" ? "I" : estatus;     
        
        var ultimoMesNew;
        var anioNew;
        if (vm.Reporte === "DESCONECTADOS" ||  vm.Reporte === "SUSPENDIDOS"){
          anioNew = 0;
          ultimoMesNew = 0; //vm.selectedMes = 0;
        } else {
          ultimoMesNew = vm.selectedMes.mesId;
          anioNew = vm.Anio;
        }

        var obj = {
          'estatus': estatus,
          'Clv_TipSer': vm.ServicioDigitalInternet.Clv_TipSer,
          'distribuidores': vm.responseparams.distribuidores,
          'plazas': vm.responseparams.plazas,
          'ciudades': vm.responseparams.ciudades,
          'localidades': vm.responseparams.localidades,
          'colonias': vm.responseparams.colonias,
          'servicios': vm.responseparams.servicios,
          'periodos': vm.responseparams.periodos,
          'tiposcliente': vm.responseparams.tiposcliente,
          'ultimoMes': ultimoMesNew,//vm.selectedMes.mesId ,
          'ultimoAnio': anioNew, //vm.Anio,
          'OpOrdenar': vm.OpOrdenar,
          'soloInternet':vm.soloInter
        };
    
        reportesFactory.GetReportesVarios_2(obj).then(function(result) {
          $timeout(function() {
            vm.showfilters = false;
            angular.element('#reset').triggerHandler('click');
          });
          vm.rptpanel = true;
          vm.url = $sce.trustAsResourceUrl(
            globalService.getUrlReportes() +
              "/Reportes/" +
              result.GetReportesVarios_2Result
          );
        });
      }

      if( vm.Reporte === "ALCORRIENTE" ||
      vm.Reporte === "ADELANTADOS" ||
      vm.Reporte === "PORINSTALAR"){
        var estatus =   vm.Reporte       
        var obj = {
          'estatus': estatus,
          'Clv_TipSer': vm.ServicioDigitalInternet.Clv_TipSer,
          'distribuidores': vm.responseparams.distribuidores,
          'plazas': vm.responseparams.plazas,
          'ciudades': vm.responseparams.ciudades,
          'localidades': vm.responseparams.localidades,
          'colonias': vm.responseparams.colonias,
          'servicios': vm.responseparams.servicios,
          'periodos': vm.responseparams.periodos,
          'tiposcliente': vm.responseparams.tiposcliente,
          'OpOrdenar':vm.ordenRadio,
          'soloInternet':vm.soloInter
          };          
        reportesFactory.GetReportesVarios_1(obj).then(function(result){
          $timeout(function() {
            vm.showfilters = false;
            angular.element('#reset').triggerHandler('click');
          });
          vm.rptpanel=true;
          vm.url = $sce.trustAsResourceUrl(globalService.getUrlReportes() + '/Reportes/' + result.GetReportesVarios_1Result);
      }); 
      }
      if(vm.Reporte === "INSTALACIONES" || vm.Reporte === "CONTRATACIONES" ||
      vm.Reporte === "CANCELACIONES" || vm.Reporte === "FUERAAREA" ){
  
      var motivoNew = 0;
        if ( vm.Reporte === "CANCELACIONES")
        {
          motivoNew = vm.motivo.Clv_motivo;
        }   
      

        var data = {
          'fechasolInicial':$filter('date')(vm.fechainicio, 'yyyy/MM/dd'),
          'fechasolFinal':$filter('date')(vm.fechafin, 'yyyy/MM/dd'),
          'estatus':vm.Reporte,
          'OpOrdenar':vm.ordenRadio,
          'Clv_TipSer':vm.ServicioDigitalInternet.Clv_TipSer,
          'distribuidores': vm.responseparams.distribuidores,
          'plazas': vm.responseparams.plazas,
          'ciudades': vm.responseparams.ciudades,
          'localidades': vm.responseparams.localidades,
          'colonias': vm.responseparams.colonias,
          'servicios': vm.responseparams.servicios,
          'periodos': vm.responseparams.periodos,
          'tiposcliente': vm.responseparams.tiposcliente,
          'MotCan': motivoNew,//vm.motivo.Clv_motivo,
          'soloInternet':vm.soloInter
        };

        reportesFactory.GetReportesVarios_3(data).then(function(result){
          $timeout(function() {
            vm.showfilters = false;
            angular.element('#reset').triggerHandler('click');
          });
          vm.rptpanel=true;
          vm.url = $sce.trustAsResourceUrl(globalService.getUrlReportes() + '/Reportes/' + result.GetReportesVarios_3Result);
      }); 

      }
      if(vm.Reporte === "CIUDAD" || vm.Reporte === "RESCOLYSTATUS")
      {
        var ultimoMesNew, anioNew, caracter;

        if (vm.buscarPor == false)
        { 
          ultimoMesNew = 0;  //vm.selectedMes = 0
          anioNew = 0//vm.Anio = 0
          caracter = 0
        }
        else {
          ultimoMesNew = vm.selectedMes.mesId;  
          anioNew = vm.Anio;
          caracter = vm.selectedOpMes.opMesId;
        }
       
        var Op = (vm.Reporte === "CIUDAD")?1:2;
        var data = {
          'Clv_TipSer':vm.ServicioDigitalInternet.Clv_TipSer,
          'MotCan':0,
          'conectado':Isselected("C"),
          'baja':Isselected("B"),
          'instalacion':Isselected("I"),
          'desconectado':Isselected("D"),
          'suspendido':Isselected("S"),
          'fueraArea':Isselected("F"),
          'DescTmp':Isselected("T"),
          'OpOrdenar': vm.ordenRadio,
          'distribuidores': vm.responseparams.distribuidores,
          'plazas': vm.responseparams.plazas,
          'ciudades': vm.responseparams.ciudades,
          'localidades': vm.responseparams.localidades,
          'colonias': vm.responseparams.colonias,
          'servicios': vm.responseparams.servicios,
          'periodos': vm.responseparams.periodos,
          'tiposcliente': vm.responseparams.tiposcliente,
          'calles':vm.responseparams.calles,
          'Op':Op,
          'soloInternet':vm.soloInter,
          'ultimoMes': ultimoMesNew, //vm.selectedMes.mesId ,
          'ultimoAnio': anioNew,//vm.Anio,
          'opCaracter': caracter
        }
              
        reportesFactory.GetReportesVarios_4(data).then(function(result){
          $timeout(function() {
            vm.showfilters = false;
            angular.element('#reset').triggerHandler('click');
          });
          vm.rptpanel=true;
          vm.url = $sce.trustAsResourceUrl(globalService.getUrlReportes() + '/Reportes/' + result.GetReportesVarios_4Result);
        });               
      }
        if(vm.Reporte === "PAQUETECORTESIA"){
      
        var Op=(vm.Reporte === "PAQUETECORTESIA")?1:2;
        var data={
          'fecha1':$filter('date')(vm.fechainicio, 'yyyy/MM/dd'),
          'fecha2':$filter('date')(vm.fechafin, 'yyyy/MM/dd'),
          'Clv_TipSer':vm.ServicioDigitalInternet.Clv_TipSer,  
          'OpOrdenar': vm.ordenRadio,
          'distribuidores': vm.responseparams.distribuidores,
          'plazas': vm.responseparams.plazas,
          'ciudades': vm.responseparams.ciudades,
          'localidades': vm.responseparams.localidades,
          'colonias': vm.responseparams.colonias,
          'servicios': vm.responseparams.servicios,
          'periodos': vm.responseparams.periodos,
          'tiposcliente': vm.responseparams.tiposcliente,
          'calles':vm.responseparams.calles,
          'Op':Op,
          'soloInternet':vm.soloInter
        }     
        reportesFactory.GetReportesVarios_5(data).then(function(result){       
          $timeout(function() {
            vm.showfilters = false;
            angular.element('#reset').triggerHandler('click');
          });
          vm.rptpanel=true;
          vm.url = $sce.trustAsResourceUrl(globalService.getUrlReportes() + '/Reportes/' + result.GetReportesVarios_5Result);
        }); 
      
      }
     
      getFilters();
      //  reiniciaVar();
    }

    function getFilters() {

      reiniciaVar();  
      if ( vm.Reporte === "RESCOLYSTATUS" || vm.Reporte === "CIUDAD" || vm.Reporte === "PORPAGAR"
          || vm.Reporte === "CONTRATACIONES" || vm.Reporte === "INSTALACIONES" || vm.Reporte === "CANCELACIONES" )
      {

        if (vm.Reporte === "CONTRATACIONES" || vm.Reporte === "INSTALACIONES" || vm.Reporte === "CANCELACIONES")
        {
          if (vm.fechainicio === "" && vm.fechafin === "")
          {
             vm.bloquearGeneraRep = 'bloquear';
          }
          else {
            vm.bloquearGeneraRep = 'desbloquear';
          }
        }
        else
        {
          vm.bloquearGeneraRep = 'bloquear';
        }
      }
      else
      { 
        vm.bloquearGeneraRep = 'desbloquear';
      }

      $timeout(function() {
        angular.element('#reset').triggerHandler('click');
      });
      vm.report = "RVDesconectados";    
      if (
        vm.Reporte === "DESCONECTADOS" ||
        vm.Reporte === "SUSPENDIDOS" ||
        vm.Reporte === "ALCORRIENTE" ||
        vm.Reporte === "ADELANTADOS" ||
        vm.Reporte === "PORINSTALAR"
      ) {
       
        vm.order = [
          { step: 1, function: "getplazas" },
          { step: 2, function: "getEstadosByPlaza" },
          { step: 3, function: "getCiudadesByEstado" },
          { step: 4, function: "getLocalidadesByCiudades" },
          { step: 5, function: "getColoniasByLocalidad" },
          { step: 6, function: "getCallesByColonia" },
          { step: 7, function: "getServiciosRV" },
          { step: 8, function: "getTipoCliente" },
          { step: 9, function: "getPeriodos" },
          { step: 10, function: "getReporBtn" }
        ];             
        
      }
      if(vm.Reporte === "INSTALACIONES" ||
        vm.Reporte === "CONTRATACIONES" ||
        vm.Reporte === "CANCELACIONES" ||
        vm.Reporte === "FUERAAREA" 
      ){
        vm.order = [
          { step: 1, function: "getplazas" },
          { step: 2, function: "getEstadosByPlaza" },
          { step: 3, function: "getCiudadesByEstado" },
          { step: 4, function: "getLocalidadesByCiudades" },
          { step: 5, function: "getColoniasByLocalidad" },
          { step: 6, function: "getCallesByColonia" },
          { step: 7, function: "getServiciosRV" },
          { step: 8, function: "getTipoCliente" },
          { step: 9, function: "getPeriodos" },
          { step: 10, function: "getReporBtn" }
        ];            

      }
      if(vm.Reporte === "CIUDAD"){
        vm.order = [
          { step: 1, function: "getplazas" },
          { step: 2, function: "getEstadosByPlaza" },
          { step: 3, function: "getCiudadesByEstado" },
          { step: 4, function: "getLocalidadesByCiudades" },
          { step: 5, function: "getColoniasByLocalidad" },
          { step: 6, function: "getCallesByColonia" },
          { step: 7, function: "getServiciosRV" },
          { step: 8, function: "getTipoCliente" },
          { step: 9, function: "getPeriodos" },
          { step: 10, function: "getReporBtn" }
        ]; 
      }
      if(vm.Reporte === "PAQUETECORTESIA"){
        vm.order = [
          { step: 1, function: "getplazas" },
          { step: 2, function: "getEstadosByPlaza" },
          { step: 3, function: "getCiudadesByEstado" },
          { step: 4, function: "getLocalidadesByCiudades" },
          { step: 5, function: "getColoniasByLocalidad" },
          { step: 6, function: "getCallesByColonia" },
          { step: 7, function: "getServiciosRV" },
          { step: 8, function: "getTipoCliente" },
          { step: 9, function: "getPeriodos" },
          { step: 10, function: "getReporBtn" }
        ]; 
      }
    }




    function getmotivos(){
      var OjbMotivo = {
          'Clv_MOTCAN': 0,
          'MOTCAN': 0,
          'op': 3
      };

      var motivo1 = {
        'Clv_motivo': 0,
        'Descripcion':'TODOS'
      }

        vm.motivo = undefined;

        CatalogosFactory.GetBuscaMotivoCancelacion(OjbMotivo).then(function(data){ 
          vm.MotivoCancelacionList = data.GetBuscaMotivoCancelacionResult;
          vm.MotivoCancelacionList.push(motivo1); 
        });
    }


   function Isselected(clave){ 

   var result=[];
   vm.listaStatus.forEach(function(element) {
      if( element.clave === clave && element.selected===true){
        result.push(element)
      }
     });   
     return result.length > 0 ? true:false;   
   }

   function llenarOpMes(){
      var opMesData = [];      
          opMesData.unshift(
            {
            'opMesName': 'Menor o Igual',
            'opMesId': 1
            } ,
            {
              'opMesName': 'Igual',
              'opMesId': 2
            }     
          );   
          vm.selectOpMes = opMesData;
          vm.selectedOpMes = opMesData[0]; 
   }

   function llenarMes(){

    var d = new Date();
    var mesActual = d.getMonth();   

    var mesData = [];   
    vm.seletedMes = undefined; 
          mesData.unshift(
             // {
             // 'mesName': 'Selecciona un mes',
             // 'mesId': 0
             // } ,
            {
            'mesName': 'Enero',
            'mesId': 1
            } ,
            {
              'mesName': 'Febrero',
              'mesId': 2
            } ,
             {
              'mesName': 'Marzo',
              'mesId': 3
            } ,
             {
              'mesName': 'Abril',
              'mesId': 4
            } ,
             {
              'mesName': 'Mayo',
              'mesId': 5
            } ,
            {
              'mesName': 'Junio',
              'mesId': 6
            } ,
             {
              'mesName': 'Julio',
              'mesId': 7
            },
             {
              'mesName': 'Agosto',
              'mesId': 8
            },
             {
              'mesName': 'Septiembre',
              'mesId': 9
            },
             {
              'mesName': 'Octubre',
              'mesId': 10
            },
             {
              'mesName': 'Noviembre',
              'mesId': 11
            },
              {
              'mesName': 'Diciembre',
              'mesId': 12
            }
          );   
          vm.selectMes = mesData;  
          vm.selectedMes = mesData[mesActual] ;
          //vm.selectedMes = mesData[0];    
          /*vm.selectedMes = mesData[0]; */
   }



     function bloqueaMotivo()
     {
        //bloquearBtnReporte();
      
        var bloquear = vm.listaStatus.some(function(item){ return item.clave=='B' && item.selected ==true });     
        if (bloquear == true )
        { 
          vm.motivoBloqueado = false;
        }
        else 
        {
          vm.motivoBloqueado = true;
          vm.motivo = undefined;
        } 
        bloquearBtnReporte();      
     }


     
     function bloquearBtnReporte()
     {          
          var con = vm.listaStatus.some(function(item){ return item.clave=='C' && item.selected ==true });
          var fa = vm.listaStatus.some(function(item){ return item.clave=='F' && item.selected ==true });
          var ins = vm.listaStatus.some(function(item){ return item.clave=='I' && item.selected ==true });
          var st = vm.listaStatus.some(function(item){ return item.clave=='T' && item.selected ==true });
          var des = vm.listaStatus.some(function(item){ return item.clave=='D' && item.selected ==true });
          var canc = vm.listaStatus.some(function(item){ return item.clave=='B' && item.selected ==true });    
          var sus = vm.listaStatus.some(function(item){ return item.clave=='S' && item.selected ==true });  
         
        if (con === false && fa === false && ins === false && st === false && des === false && canc === false && sus === false )
        {
          vm.bloquearGeneraRep = 'bloquear';        
        }
        else 
        {
          if (canc === true){           
            if ((typeof vm.motivo === "undefined") || (vm.motivo === null))
            {
              vm.bloquearGeneraRep = 'bloquear';
            }
            else {
              vm.bloquearGeneraRep = 'desbloquear';
            }
          }
          else
          {
            vm.bloquearGeneraRep = 'desbloquear';
          }

          if (vm.buscarPor === true )
          { 
            //console.log('vm.selectedOpMes: ', vm.selectedOpMes.opMesId ,'vm.selectedMes: ' ,vm.selectedMes.mesId, 'vm.Anio: ',vm.Anio);
            if (vm.selectedOpMes.opMesId >= 0 && vm.selectedMes.mesId >= 0 && (typeof vm.Anio !== "undefined" && vm.Anio !== null && vm.Anio >= 0 ) ){
              vm.bloquearGeneraRep = 'desbloquear';
            }
            else{
              vm.bloquearGeneraRep = 'bloquear';            
            }
          }
        }
     }
  


     function cancelar()
     {
       $window.location.reload();
     }

    //  function anioActual()
    //  {
    //   var fecha = new Date();
    //   vm.Anio = fecha.getFullYear();
    //  }

    /*
    function anioActual(){
      vm.Anio = "";
    }*/   

    function anioPorPagar(){
      //console.log('anio ', vm.Anio ,', selectedmes ', vm.selectedMes.mesId)
       if( vm.Reporte === "PORPAGAR" )
       {
          if (vm.Anio > 0 && vm.selectedMes.mesId > 0)
          {
            vm.bloquearGeneraRep = 'desbloquear';
          }
          else{
            vm.bloquearGeneraRep = 'bloquear';
          }
      }

      if( vm.Reporte === "INSTALACIONES" ||vm.Reporte === "CONTRATACIONES")
      {   
        if (vm.fechainicio !== "" && vm.fechafin !== "")
        {
            vm.bloquearGeneraRep = 'desbloquear';
        }
        else {
          vm.bloquearGeneraRep = 'bloquear';
        }
      }
      
      if( vm.Reporte === "CANCELACIONES")
      { 
        if ((typeof vm.motivo !== "undefined") || (vm.motivo !== null) || (vm.motivo.Clv_motivo >= 0))
        {
          if (vm.fechainicio !== "" && vm.fechafin !== "")
          {
              vm.bloquearGeneraRep = 'desbloquear';
          }
          else {
            vm.bloquearGeneraRep = 'bloquear';
          }
        }
        else {
          vm.bloquearGeneraRep = 'bloquear';
        }
      }     

    }

     function reiniciaVar(){ 
        vm.listaStatus = [];    
        vm.listaStatus = [
          {clave:'C',nombre:'Contratado'},
          {clave:'I',nombre:'Instalado'},
          {clave:'D',nombre:'Desconectados'},
          {clave:'S',nombre:'Suspendidos'},
          {clave:'F',nombre:'Fuera de area'},
          {clave:'T',nombre:'Suspendidos Temporalmente'},
          {clave:'B',nombre:'Cancelados'},
        ];
        vm.bloqueaMotivo = bloqueaMotivo;
        vm.motivoBloqueado = true;
        vm.buscarPor = false;
        vm.soloInter = false;     
        vm.generaBloqueado = true;
        vm.cancelar = cancelar;
        vm.bloquearGeneraRep = 'bloquear';
        vm.Anio = undefined;   
        vm.fechainicio = new Date();//vm.fechainicio = "";
        vm.fechafin =new Date(); //vm.fechafin = "";

        llenarMes(); //mes
        anioPorPagar(); 
        getmotivos();
     }

    var vm = this;  
        vm.getFilters = getFilters;
        vm.GetReport = GetReport;
        vm.responseparams = {};
        vm.showfilters = false;
        vm.rptpanel = false;
        vm.op = 0;
        vm.ordenRadio='1';
        vm.tiporeporte='1';
        vm.order = [];
        vm.Reporte = 'DESCONECTADOS';
        vm.listaStatus = [
          {clave:'C',nombre:'Contratado'},
          {clave:'I',nombre:'Instalado'},
          {clave:'D',nombre:'Desconectados'},
          {clave:'S',nombre:'Suspendidos'},
          {clave:'F',nombre:'Fuera de area'},
          {clave:'T',nombre:'Suspendidos Temporalmente'},
          {clave:'B',nombre:'Cancelados'},
        ];
        vm.bloqueaMotivo = bloqueaMotivo;
        vm.motivoBloqueado = true;
        vm.buscarPor = false;
        vm.soloInter = false;          
        vm.generaBloqueado = true;
        vm.cancelar = cancelar;
        vm.bloquearGeneraRep = 'desbloquear';
        vm.anioPorPagar = anioPorPagar;

    getFilters();
    getmotivos(); 
  });
