"use strict";
angular
  .module("softvApp")
  .controller("ReporteAcumuladoCICtrl", function($state, $localStorage, $rootScope, $sce, reportesFactory, ngNotify, $filter, globalService, $window) {

    function Init() {  
      var menuData = [];      
          menuData.unshift(
            {
            'reportName': 'Reporte Acumulado de Clientes e Ingresos',
            'reportId': 0
            } ,
            {
              'reportName': 'Reporte Acumulado de Clientes e Ingresos Histórico',
              'reportId': 1
            }   
          );   
          vm.selectMenu = menuData;
          vm.selectedMenu = menuData[-1]; 
          // menuChange();     
     }


    function menuChange() {
      if (vm.selectedMenu.reportId == 0 ) {
        vm.showfecha = false;     
        vm.order = [
            { step: 1, function: "getplazas", confirm: false },
            { step: 2, function: "getFiltroAcumulado_ClientesIngresos", confirm: false },          
        ];
        // vm.showOpcionReporte = false;
       }  else {
        vm.showfecha = true;      
        vm.order = [
          { step: 1, function: "getplazas", confirm: false },
          { step: 2, function: "getFiltroAcumulado_ClientesIngresosHistorico", confirm: false }  ,  
        ];  
      //  vm.showOpcionReporte = false;
      }
      vm.showOpcionReporte = false; 
    }
    
    function cancelar(){
      $window.location.reload();
    }


    function aceptar() {
      vm.showOpcionReporte = false; 
     // vm.showfecha = true; 
      var plazas= vm.responseparams.plazas;
      if (vm.selectedMenu.reportId == 0) {
        reportesFactory.GetReporteAcumuladoCI_Actual( plazas).then(function(result) {  
          var Name = result.GetReporteAcumuladoDeClientesEIngresosResult;
          var FileName = globalService.getUrlReportes() + '/Reportes/' + Name;
          vm.FileName = $sce.trustAsResourceUrl(FileName);
        });
      }else{
        var fecha = $filter("date")(vm.fechainicio, "dd/MM/yyyy");
       if (angular.isUndefined(fecha) == false){
        reportesFactory.GetReporteAcumuladoCI_Historico( plazas, fecha).then(function(result) {                
          var Name = result.GetReporteAcumuladoDeClientesEIngresosHistoricoResult;
          var FileName = globalService.getUrlReportes() + '/Reportes/' + Name;
          vm.FileName = $sce.trustAsResourceUrl(FileName);
        });
      } 
      }
    }

    
    var vm = this;
    vm.report = "Reporte de Ingreso por Sucursal y Plaza";
    vm.responseparams = {};    
    vm.showfilters = false;
    vm.showfecha = false;
    vm.showOpcionReporte = true;
    vm.aceptar = aceptar;
    vm.cancelar= cancelar;
    vm.menuChange = menuChange;
    vm.fechainicio= new Date();
    Init();

      

  });
