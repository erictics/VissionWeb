"use strict";
angular
  .module("softvApp")
  .controller("reporteNotaCreditoCtrl", function(
    $state,
    $localStorage,
    $rootScope,
    $sce,
    reportesFactory,
    ngNotify,
    $filter,
    globalService,
    reportesVariosFactory
  ) {

    function aceptar() {
        var finicio = $filter("date")(vm.fechainicio, "yyyy/MM/dd");
        var ffin = $filter("date")(vm.fechafin, "yyyy/MM/dd");     
        vm.distribuidoresObj.selectedItems.forEach(function(item){
          item.nombre=item.Nombre;
        });
       reportesFactory.GetReporteNotasCredito(vm.distribuidoresObj.selectedItems,finicio,ffin).then(function(result){
            vm.url = $sce.trustAsResourceUrl(globalService.getUrlReportes() + '/Reportes/' + result.GetReporteNotasCreditoResult);
        }); 
    }

    function getDistribuidores() {
        reportesVariosFactory.mostrarDistribuidorByUsuario($localStorage.currentUser.idUsuario)
          .then(function (data) {
            vm.distribuidoresObj = {
              filterPlaceHolder: 'filtrar Empresa',
              labelAll: 'Todas las Empresas',
              labelSelected: 'Empresa seleccionadas',
              labelShow: 'Nombre',
              orderProperty: 'Nombre',
              items: data.GetDistribuidorByUsuarioResult,
              selectedItems: []
            };
          });
      }
    var vm = this;   
    vm.aceptar = aceptar;       
    getDistribuidores();
  });