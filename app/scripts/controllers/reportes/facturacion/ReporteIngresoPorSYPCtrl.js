"use strict";
angular
  .module("softvApp")
  .controller("ReporteIngresoPorSYPCtrl", function($state, $localStorage, $rootScope, $sce, reportesFactory, ngNotify, $filter, globalService
  ) {
    function Init() {}

    function aceptar() {
      var finicio = $filter("date")(vm.fechainicio, "dd/MM/yyyy");
      var ffin = $filter("date")(vm.fechafin, "dd/MM/yyyy");     
      var plazas= vm.responseparams.plazas;
      var sucursales = vm.responseparams.sucursales;
       if (angular.isUndefined(finicio) == false || angular.isUndefined(ffin) == false){
      reportesFactory.GetReporteDeIngresoPorSucursalYPlaza( plazas, sucursales, finicio, ffin).then(function(result) {               
        var Name1 = result.GetReporteDeIngresoPorSucursalYPlazaResult;
        var FileName1 = globalService.getUrlReportes() + '/Reportes/' + Name1;
        vm.FileName1 = $sce.trustAsResourceUrl(FileName1);
      });

      reportesFactory.GetReporteCobranzaCruzada( plazas, finicio, ffin).then(function(result) {
        var Name2 = result.GetReporteCobranzaCruzadaResult;        
        var FileName2 = globalService.getUrlReportes() + '/Reportes/' + Name2;
        vm.FileName2 = $sce.trustAsResourceUrl(FileName2);

      });

      reportesFactory.GetReporteEntregaDeDinero( sucursales, finicio, ffin).then(function(result) {      
        var Name3 = result.GetReporteEntregaDeDineroResult;
        var FileName3 = globalService.getUrlReportes() + '/Reportes/' + Name3;
        vm.FileName3 = $sce.trustAsResourceUrl(FileName3);
      });
      }
    }


    var vm = this;
    vm.report = "Reporte de Ingreso por Sucursal y Plaza";
    vm.responseparams = {};    
    vm.showfilters = false;
    vm.aceptar = aceptar;
    vm.Reporte1 = "";
    vm.Reporte2 = "";
    vm.Reporte3 = "";

    vm.order = [
        { step: 1, function: "getplazas", confirm: false },
        { step: 2, function: "getSucursalesByPlaza", confirm: false },
        { step: 3, function: "getFiltroIngresoPorSucursalYPlaza", confirm: false }
    ];


    vm.FileName1 = "";
    vm.FileName2 = "";
    vm.FileName3 = "";
    vm.fechainicio = new Date();
    vm.fechafin = new Date();

    Init();
  });
