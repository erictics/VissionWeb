"use strict";
angular
  .module("softvApp")
  .controller("reporteBonificacionesCtrl", function(
    $state,
    $localStorage,
    $rootScope,
    $sce,
    reportesFactory,
    ngNotify,
    $filter,
    globalService
  ) {
    function Init() {}

    function aceptar() {
      var finicio = $filter("date")(vm.fechainicio, "dd/MM/yyyy");
      var ffin = $filter("date")(vm.fechafin, "dd/MM/yyyy");
      var aplicada= (vm.estatus==='A')?1:0;
      var plazas= vm.responseparams.plazas;
      console.log(finicio,ffin);
      console.log(aplicada);
      console.log(plazas);      
      reportesFactory.GetReporteBonificacion(aplicada,finicio,ffin,plazas).then(function(result) {
        console.log(result);
        vm.url = $sce.trustAsResourceUrl(globalService.getUrlReportes() + '/Reportes/' + result.GetReporteBonificacionResult);
     });
    }

    var vm = this;
    vm.report = "BONIFICACIONES";
    vm.responseparams = {};
    vm.estatus='A';
    vm.showfilters = false;
    vm.aceptar = aceptar;
    vm.order = [
        { step: 1, function: "getplazas", confirm: false },
        { step: 2, function: "GetFiltroBONIFICACIONES", confirm: false },
        
    ];
    Init();
  });
