'use strict';
angular
	.module('softvApp')
	.controller('reporteRelacionIngresosCtrl', function($state,$sce,reportesVariosFactory, $localStorage, $rootScope, reportesFactory, ngNotify, $filter, globalService) {
      
        function aceptar(){            
            var finicio = $filter("date")(vm.fechainicio, "yyyy/MM/dd");
            var ffin = $filter("date")(vm.fechafin, "yyyy/MM/dd");         
            console.log(finicio);
            console.log(ffin);
            console.log(vm.responseparams.plazas);       
            reportesFactory.GetReporteRelacionIngresosDistribuidor(0,finicio,ffin,vm.responseparams.plazas).then(function(result){
                vm.url = $sce.trustAsResourceUrl(globalService.getUrlReportes() + '/Reportes/' + result.GetReporteRelacionIngresosDistribuidorResult);
            });
        }

        /* function getDistribuidores() {
            reportesVariosFactory.mostrarDistribuidorByUsuario($localStorage.currentUser.idUsuario)
              .then(function (data) {
                vm.distribuidoresObj = {
                  filterPlaceHolder: 'filtrar Empresa',
                  labelAll: 'Todas las Empresas',
                  labelSelected: 'Empresa seleccionadas',
                  labelShow: 'Nombre',
                  orderProperty: 'Nombre',
                  items: data.GetDistribuidorByUsuarioResult,
                  selectedItems: []
                };
              });
          } */


        /*   function getplazas() {
            
            reportesVariosFactory.mostrarPlazaByDistribuidor($localStorage.currentUser.idUsuario, vm.options.selectedItems)
              .then(function (result) {
                vm.distribuidoresObj = {
                    filterPlaceHolder: 'Buscar regiones',
                    labelAll: 'Todas las Regiones',
                    labelSelected: 'Regiones seleccionadas',
                    labelShow: 'razon_social',
                    orderProperty: 'razon_social',
                    items: result.GetPlazasByDistribuidorResult,
                    selectedItems: []
                  };               
              });
          } */
      
        
     
      
        /* vm.getplazas=getplazas;  
        getDistribuidores();
        vm.blockGenera=true; */


        var vm=this;
        vm.report='RELACIONINGRESOS';
        vm.aceptar=aceptar;   
        vm.responseparams={};
        vm.showfilters=false;
        vm.rptpanel=false;
        vm.tipoOrden= {};
        vm.order = [
            {  'step': 1,function: 'getplazas',   confirm: false  },          
            { 'step': 2, function: 'getRangosFechas',confirm: true }
        ];
        
       
    });