'use strict';
angular
	.module('softvApp')
	.controller('ReporteCuidadCarteraCtrl', function($state, CiudadCarteraFactory, reportesVariosFactory, reportesFactory, globalService, $sce, $filter, $uibModal, ngNotify, $localStorage) {	


  
		function GetServiciosList(){
			CiudadCarteraFactory.GetServicios().then(function(data) {         
             vm.servicios=data.GetListTipServResult;
             GetFechaList();
             GetDistribuidorList();   
      });
    }

    function SetServicio(){
      GetFechaList();
      GetDistribuidorList();
    }

    function GetFechaList(){
			CiudadCarteraFactory.GetFechas($localStorage.currentUser.idUsuario, vm.servicio.Clv_TipSer).then(function(data) {         
             vm.fechas=data.GetListFechaCiudadCarteraResult;
             var servicio=vm.servicios[1].Clv_TipSer;
             var fecha=vm.fechas[0];           
              GetDetalleCartera(servicio,fecha, vm.TipoReporte);
			});
    }

    function GetDistribuidorList(){
      CiudadCarteraFactory.GetSp_MuestraDistribuidorCartera(vm.servicio.Clv_TipSer, $localStorage.currentUser.idUsuario).then(function(data){
        console.log(data);
        var DistribuidorList = data.GetSp_MuestraDistribuidorCarteraResult;
        DistribuidorList.forEach(function(element) {
          var Obj = {
            'Clv_Plaza': element.Clv_Plaza ,
            'Nombre': element.Nombre
          };
          vm.DistribuidorList.push(Obj);
        });
      });
       /*reportesVariosFactory.mostrarDistribuidorByUsuario($localStorage.currentUser.idUsuario).then(function(data) {
        console.log(data);
        var DistribuidorList = data.GetDistribuidorByUsuarioResult;
        DistribuidorList.forEach(function(element) {
          var Obj = {
            'Clv_Plaza': element.Clv_Plaza ,
            'Nombre': element.Nombre
          };
          vm.DistribuidorList.push(Obj);
        });
       });*/
    }
    
    function CambioFecha(){
      var Clv_Plaza = (vm.Distribuidor != null && vm.Distribuidor != undefined)? vm.Distribuidor.Clv_Plaza:null;
      var Fecha = (vm.fecha != '')? $filter('date')(vm.fecha, 'yyyy/MM/dd'):null;
      if(Fecha != null){
        console.log(Clv_Plaza);
        reportesFactory.GetSoftvweb_GetDistribuidorRegionCartera(Fecha, Clv_Plaza, $localStorage.currentUser.idUsuario)
        .then(function(resp){
          vm.lista=resp.GetSoftvweb_GetDistribuidorRegionCarteraResult;
        console.log(resp);
        });
      }
    }

    function ObtenerReporte(item){
      console.log(item);
      var Clv_TipSer = (item.Clv_TipSerC != null && item.Clv_TipSerC != undefined)? item.Clv_TipSerC:vm.servicio.Clv_TipSer;
      console.log(Clv_TipSer);
      reportesFactory.GetReporteCartera(item.Clv_cartera,Clv_TipSer,item.identificador).then(function(res){
          vm.OpenReporte = true;
          vm.url = $sce.trustAsResourceUrl(globalService.getUrlReportes() + '/Reportes/' + res.GetReporteCarteraResult);
          console.log(res);
      });
    }

    function OpenAddCartera(){
      var Clv_TipSer = vm.servicio.Clv_TipSer
      var modalInstance = $uibModal.open({
          animation: true,
          ariaLabelledBy: 'modal-title',
          ariaDescribedBy: 'modal-body',
          templateUrl: 'views/reportes/AddReporteCuidadCartera.html',
          controller: 'AddCuidadCarteraCtrl',
          controllerAs: 'ctrl',
          backdrop: 'static',
          keyboard: false,
          class: 'modal-backdrop fade',
          size: 'lg',
          resolve: {
            Clv_TipSer: function () {
                  return Clv_TipSer;
              }
          }
      });
      modalInstance.result.then(function (ObjCartera) {
        console.log(ObjCartera);
          CambioFecha();
          if(ObjCartera != null){
            ObtenerReporte(ObjCartera);
          }
      });
    }

/* 	function GetDetalleCartera(servicio,fecha,TipoReporte){
			CiudadCarteraFactory.GetDetalleCartera(servicio,fecha,TipoReporte).then(function(data) {   		
			console.log(data)		
			  vm.carteraurl=$sce.trustAsResourceUrl(globalService.getUrlReportes()+'/reportes/'+data.GetListDetalleCarteraResult[0].Fecha);            
			});
			}

     function ObtenReporte(){
      var tiporeporte=vm.TipoReporte;
      var servicio=vm.servicio;
       var fecha=vm.fecha;
       GetDetalleCartera(servicio,fecha,tiporeporte);

     }; */

     var vm=this;     
    GetServiciosList();
    vm.SetServicio = SetServicio;
   // vm.ObtenReporte = ObtenReporte;
    vm.OpenReporte = false;
    vm.TipoReporte = 'cartera';
    vm.DistribuidorList = [
      {
        'Clv_Plaza': 9999,
        'Nombre': 'Todos'
      }
    ];
    vm.ObtenerReporte  =ObtenerReporte;
    vm.CambioFecha = CambioFecha;
    vm.OpenAddCartera = OpenAddCartera;
	});

