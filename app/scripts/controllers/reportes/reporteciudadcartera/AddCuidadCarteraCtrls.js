'use strict';

angular
    .module('softvApp')
    .controller('AddCuidadCarteraCtrl', function(CiudadCarteraFactory, reportesVariosFactory, $uibModalInstance, $uibModal, $state, Clv_TipSer, $localStorage, ngNotify){

        function iniData(){
            reportesVariosFactory.mostrarDistribuidorByUsuario($localStorage.currentUser.idUsuario).then(function(data) {
                console.log(data);
                var Distribuidores = data.GetDistribuidorByUsuarioResult;
                Distribuidores.forEach(function(element, key){
                    if(element.Clv_Plaza == 9999){
                        Distribuidores.splice(key, 1);
                    }
                });
                Distribuidores.forEach(function(element){
                    var Obj = {
                        'Id': element.Clv_Plaza,
                        'Nombre': element.Nombre };
                    vm.List.push(Obj);    
                });
                /*vm.DistribuidorList = result.GetDistribuidorByUsuarioResult;*/
            });
            /*CiudadCarteraFactory.GetSp_MuestraDistribuidorCartera(Clv_TipSer).then(function(data){
                console.log(data);
                var Distribuidores = data.GetSp_MuestraDistribuidorCarteraResult;
                Distribuidores.forEach(function(element, key){
                    if(element.Clv_Plaza == 9999){
                        Distribuidores.splice(key, 1);
                    }
                });
                Distribuidores.forEach(function(element){
                    var Obj = {
                        'Id': element.Clv_Plaza,
                        'Nombre': element.Nombre };
                    vm.List.push(Obj);    
                });
                console.log(vm.List);
            });*/
        }

        function AddObj(Obj, Op){
            console.log(Obj.Id);
            if(Op == 1){
                vm.ListInc.push(Obj);
                vm.List.forEach(function(element, key){
                    if(element.Id == Obj.Id){
                        vm.List.splice(key, 1);
                    }
                    
                });
            }else if(Op == 2){
                vm.List.push(Obj);
                vm.ListInc.forEach(function(element, key){
                    if(element.Id == Obj.Id){
                        vm.ListInc.splice(key, 1);
                    }
                });
            }
            vm.ALL = false;
            
        }

        function Todo(Op){
            if(Op == 1){
                vm.List.forEach(function(element){
                    vm.ListInc.push(element);
                });
                vm.List = [];
                vm.ALL = true;
                
            }else if(Op == 2){
                vm.ListInc.forEach(function(element){
                    vm.List.push(element);
                });
                vm.ListInc = [];
                vm.ALL = false;
                
            }
        }

        function GetPlazas(){
            var ObjDistribuidor = [];
            vm.title = 'Regiones';
            vm.ListInc.forEach(function(element){
                var Obj = {
                    'Clv_Plaza': element.Id,
                    'Nombre': element.Nombre
                };
                ObjDistribuidor.push(Obj);
            });

            if(vm.ALL == true){
                var Obj={ 'Clv_Plaza' : '9999'};
                vm.ListDist.push(Obj);
            }else{
                vm.ListInc.forEach(function(element){
                    var Obj = {
                        'Clv_Plaza': element.Id};
                    vm.ListDist.push(Obj);
                });
            }
            
            console.log(ObjDistribuidor);
            if (ObjDistribuidor.length > 0) {
                    reportesVariosFactory.mostrarPlazaByDistribuidor($localStorage.currentUser.idUsuario,ObjDistribuidor).then(function(data) {
                        console.log(data);
                        vm.List = [];
                        vm.ListInc =  [];
                        var Plazas = data.GetPlazasByDistribuidorResult;
                        Plazas.forEach(function(element, key){
                            if(element.id_compania == 9999){
                                Plazas.splice(key, 1);
                            }
                        });
                        Plazas.forEach(function(element){
                            var Obj = {
                                'Id': element.id_compania,
                                'Nombre': element.razon_social
                            };
                            
                            vm.List.push(Obj);
                        });
                        
                        vm.Proceso = 2;
                        vm.ALL = false;
                        console.log(vm.List);
                        /*vm.PlazaList = result.GetPlazasByDistribuidorResult;*/
                    });
                }
            /*CiudadCarteraFactory.GetSp_MuestraCompaniaCartera(ObjDistribuidor, Clv_TipSer).then(function(data){
                console.log(data);
                vm.List = [];
                vm.ListInc =  [];
                var Plazas = data.GetSp_MuestraCompaniaCarteraResult;
                Plazas.forEach(function(element, key){
                    if(element.id_compania == 9999){
                        Plazas.splice(key, 1);
                    }
                });
                Plazas.forEach(function(element){
                    var Obj = {
                        'Id': element.id_compania,
                        'Nombre': element.razon_social
                    };
                    
                    vm.List.push(Obj);
                });
                
                vm.Proceso = 2;
                vm.ALL = false;
                console.log(vm.List);
            });*/
        }

        function GetPeriodos(){
            var ObjCompania = [];
            vm.title = 'Periodos';
            vm.ListInc.forEach(function(element){
                var Obj = {
                    'Id_compania': element.id_compania,
                    'Clv_TipSer': Clv_TipSer };
                ObjCompania.push(Obj);
            });

           if(vm.ALL == true){
            var Obj={ 'id_compania' : '9999'};
            vm.ListPla.push(Obj);
           }else{
                vm.ListInc.forEach(function(element){
                  var Obj={ 'id_compania' : element.Id};
                  vm.ListPla.push(Obj);
              });
             } 

            console.log(ObjCompania);
            CiudadCarteraFactory.GetSp_MuestraPeriodoCartera(ObjCompania).then(function(data){
                console.log(data);
                vm.List = [];
                vm.ListInc =  [];
                var Plazas = data.GetSp_MuestraPeriodoCarteraResult;
                Plazas.forEach(function(element, key){
                    if(element.Clv_Periodo == 9999){
                        Plazas.splice(key, 1);
                    }
                });
                Plazas.forEach(function(element){
                    var Obj = {
                        'Id': element.Clv_Periodo,
                        'Nombre': element.Descripcion
                    };
                    vm.List.push(Obj);
                });
                vm.Proceso = 3;
            });
            console.log(vm.ALL);
        }

        function GetGenerarCartera(){
            vm.ListPer=[];
            console.log(vm.ALL);
            if(vm.ALL == true){
                var Obj={ 'Clv_Periodo' : '9999'};
                vm.ListPer.push(Obj);
            }else{
                vm.ListInc.forEach(function(element){
                    var Obj={ 'Clv_Periodo' : element.Id};
                    vm.ListPer.push(Obj);
                });
            }
            
            var ObjCartera = {
                'Clv_TipSer': Clv_TipSer,
                 'Clv_Usuario': $localStorage.currentUser.usuario
            }

            var ObjDis = vm.ListDist;
            var ObjCom = vm.ListPla;
            var ObjPer = vm.ListPer;
            
            console.log(ObjCartera, ObjDis, ObjCom, ObjPer);
            CiudadCarteraFactory.GetGenera_CarteraDIG_PorPeriodo_ALL(ObjCartera, ObjDis, ObjCom, ObjPer).then(function(data){
                if(data.GetGenera_CarteraDIG_PorPeriodo_ALLResult.Clv_Cartera > 0){
                    ngNotify.set('Correcto, La Cartera se ha generado', 'success');
                    vm.ObjCartera = {
                        'Clv_cartera': data.GetGenera_CarteraDIG_PorPeriodo_ALLResult.Clv_Cartera,
                        'Clv_TipSerC': Clv_TipSer,
                        'identificador': 1
                    };
                    cancel();
                }else{
                    ngNotify.set('Error, Al generar la Cartera', 'warn');
                    cancel();
                }
            });
        }

        function Next(){
            if(vm.Proceso == 1){
                GetPlazas();
            }

            if(vm.Proceso == 2){
                GetPeriodos();
            }

            if(vm.Proceso == 3){
                GetGenerarCartera();
            }
        }

        function cancel() {
            $uibModalInstance.close(vm.ObjCartera);
        }

        var vm = this;
        vm.Titulo = 'Nuevo Reporte Ciudad Cartera';
        vm.Icono = 'fa fa-plus';
        vm.title = 'Empresas';
        vm.List = [];
        vm.ListInc = [];
        vm.Proceso = 1;
        vm.ListDist= [];
        vm.ListPla=[];
        vm.ListPer=[];
        vm.ObjCartera = null;
        console.log(Clv_TipSer);
        vm.AddObj = AddObj;
        vm.Todo = Todo;
        vm.Next = Next;
        vm.cancel = cancel;
        vm.ALL = false;
        vm.blockNext = true;
        iniData();
        
    });