'use strict';
angular
	.module('softvApp')
	.controller('ReporteInterfazAparatosCtrl', function($state, reportesVariosFactory, CatalogosFactory, ServiciosFactory, reportesFactory, globalService, $sce, $filter, $localStorage){	

        function initData(){
            reportesVariosFactory.mostrarDistribuidorByUsuario($localStorage.currentUser.idUsuario).then(function(result) {
                vm.Distribuidores = result.GetDistribuidorByUsuarioResult;
                CatalogosFactory.GetTipServ_NewList().then(function(data){
                    vm.Servicios = data.GetTipServ_NewListResult;
                    ServiciosFactory.GetMedioList().then(function(result){
                        vm.tecnologias = result.GetMedioListResult;
                    });
                });
            });
        }

        function GetPlazaList(){
            var distribuidores = [];
            distribuidores.push(vm.distribuidor);
            reportesVariosFactory.mostrarPlazaByDistribuidor($localStorage.currentUser.idUsuario,distribuidores).then(function(result) {
                vm.Plazas = result.GetPlazasByDistribuidorResult;
            });
        }

        function GetReprote(){
            var ObjReporte = {
                'CLVDISTRIBUIDOR':vm.distribuidor.Clv_Plaza,
                'CLVPLAZA': vm.plaza.id_compania,
                'CLVTIPSER':vm.servicio.Clv_TipSer,
                'fecha_habilitar':(vm.fechasolicitud)? $filter('date')(vm.fechasolicitud, 'yyyy/MM/dd'):'9999/09/09',
                'IdMedio': vm.tecnologia.IdMedio,
                'Op': 5
            };
            reportesFactory.GetReporteInterfazAparatos(ObjReporte).then(function(data){
                vm.FileName = $sce.trustAsResourceUrl(globalService.getUrlReportes() + '/Reportes/' + data.GetReporteInterfazAparatosResult);
                vm.OpenReporte = true;
            });
        }

        var vm = this;
        vm.OpenReporte = false;
        vm.GetPlazaList = GetPlazaList;
        vm.GetReprote = GetReprote;
        vm.FileName = "";
        initData();

    });