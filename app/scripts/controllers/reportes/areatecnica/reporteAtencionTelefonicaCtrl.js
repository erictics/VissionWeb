'use strict';
angular
  .module('softvApp')
  .controller('reporteAtencionTelefonicaCtrl', function ($state,atencionFactory,reportesFactory,usuarioFactory , $filter, reportesVariosFactory, globalService, $sce, $localStorage) {
    
    function initData(){
      atencionFactory.MuestraTrabajos().then(function(result){
        console.log(result);
        vm.trabajosQuejas = result.GetMUESTRATRABAJOSQUEJASListResult;
      });
    }

    function getUsuarios(){
      var Parametros = {
        'ClvUsuario':'',
        'Nombre':'',
        'Op':0,
        'idcompania':0
      };
      usuarioFactory.GetUsuarioSoftvList(Parametros).then(function (result) {
        console.log(result);
        vm.usuarios = result.GetUsuarioSoftvListResult;
      });
    }

    function getTrabajosQuejas(){
      atencionFactory.MuestraTrabajos(vm.tiposervicio.Clv_TipSerPrincipal).then(function(result){
        console.log(result);
        vm.trabajosQuejas = result.GetMUESTRATRABAJOSQUEJASListResult;
      });
    }

    function getTipoServicios(){
      atencionFactory.getServicios().then(function(result) {
        console.log(result);
        vm.Tiposervicios = result.GetMuestraTipSerPrincipalListResult;
      });
    }

    function GetReport() {
      if (vm.contrato == 0){ vm.contrato =""; }
      //console.log(vm.responseparams);
       var obj={
        'Clv_TipSer':(vm.tiposervicio)?vm.tiposervicio.Clv_TipSerPrincipal:0,           
        'Clv_inicio':(vm.clvLlamadaInicio)?vm.clvLlamadaInicio:0,
        'Clv_fin':(vm.clvLlamadaFin)?vm.clvLlamadaFin:0,
        'fechasolInicial':(vm.fechasolInicial)? $filter('date')(vm.fechasolInicial, 'yyyy/MM/dd'):'19000101',
        'fechasolFinal':(vm.fechasolFinal)? $filter('date')(vm.fechasolFinal, 'yyyy/MM/dd') :'19000101',
        'Clv_trabajo':(vm.servicio)?vm.servicio.CLV_TRABAJO:0,
        'clvQueja':(vm.reporte)?vm.reporte:0,
        'Op':vm.tiporeporteord,
        'OpOrdenar':vm.tiporeporte,
        'Clv_usuario':(vm.usuario)? vm.usuario.Clave:0,
        'estatus':(vm.tipoqueja)?vm.tipoqueja:'',
        'contrato':(vm.contrato)?vm.contrato:'',        
        'distribuidores':vm.responseparams.distribuidores,
        'plazas':vm.responseparams.plazas,
        'estados': vm.responseparams.estados,
        'ciudades':vm.responseparams.ciudades,
        'localidades':vm.responseparams.localidades,
        'colonias':vm.responseparams.colonias
       };
       console.log(obj);
       reportesFactory.GetReporteAtencion(obj).then(function(result){
        vm.rptpanel=true;
        vm.url = $sce.trustAsResourceUrl(
          globalService.getUrlReportes() + 
           '/Reportes/' + result.GetReporteAtencionResult);
       });
    }

    initData();
    var vm = this;
    vm.url=$sce.trustAsResourceUrl(globalService.getUrlReportes() + '/ReportesSistema/ReporteWrapper.html');
    vm.report = 'ATENCIONTELEFONICA';
    vm.GetReport = GetReport;
    vm.responseparams = {};
    vm.showfilters = false;
    vm.tiporeporteord="1";
    vm.tiporeporte="1";
    vm.rptpanel=false;
    getUsuarios();
    getTipoServicios();
    vm.order=[
      { 'step': 1, function: 'getplazas',   confirm: false  },
      { 'step': 2, function: 'getEstadosByPlaza',confirm: false },
      { 'step': 3, function: 'getCiudadesByEstado',confirm: false },
      { 'step': 4 ,  function :'getLocalidadesByCiudades',confirm: false },
      { 'step': 5 ,  function :'getColoniasByLocalidad',confirm: false },     
      { 'step': 6 ,  function :'getcoloniasAtencion',confirm: false },
      { 'step': 7 ,  function :'getfiltrosAtencion',confirm: true },
    
      ];
    vm.getTrabajosQuejas=getTrabajosQuejas;

  });
