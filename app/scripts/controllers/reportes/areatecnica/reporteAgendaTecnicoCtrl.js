'use strict';
angular
	.module('softvApp')
	.controller('reporteAgendaTecnicoCtrl', function($state,ngNotify ,reportesFactory,$filter,reportesVariosFactory,globalService,$sce,$localStorage) {	
		
 function GetReport(){

  var fechaInicio = $filter('date')(document.getElementById('fechaInicio').value, 'MM/dd/yyyy');
  var fechafin = $filter('date')(document.getElementById('fechaFin').value, 'MM/dd/yyyy');
  
  var parts = fechaInicio.split("/");
  var dt = new Date(parseInt(parts[2], 10),
                    parseInt(parts[1], 10) - 1,
                    parseInt(parts[0], 10));

  /*
  console.log('fecha',parseInt(parts[2], 10),  
                parseInt(parts[1], 10) - 1,
                parseInt(parts[0], 10));
  */

  var parts2 = fechafin.split("/");
  var dt2 = new Date(parseInt(parts2[2], 10),
                    parseInt(parts2[1], 10) - 1,
                    parseInt(parts2[0], 10));

  /*console.log('dt2', dt2, '| dt', dt);  
  if(dt2 < dt){
    ngNotify.set('Selecione una fecha inicial menor al a la fecha final','warn')
    return;
  }
  console.log(vm.tecnicoAgenda.clv_tecnico)
  console.log(document.getElementById('fechaInicio').value);
  console.log(document.getElementById('fechaFin').value);
  return; */
  // $filter('date')(document.getElementById('fechaFin').value, 'yyyy/MM/dd'), $filter('date')(vm.fechafin, 'yyyy/MM/dd')


    if (isNaN(dt) || isNaN(dt2) ){
      ngNotify.set('Seleccione las fecha de inicio y fin ','warn');
    }
    else{

        if(dt2 < dt){
          ngNotify.set('Selecione una fecha inicial menor al a la fecha final','warn');     
        }
        else 
        {
          reportesFactory.GetReporteAgendaTecnico(vm.tecnicoAgenda.clv_tecnico,
            $filter('date')( document.getElementById('fechaInicio').value,'yyyy/MM/dd'),
            $filter('date')(document.getElementById('fechaFin').value,'yyyy/MM/dd'))
          .then(function (data) {    
            vm.rptpanel=true;
            vm.url = $sce.trustAsResourceUrl(globalService.getUrlReportes() + '/Reportes/' + data.GetReporteAgendaTecnicoResult);
          });       
        }  
    } 
  }


  var vm = this;
  vm.url = $sce.trustAsResourceUrl(globalService.getUrlReportes() + '/ReportesSistema/ReporteWrapper.html');
  //console.log(vm.url);
  $('#fechaInicio').datepicker();
  $('#fechaFin').datepicker();
  vm.report='AGENDATECNICO';
  vm.GetReport=GetReport;
  vm.responseparams={};
  vm.showfilters=false;
  vm.rptpanel=false;
	vm.order = [{
        'step': 1,
        function: 'getplazas',
        confirm: false
      },
      {
        'step': 2,
        function: 'muestrafiltroAgenda',
        confirm: true
      }
    ]
	
});