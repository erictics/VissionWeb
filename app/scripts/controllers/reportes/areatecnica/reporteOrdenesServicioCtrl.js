"use strict";
angular
  .module("softvApp")
  .controller("reporteOrdenesServicioCtrl", function(
    $state,
    reportesFactory,
    reportesVariosFactory,
    globalService,
    $sce,
    $localStorage,
    trabajosFactory,
    $filter
  ) {

    function initData(){
      GetTrabajoList();
    }

    function GetReport() {
      if (vm.NumOrdFin > 0)
      {
        if( isNaN(vm.NumOrdIni) || vm.NumOrdIni == 0 ){
          vm.NumOrdIni = 1;
        }
      }

      var params = {
        fechaejeInicial: vm.FechaEjeIni ? $filter('date')(vm.FechaEjeIni, 'yyyy-MM-dd') : "",
        fechaejeFinal: vm.FechaEjeFin ? $filter('date')(vm.FechaEjeFin, 'yyyy-MM-dd') : "",
        fechasolInicial : vm.FechaOrdIni ? $filter('date')(vm.FechaOrdIni, 'yyyy-MM-dd') : "",
        fechasolFinal: vm.FechaOrdFin ? $filter('date')(vm.FechaOrdFin, 'yyyy-MM-dd') : "",
        estatus: (vm.estatus) ? vm.estatus : "",
        Clv_inicio: vm.NumOrdIni ? vm.NumOrdIni : 0,
        Clv_fin: vm.NumOrdFin ? vm.NumOrdFin : 0,
        Clv_trabajo: vm.servicio ? vm.servicio.clv_trabajo : 0,
        OpOrdenar: vm.tiporeporte ? parseInt(vm.tiporeporte) : 0,
        Op: vm.tipoImpresion? parseInt(vm.tipoImpresion):0,
        distribuidores: (vm.responseparams.distribuidores != undefined)? vm.responseparams.distribuidores:[],
        plazas: (vm.responseparams.plazas != undefined)? vm.responseparams.plazas:[],
        estados: (vm.responseparams.estados != undefined)? vm.responseparams.estados:[],
        ciudades: (vm.responseparams.ciudades != undefined)? vm.responseparams.ciudades:[],
        localidades: (vm.responseparams.localidades != undefined)? vm.responseparams.localidades:[],
        colonias: (vm.responseparams.colonias != undefined)? vm.responseparams.colonias:[],
        calles: (vm.responseparams.calles != undefined)? vm.responseparams.calles:[],
        Clv_usuario: $localStorage.currentUser.idUsuario
      };
      reportesFactory.GetReporteOrdenes(params).then(function(result) {		
            vm.rptpanel=true;
            vm.url = $sce.trustAsResourceUrl(
              globalService.getUrlReportes() +
                "/Reportes/" +
                result.GetReporteOrdenesResult);
      });
    }

    function GetTrabajoList(){
      trabajosFactory.GetSoftv_GetTrabajoByClv_TipSerTipo(-1, 'O').then(function(data){
        vm.TrabajoList = data.GetSoftv_GetTrabajoByClv_TipSerTipoResult;
      });
    }

    function GetDatetoString(Fecha){
        var D = Fecha.getDate();
        var M = Fecha.getMonth() + 1;
        var Y = Fecha.getFullYear();
        return D + '/' + M + '/' + Y;
    }

    var vm = this;
    vm.report = "ORDENESDESERVICIO";
    vm.url = "";
    vm.order = [
      { step: 1, function: "getplazas", confirm: false },
      { step: 2, function: "getEstadosByPlaza", confirm: false },
      { step: 3, function: "getCiudadesByEstado", confirm: false },
      { step: 4, function: "getLocalidadesByCiudades", confirm: false },
      { step: 5, function: "getColoniasByLocalidad", confirm: false },
      { step: 6, function: "getCallesByColonia", confirm: false },
      { step: 7, function: "getfiltrosOrden", confirm: true }
    ];

    vm.url=$sce.trustAsResourceUrl(globalService.getUrlReportes() + '/ReportesSistema/ReporteWrapper.html');
    vm.plazas = [];
    vm.distribuidores = [];
    vm.tecnicos = [];
    vm.estados = [];
    vm.localidades = [];
    vm.ciudades = [];
    vm.colonias = [];
    vm.calles = [];
    vm.responseparams = {};
    vm.showfilters = false;
    vm.rptpanel=false;
    vm.GetReport = GetReport;
    vm.estatus='P';
    vm.tipoImpresion='0';
    vm.tiporeporte='0';
    initData();
    
  });
