"use strict";
angular
  .module("softvApp")
  .controller("TiempoDeAtencionOrdYRepCtrl", function($state, $localStorage, $rootScope, $sce, reportesFactory, ngNotify, $filter, globalService
  ) {
    function Init() {}

    function aceptar() {
      var finicio = $filter("date")(vm.fechainicio, "yyyy/MM/dd");
      var ffin = $filter("date")(vm.fechafin, "yyyy/MM/dd"); 
      var distribuidores = vm.responseparams.distribuidores;  
      var plazas = vm.responseparams.plazas;
      var ciudades = vm.responseparams.ciudades;
      var localidades = vm.responseparams.localidades;
      var trabajos = vm.responseparams.trabajos;

      if (angular.isUndefined(finicio) == false || angular.isUndefined(ffin) == false){     
        reportesFactory.GetReporte_TiempoDeAtencionDeOrdernesYReportes(vm.opcionFecha, vm.tiporeporte, finicio, ffin, distribuidores, plazas, ciudades, localidades, trabajos)
        .then(function(result) {              
          var Name1 = result.GetReporte_TiempoDeAtencionDeOrdernesYReportesResult;
          var FileName1 = globalService.getUrlReportes() + '/Reportes/' + Name1;
          vm.FileName1 = $sce.trustAsResourceUrl(FileName1);
        });   
      }
    }

    function buscarPor(){
      vm.opcionFecha = '0';
    }

    var vm = this;
    vm.report = "Antigüedad de órdenes y reportes";
    vm.responseparams = {};    
    vm.showfilters = false;
    vm.aceptar = aceptar;
    vm.Reporte1 = "";
    vm.url=$sce.trustAsResourceUrl(globalService.getUrlReportes() + '/ReportesSistema/ReporteWrapper.html');
    vm.order = [
        { step: 1, function: "getplazas", confirm: false },
        { step: 2, function: "getEstadosByPlaza", confirm: false },
        { step: 3, function: "getCiudadesByEstado", confirm: false },
        { step: 4, function: "getLocalidadesByCiudades", confirm: false },
        { step: 5, function: "getMuestraTrabajosResumen", confirm: false },
        { step: 6, function: "getFiltroTiempoAtencionOrdenesYReportes", confirm: false }   
    ];

    vm.FileName1 = "";
    vm.fechainicio = new Date();
    vm.fechafin = new Date();
    vm.opcionFecha = '0';
    vm.tiporeporte = '1';
    vm.buscarPor = buscarPor;


    Init();
  });
