'use strict';
angular
	.module('softvApp')
	.controller('reporteProductividadTecnicoCtrl', function($state,reportesFactory,reportesVariosFactory,$filter,globalService,$sce,$localStorage) {	
		
    function GetReport(){
		var Parametros = {
			'plazas': vm.responseparams.plazas,
			'tecnicos': vm.responseparams.tecnicosAgenda,
			'fechainicio': $filter('date')(vm.fechainicio, 'yyyy/MM/dd'),
			'fechafin': $filter('date')(vm.fechafin, 'yyyy/MM/dd')
		  };
			 
		  //return;		  
		  reportesFactory.GetReporteProductividadTecnico(Parametros).then(function (result) {
		 	vm.rptpanel = true;
			vm.url = $sce.trustAsResourceUrl(globalService.getUrlReportes() + '/Reportes/' + result.GetReporteProductividadTecnicoResult);
		 });
	}
		
	var vm = this;
	vm.report = 'PRODUCTIVIDADTECNICO';
	vm.url=$sce.trustAsResourceUrl(globalService.getUrlReportes() + '/ReportesSistema/ReporteWrapper.html');
	vm.GetReport = GetReport;
	vm.responseparams = {};
	vm.showfilters = false;
	vm.rptpanel = false;
	vm.fechainicio = new Date();
    vm.fechafin = new Date();
	vm.order = [
		{ 'step': 1, function: 'getplazas',   confirm: false  },
		{ 'step': 2, function: 'muestrafiltroAgenda', confirm: false },
		{ 'step': 3, function: 'getRangosFechas',confirm: true }
		];
	
});