'use strict';
angular
	.module('softvApp')
	.controller('trabajosTecnicoCtrl', function($state,reportesFactory,reportesVariosFactory,$filter,globalService,$sce,$localStorage) {	
		
    function GetReport(){
        var O=false;
        var Q=false;
       
        if(vm.tiporeporte === '0'){
          O=true;
        }
        if(vm.tiporeporte === '1'){
            Q=true;
        }
        if(vm.tiporeporte === '2'){
            O=true;
            Q=true;
        }

       console.log(vm.responseparams.tecnicosAgenda);
		var Parametros = {			
			'tecnicos': vm.responseparams.tecnicosAgenda,
			'fechainicio': $filter('date')(vm.fechainicio, 'yyyy/MM/dd'),
			'fechafin': $filter('date')(vm.fechafin, 'yyyy/MM/dd'),
			'ordenes': O,
			'quejas':Q			
		  };
		  	  
		  reportesFactory.GetReporteTrabajosTecnico(Parametros).then(function (result) {
			vm.rptpanel=true;
			vm.url = $sce.trustAsResourceUrl(globalService.getUrlReportes() + '/Reportes/' + result.GetReporteTrabajosTecnicoResult);
		 });
	}
		
	var vm=this;
	vm.report='TRABAJOSTECNICO';
	vm.GetReport=GetReport;
	vm.responseparams={};
	vm.showfilters=false;
	vm.url=$sce.trustAsResourceUrl(globalService.getUrlReportes() + '/ReportesSistema/ReporteWrapper.html');
    vm.rptpanel=false;
    vm.tiporeporte='0';
	vm.tipoOrden= {};
	vm.order = [
		{  'step': 1,function: 'getplazas',   confirm: false  },
		{ 'step': 2, function: 'muestrafiltroAgenda', confirm: false },		
		{ 'step': 3, function: 'getRangosFechas',confirm: true }
		];
	
});