'use strict';
angular
	.module('softvApp')
	.controller('ReporteMantenimientoCtrl', function(atencionFactory, ngNotify, QuejaMasivaFactory,reportesFactory,$filter,globalService,$sce,$localStorage) {
        
      

        function GetProblemaList(){
			atencionFactory.GetClasificacionProblemas().then(function (data) {
				vm.Problemas = data.GetuspConsultaTblClasificacionProblemasListResult;
				vm.Problema = vm.Problemas[0];
			});
		}

		function GetTrabajos(){
			QuejaMasivaFactory.GetMUESTRATRABAJOSQUEJAS_MasivosList().then(function(data){
				vm.Trabajos = data.GetMUESTRATRABAJOSQUEJAS_MasivosListResult;
			});
        }
        
        function generaReporte(){
          
            if(vm.numeroFin > 0 && (vm.numeroInicio == 0 || isNan(vm.numeroInicio) ))
            {
                vm.numeroInicio = 1;
            }


            var Op1 = 0;
            var Op2 = 0;
            var Op3= 0;
            var Op4 = 0;
            var Op5 = 0;
            var OpF =0;
            
            if(vm.numeroInicio > 0 && vm.numeroFin > 0 && (vm.numeroInicio !==null || vm.numeroInicio !==null )){
                Op1=1;
            }
            if(vm.fechasolIni && vm.fechasolFin) {
                Op2=1;
                OpF=1;
            }
            if(vm.fechaejeIni && vm.fechaejeFin) {
                Op2=1;
                OpF=2;
            }
            if(vm.Trabajo){
                Op4=1;
            }

            if(vm.Problema.clvProblema != 0){
                Op5=1; 
            }
            /* 
            console.log('numeroInicio',vm.numeroInicio);
            console.log('numeroFin',vm.numeroFin);
            console.log('fechasolIni',vm.fechasolIni);
            console.log('fechasolFin',vm.fechasolFin);
            console.log('fechaejeIni',vm.fechaejeIni);
            console.log('fechaejeFin',vm.fechaejeFin);
            console.log('Problema',vm.Problema);
            console.log('Trabajo',vm.Trabajo);

            console.log(Op1);
            console.log(Op2);
            console.log(Op3);
            console.log(Op4);
            console.log(Op5); */
            var fechaInicio="";
            var fechafin="";
            
            if(OpF===1){
                fechaInicio= (vm.fechasolIni)? $filter('date')(vm.fechasolIni, 'dd/MM/yyyy'):'01/01/1999';
                fechafin= (vm.fechasolFin)? $filter('date')(vm.fechasolFin, 'dd/MM/yyyy'):'01/01/1999';

            }else{

                fechaInicio=(vm.fechaejeIni)? $filter('date')(vm.fechaejeIni, 'dd/MM/yyyy'):'01/01/1999';
                fechafin=(vm.fechaejeFin)? $filter('date')(vm.fechaejeFin, 'dd/MM/yyyy'):'01/01/1999';
            }           

            var obj = {
               'Clv_QuejaIni':vm.numeroInicio,
               'Clv_QuejaFin':vm.numeroFin,
               'FechaIni':fechaInicio ,
               'FechaFin':fechafin,
               'StatusE': (vm.ejecutados)?'E':'' ,
               'StatusP': (vm.pendientes)?'P':'' ,
               'StatusS': (vm.proceso)? 'S':'',
               'Clv_Trabajo':(vm.Trabajo)?vm.Trabajo.CLV_TRABAJO:0,
               'ClvProblema':(vm.Problema)?vm.Problema.clvProblema:0,
               'Op1':Op1,
               'Op2':Op2,
               'Op4':Op4,
               'Op5':Op5,
               'OpF':OpF,
               'TipoReporte': (vm.TipoR == 'D')? true:false
            };
      
            if (vm.ejecutados == false  && vm.pendientes == false  && vm.proceso == false)
            {
                ngNotify.set('Selecione un estatus','warn');
            }
            else {
                reportesFactory.GetReporteMantenimiento(obj).then(function(res){
                    vm.url = $sce.trustAsResourceUrl(globalService.getUrlReportes() + '/Reportes/' +  res.GetReporteMantenimientoResult);
                });
            }           
        }

        var vm = this;
        vm.url=$sce.trustAsResourceUrl(globalService.getUrlReportes() + '/ReportesSistema/ReporteWrapper.html');
        GetTrabajos();
        GetProblemaList();
        vm.generaReporte=generaReporte;
        vm.numeroInicio =0;
        vm.numeroFin = 0;
        vm.TipoR = 'D';
        vm.ejecutados = false;
        vm.pendientes = false;
        vm.proceso = false;
    });