'use strict';

angular
    .module('softvApp')
    .controller('SetPeriodoCtrl', function(CargosAutomaticosFactory, $localStorage, $uibModalInstance, $uibModal, ngNotify, $state, logFactory){

        function initData(){
            CargosAutomaticosFactory.GetAllMUESTRAPERIODOS_Seleccionar().then(function(data){
                vm.PeriodoList = data.GetAllMUESTRAPERIODOS_SeleccionarResult;
            });
        }

        function Save(){
            $uibModalInstance.close(vm.Periodo);
        }

        function Cancel (){

            $state.go("home.dashboard");
            $uibModalInstance.close('cancel');
        }

        var vm = this;
        vm.Save = Save;
        vm.Cancel= Cancel;
        initData();
        
    });