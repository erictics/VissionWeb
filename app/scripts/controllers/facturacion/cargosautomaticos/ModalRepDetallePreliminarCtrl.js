'use strict';
angular
	.module('softvApp')
	.controller('ModalRepDetallePreliminarCtrl', function($uibModalInstance, CargosAutomaticosFactory, ClvSessionDetFac, globalService, $sce) {

		function initialData() {
			CargosAutomaticosFactory.GetRepDetallePreliminar(ClvSessionDetFac).then(function(data) {
				var Name = data.GetRepDetallePreliminarResult;
				var FileName = globalService.getUrlReportes() + '/Reportes/' + Name;
				vm.FileName = $sce.trustAsResourceUrl(FileName);				
			});
		
		}


		function cancel() {
			$uibModalInstance.dismiss('cancel');
		}

		var vm = this;
		vm.cancel = cancel;
		initialData();
	});
