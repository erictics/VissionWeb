'use strict';
angular
	.module('softvApp')
	.controller('ModalTxtCtrl', function($uibModalInstance, CargosAutomaticosFactory, CatalogosFactory, ClvSessionDetFac, ngNotify, $localStorage) {

		function initialData() {
			
        }
        
        function SaveDocument (){
            vm.FileName = null;
            vm.TituloDoc = null;
            if (ClvSessionDetFac > 0) {
                if (vm.Evidencia != null) {
                    if (vm.Evidencia.type == "text/plain") {
                        var EvidenciaFD = new FormData();
                        EvidenciaFD.append('file', vm.Evidencia);
                        EvidenciaFD.append('ClvSessionDetFac', ClvSessionDetFac);
                        CargosAutomaticosFactory.GetGuardaDocumentoTxt(EvidenciaFD).then(function (data) {
                            var result = data;
                            if (result == 1) {
                                ngNotify.set('ERROR, El Archivo Seleccionado No Es De Respuesta', 'warn');
                            } else if (result == 2) {
                                ngNotify.set('ERROR, El Número De Proceso No Corresponde Al Archivo Seleccionado', 'warn');
                            }
                            else if (result == 0) {
                                ngNotify.set('Archivo Procesado Exitosamente', 'success');
                                SaveMovimientoSistema('Afectó archivo Bancomer', ClvSessionDetFac, 70);
                                cancel();
                            }
                            vm.Evidencia = null;
                            vm.File = null;
                            angular.element("input[type='file']").val(null);
                        });
                    } else {
                        ngNotify.set('ERROR, Formato invalido', 'warn');
                    }
                } else {
                    ngNotify.set('ERROR, No se ha seleccionado el archivo', 'warn');
                }
            } else {
                ngNotify.set('Seleccione el Proceso que desea Afectar', 'warn');
            }

        }

        function SaveMovimientoSistema(Observaciones, Comando, IdClassLog){
            var objMovSist = {
                'Clv_usuario': $localStorage.currentUser.idUsuario, 
                'Modulo': 'home.facturacion', 
                'Submodulo': 'home.facturacion.cargosautomaticos', 
                'Observaciones': Observaciones, 
                'Usuario': $localStorage.currentUser.usuario, 
                'Comando': (Comando != '')? JSON.stringify(Comando):'', 
                'Clv_afectada': vm.IdContrato,
                'IdClassLog': IdClassLog
            };
            CatalogosFactory.AddMovSist(objMovSist).then(function(data){});
        }

		function cancel() {
			$uibModalInstance.close('cancel');
		}

        var vm = this;
        vm.TouchFile= false;
        vm.SaveDocument= SaveDocument;
		vm.cancel = cancel;
		initialData();
	});
