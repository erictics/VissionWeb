'use strict';

angular
    .module('softvApp')
    .controller('CargosAutomaticosCtrl', function (CargosAutomaticosFactory, generalesSistemaFactory, CatalogosFactory, $localStorage, $scope, ngNotify, $uibModal, $rootScope, $state, globalService, $sce) {

        function initData() {
            generalesSistemaFactory.GetvalidaAccesoFacturacion().then(function (data) {
                if (data.GetvalidaAccesoFacturacionResult === 0) {
                    $state.go("home.dashboard");
                    ngNotify.set(
                        "Su máquina no esta registrada como una caja, por tal motivo no tiene acceso a facturación",
                        "warn"
                    );
                    return
                } else {
                    SetPeriodo();
                    vm.NameCaja = $localStorage.currentUser.CajaNombre;
                    vm.Sucursal = $localStorage.currentUser.SucursalNombre;
                    vm.Cajera = $localStorage.currentUser.usuario;
                    vm.Caja = $localStorage.currentUser.IdCaja;
                    vm.idSucursal = $localStorage.currentUser.sucursal;

                    CargosAutomaticosFactory.DeleteBorraTablasArchivos().then(function (data) {
                    });

                    CargosAutomaticosFactory.GetDameDatosUsuario().then(function (data) {
                        vm.NameUser = data.GetDameDatosUsuarioResult.Nombre;
                    });

                    CargosAutomaticosFactory.GetDameDatosGenerales().then(function (data) {
                        vm.NameEmpresa = data.GetDameDatosGeneralesResult.Nombre;
                        vm.NameSistema = data.GetDameDatosGeneralesResult.Ciudad;
                        vm.NameFecha = data.GetDameDatosGeneralesResult.Fecha;
                    });
                }
            });
        }

        function SetPeriodo() {
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'views/facturacion/ModalSetPeriodo.html',
                controller: 'SetPeriodoCtrl',
                controllerAs: 'ctrl',
                backdrop: 'static',
                keyboard: false,
                class: 'modal-backdrop fade',
                size: 'md'
            });

            modalInstance.result.then(function (Periodo) {
                vm.Periodo = Periodo;
                GetConPreliminar();
            });
        }

        function SetFecha() {
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'views/facturacion/ModalSetFecha.html',
                controller: 'GenerarListadoCtrl',
                controllerAs: 'ctrl',
                backdrop: 'static',
                keyboard: false,
                class: 'modal-backdrop fade',
                size: 'sm'
            });

            modalInstance.result.then(function (Fecha) {
                vm.Fecha = Fecha;
                GeneraBancos();
            });
        }

        function GetConPreliminar() {
            CargosAutomaticosFactory.GetConPreliminarBancosList(vm.Periodo.Clv_Periodo).then(function (data) {
                vm.ConPreliminarBancosList = data.GetConPreliminarBancosListResult;
                GetConDetFacturas(vm.ConPreliminarBancosList[0].Clv_SessionBancos);
                vm.ClvSession = vm.ConPreliminarBancosList[0].Clv_SessionBancos;
                vm.Realizado = vm.ConPreliminarBancosList[0].Realizado;
                vm.ProcesoCancelado = vm.ConPreliminarBancosList[0].Proceso_Cancelado;
                vm.BlckChecKPre = (vm.ProcesoCancelado == 1)? true:false;
                vm.B3 = (vm.ProcesoCancelado == 0 && vm.Realizado == false)? false:true;
                vm.B4 = (vm.ProcesoCancelado == 0 && vm.Realizado == false)? false:true;
                Seleccionados();
            });
        }

        function GetConDetFacturas(Clv_SessionBancos) {
            CargosAutomaticosFactory.GetCONDETFACTURASBANCOSList(Clv_SessionBancos).then(function (data) {
                vm.ConDetFacturasList = data.GetCONDETFACTURASBANCOSListResult;
            });
        }

        function SelectConPreliminar(Obj, index) {
            GetConDetFacturas(Obj.Clv_SessionBancos);
            vm.ClvSession = Obj.Clv_SessionBancos;
            vm.Realizado = Obj.Realizado;
            vm.ProcesoCancelado = Obj.Proceso_Cancelado;
            vm.BlckChecKPre = (vm.ProcesoCancelado == 1)? true:false;
            vm.B3 = (vm.ProcesoCancelado == 0 && vm.Realizado == false)? false:true;
            vm.B4 = (vm.ProcesoCancelado == 0 && vm.Realizado == false)? false:true;
            vm.Clv_Id = 0;
            vm.Pagado = null;
            vm.Cancelada = null;
            vm.ClvFacMenor = 0;
            vm.Realizada = null;
            vm.B1 = true;
            Seleccionados();
        }

        function Seleccionados() {
            if (vm.ProcesoCancelado == 1) {
                //vm.B1 = true;
                vm.B2 = true;
                //vm.B3 = true;
                //vm.B4 = true;
            } else {
                if (vm.Realizado == false) {
                    //vm.B1 = false;
                    vm.B2 = false;
                    //vm.B3 = false;
                    //vm.B4 = false;
                }
            }
            if (vm.Realizado == true) {
                //vm.B1 = false;
                vm.B2 = true;
                //vm.B3 = true;
                //vm.B4 = true;
            } else {
                if (vm.ProcesoCancelado == 0) {
                    //vm.B1 = false;
                    vm.B2 = false;
                    //vm.B3 = false;
                    //vm.B4 = false;
                }
            }
        }

        function SelectConDetFacturas(Obj, index) {
            vm.ClvSessionDetFac = Obj.Clv_SessionBancos;
            vm.Clv_Id = Obj.Clv_Id;
            vm.Pagado = Obj.PAGADO;
            vm.Cancelada = Obj.CANCELADA;
            vm.Realizada = Obj.REALIZADO;
            CargosAutomaticosFactory.GetDame_PreRecibo_oFactura(vm.Clv_Id).then(function (data) {
                    vm.ClvFacMenor = data.GetDame_PreRecibo_oFacturaResult.Clv_Factura;
                    vm.B1 = (vm.ClvFacMenor > 0 && vm.BlckChecKPre == false)? false:true;
                });
            //GetDameRangoFacturasCtrl();btn ticket
        }

        function GeneraBancos() {
            CargosAutomaticosFactory.AddGeneraBancos(vm.Periodo.Clv_Periodo, vm.Cajera, vm.idSucursal, vm.Caja, vm.Fecha).then(function (data) {
                vm.ClvSessionBancos = data.AddGeneraBancosResult;
                if (vm.ClvSessionBancos > 0) {
                    GetConPreliminar();
                    GetConDetFacturas(ClvSessionBancos);
                    var Comando = {
                        'Clv_Periodo': vm.Periodo.Clv_Periodo,
                        'Cajera': vm.Cajera,
                        'idSucursal': vm.idSucursal,
                        'Caja': vm.Caja,
                        'Fecha': vm.Fecha
                    };
                    SaveMovimientoSistema('Se Generó Listado Premliminar', Comando, 67);
                } else {
                    ngNotify.set('No hay clientes que adeuden el mes actual', 'warn');
                }
            });
        }

        function BtnAfectarListado() {
            if (vm.ClvSession > 0) {
                if (vm.Realizado == false && vm.ProcesoCancelado == 0) {
                    CargosAutomaticosFactory.GetValidaAfectacionBancos_WEB(vm.ClvSession).then(function (data) {
                        vm.MSG = data.GetValidaAfectacionBancos_WEBResult.MSG;
                        vm.NUMERO = data.GetValidaAfectacionBancos_WEBResult.NUMERO;
                        if (vm.NUMERO == 0) {
                            var objCargoAutomatico = { 'Clv_SessionBancos': vm.ClvSession, 'CAJERA': vm.Cajera, 'SUCURSAL': vm.idSucursal, 'CAJA': vm.Caja };
                            CargosAutomaticosFactory.UpdateGUARDAFACTURASBANCOS(objCargoAutomatico).then(function (data) {
                                GetConPreliminar();
                                SaveMovimientoSistema('Se Afectó el listado preliminar', objCargoAutomatico, 68);
                                ngNotify.set('Proceso de Afectación Finalizado con Éxito', 'info');
                            });
                        } else {
                            ngNotify.set(vm.MSG, 'warn');
                        }
                    });
                } else {
                    if (vm.Realizado == true) {
                        ngNotify.set('El Número de Proceso: ' + vm.ClvSession + ' ya fue Afectado', 'warn');
                    }
                    if (vm.ProcesoCancelado == 1) {
                        ngNotify.set('El Número de Proceso: ' + vm.ClvSession + ' esta Cancelado por lo cual no se puede generar el proceso de Afectación', 'warn');
                    }
                }
            } else {
                ngNotify.set('Seleccione el Proceso que desea Afectar', 'warn');
            }
        }


        function BtnCancelarListado() {
            if (vm.ClvSession > 0) {
                if (vm.ProcesoCancelado >= 0) {
                    if (vm.ProcesoCancelado == 1) {
                        /*ngNotify.set('El Número de Proceso: ' + vm.ClvSession + ' ya fue cancelado', 'warn');*/
                        ngNotify.set('El proceso ya fue cancelado, no se puede cancelar', 'warn');
                        return
                    }
                    if (vm.Realizado == true) {
                        //ngNotify.set('El Número de Proceso: ' + vm.ClvSession + ' ya fue afectado por lo cual no se puede cancelar', 'warn');
                        ngNotify.set('El proceso ya fue afectado, no se puede cancelar', 'warn');
                        return
                    }
                    if (vm.ClvSession > 0 && vm.ProcesoCancelado == 0) {
                        CargosAutomaticosFactory.GetCANCELALISTADOPRELIMINAR_web(vm.ClvSession).then(function (data) {
                            vm.MSG2 = data.GetCANCELALISTADOPRELIMINAR_webResult.MSG;
                            /*ngNotify.set(vm.MSG2, 'info');*/
                            if (vm.MSG2 == ' Listado Preliminar Cancelado con Éxito ') {
                                ngNotify.set(vm.MSG2, 'info');
                                GetConPreliminar();
                                GetConDetFacturas(vm.ClvSession);
                                SaveMovimientoSistema('Se canceló listado preliminar', vm.ClvSession, 69);
                            }
                        });
                    }
                } else {
                    ngNotify.set('Por el momento no existen clientes con cargo automático', 'warn');
                }
            } else {
                ngNotify.set('Seleccione el Proceso que desea Afectar', 'warn');
            }
        }


        function BtnGuardarDetalle() {
            if (vm.ClvSessionDetFac > 0 && vm.Realizada != true) {
                var objCargoAutomatico = { 'Clv_SessionBancos': vm.ClvSession, 'Clv_Id': vm.Clv_Id, 'PAGADO': vm.Pagado, 'CANCELADA': vm.Cancelada, 'REALIZADO': vm.Realizada };
                CargosAutomaticosFactory.UpdateMODDetFacturasBancos(objCargoAutomatico).then(function (data) {
                    SaveMovimientoSistema('Se Guardó Detalle', objCargoAutomatico, 72);
                    ngNotify.set('Se guardó detalle', 'success');
                    GetConDetFacturas(vm.ClvSessionDetFac);
                });
            }
            else {
                ngNotify.set('Seleccione el Proceso que desea Afectar', 'warn');
            }
        }

        function GetDameRangoFacturasCtrl() {
            /*if (vm.Clv_Id > 0) {
                CargosAutomaticosFactory.GetDame_PreRecibo_oFactura(vm.Clv_Id).then(function (data) {
                    console.log(data);
                    vm.ClvFacMenor = data.GetDame_PreRecibo_oFacturaResult.Clv_Factura;
                    vm.B1 (vm.ClvFacMenor > 0)? false:true;
                    console.log(vm.B1);
                    GetTicket()
                });
            } else {
                ngNotify.set('Seleccione el Proceso que desea Afectar', 'warn');
            }*/

            // CargosAutomaticosFactory.GetDameRangoFacturas(vm.ClvSessionDetFac).then(function (data) {
            //     vm.ClvFacMenor = data.GetDameRangoFacturasResult.Clv_Factura_Menor;
            //     vm.ClvFacMayor = data.GetDameRangoFacturasResult.Clv_Factura_Mayor;
            //     console.log(vm.ClvFacMenor, vm.ClvFacMayor);
            //     if (vm.ClvFacMenor == 0 && vm.ClvFacMayor == 0) {
            //         ngNotify.set('No se puede imprimir pues no hay facturas generadas', 'warn');
            //     } else {
            //         GetTicket()
            //     }
            // });
        }

        function GetTicket() {
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'views/facturacion/modalSingleTicket.html',
                controller: 'ModalSingleTicketCtrl',
                controllerAs: 'ctrl',
                backdrop: 'static',
                keyboard: false,
                size: 'sm',
                resolve: {
                    factura: function () {
                        return vm.ClvFacMenor;
                    },
                    imprimir: function () {
                        return false;
                    }
                }
            });
        }


        function BtnImprimirDetalle() {
            if (vm.ClvSession > 0) {
                GetRepDetalloPreliminar()
                // CargosAutomaticosFactory.GetDameRangoFacturas(vm.ClvSession).then(function (data) {
                //     console.log(data);
                //     vm.ClvFacMenor = data.GetDameRangoFacturasResult.Clv_Factura_Menor;
                //     vm.ClvFacMayor = data.GetDameRangoFacturasResult.Clv_Factura_Mayor;
                //     if (vm.ClvFacMenor == 0 && vm.ClvFacMayor == 0) {
                //         ngNotify.set('No se puede imprimir pues no hay facturas generadas', 'warn');
                //     } else {
                //         GetRepDetalloPreliminar()
                //     }
                // });
            } else {
                ngNotify.set('Seleccione el Proceso que desea Afectar', 'warn');
            }
        }

        function GetRepDetalloPreliminar() {
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'views/facturacion/ModalRepDetallePreliminar.html',
                controller: 'ModalRepDetallePreliminarCtrl',
                controllerAs: 'ctrl',
                backdrop: 'static',
                keyboard: false,
                size: 'lg',
                resolve: {
                    ClvSessionDetFac: function () {
                        return vm.ClvSession;
                    }
                    // },
                    // imprimir: function() {
                    // return false;
                    // }
                }
            });
        }

        function BtnGeneraPROSA() {
            if (vm.ClvSession > 0) {
                CargosAutomaticosFactory.GetConsulta_Grales_Prosa_bancomer(vm.ClvSession).then(function (data) {
                    var Archivo = data.GetConsulta_Grales_Prosa_bancomerResult;
                    if (Archivo.MSG == '1') {
                        ngNotify.set('El Núm. De Afiliación y/o Nom. Del Comercio No Se Ha Capturado En Generales Del Sistema', 'warn');
                    } else {
                        var dlnk
                        var txt =
                            'data:text/plain;base64,' +
                            Archivo.NomArchivo
                        dlnk = document.getElementById('dwnldLnk')
                        dlnk.download = Archivo.Descripcion
                        dlnk.href = txt
                        dlnk.click()
                        var Obj = {
                            'ClvSession': vm.ClvSession,
                            'NomArchivo': Archivo.Descripcion
                        };
                        SaveMovimientoSistema('Se Generó Archivo Prosa', Obj, 71);
                        /*    document.getElementById('download').href = 'data:text/plain;base64,' + Archivo.NomArchivo;
                            document.getElementById('download').click();
        
        */
                        //var FileName = globalService.getUrlReportes() + '/Reportes/' + Archivo.NomArchivo;
                        /*
                        document.getElementById('download').href = FileName;
                        document.getElementById('download').target = '_blank';
                        document.getElementById('download').download = Archivo.NomArchivo || 'archivo.dat'
                        document.getElementById('download').click();
                        */
                        /*
                       var blob = null;
                       var xhr = new XMLHttpRequest();
                       xhr.open('GET', $sce.trustAsResourceUrl(FileName, true));
                       xhr.responseType = "blob";//force the HTTP response, response-type header to be blob
                       xhr.onload = function() 
                       {
                           blob = xhr.response;//xhr.response is now a blob object+
                           console.log(blob);
                           descargarArchivo(blob, Archivo.NomArchivo)
                       }
                       xhr.send();*/

                    }
                });
            } else {
                ngNotify.set('Seleccione el Proceso que desea Afectar', 'warn');
            }
        }

        function descargarArchivo(contenidoEnBlob, nombreArchivo) {
            var reader = new FileReader();
            reader.onload = function (event) {
                var save = document.createElement('a');
                save.href = event.target.result;
                save.target = '_blank';
                save.download = nombreArchivo || 'archivo.dat';
                var clicEvent = new MouseEvent('click', {
                    'view': window,
                    'bubbles': true,
                    'cancelable': true
                });
                save.dispatchEvent(clicEvent);
                (window.URL || window.webkitURL).revokeObjectURL(save.href);
            };
            reader.readAsDataURL(contenidoEnBlob);
        };

        function SaveDocumentotxt() {
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'views/facturacion/ModalTxt.html',
                controller: 'ModalTxtCtrl',
                controllerAs: 'ctrl',
                backdrop: 'static',
                keyboard: false,
                size: 'sm',
                resolve: {
                    ClvSessionDetFac: function () {
                        return vm.ClvSession;
                    }
                }

            });
            modalInstance.result.then(function (Fecha) {
                GetConDetFacturas(vm.ClvSession)
            });
            /*vm.FileName = null;
            vm.TituloDoc = null;
            if (vm.ClvSessionDetFac > 0) {
                if (vm.Evidencia != null) {
                    if (vm.Evidencia.type == "text/plain") {
                        console.log(vm.Evidencia.type);
                        var EvidenciaFD = new FormData();
                        EvidenciaFD.append('file', vm.Evidencia);
                        EvidenciaFD.append('ClvSessionDetFac', vm.ClvSessionDetFac);
                        CargosAutomaticosFactory.GetGuardaDocumentoTxt(EvidenciaFD).then(function (data) {
                            console.log(data);
                            var result = data;
                            if (result == 1) {
                                ngNotify.set('warn, El Archivo Seleccionado No Es De Respuesta', 'warn');
                            } else if (result == 2) {
                                ngNotify.set('warn, El Número De Proceso No Corresponde Al Archivo Seleccionado', 'warn');
                            }
                            else if (result == 0) {
                                ngNotify.set('Archivo Procesado Exitosamente', 'success');
                                GetConDetFacturas(vm.ClvSessionDetFac)
                            }
                            vm.Evidencia = null;
                            vm.File = null;
                            angular.element("input[type='file']").val(null);
                        });
                    } else {
                        ngNotify.set('warn, Formato invalido', 'warn');
                    }
                } else {
                    ngNotify.set('warn, No se ha seleccionado el archivo', 'warn');
                }
            } else {
                ngNotify.set('Seleccione el Proceso que desea Afectar', 'warn');
            }*/
        }

        function SaveMovimientoSistema(Observaciones, Comando, IdClassLog){
            var objMovSist = {
                'Clv_usuario': $localStorage.currentUser.idUsuario, 
                'Modulo': 'home.facturacion', 
                'Submodulo': 'home.facturacion.cargosautomaticos', 
                'Observaciones': Observaciones, 
                'Usuario': $localStorage.currentUser.usuario, 
                'Comando': (Comando != '')? JSON.stringify(Comando):'', 
                'Clv_afectada': 0,
                'IdClassLog': IdClassLog
            };
            CatalogosFactory.AddMovSist(objMovSist).then(function(data){});
        }

        var vm = this;
        vm.SelectConPreliminar = SelectConPreliminar;
        vm.SetFecha = SetFecha;
        vm.BtnAfectarListado = BtnAfectarListado;
        vm.BtnCancelarListado = BtnCancelarListado;
        vm.BtnGuardarDetalle = BtnGuardarDetalle;
        vm.SelectConDetFacturas = SelectConDetFacturas;
        vm.GetDameRangoFacturasCtrl = GetDameRangoFacturasCtrl;
        vm.BtnImprimirDetalle = BtnImprimirDetalle;
        vm.SaveDocumentotxt = SaveDocumentotxt;
        vm.GetTicket = GetTicket;
        vm.B1 = true;
        vm.B2 = true;
        vm.B3 = true;
        vm.B4 = true;
        vm.Evidencia = null;
        vm.BtnGeneraPROSA = BtnGeneraPROSA;
        initData();

    });