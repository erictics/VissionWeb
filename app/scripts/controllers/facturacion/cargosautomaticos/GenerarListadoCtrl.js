'use strict';
angular.module('softvApp')
	.controller('GenerarListadoCtrl', function($uibModalInstance, $uibModal, ticketsFactory, $rootScope, ngNotify) {
		function initialData() {
		}

		function cancel() {
			$uibModalInstance.dismiss('cancel');
        }

        function Save() {    
			var FechaFilto=  "01/" + vm.Mes + "/" + vm.Ano 
			vm.FF= String(FechaFilto);
            $uibModalInstance.close(vm.FF);
        }

		var vm = this;
        vm.cancel = cancel;
		vm.Save = Save;
		initialData();
	});
