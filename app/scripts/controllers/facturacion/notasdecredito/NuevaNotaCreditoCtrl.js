"use strict";

angular
  .module("softvApp")
  .controller("NuevaNotaCreditoCtrl", function(
    NotasDeCreditoFactory,
    mizarFactory,
    $filter,
    CatalogosFactory,
    ordenesFactory,
    $localStorage,
    $scope,
    ngNotify,
    $uibModal,
    $rootScope,
    $state,
    globalService,
    $sce
  ) {

    function initData() {
        muestraTipoNota();
      vm.fechaGeneracion = $filter("date")(new Date(), "dd/MM/yyyy");
    }

    function imprimeFactura() {
      console.log('imprime factura');
      mizarFactory
        .GetGraba_Factura_Nota(vm.clv_nota)
        .then(function(res) {
          console.log(res);
          if(res.GetGraba_Factura_NotaResult.IdResult === 0  || res.GetGraba_Factura_NotaResult.IdResult === -1) {
             ngNotify.set(res.GetGraba_Factura_NotaResult.Message,'error');
            return;
          }
          var url = globalService.getUrlMizarReportes() +"/Reportes/" +   res.GetGraba_Factura_NotaResult.urlReporte;

          $uibModal.open({
            animation: true,
            ariaLabelledBy: "modal-title",
            ariaDescribedBy: "modal-body",
            templateUrl: "views/facturacion/modalFactura.html",
            controller: "modalFacturaCtrl",
            controllerAs: "ctrl",
            backdrop: "static",
            keyboard: false,
            size: "lg",
            resolve: {
              url: function() {
                return url;
              }
            }
          });
        });
    }

    function limpia(){
        vm.facturas = [];
        vm.conceptos =[];
        vm.contratoBueno =0;
        vm.contrato='';
        vm.ServiciosCliente =[];
        vm.sucursales =[];
        vm.cajas =[];
        vm.datosCli ={};
        vm.MONTO =0;
        //uestraTipoNota();
    }

    function RevertirProceso(){
      NotasDeCreditoFactory.GetCANCELACIONFACTURAS_Notas(vm.factura.Clv_factura).then(function(res){
        if(res.GetCANCELACIONFACTURAS_NotasResult > 0){
           ngNotify.set('el proceso se revirtió correctamente');
        }
      });
    }

    function muestraTipoNota() {
      var modalInstance = $uibModal.open({
        animation: true,
        ariaLabelledBy: "modal-title",
        ariaDescribedBy: "modal-body",
        templateUrl: "views/facturacion/ModalTipoNotaCredito.html",
        controller: "ModalTipoNotaCreditoCtrl",
        controllerAs: "$ctrl",
        backdrop: "static",
        keyboard: false,
        size: "md"
      });
      modalInstance.result.then(function(tipo) {
        vm.tipoNota = tipo;
      });
    }

    function Revertir() {
      var modalInstance = $uibModal.open({
        animation: true,
        ariaLabelledBy: "modal-title",
        ariaDescribedBy: "modal-body",
        templateUrl: "views/facturacion/ModalRevierteNota.html",
        controller: "ModalRevertirNotaCtrl",
        controllerAs: "$ctrl",
        backdrop: "static",
        keyboard: false,
        size: "md"
      });
      modalInstance.result.then(function(accion) {
        console.log('accion',accion)
        if (accion === 0) {

         if( vm.tipoNota == 0){
         imprimeFactura();
          $state.go('home.facturacion.notasdecredito');
         }
          
        }
        if (accion === 1) {
          RevertirProceso();

          if( vm.tipoNota === 1){            
            MuestraTicket();
            imprimeFactura();
            $state.go('home.facturacion.notasdecredito');
           }
         
        }
      });
    }

    function MuestraTicket() {
      console.log('muestra ticket');
      NotasDeCreditoFactory.GetReportesNotasDeCredito(vm.clv_nota).then(
        function(res) {
          var Name = res.GetReportesNotasDeCreditoResult;
          vm.ruta = globalService.getUrlReportes() + "/Reportes/" + Name;
           
          $uibModal.open({
            animation: true,
            ariaLabelledBy: "modal-title",
            ariaDescribedBy: "modal-body",
            templateUrl: "views/facturacion/modalSingleTicket.html",
            controller: "ModalTicketNotaCreditoCtrl",
            controllerAs: "ctrl",
            backdrop: "static",
            keyboard: false,
            size: "sm",
            resolve: {
              ruta: function() {
                return vm.ruta;
              }
            }
          });
        }
      );
    }

    function obtenFacturas(contrato) {
      NotasDeCreditoFactory.GetDAME_FACTURASDECLIENTE(contrato, 0).then(
        function(res) {
          vm.facturas = res.GetDAME_FACTURASDECLIENTEResult;
          console.log(res);
        }
      );
    }

    function modificaImporteNota(item) {
      if (vm.factura) {
        NotasDeCreditoFactory.GetModifica_DetalleNotas(
          item.Clv_detalle,
          vm.factura.Clv_factura,
          item.ImporteNota,
          item.seCobra
        ).then(function(res) {
          console.log(res);
        });
      }
    }

    function GetObtieneDatosTicket() {
      if (vm.factura) {
        NotasDeCreditoFactory.GetObtieneDatosTicket(
          vm.factura.Clv_factura
        ).then(function(res) {
          console.log(res);
          GetDetalle_NotasdeCredito();
          getSucursales();
        });
      }
    }

    function ModalClientes() {
      var modalInstance = $uibModal.open({
        animation: true,
        ariaLabelledBy: "modal-title",
        ariaDescribedBy: "modal-body",
        templateUrl: "views/procesos/buscarCliente.html",
        controller: "BuscarNuevoCtrl",
        controllerAs: "$ctrl",
        backdrop: "static",
        keyboard: false,
        size: "lg"
      });
    }

    $rootScope.$on("cliente_select", function(e, contrato) {
      limpia();
      vm.contrato = contrato.CONTRATO;
      vm.contratoBueno = contrato.ContratoBueno;
      AsignaCliente(contrato.ContratoBueno);
    });

    function AsignaCliente(contrato) {
      ordenesFactory.serviciosCliente(contrato).then(function(data) {
        vm.ServiciosCliente = data.GetDameSerDelCliFacListResult;
        console.log(vm.servicios);
        ordenesFactory.buscarCliPorContrato(contrato).then(function(data) {
          console.log(data.GetDeepBUSCLIPORCONTRATO_OrdSerResult);
          vm.datosCli = data.GetDeepBUSCLIPORCONTRATO_OrdSerResult;
          obtenFacturas(vm.contratoBueno);
          vm.usuarioRealiza = $localStorage.currentUser.usuario;
        });
      });
    }

    function obtieneCajas() {
      if (vm.sucursal) {
        NotasDeCreditoFactory.GetObtieneCajasSucursal(
          vm.sucursal.Clv_Sucursal
        ).then(function(res) {
          vm.cajas = res.GetObtieneCajasSucursalResult;
         // AsignaCliente();
        });
      }
    }

    function UsuariosSucursal() {
      if (vm.sucursal) {
        NotasDeCreditoFactory.GetobtenUsuariosSucursal(
          vm.sucursal.Clv_Sucursal
        ).then(function(res) {
          vm.cajeros = res.GetobtenUsuariosSucursalResult;
        });
      }
    }

    function buscarContrato(event) {
     
      if (event.keyCode === 13) {
        event.preventDefault();

        if (!vm.contrato) {
          ngNotify.set("Coloque un contrato válido", "error");
          limpia();
          return;
        }
        if (!vm.contrato.includes("-")) {
          ngNotify.set("Coloque un contrato válido", "error");
          limpia();
          return;
        }
        ordenesFactory.getContratoReal(vm.contrato).then(function(data) {
          limpia();
          vm.contratoBueno = data.GetuspBuscaContratoSeparado2ListResult[0].ContratoBueno;
          vm.contrato = data.GetuspBuscaContratoSeparado2ListResult[0].CONTRATO;
          AsignaCliente(vm.contratoBueno);
        });
      }
    }

    function GetDetalle_NotasdeCredito() {
      if (vm.factura) {
        NotasDeCreditoFactory.GetDetalle_NotasdeCredito(
          vm.factura.Clv_factura,
          0
        ).then(function(res) {
          console.log(res);
          vm.conceptos = res.GetDetalle_NotasdeCreditoResult;
          vm.conceptos.forEach(function(item){
            item.ImporteNota = item.Importe;
          });
          obtieneMontoNota();
        });
      }
    }

    function SetPagosRealizados() {
      var modalInstance = $uibModal.open({
        animation: true,
        ariaLabelledBy: "modal-title",
        ariaDescribedBy: "modal-body",
        templateUrl: "views/facturacion/ModalViewPagosRealizadosNC.html",
        // controller: 'ModalViewPagosRealizadosCtrl',
        controllerAs: "ctrl",
        backdrop: "static",
        keyboard: false,
        class: "modal-backdrop fade",
        size: "md"
      });
    }

    function cambiaImporteNota(item) {
      if (item.ImporteNota > item.Importe) {
        item.ImporteNota = item.Importe;
      }
      if (item.ImporteNota < 0) {
        item.ImporteNota = 0;
      }
      if (item.ImporteNota > 0) {
        item.seCobra = 1;
      } else {
        item.seCobra = 0;
      }
      obtieneMontoNota();
      modificaImporteNota(item);
    }

    function getSucursales() {
      var Parametros = {
        Clv_Sucursal: 0,
        Nombre: "",
        OP: 3,
        idcompania: 0
      };
      CatalogosFactory.GetSUCURSALES(Parametros).then(function(data) {
        vm.sucursales = data.GetSUCURSALESResult;
        console.log(data);
      });
    }

    function GuardaNota() {
      var data = {
        Contrato: vm.contratoBueno,
        Factura: vm.factura.Clv_factura,
        FechaGeneracion: $filter("date")(new Date(), "yyyy/MM/dd")
          .replace("/", "")
          .replace("/", ""),
        UsuarioCaptura: $localStorage.currentUser.usuario,
        Clv_UsuarioAutorizo: $localStorage.currentUser.idUsuario,
        FechaCaducidad: $filter("date")(vm.fechaCaducidad, "yyyy/MM/dd")
          .replace("/", "")
          .replace("/", ""),
        Monto: obtieneMontoNota(),
        Status: "A",
        Observaciones: vm.Observaciones,
        Clv_sucursal: vm.sucursal.Clv_Sucursal,
        tipo: vm.tipoNota,
        Clv_caja: vm.caja.Clave
      };
      console.log(data);
      NotasDeCreditoFactory.GetNueva_NotadeCredito(data).then(function(res) {
        console.log(res.GetNueva_NotadeCreditoResult);
        vm.clv_nota = res.GetNueva_NotadeCreditoResult;
        NotasDeCreditoFactory.GetGuarda_DetalleNotaweb(
          vm.factura.Clv_factura,
          vm.clv_nota
        ).then(function(res) {
          console.log(res);
          Revertir();
        });
      });
    }

    function obtieneMontoNota() {
      var monto = 0;
      vm.conceptos.forEach(function(e) {
        monto = monto + e.ImporteNota;
      });
      vm.MONTO = monto;
      return monto;
    }

    var vm = this;
    vm.SetPagosRealizados = SetPagosRealizados;
    vm.ModalClientes = ModalClientes;
    vm.buscarContrato = buscarContrato;
    vm.GetObtieneDatosTicket = GetObtieneDatosTicket;
    vm.cambiaImporteNota = cambiaImporteNota;
    vm.obtieneCajas = obtieneCajas;
    vm.GuardaNota = GuardaNota;
    vm.detalle = false;
    vm.RevertirProceso=RevertirProceso;
    initData();
  });
