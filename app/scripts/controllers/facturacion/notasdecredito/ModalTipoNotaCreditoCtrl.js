'use strict';

angular
    .module('softvApp')
    .controller('ModalTipoNotaCreditoCtrl', function (NotasDeCreditoFactory, $localStorage, $uibModalInstance) {
        function ok(tipo) {
            $uibModalInstance.close(tipo);
        }
        function Cancel() {           
            $uibModalInstance.dismiss("cancel");
        }

        var vm = this;        
        vm.Cancel = Cancel;
        vm.ok=ok;
    });