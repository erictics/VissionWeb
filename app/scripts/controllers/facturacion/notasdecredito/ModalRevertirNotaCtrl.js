'use strict';

angular
    .module('softvApp')
    .controller('ModalRevertirNotaCtrl', function (NotasDeCreditoFactory, $localStorage, $uibModalInstance) {
        function ok() {
            $uibModalInstance.close(1);
        }
        function Cancel() {  
            $uibModalInstance.close(0);
        }

        var vm = this;        
        vm.Cancel = Cancel;
        vm.ok=ok;
    });