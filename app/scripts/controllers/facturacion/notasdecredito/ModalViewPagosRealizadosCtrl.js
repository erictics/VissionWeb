'use strict';
angular
    .module('softvApp')
    .controller('ModalViewPagosRealizadosCtrl', function ($uibModalInstance, CargosAutomaticosFactory, ClvSessionDetFac, ngNotify) {

        function initialData() {

        }


        function cancel() {
            $uibModalInstance.close('cancel');
        }

        var vm = this;
        vm.cancel = cancel;
        initialData();
    });
