'use strict';
angular
    .module('softvApp')
    .controller('ModalTicketNotaCreditoCtrl', function (ruta,$sce, $uibModalInstance) {
       
        function cancel() {  
            $uibModalInstance.dismiss('cancel');
        }

        var vm = this;
        vm.mostrarImprimir =true;  
        vm.FileName =$sce.trustAsResourceUrl(ruta);
       // console.log(vm.FileName);
        vm.cancel = cancel;        
    });