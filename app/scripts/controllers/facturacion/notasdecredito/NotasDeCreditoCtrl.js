'use strict';

angular
  .module('softvApp')
  .controller('NotasDeCreditoCtrl', function (mizarFactory,NotasDeCreditoFactory,cajasFactory, $filter,CatalogosFactory,atencionFactory, generalesSistemaFactory, $localStorage, $scope, ngNotify, $uibModal, $rootScope, $state, globalService, $sce) {

    function initData() {
      getPlazas();
    /*   generalesSistemaFactory.GetvalidaAccesoFacturacion().then(function (data) {
        if (data.GetvalidaAccesoFacturacionResult === 0) {
          $state.go("home.dashboard");
          ngNotify.set(
            "Su máquina no esta registrada como una caja, por tal motivo no tiene acceso a facturación",
            "warn"
          );
        }
      }); */
     // SetTipoNota();
    }


    function buscaNotas (op) { 

      var params = {
        'op': op,
        'Clv_NotadeCredito':(op ===1)? vm.clv_nota:0,
        'Fecha': (op === 2) ?   $filter('date')(vm.Fecha, 'yyyy/MM/dd'):'1900/01/01',
        'contrato': (op === 3) ? vm.contrato:0,
        'sucursal': (op === 4) ? vm.sucursal.Clv_Sucursal:0,
        'Nombre':'',
        'Idcompania':(op === 10) ? vm.plaza.id_compania: 0
      };
      NotasDeCreditoFactory.GetBUSCANOTASDECREDITO(params).then(function(res){
        vm.notas = res.GetBUSCANOTASDECREDITOResult;
        console.log(res);
      });

    }

    function reimprime(item){
         
      cajasFactory.GetValidaDFCliente(item.Contrato).then(function(data){
         
        if(data.GetValidaDFClienteResult.Resultado === true) {
         
          mizarFactory.GetImprimeFacturaFiscalFac(item.Factura,'T').then(function(res){ 
            if(res.GetImprimeFacturaFiscalFacResult.IdResult > 0){
             var url= globalService.getUrlMizarReportes()+'/Reportes/' + res.GetImprimeFacturaFiscalFacResult.urlReporte;
             $uibModal.open({
               animation: true,
               ariaLabelledBy: 'modal-title',
               ariaDescribedBy: 'modal-body',
               templateUrl: 'views/facturacion/modalFactura.html',
               controller: 'modalFacturaCtrl',
               controllerAs: 'ctrl',
               backdrop: 'static',
               keyboard: false,
               size: 'lg',
               resolve: {
                 url: function() {
                   return url;
                 }
                 
               }
             });
           }else{
             ngNotify.set('La factura no se puede imprimir ya que no cuenta con folio fiscal','error');
           }
         });

        }else {
          MuestraTicket(item.Clv_NotadeCredito);
        }

      });       
      
    }


    function MuestraTicket(clv_nota) {
      NotasDeCreditoFactory.GetReportesNotasDeCredito(clv_nota).then(
        function(res) {
          var Name = res.GetReportesNotasDeCreditoResult;
          vm.ruta = globalService.getUrlReportes() + "/Reportes/" + Name;

          $uibModal.open({
            animation: true,
            ariaLabelledBy: "modal-title",
            ariaDescribedBy: "modal-body",
            templateUrl: "views/facturacion/modalSingleTicket.html",
            controller: "ModalTicketNotaCreditoCtrl",
            controllerAs: "ctrl",
            backdrop: "static",
            keyboard: false,
            size: "sm",
            resolve: {
              ruta: function() {
                return vm.ruta;
              }
            }
          });
        }
      );
    }

    function getPlazas () {
      atencionFactory.getPlazas().then(function (res) {
        vm.plazas = res.GetMuestra_Compania_RelUsuarioListResult;
       console.log(res);
      });
    }

    function getSucursales() {
      var Parametros = {
        'Clv_Sucursal': 0,
        'Nombre': '',
        'OP': 2,
        'idcompania': (vm.plaza) ? vm.plaza.id_compania : 0
      };     
      CatalogosFactory.GetSUCURSALES(Parametros).then(function (data) {
            vm.sucursales=data.GetSUCURSALESResult;
          console.log(data);         
        });

    }

    function SetTipoNota() {
      var modalInstance = $uibModal.open({
        animation: true,
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: 'views/facturacion/ModalSetTipoNotaCredito.html',
        // controller: 'ModalSetTipoNotaCreditoCtrl',
        controllerAs: 'ctrl',
        backdrop: 'static',
        keyboard: false,
        class: 'modal-backdrop fade',
        size: 'sm'
      });
    }

    var vm = this;
    initData();
    vm.getSucursales=getSucursales;
    vm.buscaNotas=buscaNotas;
    vm.reimprime=reimprime;

  });