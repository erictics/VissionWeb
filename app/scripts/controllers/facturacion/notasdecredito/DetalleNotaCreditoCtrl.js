'use strict';

angular
    .module('softvApp')
    .controller('DetalleNotaCreditoCtrl', function (NotasDeCreditoFactory,$stateParams, $filter,CatalogosFactory,ordenesFactory, $localStorage, $scope, ngNotify, $uibModal, $rootScope, $state, globalService, $sce) {

        function initData() {
            NotasDeCreditoFactory.GetConsulta_NotaCredito($stateParams.id).then(function(res){
                var nota = res.GetConsulta_NotaCreditoResult;
               vm.Clv_NotadeCredito = nota.Clv_NotadeCredito;
               vm.Observaciones = nota.Observaciones;
               vm.contratoBueno = nota.Contrato;
               vm.Clv_caja= nota.Clv_caja;
               vm.contrato='';
               vm.Status = nota.Status;
               vm.tipoNota = nota.NotaAFactura;
               AsignaCliente(nota.Contrato);
               obtenFacturas(nota.Contrato,nota.Clv_NotadeCredito,nota.Factura);
               getSucursales(nota.Clv_sucursal);
               getStatus();
               
                console.log(res);
                vm.fechaGeneracion = $filter('date')(new Date(), 'dd/MM/yyyy');
            });
        }

        function GetDetalle_NotasdeCredito(){
            if (vm.factura){
                NotasDeCreditoFactory.GetDetalle_NotasdeCredito(vm.factura.Clv_factura, vm.Clv_NotadeCredito).then(function(res){
                    console.log(res);
                    vm.conceptos= res.GetDetalle_NotasdeCreditoResult;
                    obtieneMontoNota();
                });
            }
            
        }

        function getStatus(){
            NotasDeCreditoFactory.GetStatusNotadeCredito().then(function(res){
                vm.estatusNota= res.GetStatusNotadeCreditoResult;
                vm.estatusNota.forEach(function(item){
                    if(item.Clv_Status ===vm.Status){
                        vm.estatus = item;
                    }                   
                });
                console.log(res);
            });
        }

        function obtieneMontoNota() {
        var monto = 0;
            vm.conceptos.forEach(function(e) {
              monto = monto + e.Importe;
            });
            vm.MONTO = monto;
           
          }

        function obtenFacturas (contrato,nota,Factura) {
            NotasDeCreditoFactory.GetDAME_FACTURASDECLIENTE(contrato,nota).then(function(res){
              vm.facturas = res.GetDAME_FACTURASDECLIENTEResult;
              vm.facturas.forEach(function(item){
                  if(item.Clv_factura === Factura){
                    vm.factura = item;
                    GetDetalle_NotasdeCredito();
                  }
              });                
            });
        } 

        function getSucursales(Clv_sucursal) {
            var Parametros = {
              'Clv_Sucursal': 0,
              'Nombre': '',
              'OP': 3,
              'idcompania': 0
            };     
            CatalogosFactory.GetSUCURSALES(Parametros).then(function (data) {
                  vm.sucursales=data.GetSUCURSALESResult;
                  vm.sucursales.forEach(function(item){
                   if( Clv_sucursal === item.Clv_Sucursal){
                       vm.sucursal = item;
                       obtieneCajas();
                   }
                  });                 
              });
      
          }


        function AsignaCliente (contrato){
            ordenesFactory.serviciosCliente(contrato).then(function (data) {
                vm.ServiciosCliente = data.GetDameSerDelCliFacListResult;
                console.log(vm.servicios);
                ordenesFactory.buscarCliPorContrato(contrato).then(function (data) {
                    console.log(data.GetDeepBUSCLIPORCONTRATO_OrdSerResult)
                    vm.datosCli = data.GetDeepBUSCLIPORCONTRATO_OrdSerResult;
                  //  obtenFacturas(vm.contratoBueno);
                    vm.usuarioRealiza = $localStorage.currentUser.usuario;
                  });
              });             
        }

        function obtieneCajas(){
            if(vm.sucursal){
                NotasDeCreditoFactory.GetObtieneCajasSucursal(vm.sucursal.Clv_Sucursal).then(function(res){
                    vm.cajas = res.GetObtieneCajasSucursalResult;
                    vm.cajas.forEach(function(item){
                        if(vm.Clv_caja === item.Clave){
                            vm.caja = item;
                        }                         
                    });               
                });
            }            
        }

       

        function SetPagosRealizados() {
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'views/facturacion/ModalViewPagosRealizadosNC.html',
                // controller: 'ModalViewPagosRealizadosCtrl',
                controllerAs: 'ctrl',
                backdrop: 'static',
                keyboard: false,
                class: 'modal-backdrop fade',
                size: 'md'
            });
        }

        var vm = this;
        vm.detalle = true;
        vm.SetPagosRealizados = SetPagosRealizados;    
        vm.obtieneCajas = obtieneCajas;    
        initData();

    });