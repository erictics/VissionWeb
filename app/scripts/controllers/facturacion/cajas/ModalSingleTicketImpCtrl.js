'use strict';
angular
	.module('softvApp')
	.controller('ModalSingleTicketImpCtrl', function($uibModalInstance, cajasFactory, factura, imprimir, globalService, $sce) {

		function initialData() {
			cajasFactory.dameTicket(factura).then(function(data) {
				console.log(data);
				var Name = data.GetTicketResult;
				var FileName = globalService.getUrlReportes() + '/Reportes/' + Name;
				console.log(FileName);
				vm.FileName = $sce.trustAsResourceUrl(FileName);				
			});
		
		}

		function Imprimir(){
			cajasFactory.dameTicket(factura).then(function(data) {													
				var Name = data.GetTicketResult;
				var FileName = globalService.getUrlReportes() + '/Reportes/' + Name;
				var xmlhttp = new XMLHttpRequest();
				xmlhttp.open("GET",'http://localhost:8080?q='+FileName,true);
				xmlhttp.send();													
			});
		}

		function cancel() {
			$uibModalInstance.dismiss('cancel');
		}

		/*function printDiv(divName) {
			var printContents = document.getElementById(divName).innerHTML;
			var popupWin = window.open('', '_blank', 'width=1000,height=700');
			popupWin.document.open();
			popupWin.document.write('<html><head><link type="text/css" rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" /></head><body onload="window.print()">' + printContents + '</body></html>');
			popupWin.document.close();
		}*/

		var vm = this;
		vm.cancel = cancel;
		vm.Imprimir = Imprimir
		/*vm.printDiv = printDiv;*/
		/*vm.mostrarImprimir = true;*/
		initialData();
	});
