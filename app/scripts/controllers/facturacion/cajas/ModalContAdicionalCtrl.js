'use strict';

angular
    .module('softvApp')
    .controller('ModalContAdicionalCtrl', function($uibModalInstance,cajasFactory,$uibModal, $rootScope, ngNotify, $state, contrato){

        function init(){
            cajasFactory.GetSP_VALIDAEXTECIONESFAC(contrato).then(function(res){
               console.log(res);
               vm.maximo=res.GetSP_VALIDAEXTECIONESFACResult;
            });
        }

        function ok(){
            if(vm.numero > vm.maximo ) {
              ngNotify.set('El límite maximo de Extensiones Adicionales es' + vm.maximo, 'error');
            }      
            $uibModalInstance.close(vm.numero);
        }

        function cancel() {
            $uibModalInstance.dismiss('cancel');
        }

        var vm = this; 
        init();      
        vm.ok = ok;
        vm.maximo=0;
        vm.cancel = cancel;
   });