'use strict';
angular
	.module('softvApp')
	.controller('modalFacturaCtrl', function($uibModalInstance, url, $sce) {
		this.$onInit = function() {			
			vm.urlReal = $sce.trustAsResourceUrl(url);			
		};

		function cancel() {
			$uibModalInstance.dismiss('cancel');
		}

		var vm = this;
		vm.cancel = cancel;
	});
