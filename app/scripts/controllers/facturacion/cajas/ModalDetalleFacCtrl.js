'use strict';
angular
	.module('softvApp')
	.controller('ModalDetalleFacCtrl', function($uibModalInstance, $uibModal ,cajasFactory, item) {
		function initialData() {
            console.log(item);
                cajasFactory.GetDetalleCobroFac(item.Clv_UnicaNet).then(function(result){
                vm.lista=result.GetDetalleCobroFacResult;
                vm.title='Detalle de cobro de material'+ vm.lista[0].titulo;
                vm.total=0;
                vm.lista.forEach(function(element) {
                    vm.total=vm.total+parseFloat(element.monto);
                });             
            });	
        }
        		
		function cancel() {
			$uibModalInstance.close('');
        }
        
        function MuestraConvenio (){            
        var modalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: "modal-title",
            ariaDescribedBy: "modal-body",
            templateUrl: "views/facturacion/ModalConvenio.html",
            controller: "ModalConvenioCtrl",
            controllerAs: "ctrl",
            backdrop: "static",
            keyboard: false,
            size: "sm",
            resolve: {
                Clv_UnicaNet: function() {
                    return item.Clv_UnicaNet;
                }
            }
        });                    
        
    }

		var vm = this;
        vm.cancel = cancel;
        vm.MuestraConvenio=MuestraConvenio;	
		initialData();
	});