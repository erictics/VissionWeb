'use strict';
angular
	.module('softvApp')
	.controller('ModalCambioDomicilioCtrl', function($uibModalInstance, cajasFactory, items, $rootScope, ngNotify, $uibModal) {

		function initialData() {
			cajasFactory.dameCiudades(items.Contrato).then(function(data) {
				vm.ciudades = data.GetCiudadCAMDOListResult;
			});
		}


		function changeCiudad() {
			cajasFactory.dameLocalidades(items.Contrato, vm.selectedCiudad.Clv_Ciudad).then(function(data) {
				vm.localidades = data.GetLocalidadCAMDOListResult;
			});
		}

		function changeLocalidad() {
			cajasFactory.dameColonias(items.Contrato, vm.selectedLocalidad.Clv_Localidad).then(function(data) {
				vm.colonias = data.GetColoniaCAMDOListResult;
			});
		}

		function changeColonia() {
			cajasFactory.dameCalles(items.Contrato, vm.selectedColonia.CLV_COLONIA).then(function(data) {
				vm.calles = data.GetCalleCAMDOListResult;
			});
		}

		function ok() {
			var objCAMDOFAC = {
				'Clv_Sesion': items.Session,
				'CONTRATO': items.Contrato,
				'Clv_Calle': vm.selectedCalle.Clv_calle,
				'NUMERO': vm.numero,
				'Num_int': vm.numeroInterior,
				'ENTRECALLES': vm.entreCalles,
				'Clv_Colonia': vm.selectedColonia.CLV_COLONIA,
				'Clv_Localidad': vm.selectedLocalidad.Clv_Localidad,
				'TELEFONO': vm.telefono,
				'ClvTecnica': 0,
				'Clv_Ciudad': vm.selectedCiudad.Clv_Ciudad,
				'Clv_Sector': 0,
				'CalleNorte': vm.Obj.CalleNorte,
				'CalleSur': vm.Obj.CalleSur,
				'CalleEste': vm.Obj.CalleEste,
				'CalleOeste': vm.Obj.CalleOeste,
				'Casa': vm.Obj.Casa,
				'Referencia': vm.Obj.referencia
			};
			console.log(objCAMDOFAC);
			cajasFactory.addCamdo(objCAMDOFAC).then(function(data) {console.log(data)});
			cajasFactory.addAdicionales(items.Session, items.Texto, items.Contrato, items.Tipo,0).then(function(data) {
				console.log(data);
				$uibModalInstance.dismiss('cancel');
				$rootScope.$emit('realoadPagos', {});
			});
		}

		function OpenEntreCalles(){
			console.log('X');
            var ObjCli = {
				'ClvColonia': vm.selectedColonia.CLV_COLONIA,
            	'Clv_calle': vm.selectedCalle.Clv_calle,
                'Obj': vm.Obj
			};
			console.log(ObjCli);
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'views/catalogos/CroquisEntreCallesForm.html',
                controller: 'CambioDomicilioFacturacionCtrl',
                controllerAs: 'ctrl',
                backdrop: 'static',
                keyboard: false,
                class: 'modal-backdrop fade',
                size: 'lg',
                resolve: {
                    ObjCli: function () {
                        return ObjCli;
                    }
                }
            });
            modalInstance.result.then(function (Obj) {
                console.log(Obj);
                if(Obj != null){
                    vm.Obj = Obj;
                    vm.entreCalles = vm.Obj.entrecalles;
                }

                vm.calles.forEach(function(element, key){
                    if(element.Clv_calle == vm.Obj.callep){
                        vm.selectedCalle = vm.calles[key];
                    }
                });
            });
        }

		function cancel() {
			cajasFactory.cancelarDomicilio(items.Session, items.Contrato).then(function(data) {
				$uibModalInstance.dismiss('cancel');
				$rootScope.$emit('realoadPagos', {});
			});
		}

		var vm = this;
		vm.Obj = null;
		vm.cancel = cancel;
		vm.changeCiudad = changeCiudad;
		vm.changeLocalidad = changeLocalidad;
		vm.changeColonia = changeColonia;
		vm.OpenEntreCalles = OpenEntreCalles;
		vm.ok = ok;
		console.log(items);
		initialData();
	});
