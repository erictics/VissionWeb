'use strict';
angular
	.module('softvApp')
	.controller('ModalOrdenesCtrl', function($uibModal, $uibModalInstance, cajasFactory, contrato) {

		function initialData() {
			cajasFactory.dameHistorialOrdenes(contrato, vm.selectStatus).then(function(data) {
				if (data.GetBuscaOrdenServicioListResult.length == 0) {
					vm.mostrarMensaje = true;
					vm.ordenes = '';
				} else {
					vm.mostrarMensaje = false;
					vm.ordenes = data.GetBuscaOrdenServicioListResult;
				}
			});
		}

		function sigleOrder(clave) {

			var modalInstance = $uibModal.open({
				animation: true,
				ariaLabelledBy: "modal-title",
				ariaDescribedBy: "modal-body",
				templateUrl: "views/procesos/modalReporteOrdSer.html",
				controller: "modalReporteOrdeSerCtrl",
				controllerAs: "ctrl",
				backdrop: "static",
				keyboard: false,
				class: "modal-backdrop fade",
				size: "lg",
				resolve: {
				  clv_orden: function() {
					return clave;
				  }
				}
			  });
			
		}

		function MuestraAgenda(Clv_Orden) {
            var options = {
                'clv_queja_orden': Clv_Orden,
                'opcion': 1
            };
            var modalInstance = $uibModal.open({
              animation: vm.animationsEnabled,
              ariaLabelledBy: 'modal-title',
              ariaDescribedBy: 'modal-body',
              templateUrl: 'views/procesos/ModalAgendaQueja.html',
              controller: 'ModalAgendaQuejaCtrl',
              controllerAs: 'ctrl',
              backdrop: 'static',
              keyboard: false,
              size: 'sm',
              resolve: {
                options: function () {
                  return options;
                }
              }
            });
		  }
		  
		function cancel() {
			$uibModalInstance.dismiss('cancel');
		}

		var vm = this;
		vm.cancel = cancel;
		vm.selectStatus = 'P';
		vm.statusChange = initialData;
		vm.sigleOrder = sigleOrder;
		vm.MuestraAgenda = MuestraAgenda;
		initialData();
	});
