'use strict';
angular
  .module('softvApp')
  .controller('ModalConvenioCtrl', function ($uibModalInstance,cajasFactory, $uibModal, Clv_UnicaNet, ngNotify) {

    function initialData() {
      cajasFactory.GetspCalculaConvenioCobroMaterial(Clv_UnicaNet).then(function(result){
         console.log(result);
         var datos=result.GetspCalculaConvenioCobroMaterialResult;
         
         vm.contrato=datos.Contrato;
         vm.importe=datos.Suma;
         vm.numPagos=0;
         vm.importeMes=0;
      });
    }

    function cancel() {
			$uibModalInstance.dismiss('cancel');
    }

    function aceptar(){
      var obj={
        'Contrato':vm.contrato,
        'Suma':vm.importe,
        'ImporteMes':vm.importeMes,
        'NumPagos':vm.numPagos,
        'Clv_UnicaNet':Clv_UnicaNet
      };
      cajasFactory.GetspInsertaParcialidadesCobroMaterial(obj).then(function(result){
        ngNotify.set('Convenio Guardado correctamente','success');
        $uibModalInstance.dismiss('cancel');
      });
    }
    var vm = this;
    initialData();
    vm.cancel=cancel;
    vm.aceptar=aceptar;
  });