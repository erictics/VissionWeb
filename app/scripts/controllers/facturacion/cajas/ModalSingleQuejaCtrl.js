'use strict';
angular
	.module('softvApp')
	.controller('ModalSingleQuejaCtrl', function($uibModalInstance,$localStorage, reportesFactory, clave, globalService) {

		function initialData() {

			var obj={				
				'Clv_inicio':clave,
				'Clv_fin': clave,
				'contrato':0,
				'fechasolInicial':'1900/01/01',
				'fechasolFinal':'1900/01/01',
				'fechaejeInicial':'1900/01/01',
				'fechaejeFinal':'1900/01/01',
				'Clv_trabajo':0,
				'op1':1,
				'op2':0,
				'op3':0,
				'op4':0,
				'op5':0,
				'op6':0,
				'op7':1,
				'ejecutados':0,
				'pendientes':0,
				'visitados':0,
				'enproceso':0,
				'OpOrdenar':0,
				'Op':2,
				'clvProblema':0,
				'clv_Depto':0,
				'distribuidores':'',
				'plazas':'',
				'ciudades':'',
				'localidades':'',
				'colonias':'',
				'Clv_usuario': $localStorage.currentUser.idUsuario
			};
			reportesFactory.GetReporteQuejas(obj).then(function(data) {
				vm.url = globalService.getUrlReportes() + '/Reportes/' + data.GetReporteQuejasResult;
				$('#reporteURL').attr('src', vm.url);
			});
		}

		function cancel() {
			$uibModalInstance.dismiss('cancel');
		}

		var vm = this;
		vm.cancel = cancel;
		initialData();
	});
