'use strict';

angular
    .module('softvApp')
    .controller('ModalBonificacionCtrl', function(cajasFactory, $localStorage,options, $uibModalInstance,$scope, $uibModal, ngNotify, $state){

        function initData(){
            
              var a=options.Clv_Session;
              var b= options.CLV_DETALLE;
              cajasFactory.GetDameFilaPreDetFacturas_2(a, b).then(function(res){
                
                  vm.totalConcepto=res.GetDameFilaPreDetFacturas_2Result;
                  cajasFactory.GetDAMEUTLIMODIA().then(function(res){
                    vm.dias=res.GetDAMEUTLIMODIAResult;
                    vm.BonificacionxDia=parseFloat(vm.totalConcepto/vm.dias);
                    

              });
            }); 
        }

         $scope.value = 0;
        $scope.importeBonificar=0;
        
        $scope.$watch('value', function(val,old){
          
          vm.totalABonificar= parseFloat(parseFloat(val) * parseFloat(vm.BonificacionxDia)).toFixed(2);
          $scope.importeBonificar=0;

          if(vm.totalABonificar > vm.totalConcepto ){
            $scope.value=0; 
          }
          
        });

        $scope.$watch('importeBonificar', function(val,old){
            $scope.value=0;
            vm.totalABonificar= val;
            if(vm.totalABonificar > vm.totalConcepto ){
                $scope.importeBonificar=0; 
              }
         }); 
        

        function cancel() {
            $uibModalInstance.dismiss("cancel")
        }

        function Guarda(){
           
             cajasFactory.GetModFilaPreDetFacturasBonificacion(options.Clv_Session,options.CLV_DETALLE,$scope.value,vm.totalABonificar).then(function(res){
                console.log(res)

                cajasFactory.GetInsPreMotivoBonificacion(options.Clv_Session,options.CLV_DETALLE,vm.totalABonificar,vm.Motivo).then(function(data){
                 console.log(data)
                 $uibModalInstance.close(1);
                })
            }) 

        }

        

        var vm = this;
        vm.cancel = cancel;
        vm.Guarda=Guarda;
        vm.totalConcepto=0;
        vm.BonificacionxDia=0;
        vm.dias=0;
        vm.totalABonificar=0;
        vm.descripcion = options.DESCORTA;
        initData(); 
        
    });