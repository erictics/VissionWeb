'use strict';

angular
    .module('softvApp')
    .controller('ModalAutorizaBonificacionCtrl', function(cajasFactory, $localStorage,options, $uibModalInstance,$scope, $uibModal, ngNotify, $state){

        function cancel() {
            $uibModalInstance.dismiss('cancel');
        }

        function save(){

     cajasFactory.GetValidaUserBonAutoriza(vm.usuario,vm.password).then(function(res){

            if(res.GetValidaUserBonAutorizaResult.Resultado===true){
                $uibModalInstance.close(true);
            }
            else{
                ngNotify.set('Las credenciales proporcionadas no pueden autorizar esta bonificación','warn');
            }
         })
           // $uibModalInstance.close(true);
        }

        var vm = this;
        vm.cancel = cancel;
        vm.save=save;
        vm.mensaje=options.mensaje;
    });