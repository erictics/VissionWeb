'use strict';
angular
	.module('softvApp')
	.controller('ModalPagarCtrl', function($uibModalInstance, $uibModal, items, cajasFactory, $state, $rootScope, ngNotify, globalService,mizarFactory, $sce, ticketsFactory,$http) {

		function initialData() {
			vm.monto = items.monto;
			vm.TotalAbonado = 0;
			vm.cambio = 0;
			cajasFactory.dameBancos().then(function(data) {
				data.GetMuestraBancosListResult.unshift({
					'nombre': '----------------',
					'Clave': 0
				});
				vm.bancos = data.GetMuestraBancosListResult;
				vm.selectedBancoTransferencia = data.GetMuestraBancosListResult[0];
				vm.selectedBancoCheque = data.GetMuestraBancosListResult[0];
				vm.BancoTRANSFERENCIA = data.GetMuestraBancosListResult[0];
			});
		}

		function SetCambioAbonado(){
			vm.TotalAbonado = vm.efectivo + vm.dineroCheque + vm.dineroTransferencia + vm.GLOTRANSFERENCIA+vm.pagoNota;
			if((vm.TotalAbonado - vm.monto) <= 0){
				vm.cambio = 0;
			}else{
				vm.cambio = vm.TotalAbonado - vm.monto;
			}
		}
		 

		function cambioEfectivo() {
			var TotalAbonado = 0;
			var cambio = 0;
			if(vm.efectivo >= 0){
				vm.CaseEfectivo = true;
				SetCambioAbonado();
			}else{
				vm.CaseEfectivo = false;
				SetCambioAbonado()
			}
		}

		function cambioCheque() {
			var TotalAbonado = vm.TotalAbonado;
			if(vm.dineroCheque > 0){
				if(vm.dineroCheque <= vm.monto - (vm.dineroTransferencia + vm.GLOTRANSFERENCIA)){
					vm.CaseCheque = true;
					cambioEfectivo();
					SetCambioAbonado();
				}else{
					vm.dineroCheque = (vm.monto - (vm.dineroTransferencia + vm.GLOTRANSFERENCIA));
					if(vm.dineroCheque > 0){
						vm.CaseCheque = true;
						cambioEfectivo();
						SetCambioAbonado();
					}else{
						vm.CaseCheque = false;
						cambioEfectivo();
						SetCambioAbonado();
					}
				}
			}else{
				vm.CaseCheque = false;
				cambioEfectivo();
				SetCambioAbonado();
			}
		}

		function cambioTransferencia() {
			var TotalAbonado = vm.TotalAbonado;
			if(vm.dineroTransferencia > 0){
				if(vm.dineroTransferencia <= vm.monto - (vm.dineroCheque + vm.GLOTRANSFERENCIA)){
					vm.CaseTransferencia = true;
					cambioEfectivo();
					SetCambioAbonado();
				}else{
					vm.dineroTransferencia = (vm.monto - (vm.dineroCheque + vm.GLOTRANSFERENCIA));
					if(vm.dineroTransferencia > 0){
						vm.CaseTransferencia = true;
						cambioEfectivo();
						SetCambioAbonado();
					}else{
						vm.CaseTransferencia = false
						cambioEfectivo();
						SetCambioAbonado();
					}
				}
			}else{
				vm.CaseTransferencia = false
				cambioEfectivo();
				SetCambioAbonado();
			}
		}

		function cambioTransBancaria(event) {
			/*if (event.keyCode === 13) {
                event.preventDefault();*/
				var TotalAbonado = vm.TotalAbonado;
				if(vm.GLOTRANSFERENCIA > 0){
					if(vm.GLOTRANSFERENCIA <= vm.monto - (vm.dineroCheque + vm.dineroTransferencia)){
						vm.CaseTransBancaria = true;
						cambioEfectivo();
						SetCambioAbonado();
					}else{
						vm.GLOTRANSFERENCIA = (vm.monto - (vm.dineroCheque + vm.dineroTransferencia));
						if(vm.GLOTRANSFERENCIA > 0){
							vm.CaseTransBancaria = true;
							cambioEfectivo();
							SetCambioAbonado();
						}else{
							vm.CaseTransBancaria = false;
							cambioEfectivo();
							SetCambioAbonado();
						}
					}
				}else{
					vm.CaseTransBancaria = false;
					cambioEfectivo();
					SetCambioAbonado();
				}
			//}
		}

		function cambioCredito() {
			/*vm.cuentaTransferencia = '';
			vm.dineroTransferencia = 0;
			vm.autorizacionTransferencia = '';
			vm.dineroCheque = 0;
			vm.numeroCheque = '';
			vm.efectivo = 0;
			vm.GLOTRANSFERENCIA = 0;
			vm.casePago = true;
			vm.montoTarjeta='';*/
			var Nota = (vm.dineroCredito > 0)? vm.dineroCredito:0
			cajasFactory.dameMontoCredito(Nota, items.Contrato).then(function(data) {
				vm.pagoNota = data.GetDeepMontoNotaCreditoResult.Monto;
				if(vm.pagoNota == 0){
					//vm.dineroCredito = 0;
					vm.CaseNota = false;
				}else{
					vm.CaseNota = true;
					/*vm.CaseEfectivo = false;
					vm.CaseCheque = false;
					vm.CaseTransferencia = false;*/
				}
				SetCambioAbonado();
				/* vm.TotalAbonado = vm.pagoNota;
				if (vm.TotalAbonado > vm.monto) {
					vm.TotalAbonado = vm.monto;
				} */
			});
		}

		function ImprimeOrden(clv_orden) {
			var modalInstance = $uibModal.open({
				animation: true,
				ariaLabelledBy: "modal-title",
				ariaDescribedBy: "modal-body",
				templateUrl: "views/procesos/modalReporteOrdSer.html",
				controller: "modalReporteOrdeSerCtrl",
				controllerAs: "ctrl",
				backdrop: "static",
				keyboard: false,
				class: "modal-backdrop fade",
				size: "lg",
				resolve: {
				clv_orden: function() {
					return clv_orden;
				}
				}
			});
		}

		function ImprimeFactura(Clv_Factura){
			mizarFactory.GetGraba_Factura_Digital(Clv_Factura).then(function(res){
				console.log(res);
				if(res.GetGraba_Factura_DigitalResult.IdResult > 0){
					console.log('si grabo',globalService.getUrlMizarReportes()+'/Reportes/' +res.GetGraba_Factura_DigitalResult.urlReporte);
					var url= globalService.getUrlMizarReportes()+'/Reportes/' +res.GetGraba_Factura_DigitalResult.urlReporte;	
									
					 $uibModal.open({
						 animation: true,
						 ariaLabelledBy: 'modal-title',
						 ariaDescribedBy: 'modal-body',
						 templateUrl: 'views/facturacion/modalFactura.html',
						 controller: 'modalFacturaCtrl',
						 controllerAs: 'ctrl',
						 backdrop: 'static',
						 keyboard: false,
						 size: 'lg',
						 resolve: {
							 url: function() {
								 return url;
							 }
						 }
					 });
				}else{
				}
			});
		}

		function ok(){
			console.log(vm.CaseEfectivo);
			console.log(vm.CaseNota);
			if(vm.CaseEfectivo == true){
				vm.Cre = vm.dineroCheque + vm.dineroTransferencia + vm.GLOTRANSFERENCIA + vm.pagoNota;
				if(vm.Cre < vm.monto){
					var Efe = vm.monto - vm.Cre;
					if(vm.efectivo >= Efe){
						vm.Efe = Efe;
					}else{
						vm.CaseEfectivo = false;
					}
				}else{
					vm.CaseEfectivo = false;
				}
			}

			if(vm.CaseEfectivo == false &&
			   vm.CaseCheque == false &&
			   vm.CaseTransferencia == false &&
			   vm.CaseNota == false &&
			   vm.CaseTransBancaria == false){
				    if(vm.monto == 0){
						vm.CaseEfectivo == true;
						cajasFactory.nuevoPago(items.IdSession, vm.efectivo, vm.cambio).then(function(dataNuevo) {
							Pagar();
						}); 
					}else{
						ngNotify.set('Por favor llena un metodo de pago.', 'error');
					}
			}else{
				if(vm.TotalAbonado < vm.monto){
					ngNotify.set('No se ha saldado la factura.', 'error');
				}else{
					if(vm.CaseEfectivo == true){
						cajasFactory.nuevoPago(items.IdSession, vm.efectivo, vm.cambio).then(function(dataNuevo) {
							Pagar();
						});
					}else if(vm.CaseNota == true || vm.casePago == true || vm.CaseTransferencia == true || vm.CaseCheque || vm.CaseTransBancaria == true){
						Pagar();
					}
				}
			}
		}
		
		function Pagar(){
			console.log(vm.CaseEfectivo);
			console.log(vm.CaseNota);
			var objPagar = {
				'contrato': items.Contrato,
				'clv_session': items.IdSession,
				'tipo': items.Tipo,
				'serie_v': items.Serie,
				'folio_v': items.Folio,
				'clv_vendedor': items.Vendedor,
				'tipo1': (vm.CaseTransferencia == true)? vm.selectTarjeta:0,
				'monto1': vm.monto,
				'GLOEFECTIVO2': (vm.CaseEfectivo == true)? vm.Efe:0,
				'GLOCHEQUE2': (vm.CaseCheque == true)? vm.dineroCheque:0,
				'GLOCLV_BANCOCHEQUE2': (vm.CaseCheque == true)? vm.selectedBancoCheque.Clave:0,
				'NUMEROCHEQUE2': (vm.CaseCheque == true)? vm.numeroCheque:'',
				'GLOTARJETA2': (vm.CaseTransferencia == true)? vm.dineroTransferencia:0,
				'GLOCLV_BANCOTARJETA2': (vm.CaseTransferencia == true)? vm.selectedBancoTransferencia.Clave:0,
				'NUMEROTARJETA2': (vm.CaseTransferencia == true)? vm.cuentaTransferencia:'',
				'TARJETAAUTORIZACION2': (vm.CaseTransferencia == true)? vm.autorizacionTransferencia:'',
				'CLV_Nota3': (vm.CaseNota == true)? vm.dineroCredito:0,
				'GLONOTA3': (vm.CaseNota == true)? vm.pagoNota:0,
				'GLOTRANSFERENCIA': (vm.CaseTransBancaria == true)? vm.GLOTRANSFERENCIA:0,
				'BancoTRANSFERENCIA': (vm.CaseTransBancaria == true)? vm.BancoTRANSFERENCIA.Clave:0,
				'NumeroTransferencia': (vm.CaseTransBancaria == true)? vm.NumeroTransferencia:'',
				'AutorizacionTRANSFERENCIA': (vm.CaseTransBancaria == true)? vm.AutorizacionTRANSFERENCIA:''
			};
			console.log(objPagar);
			cajasFactory.grabaPago(objPagar).then(function(dataGraba) {
				$uibModalInstance.dismiss('cancel');
				$rootScope.$emit('ocultarPagar', {});
				$rootScope.$emit('getVendedores', {});
				ngNotify.set('Pago Exitoso', 'success');
				vm.Clv_Factura = dataGraba.GetDeepGrabaFacturas2Result.Clv_FacturaSalida;
				//imprime ticket
				 cajasFactory.dameTicket(vm.Clv_Factura).then(function(data) {													
					var Name = data.GetTicketResult;
					var FileName = globalService.getUrlReportes() + '/Reportes/' + Name;
					var xmlhttp = new XMLHttpRequest();
					xmlhttp.open("GET",'http://localhost:8080?q='+FileName,true);
					xmlhttp.send();													
				}); 
				GetTicket();
				//imprime  factura
				if(vm.monto > 0){
					cajasFactory.GetValidaDFCliente(items.Contrato).then(function(data){
						console.log(data);
						cajasFactory.getOrden(vm.Clv_Factura).then(function(orden) {
							if (orden.GetDamelasOrdenesque_GeneroFacturaAgendaOrdserResult.Orden > 0) {
								ImprimeOrden(orden.GetDamelasOrdenesque_GeneroFacturaAgendaOrdserResult.Orden);
							} 
						});
						if(data.GetValidaDFClienteResult.Resultado == true){
							ImprimeFactura(vm.Clv_Factura);
							console.log('imprime Dig');
						}
					});
				}
			});
		}

		function GetTicket(){
			var modalInstance = $uibModal.open({
				animation: true,
				ariaLabelledBy: 'modal-title',
				ariaDescribedBy: 'modal-body',
				templateUrl: 'views/facturacion/modalSingleTicketImp.html',
				controller: 'ModalSingleTicketImpCtrl',
				controllerAs: 'ctrl',
				backdrop: 'static',
				keyboard: false,
				size: 'sm',
				resolve: {
					factura: function() {
					return vm.Clv_Factura;
					},
					imprimir: function() {
					return false;
					}
				}
			});
		}

		/*function ok() {
			if (vm.casePago == undefined) {
				ngNotify.set('Por favor llena un metodo de pago.', 'error');
			} else {
				switch (vm.casePago) {
					case 1:
					
						if (vm.efectivo >= vm.monto) {
							var objPagar = {
								'contrato': items.Contrato,
								'clv_session': items.IdSession,
								'tipo': items.Tipo,
								'serie_v': items.Serie,
								'folio_v': items.Folio,
								'clv_vendedor': items.Vendedor,
								'tipo1': 0,
								'monto1': vm.monto,
								'GLOEFECTIVO2': vm.monto,
								'GLOCHEQUE2': 0,
								'GLOCLV_BANCOCHEQUE2': 0,
								'NUMEROCHEQUE2': '',
								'GLOTARJETA2': 0,
								'GLOCLV_BANCOTARJETA2': 0,
								'NUMEROTARJETA2': '',
								'TARJETAAUTORIZACION2': '',
								'CLV_Nota3': 0,
								'GLONOTA3': 0
							};
							cajasFactory.insertSeguridadToken(items.IdSession).then(function(dataToken) {
								cajasFactory.nuevoPago(items.IdSession, vm.efectivo, vm.cambio).then(function(dataNuevo) {
									cajasFactory.grabaPago(objPagar).then(function(dataGraba) {

										$uibModalInstance.dismiss('cancel');
										$rootScope.$emit('ocultarPagar', {});
										$rootScope.$emit('getVendedores', {});
										ngNotify.set('Pago Exitoso', 'success');
										vm.Clv_Factura = dataGraba.GetDeepGrabaFacturas2Result.Clv_FacturaSalida;
										//imprime ticket
										cajasFactory.dameTicket(vm.Clv_Factura).then(function(data) {													
											var Name = data.GetTicketResult;
											var FileName = globalService.getUrlReportes() + '/Reportes/' + Name;
											var xmlhttp = new XMLHttpRequest();
											xmlhttp.open("GET",'http://localhost:8080?q='+FileName,true);
											xmlhttp.send();													
										}); 
										//imprime  factura
										ImprimeFactura(vm.Clv_Factura);

										//imprime orden
										cajasFactory.getOrden(vm.Clv_Factura).then(function(orden) {
											console.log(orden);
										   if (orden.GetDamelasOrdenesque_GeneroFacturaAgendaOrdserResult.Orden > 0) {

											   ImprimeOrden(orden.GetDamelasOrdenesque_GeneroFacturaAgendaOrdserResult.Orden);
											   
										   } 
									   });

									});
								});
							});
						} else {
							ngNotify.set('No se ha saldado la factura.', 'error');
						}
						break;
					case 2:

						if (vm.selectedBancoCheque.Clave == 0) {
							ngNotify.set('Selecciona un banco.', 'error');
						} else if (vm.numeroCheque == "" || vm.numeroCheque == undefined) {
							ngNotify.set('Digita el número del cheque.', 'error');
						} else {
							if (vm.dineroCheque == vm.monto) {
								var objPagar = {
									'contrato': items.Contrato,
									'clv_session': items.IdSession,
									'tipo': items.Tipo,
									'serie_v': items.Serie,
									'folio_v': items.Folio,
									'clv_vendedor': items.Vendedor,
									'tipo1': 0,
									'monto1': vm.monto,
									'GLOEFECTIVO2': 0,
									'GLOCHEQUE2': vm.dineroCheque,
									'GLOCLV_BANCOCHEQUE2': vm.selectedBancoCheque.Clave,
									'NUMEROCHEQUE2': vm.numeroCheque,
									'GLOTARJETA2': 0,
									'GLOCLV_BANCOTARJETA2': 0,
									'NUMEROTARJETA2': '',
									'TARJETAAUTORIZACION2': '',
									'CLV_Nota3': 0,
									'GLONOTA3': 0
								};
								cajasFactory.insertSeguridadToken(items.IdSession).then(function(data) {

									cajasFactory.grabaPago(objPagar).then(function(dataGraba) {
										$uibModalInstance.dismiss('cancel');
										$rootScope.$emit('ocultarPagar', {});
										$rootScope.$emit('getVendedores', {});
										ngNotify.set('Pago Exitoso', 'success');

										vm.Clv_Factura = dataGraba.GetDeepGrabaFacturas2Result.Clv_FacturaSalida;
										
											//imprime ticket
											cajasFactory.dameTicket(vm.Clv_Factura).then(function(data) {													
												var Name = data.GetTicketResult;
												var FileName = globalService.getUrlReportes() + '/Reportes/' + Name;
												var xmlhttp = new XMLHttpRequest();
												xmlhttp.open("GET",'http://localhost:8080?q='+FileName,true);
												xmlhttp.send();													
											}); 
											//imprime  factura
											ImprimeFactura(vm.Clv_Factura);
	
											//imprime orden
											cajasFactory.getOrden(vm.Clv_Factura).then(function(orden) {
												console.log(orden);
											   if (orden.GetDamelasOrdenesque_GeneroFacturaAgendaOrdserResult.Orden > 0) {
	
												   ImprimeOrden(orden.GetDamelasOrdenesque_GeneroFacturaAgendaOrdserResult.Orden);
												   
											   } 
										   });									
									});
								});
							} else {
								ngNotify.set('No se ha saldado la factura', 'error');
							}
						}
						break;
					case 3:	
						if (vm.selectTarjeta === '' || vm.selectTarjeta === undefined) {
							ngNotify.set('Seleccione un tipo de tarjeta', 'error');
						}
						else if (vm.selectedBancoTransferencia.Clave == 0) {
							ngNotify.set('Selecciona un banco', 'error');
						} else if (vm.cuentaTransferencia == "" || vm.cuentaTransferencia == undefined) {
							ngNotify.set('Digita el número de cuenta por favor.', 'error');
						} else if (vm.autorizacionTransferencia == "" || vm.autorizacionTransferencia == undefined) {
							ngNotify.set('Digita el número de autorizacion.', 'error');
						} else {
							if (vm.dineroTransferencia == vm.monto) {
								var objPagar = {
									'contrato': items.Contrato,
									'clv_session': items.IdSession,
									'tipo': items.Tipo,
									'serie_v': items.Serie,
									'folio_v': items.Folio,
									'clv_vendedor': items.Vendedor,
									'tipo1': vm.selectTarjeta,
									'monto1': vm.monto,
									'GLOEFECTIVO2': 0,
									'GLOCHEQUE2': 0,
									'GLOCLV_BANCOCHEQUE2': 0,
									'NUMEROCHEQUE2': '',
									'GLOTARJETA2': vm.dineroTransferencia,
									'GLOCLV_BANCOTARJETA2': vm.selectedBancoTransferencia.Clave,
									'NUMEROTARJETA2': vm.cuentaTransferencia,
									'TARJETAAUTORIZACION2': vm.autorizacionTransferencia,
									'CLV_Nota3': 0,
									'GLONOTA3': 0
								};
								cajasFactory.insertSeguridadToken(items.IdSession).then(function(data) {
									cajasFactory.grabaPago(objPagar).then(function(dataGraba) {
										$uibModalInstance.dismiss('cancel');
										$rootScope.$emit('ocultarPagar', {});
										$rootScope.$emit('getVendedores', {});
										ngNotify.set('Pago Exitoso', 'success');

										vm.Clv_Factura = dataGraba.GetDeepGrabaFacturas2Result.Clv_FacturaSalida;
										
											//imprime ticket
											cajasFactory.dameTicket(vm.Clv_Factura).then(function(data) {													
												var Name = data.GetTicketResult;
												var FileName = globalService.getUrlReportes() + '/Reportes/' + Name;
												var xmlhttp = new XMLHttpRequest();
												xmlhttp.open("GET",'http://localhost:8080?q='+FileName,true);
												xmlhttp.send();													
											}); 
											//imprime  factura
											ImprimeFactura(vm.Clv_Factura);
	
											//imprime orden
											cajasFactory.getOrden(vm.Clv_Factura).then(function(orden) {
												console.log(orden);
											   if (orden.GetDamelasOrdenesque_GeneroFacturaAgendaOrdserResult.Orden > 0) {
	
												   ImprimeOrden(orden.GetDamelasOrdenesque_GeneroFacturaAgendaOrdserResult.Orden);
												   
											   } 
										   });
	
								
										
									});
								});
							} else {
								ngNotify.set('No se ha saldado la factura', 'error');
							}
						}
						break;
					case 4:
					
						if (vm.dineroCredito == '' || vm.dineroCredito == undefined) {
							ngNotify.set('Seleccione un número de nota por favor', 'error');
						} else {
							if (vm.pagoNota >= vm.monto) {
								var objPagar = {
									'contrato': items.Contrato,
									'clv_session': items.IdSession,
									'tipo': items.Tipo,
									'serie_v': items.Serie,
									'folio_v': items.Folio,
									'clv_vendedor': items.Vendedor,
									'tipo1': 0,
									'monto1': vm.monto,
									'GLOEFECTIVO2': 0,
									'GLOCHEQUE2': 0,
									'GLOCLV_BANCOCHEQUE2': 0,
									'NUMEROCHEQUE2': '',
									'GLOTARJETA2': 0,
									'GLOCLV_BANCOTARJETA2': 0,
									'NUMEROTARJETA2': '',
									'TARJETAAUTORIZACION2': '',
									'CLV_Nota3': vm.dineroCredito,
									'GLONOTA3': vm.pagoNota
								};
								cajasFactory.insertSeguridadToken(items.IdSession).then(function(data) {
									cajasFactory.grabaPago(objPagar).then(function(dataGraba) {
										$uibModalInstance.dismiss('cancel');
										$rootScope.$emit('ocultarPagar', {});
										$rootScope.$emit('getVendedores', {});
										ngNotify.set('Pago Exitoso', 'success');
										vm.Clv_Factura = dataGraba.GetDeepGrabaFacturas2Result.Clv_FacturaSalida;
										
											//imprime ticket
											cajasFactory.dameTicket(vm.Clv_Factura).then(function(data) {													
												var Name = data.GetTicketResult;
												var FileName = globalService.getUrlReportes() + '/Reportes/' + Name;
												var xmlhttp = new XMLHttpRequest();
												xmlhttp.open("GET",'http://localhost:8080?q='+FileName,true);
												xmlhttp.send();													
											}); 
											//imprime  factura
											ImprimeFactura(vm.Clv_Factura);
	
											//imprime orden
											cajasFactory.getOrden(vm.Clv_Factura).then(function(orden) {
	                                             console.log(orden);
											   if (orden.GetDamelasOrdenesque_GeneroFacturaAgendaOrdserResult.Orden > 0) {
	
												   ImprimeOrden(orden.GetDamelasOrdenesque_GeneroFacturaAgendaOrdserResult.Orden);
												   
											   } 
										   });	
									});
								});
							} else {
								ngNotify.set('No se ha saldado la factura', 'error');
							}
						}
						break;*/
					/* case 5:
					    					
						if (vm.selectTarjeta === '' || vm.selectTarjeta === undefined) {
							ngNotify.set('Seleccione un tipo de tarjeta', 'error');
						} else {
							if (vm.montoTarjeta >= vm.monto) {
								var objPagar = {
									'contrato': items.Contrato,
									'clv_session': items.IdSession,
									'tipo': items.Tipo,
									'serie_v': items.Serie,
									'folio_v': items.Folio,
									'clv_vendedor': items.Vendedor,
									'tipo1': vm.selectTarjeta,
									'monto1': vm.monto,
									'GLOEFECTIVO2': 0,
									'GLOCHEQUE2': 0,
									'GLOCLV_BANCOCHEQUE2': 0,
									'NUMEROCHEQUE2': '',
									'GLOTARJETA2': 0,
									'GLOCLV_BANCOTARJETA2': 0,
									'NUMEROTARJETA2': '',
									'TARJETAAUTORIZACION2': '',
									'CLV_Nota3': 0,
									'GLONOTA3': 0
								};
								cajasFactory.insertSeguridadToken(items.IdSession).then(function(data) {
									cajasFactory.grabaPago(objPagar).then(function(dataGraba) {
										$uibModalInstance.dismiss('cancel');
										$rootScope.$emit('ocultarPagar', {});
										$rootScope.$emit('getVendedores', {});
										ngNotify.set('Pago Exitoso', 'success');
										vm.Clv_Factura = dataGraba.GetDeepGrabaFacturas2Result.Clv_FacturaSalida;
										
											//imprime ticket
											cajasFactory.dameTicket(vm.Clv_Factura).then(function(data) {													
												var Name = data.GetTicketResult;
												var FileName = globalService.getUrlReportes() + '/Reportes/' + Name;
												var xmlhttp = new XMLHttpRequest();
												xmlhttp.open("GET",'http://localhost:8080?q='+FileName,true);
												xmlhttp.send();													
											}); 
											//imprime  factura
											ImprimeFactura(vm.Clv_Factura);
	
											//imprime orden
											cajasFactory.getOrden(vm.Clv_Factura).then(function(orden) {
	                                             console.log(orden);
											   if (orden.GetDamelasOrdenesque_GeneroFacturaAgendaOrdserResult.Orden > 0) {
	
												   ImprimeOrden(orden.GetDamelasOrdenesque_GeneroFacturaAgendaOrdserResult.Orden);
												   
											   } 
										   });	
									});
								});
							} else {
								ngNotify.set('No se ha saldado la factura', 'error');
							}
						}
						break;	 */
					/*default:
					
				}
			}
		}*/

		function cancel() {
			$uibModalInstance.dismiss('cancel');
		}

		var vm = this;
		vm.cancel = cancel;
		vm.cambioEfectivo = cambioEfectivo;
		vm.cambioCheque = cambioCheque;
		vm.cambioTransferencia = cambioTransferencia;
		vm.cambioCredito = cambioCredito;
		vm.cambioTransBancaria = cambioTransBancaria;
		vm.cambio = '';
		vm.TotalAbonado = '';
		vm.ok = ok;
		vm.selectTarjeta = '';
		vm.CaseEfectivo = false;
		vm.CaseCheque = false;
		vm.CaseTransferencia = false;
		vm.CaseNota = false;
		vm.CaseTransBancaria = false;
		vm.efectivo = 0;
		vm.dineroCheque = 0;
		vm.dineroTransferencia = 0;
		vm.pagoNota = 0;
		vm.GLOTRANSFERENCIA = 0;
		initialData();
	});
