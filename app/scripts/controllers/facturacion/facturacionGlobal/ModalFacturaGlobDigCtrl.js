'use strict';
angular
    .module('softvApp')
    .controller('ModalFacturaGlobDigCtrl', function (FacturacionGlobalFactory, $localStorage, $uibModalInstance, $uibModal, ngNotify, $state, logFactory, globalService, $sce, urlReporte) {

        function cancel() {
            $uibModalInstance.close();
        }

        var vm = this;
        vm.cancel = cancel;
        vm.urlReal = $sce.trustAsResourceUrl(globalService.getUrlMizarReportes() + '/Reportes/' + urlReporte);

    });