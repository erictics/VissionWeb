'use strict';
angular
    .module('softvApp')
    .controller('ModalFacturacionGlobalCancelarCtrl', function (FacturacionGlobalFactory, $localStorage, $uibModalInstance, $uibModal, ngNotify, $state, logFactory, ObjFacGlo) {

        function SetCancelar(){
            var ObjFacGlo = {
                'IdFactura': vm.IdFactura
            };
            FacturacionGlobalFactory.GetCANCELAFACTGLOBAL(ObjFacGlo).then(function(data){
                vm.MSG = data.GetCANCELAFACTGLOBALResult.MSG;
                var Obj = {
                    'Clv_Factura': vm.IdFactura,
                    'tipo': 'G'
                };
                FacturacionGlobalFactory.GetupsDameFacDig_Clv_FacturaCDF(Obj).then(function(data){
                    vm.Clv_FacturaCDF = data.GetupsDameFacDig_Clv_FacturaCDFResult.Clv_FacturaCDF;
                    vm.MSJ = (vm.Clv_FacturaCDF == 0)? 'No se generó factura Digital':'Se generó factura Digital';
                    if(vm.MSG == ' La Factura Ha Sido Cancelada con Éxito '){
                        ngNotify.set('CORRECTO, ' + vm.MSG + ', ' + vm.MSJ, 'success');
                        cancel();
                    }else{
                        ngNotify.set('ERROR, al cancelar la Factura Global' + ', ' + vm.MSJ, 'warn');
                        cancel();
                    }
                });
            });
        }

        function cancel() {
            $uibModalInstance.close();
        }

        var vm = this;
        vm.IdFactura = ObjFacGlo.Obj.IdFactura;
        vm.ViewForm = true;
        vm.SetCancelar = SetCancelar;
        vm.cancel = cancel;

    });