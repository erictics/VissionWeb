'use strict';

angular
    .module('softvApp')
    .controller('FacturacionGlobalCtrl', function (FacturacionGlobalFactory, generalesSistemaFactory, $localStorage, $scope, ngNotify, $uibModal, $rootScope, $state, globalService, $sce) {

        function initData() {
            generalesSistemaFactory.GetvalidaAccesoFacturacion().then(function (data) {
                if (data.GetvalidaAccesoFacturacionResult === 0) {
                    $state.go("home.dashboard");
                    ngNotify.set(
                    "Su máquina no esta registrada como una caja, por tal motivo no tiene acceso a facturación",
                    "warn"
                    );
                }
            });
            FacturacionGlobalFactory.GetMuestra_Distribuidor_RelUsuario().then(function(data){
                vm.DistribuidorList = data.GetMuestra_Distribuidor_RelUsuarioResult;
                vm.Distribuidor = vm.DistribuidorList[0];
                GetFacGloList(4);
            });
        }

        function GetFacGloList(Op){
            var ObjFacGlo = {
                'Serie': (vm.Serie != undefined && vm.Serie != null && vm.Serie != '' && Op == 0)? vm.Serie:'',
                'factura': (vm.Folio != undefined && vm.Folio != null && vm.Folio != '' && Op == 0)?vm.Folio.toString():'',
                'fecha': (vm.Fecha != undefined && vm.Fecha != null && vm.Fecha != '' && Op == 1)? GetDateSTR(vm.Fecha):'1900-01-01',
                'sucursal': (vm.Sucursal != undefined && vm.Sucursal != null && vm.Sucursal != '' && Op == 2)? vm.Sucursal:'',
                'Op': Op,
                'tipo': 'C',
                'idDistribuidor': (vm.Distribuidor != undefined && vm.Distribuidor != null && vm.Distribuidor != '')? vm.Distribuidor.id_compania:0,
                'ClvUsuario': $localStorage.currentUser.idUsuario
            };
            FacturacionGlobalFactory.GetBuscaFacturasGlobales(ObjFacGlo).then(function(data){
                vm.FacGloList = data.GetBuscaFacturasGlobalesResult;
                vm.ViewList = (vm.FacGloList.length > 0)? true:false;
                ResetFilter();
            });
        }

        function SetTipoFac() {
            var ObjNewFacGlo = {
                'IdDistribuidor': vm.Distribuidor.id_compania
            };
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'views/facturacion/ModalSetTipoFac.html',
                controller: 'ModalSetTipoFacCtrl',
                controllerAs: 'ctrl',
                backdrop: 'static',
                keyboard: false,
                class: 'modal-backdrop fade',
                size: 'sm',
                resolve: {
                    ObjNewFacGlo: function () {
                        return ObjNewFacGlo;
                    }
                }
            });
            modalInstance.result.then(function (urlReporte) {
                GetFacGloList(4);
                if(urlReporte != null && urlReporte != undefined && urlReporte != ''){
                    vm.urlReporte = urlReporte;
                    GetFacturaDig();
                }
            });
        }

        function OpenFacGloView(Obj){
            var NewFacGlo = {
                'IdDistribuidor': vm.Distribuidor.id_compania,
                'Obj': Obj
            };
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'views/facturacion/ModalFacturacionGlobal.html',
                controller: 'ModalFacturacionGlobalViewCtrl',
                controllerAs: 'ctrl',
                backdrop: 'static',
                keyboard: false,
                class: 'modal-backdrop fade',
                size: 'md',
                resolve: {
                    NewFacGlo: function () {
                        return NewFacGlo;
                    }
                }
            });
            modalInstance.result.then(function () {
                GetFacGloList(4);
            });
        }

        function Reimprimir(IdFactura){
            var ObjFac = {
                'oClv_Factura': IdFactura,
                'Tipo': 'G'
            };
            FacturacionGlobalFactory.GetImprimeFacturaFiscalFac(ObjFac).then(function(data){
                var Res = data.GetImprimeFacturaFiscalFacResult;
                var IdResult = Res.IdResult;
                var Message = Res.Message;
                vm.urlReporte = Res.urlReporte;
                if(IdResult == 1){
                    ngNotify.set('CORRECTO, ' + Message + '.', 'success');
                    GetFacturaDig();
                }else{
                    ngNotify.set('ERROR, La Factura Global ya ha sido cancelada con anterioridad.', 'warn');
                }
            });
        }

        function GetFacturaDig(){
            var urlReporte = vm.urlReporte;
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'views/facturacion/ModalFacturaGlobDig.html',
                controller: 'ModalFacturaGlobDigCtrl',
                controllerAs: 'ctrl',
                backdrop: 'static',
                keyboard: false,
                class: 'modal-backdrop fade',
                size: 'lg',
                resolve: {
                    urlReporte: function () {
                        return urlReporte;
                    }
                }
            });
            modalInstance.result.then(function () {

            });
        }

        function OpenFacGloCancelar(Obj){
            if(Obj.cancelada == 0){
            var ObjFacGlo = {
                'IdDistribuidor': vm.Distribuidor.id_compania,
                'Obj': Obj
            };
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'views/facturacion/ModalFacturacionGlobalCancelar.html',
                controller: 'ModalFacturacionGlobalCancelarCtrl',
                controllerAs: 'ctrl',
                backdrop: 'static',
                keyboard: false,
                class: 'modal-backdrop fade',
                size: 'sm',
                resolve: {
                    ObjFacGlo: function () {
                        return ObjFacGlo;
                    }
                }
            });
            modalInstance.result.then(function () {
                GetFacGloList(4);
            });
            }else{
                ngNotify.set('ERROR, La Factura Global ya ha sido cancelada con anterioridad.', 'warn');
            }
        }

        function OpenFacGloReFacturar(Obj){
            var ObjFacGlo = {
                'IdDistribuidor': vm.Distribuidor.id_compania,
                'Obj': Obj
            };
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'views/facturacion/ModalFacturacionGlobalReFacturar.html',
                controller: 'ModalFacturacionGlobalReFacturarCtrl',
                controllerAs: 'ctrl',
                backdrop: 'static',
                keyboard: false,
                class: 'modal-backdrop fade',
                size: 'sm',
                resolve: {
                    ObjFacGlo: function () {
                        return ObjFacGlo;
                    }
                }
            });
            modalInstance.result.then(function () {
                GetFacGloList(4);
            });
        }

        function GetDateSTR(Fecha){
            var D = Fecha.getDate();
            var M = Fecha.getMonth() + 1;
            var Y = Fecha.getFullYear();
            return Y + '-' + D + '-' + M
        }

        function ResetFilter(){
            vm.Serie = null;
            vm.Folio = null;
            vm.Sucursal = null;
            vm.Fecha = null;
        }

        var vm = this;
        vm.GetFacGloList = GetFacGloList;
        vm.SetTipoFac = SetTipoFac;
        vm.OpenFacGloView = OpenFacGloView;
        vm.OpenFacGloReFacturar = OpenFacGloReFacturar;
        vm.OpenFacGloCancelar = OpenFacGloCancelar;
        vm.Reimprimir = Reimprimir;
        initData();

    });