'use strict';
angular
    .module('softvApp')
    .controller('ModalFacturacionGlobalCtrl', function (FacturacionGlobalFactory, $localStorage, $uibModalInstance, $uibModal, ngNotify, $state, logFactory, NewFacGlo) {

        function initialData() {
            GetInporteFacGlo();
        }

        function GetInporteFacGlo(){
            var ObjFacGlo = {
                'fechai': GetDateSTR(vm.FechaI),
                'fechaf': (vm.TipoFacGlo == 1)? GetDateSTR(vm.FechaF):GetDateSTR(vm.FechaI),
                'IdFactura': 0,
                'Op': vm.TipoFacGlo,
                'idDistribuidor': vm.IdDistribuidor
            };
            FacturacionGlobalFactory.GetSP_DameImporteFacturaGlobalDistribuidor(ObjFacGlo).then(function(data){
                var Importe = data.GetSP_DameImporteFacturaGlobalDistribuidorResult;
                vm.Total = Importe.TOTAL;
            });
        }

        function Save(){
            var ObjFacGlo = {
                'fechai': GetDateSTR(vm.FechaI),
                'fechaf': (vm.TipoFacGlo == 1)? GetDateSTR(vm.FechaF):GetDateSTR(vm.FechaI),
                'Op': 2,
                'idcompania': vm.IdDistribuidor
            };
            FacturacionGlobalFactory.GetDime_FacGlo_Ex(ObjFacGlo).then(function(data){
                if(data.GetDime_FacGlo_ExResult.Cont == 0){
                    AddFacGlo();
                }else{
                    var MSJ = (vm.TipoFacGlo == 0)? 'del día ' + GetDateSTR(vm.FechaI):'de los días ' + GetDateSTR(vm.FechaI) + ' - ' + GetDateSTR(vm.FechaF)
                    ngNotify.set('ERROR, La Factura Globlal ' + MSJ + ' ya ha sido generada', 'warn');
                }
            });
        }

        function AddFacGlo(){
            FacturacionGlobalFactory.GetUspDameClvCompañia().then(function(data){
                vm.id_compania = data.GetUspDameClvCompañiaResult.id_compania;
                var ObjFacGlo = {
                    'idcompania': vm.id_compania,
                    'fechai': GetDateSTR(vm.FechaI),
                    'fechaf': (vm.TipoFacGlo == 1)? GetDateSTR(vm.FechaF):GetDateSTR(vm.FechaI),
                    'cajera': vm.CajaNombre,
                    'sucursal': 0,
                    'idDistribuidor': vm.IdDistribuidor
                };
                FacturacionGlobalFactory.Getups_NUEVAFACTGLOBALporcompania(ObjFacGlo).then(function(data){
                    vm.IdFactura = data.Getups_NUEVAFACTGLOBALporcompaniaResult.IdFactura;
                    if(vm.IdFactura > 0){
                        var Obj = {
                            'Clv_Factura': vm.IdFactura,
                            'TipoFact': 'G'
                        };
                        FacturacionGlobalFactory.GetGraba_Factura_Digital_Global(vm.IdFactura).then(function(data){
                            var CFD = data.GetGraba_Factura_Digital_GlobalResult;
                            vm.Clv_FacturaCFD = CFD.IdResult;
                            vm.urlReporte = CFD.urlReporte;
                            ngNotify.set('CORRECTO, Se generó la factura global ' + vm.IdFactura, 'success');
                            cancel();
                        });
                    }else{
                        ngNotify.set('ERROR, Al generar la Factura Global', 'warn');
                    }
                });
            });
        }

        function GetDateSTR(Fecha){
            var D = Fecha.getDate();
            var M = Fecha.getMonth() + 1;
            var Y = Fecha.getFullYear();
            return Y + '-' + D + '-' + M
        }

        function cancel() {
            $uibModalInstance.close(vm.urlReporte);
        }

        var vm = this;
        vm.IdDistribuidor = NewFacGlo.IdDistribuidor;
        vm.CajaNombre = $localStorage.currentUser.CajaNombre;
        vm.FechaI = new Date();
        vm.FechaF = new Date();
        vm.urlReporte = '';
        vm.TipoFacGlo = NewFacGlo.TipoFacGlo;
        vm.GetInporteFacGlo = GetInporteFacGlo;
        vm.ViewForm = false;
        vm.Title = 'Nueva Factura Global De Cajas Por Sucursal'
        vm.Save = Save;
        vm.cancel = cancel;
        initialData();

    });