'use strict';
angular
    .module('softvApp')
    .controller('ModalFacturacionGlobalViewCtrl', function (FacturacionGlobalFactory, $localStorage, $uibModalInstance, $uibModal, ngNotify, $state, logFactory, NewFacGlo) {

        function initialData() {
            GetInporteFacGlo();
        }

        function GetInporteFacGlo(){
            var ObjFacGlo = {
                'fechai': GetDateSTR(vm.FechaI),
                'fechaf': (vm.TipoFacGlo == 1)? GetDateSTR(vm.FechaF):GetDateSTR(vm.FechaI),
                'IdFactura': vm.IdFactura,
                'Op': vm.TipoFacGlo,
                'idDistribuidor': vm.IdDistribuidor
            };
            FacturacionGlobalFactory.GetSP_DameImporteFacturaGlobalDistribuidor(ObjFacGlo).then(function(data){
                var Importe = data.GetSP_DameImporteFacturaGlobalDistribuidorResult;
                vm.Total = Importe.TOTAL;
            });
        }

        function GetDateSTR(Fecha){
            var D = Fecha.getDate();
            var M = Fecha.getMonth() + 1;
            var Y = Fecha.getFullYear();
            return Y + '-' + D + '-' + M
        }

        function GetDateF(Fecha){
            var date = Fecha.split('/');
            return new Date(date[2] + '/' + date[1] + '/' + date[0])
        }

        function cancel() {
            $uibModalInstance.close();
        }

        var vm = this;
        vm.IdDistribuidor = NewFacGlo.IdDistribuidor;
        vm.CajaNombre = NewFacGlo.Obj.cajera;
        vm.TipoFacGlo = (NewFacGlo.Obj.fechai == NewFacGlo.Obj.fechaf)? 0:1;
        vm.FechaI = GetDateF(NewFacGlo.Obj.fechai);
        vm.FechaF = GetDateF(NewFacGlo.Obj.fechaf);
        vm.IdFactura = NewFacGlo.Obj.IdFactura;
        vm.ViewForm = true;
        vm.Title = 'Consulta Factura Global De Cajas Por Sucursal';
        vm.cancel = cancel;
        initialData();

    });