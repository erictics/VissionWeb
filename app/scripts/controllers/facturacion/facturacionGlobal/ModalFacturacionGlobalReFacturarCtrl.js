'use strict';
angular
    .module('softvApp')
    .controller('ModalFacturacionGlobalReFacturarCtrl', function (FacturacionGlobalFactory, $localStorage, $uibModalInstance, $uibModal, ngNotify, $state, logFactory, ObjFacGlo) {

        function ReFacturar(){
            var ObjFacGlo = {
                'Clv_Factura': vm.IdFactura
            };
            FacturacionGlobalFactory.GetValidaReFacturaGlobal(ObjFacGlo).then(function(data){
                var Result = data.GetValidaReFacturaGlobalResult;
                vm.Msg = Result.Msg;
                vm.Res = Result.Res;
                if(vm.Res == 1){
                    ngNotify.set('ERROR, ' + vm.Msg, 'warn');
                    cancel();
                }
            });
        }

        function cancel() {
            $uibModalInstance.close();
        }

        var vm = this;
        vm.IdFactura = ObjFacGlo.Obj.IdFactura;
        vm.ReFacturar = ReFacturar;
        vm.cancel = cancel;

    });