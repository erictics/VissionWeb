'use strict';

angular
    .module('softvApp')
    .controller('ModalSetTipoFacCtrl', function (FacturacionGlobalFactory, $localStorage, $uibModalInstance, $uibModal, ngNotify, $state, logFactory, ObjNewFacGlo) {

        function initData() {
            GetTipoFacGloList();
        }

        function GetTipoFacGloList(){
            var ObjFacGlo = {
                'Op': 4
            };
            FacturacionGlobalFactory.GetMuestra_Tipo_Nota(ObjFacGlo).then(function(data){
                vm.TipoFacGloList = data.GetMuestra_Tipo_NotaResult;
                vm.TipoFacGlo = vm.TipoFacGloList[0];
            });
        }

        function NewFactura() {
            var NewFacGlo = {
                'IdDistribuidor': ObjNewFacGlo.IdDistribuidor,
                'TipoFacGlo': vm.TipoFacGlo.clv
            };
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'views/facturacion/ModalFacturacionGlobal.html',
                controller: 'ModalFacturacionGlobalCtrl',
                controllerAs: 'ctrl',
                backdrop: 'static',
                keyboard: false,
                class: 'modal-backdrop fade',
                size: 'md',
                resolve: {
                    NewFacGlo: function () {
                        return NewFacGlo;
                    }
                }
            });
            modalInstance.result.then(function (urlReporte) {
                vm.urlReporte = urlReporte;
                Cancel();
            });
        }

        function Cancel() {
            $uibModalInstance.close(vm.urlReporte);
        }

        var vm = this;
        vm.NewFactura = NewFactura;
        vm.Cancel = Cancel;
        initData();
    });