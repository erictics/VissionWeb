'use strict';
angular
    .module('softvApp')
    .controller('DesgloseDeMonedaReporteCtrl', function(desgloseFactory, $uibModalInstance, $uibModal, ngNotify, $state, $sce, globalService, NameFile){

        function initData(){
            //EntregaEfectivoFactory.GetDocEntregaEfectivoCorp(IdEntrega).then(function(data){
                vm.FileName = $sce.trustAsResourceUrl(globalService.getUrlReportes() + '/Reportes/' + NameFile);
            //});
        }

        function cancel() {
            $uibModalInstance.close();
        }

        var vm = this;
        console.log(NameFile);
        vm.cancel = cancel;
        initData();
        
    });