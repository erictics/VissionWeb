'use strict';
angular
    .module('softvApp')
    .controller('EntregaEfectivoUpdateCtrl', function(EntregaEfectivoFactory, $uibModalInstance, $uibModal, ngNotify, $state, IdEntrega, $localStorage){

        function intData(){
            GetEntrega();
        }

        function GetEntrega(){
            var ObjEntrega = {
                'IdEntrega': IdEntrega
            };
            EntregaEfectivoFactory.GetConsultarEntregaDeEfectivo(ObjEntrega).then(function(data){
                var EntEfe = data.GetConsultarEntregaDeEfectivoResult;
                vm.IdEntrega = EntEfe.IdEntrega
                vm.Fecha = ToDateF(EntEfe.Fecha);
                vm.EfeDis = EntEfe.EfectivoDisponible;
                vm.Cometra = EntEfe.ImporteCometra;
                vm.DepBanc = EntEfe.ImporteDeoBancario;
                vm.Coriporativo = EntEfe.ImporteCorporativo;
                vm.Diferencia = EntEfe.Diferencia;
                vm.CometraF = ToDateF(EntEfe.FechaCometra);
                vm.DepBancF = ToDateF(EntEfe.FechaDepBancario);
                vm.CoriporativoF = ToDateF(EntEfe.FechaCorporativo);
                vm.RecibeCorporativo = EntEfe.RecibeCorporativo;
                vm.FechaCorporativoFin = ToDateF(EntEfe.FechaCorporativoFin);
                EntregaEfectivoFactory.GetMuestra_Plaza_RelUsuario($localStorage.currentUser.idUsuario).then(function(data){
                    vm.DistribuidoresList = data.GetMuestra_Plaza_RelUsuarioResult;
                    vm.DistribuidoresList.forEach(function(element, key) {
                        if(element.Clv_Plaza == EntEfe.Clv_plaza){
                            vm.Distribuidor = vm.DistribuidoresList[key];
                            GetSucursalList(EntEfe.Clv_sucursal);
                        }
                    });
                });
            });
        }

        function GetSucursalList(Clv_sucursal){
            if(vm.Distribuidor != undefined || vm.Distribuidor != null){
                EntregaEfectivoFactory.GetMUESTRASucursalesPorDistribuidor(vm.Distribuidor.Clv_Plaza).then(function(data){
                    vm.SucursalList = data.GetMUESTRASucursalesPorDistribuidorResult;
                    vm.SucursalList.forEach(function(element, key){
                        if(element.Clv_Sucursal == Clv_sucursal){
                            vm.Sucursal = vm.SucursalList[key];
                        }
                    });
                });
            }
        }

        function SaveEntrega(){
            var ObjEntrega = {
                'IdEntrega': vm.IdEntrega,
                'ImporteCometra': vm.Cometra,
                'ImporteDeoBancario': vm.DepBanc,
                'ImporteCorporativo': vm.Coriporativo,
                'Diferencia': vm.Diferencia,
                'FechaCometra': GetDatetoString(vm.CometraF),
                'FechaDepBancario': GetDatetoString(vm.DepBancF),
                'FechaCorporativo': GetDatetoString(vm.CoriporativoF),
                'RecibeCorporativo': vm.RecibeCorporativo,
                'FechaCorporativoFin': GetDatetoString(vm.FechaCorporativoFin)
            };
            EntregaEfectivoFactory.GetModEntregaEfectivo(ObjEntrega).then(function(data){
                ngNotify.set('CORRECTO, se guardó entrega de efectivo.', 'success');
                if(vm.Coriporativo > 0){
                    GetDocEntrgaEfectivoCorp();
                }else{
                    Close();
                }
            });
        }

        function GetDocEntrgaEfectivoCorp(){
            var IdEntrega = vm.IdEntrega;
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'views/facturacion/ReporteEntregaEfectivoCorp.html',
                controller: 'ReporteEntregaEfectivoCorpCtrl',
                controllerAs: 'ctrl',
                backdrop: 'static',
                keyboard: false,
                class: 'modal-backdrop fade',
                size: 'lg',
                resolve: {
                    IdEntrega: function () {
                        return IdEntrega;
                    }
                }
            });
            modalInstance.result.then(function () {
                Close();
            });
        }

        function ToDateF(date) {
            var part = date.split(" ");
            var parts = part[0].split("/");
            return new Date(parts[2], parts[1] - 1, parts[0]);
        }

        function GetDatetoString(Fecha){
            var D = Fecha.getDate();
            var M = Fecha.getMonth() + 1;
            var Y = Fecha.getFullYear();
            return D + '/' + M + '/' + Y;
        }

        function Close() {
            $uibModalInstance.close();
        }

        var vm = this;
        vm.BtnCanTitulo ='Cancelar';
        vm.ShowSave = false;
        vm.ShowSave1 = true;
        vm.ShowSave2 = false;
        vm.GetSucursalList = GetSucursalList;
        vm.SaveEntrega = SaveEntrega;
        vm.Close = Close;
        intData();

    });