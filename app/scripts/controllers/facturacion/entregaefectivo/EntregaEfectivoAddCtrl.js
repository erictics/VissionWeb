'use strict';
angular
    .module('softvApp')
    .controller('EntregaEfectivoAddCtrl', function(EntregaEfectivoFactory, $uibModalInstance, $uibModal, ngNotify, $state, $localStorage){

        function initData() {
            GetDistribuidoresList();
        }

        function GetDistribuidoresList(){
            EntregaEfectivoFactory.GetMuestra_Plaza_RelUsuario($localStorage.currentUser.idUsuario).then(function(data){
                vm.DistribuidoresList = data.GetMuestra_Plaza_RelUsuarioResult;
                vm.Distribuidor = vm.DistribuidoresList[0];
                GetSucursalList();
            });
        }

        function GetSucursalList(){
            if(vm.Distribuidor != undefined || vm.Distribuidor != null){
                EntregaEfectivoFactory.GetMUESTRASucursalesPorDistribuidor(vm.Distribuidor.Clv_Plaza).then(function(data){
                    vm.SucursalList = data.GetMUESTRASucursalesPorDistribuidorResult;
                    vm.Sucursal = vm.SucursalList[0];
                    GetEfectivo();
                });
            }
        }

        function GetEfectivo(){
            var ObjEntrega = {
                'Clv_sucursal': vm.Sucursal.Clv_Sucursal,
                'Fecha': GetDatetoString(vm.Fecha)
            };
            EntregaEfectivoFactory.GetBuscaEfectivoDisponiblePorSucursal(ObjEntrega).then(function(data){
                vm.EfeDis = data.GetBuscaEfectivoDisponiblePorSucursalResult;
            });
        }

        function SaveEntrega(){
            var ObjEntregaV = {
                'Clv_sucursal': vm.Sucursal.Clv_Sucursal,
                'Fecha':GetDatetoString(vm.Fecha)
            };
            EntregaEfectivoFactory.GetExisteEntregaDeEfectivo(ObjEntregaV).then(function(data){
                if(data.GetExisteEntregaDeEfectivoResult.existe == false){
                    var ObjEntrega = {
                        'Clv_sucursal': vm.Sucursal.Clv_Sucursal,
                        'Fecha': GetDatetoString(vm.Fecha),
                        'EfectivoDisponible': vm.EfeDis,
                        'ImporteCometra': vm.Cometra,
                        'ImporteDeoBancario': vm.DepBanc,
                        'ImporteCorporativo': vm.Coriporativo,
                        'Diferencia': vm.Diferencia,
                        'Usuario': $localStorage.currentUser.usuario,
                        'FechaCometra': GetDatetoString(vm.CometraF),
                        'FechaDepBancario': GetDatetoString(vm.DepBancF),
                        'FechaCorporativo': GetDatetoString(vm.CoriporativoF),
                        'RecibeCorporativo': (vm.Coriporativo > 0)? vm.RecibeCorporativo:'',
                        'FechaCorporativoFin': GetDatetoString(vm.FechaCorporativoFin)
                    };
                    EntregaEfectivoFactory.GetNueEntregaEfectivo(ObjEntrega).then(function(data){
                        vm.IdEntrega = data.GetNueEntregaEfectivoResult.IdEntrega
                        if(vm.IdEntrega > 0){
                            ngNotify.set('CORRECTO, se guardó entrega de efectivo.', 'success');
                            if(vm.Coriporativo > 0){
                                GetDocEntrgaEfectivoCorp();
                            }else{
                                Close();
                            }
                        }else{
                            ngNotify.set('ERROR, no se guardó la entrega de efectivo.', 'warn');
                            Close();
                        }
                        
                    });
                }else{
                    ngNotify.set('ERROR, ya existe esta Entrega de efectivo registrada.', 'warn');
                }
            });
        }

        function GetDocEntrgaEfectivoCorp(){
            var IdEntrega = vm.IdEntrega;
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'views/facturacion/ReporteEntregaEfectivoCorp.html',
                controller: 'ReporteEntregaEfectivoCorpCtrl',
                controllerAs: 'ctrl',
                backdrop: 'static',
                keyboard: false,
                class: 'modal-backdrop fade',
                size: 'lg',
                resolve: {
                    IdEntrega: function () {
                        return IdEntrega;
                    }
                }
            });
            modalInstance.result.then(function () {
                Close();
            });
        }

        function GetDatetoString(Fecha){
            var D = Fecha.getDate();
            var M = Fecha.getMonth() + 1;
            var Y = Fecha.getFullYear();
            return D + '/' + M + '/' + Y;
        }

        function Close() {
            $uibModalInstance.close();
        }

        var vm = this;
        vm.BtnCanTitulo ='Cancelar';
        vm.ShowSave = false;
        vm.ShowSave1 = false;
        vm.ShowSave2 = false;
        vm.Fecha = new Date();
        vm.CometraF = new Date();
        vm.DepBancF = new Date();
        vm.CoriporativoF = new Date();
        vm.FechaCorporativoFin = new Date();
        vm.Cometra = 0;
        vm.DepBanc = 0;
        vm.Coriporativo = 0;
        vm.Diferencia = vm.EfeDis - (vm.Cometra + vm.Coriporativo + vm.DepBanc);
        vm.GetSucursalList = GetSucursalList;
        vm.GetEfectivo = GetEfectivo;
        vm.SaveEntrega = SaveEntrega;
        vm.Close = Close;
        initData();
    });