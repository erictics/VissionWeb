'use strict';
angular
    .module('softvApp')
    .controller('EntregaEfectivoCtrl', function(EntregaEfectivoFactory, reportesVariosFactory, generalesSistemaFactory, $localStorage, $uibModal, ngNotify, $state){
        
        this.$onInit = function() {
            generalesSistemaFactory.GetvalidaAccesoFacturacion().then(function(data) {
                if (data.GetvalidaAccesoFacturacionResult === 0) {
                $state.go("home.dashboard");
                ngNotify.set(
                    "Su máquina no esta registrada como una caja, por tal motivo no tiene acceso a facturación",
                    "warn"
                );
                }
            });
        };

        function initData() {
            GetDistribuidoresList();
        }

        function GetDistribuidoresList(){
            EntregaEfectivoFactory.GetMuestra_Plaza_RelUsuario($localStorage.currentUser.idUsuario).then(function(data){
                vm.DistribuidoresList = data.GetMuestra_Plaza_RelUsuarioResult;
                vm.Distribuidor = vm.DistribuidoresList[0];
                GetEntregaEfeList(0);
                GetSucursalList();
            });
        }

        function GetSucursalList(){
            if(vm.Distribuidor != undefined || vm.Distribuidor != null){
                EntregaEfectivoFactory.GetMUESTRASucursalesPorDistribuidor(vm.Distribuidor.Clv_Plaza).then(function(data){
                    vm.SucursalList = data.GetMUESTRASucursalesPorDistribuidorResult;
                });
            }
        }

        function GetEntregaEfeList(op){
            var ObjEntrega = {
                'op': op,
                'Clv_plaza': (vm.Distribuidor != undefined || vm.Distribuidor != null)? vm.Distribuidor.Clv_Plaza:0,
                'Clv_sucursal': (vm.Sucursal != undefined || vm.Sucursal != null)? vm.Sucursal.Clv_Sucursal:0,
                'Fecha': (vm.Fecha != undefined || vm.Fecha != null)? GetDateS(vm.Fecha):'',
                'FechaFin': (vm.FechaFin != undefined || vm.FechaFin != null)? GetDateS(vm.FechaFin):''
            };
            EntregaEfectivoFactory.GetBuscaEntregaEfectivo(ObjEntrega).then(function(data){
                vm.EntregaEfeList = data.GetBuscaEntregaEfectivoResult;
            });
        }

        function OpenEntregaEfectivoAdd(){
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'views/facturacion/EntregaEfectivoForm.html',
                controller: 'EntregaEfectivoAddCtrl',
                controllerAs: 'ctrl',
                backdrop: 'static',
                keyboard: false,
                class: 'modal-backdrop fade',
                size: 'md'
            });
            modalInstance.result.then(function () {
                initData();
            });
        }        

        function OpenEntregaEfectivoUpdate(IdEntrega){          
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'views/facturacion/EntregaEfectivoForm.html',
                controller: 'EntregaEfectivoUpdateCtrl',
                controllerAs: 'ctrl',
                backdrop: 'static',
                keyboard: false,
                class: 'modal-backdrop fade',
                size: 'md',
                resolve: {
                    IdEntrega: function () {
                        return IdEntrega;
                    }
                }
            });
            modalInstance.result.then(function () {
                initData();
            });
        }

        function OpenEntregaEfectivoView(IdEntrega){          
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'views/facturacion/EntregaEfectivoForm.html',
                controller: 'EntregaEfectivoViewCtrl',
                controllerAs: 'ctrl',
                backdrop: 'static',
                keyboard: false,
                class: 'modal-backdrop fade',
                size: 'md',
                resolve: {
                    IdEntrega: function () {
                        return IdEntrega;
                    }
                }
            });
            modalInstance.result.then(function () {
                initData();
            });
        }

        function GetDateS(Fecha){
            var D = Fecha.getDate();
            var M = Fecha.getMonth() + 1;
            var Y = Fecha.getFullYear();
            return D + '/' + M + '/' + Y
        }

        var vm = this;
        vm.GetSucursalList = GetSucursalList;
        vm.GetEntregaEfeList = GetEntregaEfeList;
        vm.OpenEntregaEfectivoAdd = OpenEntregaEfectivoAdd;
        vm.OpenEntregaEfectivoUpdate = OpenEntregaEfectivoUpdate;
        vm.OpenEntregaEfectivoView = OpenEntregaEfectivoView;
        initData();

    });