'use strict';
angular
    .module('softvApp')
    .controller('ReporteEntregaEfectivoCorpCtrl', function(EntregaEfectivoFactory, $uibModalInstance, $uibModal, ngNotify, $state, $sce, globalService, IdEntrega){

        function initData(){
            EntregaEfectivoFactory.GetDocEntregaEfectivoCorp(IdEntrega).then(function(data){
                vm.FileName = $sce.trustAsResourceUrl(globalService.getUrlReportes() + '/Reportes/' + data.GetDocEntregaEfectivoCorpResult);
            });
        }

        function cancel() {
            $uibModalInstance.close();
        }

        var vm = this;
        vm.cancel = cancel;
        initData();
        
    });