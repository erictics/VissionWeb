'use strict';
angular
  .module('softvApp')
  .controller('editaservicioCtrl', function (atencionFactory, trabajosFactory, ngNotify, $stateParams, logFactory) {

    function init() {
      
      trabajosFactory.GetSoftv_GetTrabajoById($stateParams.id)
        .then(function (result) {
          atencionFactory.getServicios().then(function (data) {
            vm.servicios = data.GetMuestraTipSerPrincipalListResult;
            var trabajo = result.GetSoftv_GetTrabajoByIdResult;            
            vm.servicios.forEach(function (item) {         
              if (item.Clv_TipSerPrincipal === trabajo.clv_tipser) {
                vm.servicio = item;
              }
            });
            vm.clv_trabajo = trabajo.clv_trabajo;
            vm.clave = trabajo.trabajo;
            vm.titulo = 'Edita trabajo - ' + vm.clave;
            vm.descripcion = trabajo.descripcion;
            vm.individual = trabajo.puntos;
            vm.proceso = trabajo.sica;
            vm.descarga = trabajo.secobramaterial;
            vm.tipo = trabajo.tipo;
            vm.blockservicio=true;
            vm.pCuadrilla=trabajo.pCuadrilla,
            vm.blockmateriales = (vm.descarga === true) ? false : true;

            obtenarticulosacom();
            consultamateriales();

            if (trabajo.tipo === 'Q') {
              vm.showimputable = true;
              MuestraImputables(trabajo);
            }
            
            
          });
        });
   
    }

    function MuestraImputables(trabajo){
      trabajosFactory.GetMuestra_Imputables().then(function (data) {
        vm.imputables = data.GetMuestra_ImputablesResult;
        vm.imputables.forEach(function (item) {
          if (item.idimputable === trabajo.imputable) {
            vm.imputable = item;
          }
        });

      });
    }


    function save() {
      if (vm.relaciones.length === 0 && vm.descarga === true) {
        ngNotify.set('capture el material o desabilite la opcion de cobro de material', 'warn');
        return;
      }
      var params = {
        'clv_trabajo': vm.clv_trabajo,
        'clv_tipser': vm.servicio.Clv_TipSerPrincipal,
        'trabajo': vm.clave,
        'descripcion': vm.descripcion,
        'puntos': vm.individual,
        'cobranza': 0,
        'tipo': vm.tipo,
        'pCuadrilla':(vm.pCuadrilla)?vm.pCuadrilla:0,
        'prospectos': 0,
        'sica': vm.proceso,
        'secobramaterial': vm.descarga
      };
      trabajosFactory.GetSoftv_EditTrabajo(params).then(function (data) {       

        if (data.GetSoftv_AddTrabajoResult === -1) {
          ngNotify.set('El trabajo no se pudo guardar ,por que existe una clave regitrada con el mismo valor', 'error');
        } else {
          var log={
            'Modulo':'home.areatecnica',
            'Submodulo':'home.areatecnica.editaservicio',
            'Observaciones':'Se edito un nuevo Trabajo ',
            'Comando':JSON.stringify(params),
            'Clv_afectada':vm.clv_trabajo,
            'IdClassLog':2
           };
          logFactory.AddMovSist(log).then(function(result){ console.log('add'); });

          vm.clv_trabajo = data.GetSoftv_AddTrabajoResult;
          vm.blockdescarga = true;
          vm.blocksave = true;
          vm.blockform = true;
          ngNotify.set('El trabajo se guardo con exito ,ahora puede continuar con la configuaración o salir del catálogo');
        }
      });
    }

    function agregarelacion() {
      if(!vm.descripcionart){
        ngNotify.set('Selecciona un artículo','warn');
        return;
      }
      if(vm.cantidad < 1){
        ngNotify.set('Selecciona una cantidad Mayor a 0 para agregar el articulo','warn');
        return;
     }
     if(!vm.cantidad){
      ngNotify.set('Selecciona una cantidad Mayor a 0 para agregar el articulo','warn');
      return;
   }
      trabajosFactory.
      GetSoftv_AddRelMaterialTrabajo(vm.clasificacion.clvtipo, vm.descripcionart.clave,
          vm.cantidad, vm.clv_trabajo, vm.servicio.Clv_TipSerPrincipal)
        .then(function (data) {
          if (data.GetSoftv_AddRelMaterialTrabajoResult === -1) {
            var log={
              'Modulo':'home.areatecnica',
              'Submodulo':'home.areatecnica.editaservicio',
              'Observaciones':'Se agrego una Relacion  de Material a un Trabajo ',
              'Comando':JSON.stringify(vm.clasificacion.clvtipo, vm.descripcionart.clave,
                vm.cantidad, vm.clv_trabajo, vm.servicio.Clv_TipSerPrincipal),
              'Clv_afectada':vm.clv_trabajo,
              'IdClassLog':51
            };
             logFactory.AddMovSist(log).then(function(result){ console.log('add'); });

            ngNotify.set('El artículo ya esta registrado ', 'warn');
          }
          consultamateriales();
        });
    }

    function obtenarticulosacom() {    
      trabajosFactory.GetMuestra_Articulos_Acometida()
        .then(function (result) {
          vm.articulosacom = result.GetMuestra_Articulos_AcometidaResult;
        });
    }

    function bloquearmaterial() {
      vm.blockmateriales = (vm.descarga === true) ? false : true;
    }

    function consultamateriales() {
      trabajosFactory.GetConsultaRelMaterialTrabajos(vm.clv_trabajo)
        .then(function (data) {
          vm.relaciones = data.GetConsultaRelMaterialTrabajosResult;
        });
    }

    function obtenarticulos() {
      var servicio=(vm.servicio)?vm.servicio.Clv_TipSerPrincipal:0;
      trabajosFactory.GetMuestra_Articulos_Clasificacion(vm.clasificacion.clvtipo, servicio)
        .then(function (data) {
          vm.articulos = data.GetMuestra_Articulos_ClasificacionResult;
        });
    }

    function DeleteRelacio(clvmaterial){
      trabajosFactory.GetSoftv_DeleteRelMaterialTrabajo(clvmaterial).then(function(data){
        ngNotify.set('Se eliminó la relación','success');
        consultamateriales();
      });
    }

    var vm = this;
    init();
    vm.blockdescarga = false;
    vm.blocksave = false;
    vm.blockform = false;
    vm.obtenarticulos = obtenarticulos;
    vm.agregarelacion = agregarelacion;
    vm.save = save;
    vm.showimputable = false;
    vm.bloquearmaterial = bloquearmaterial;
    vm.DeleteRelacio = DeleteRelacio;
  });
