'use strict';
angular
  .module('softvApp')
  .controller('nuevoNapCtrl', function (areaTecnicaFactory, $uibModalInstance, ngNotify, logFactory) {

    function init(){
      GetHUBList(4);
      GetOLTList();
    }

    function GetHUBList(op){
      areaTecnicaFactory.GetConHub(vm.clv_sector, vm.clv_sector,  vm.clv_sector, op).then(function(data){
        vm.HUBList = data.GetConHubResult;
        vm.HUB = vm.HUBList[0];
      });
    }

    function GetOLTList(){
      areaTecnicaFactory.GetMUESTRAOlt(vm.clv_poste).then(function(data){
        vm.OLTList = data.GetMUESTRAOltResult;
      });
    }
    
    function GetColoniaList(Clv_Colonia, Op){
      areaTecnicaFactory.GetMuestraColoniaHub(Clv_Colonia, vm.clv_sector, Op).then(function(data){
        vm.ColoniaList = data.GetMuestraColoniaHubResult;
        vm.Colonia = vm.ColoniaList[0];
        GetCalleList(0);
      });
    }

    function GetCalleList(op){
      var ObjCalle = {
        'Clv_Sector': vm.clv_sector,
        'Clv_Colonia': (vm.Colonia.IdColonia != undefined && vm.Colonia.IdColonia != null)? vm.Colonia.IdColonia:0,
        'Clv_Calle': 0,
        'Op': op
      };
      areaTecnicaFactory.GetMuestraCalleHub(ObjCalle).then(function(data){
        vm.CalleList = data.GetMuestraCalleHubResult;
        vm.Calle = vm.CalleList[0];
      });
    }
  
    function SaveNAP(){
      if(vm.Clavetecnica == null){
        AddNap();
      }else{
        UpdateNap();
      }
    }

    function AddNap(){
      var ObjNAP = {
        'clv_sector': vm.HUB.Clv_Sector,
        'clv_colonia': 0,
        'clv_calle': 0,
        'clv_poste': vm.OLT.id,
        'Ingenieria': (vm.Ingenieria != null)? vm.Ingenieria:0,
        'Salidas': (vm.Salidas != null)? vm.Salidas:0,
        'Clavetecnica': '',
        'NoCasas': 0,
        'NoNegocios': 0,
        'NoLotes': 0,
        'NoServicios': 0,
        'FrenteANumero': ''
      };
      areaTecnicaFactory.GetINSERTAnap(ObjNAP).then(function(data){
        vm.IdTap = data.GetINSERTAnapResult.IdTap;
        vm.MSJ = data.GetINSERTAnapResult.MSJ;
        if(vm.IdTap > 0){
          var log={
            'Modulo':'home.areatecnica',
            'Submodulo':'home.areatecnica.nuevanap',
            'Observaciones':'Se agrego un Nap ',
            'Comando':JSON.stringify(ObjNAP),
            'Clv_afectada':0,
            'IdClassLog':1
           };
            logFactory.AddMovSist(log).then(function(result){ console.log('add'); });

          ngNotify.set('CORRECTO, se guardó el NAP.', 'success');
          vm.AfterSave = true;
          GetNAP();
         }else{
          ngNotify.set('ERROR, ' + vm.MSJ, 'warn');
        }
      });
    }

    function UpdateNap(){
      var ObjNAP = {
        'IdTap': vm.IdTap,
        'clv_sector': vm.HUB.Clv_Sector,
        'clv_colonia': vm.Colonia.IdColonia,
        'clv_calle': vm.Calle.IdCalle,
        'clv_poste': vm.OLT.id,
        'Ingenieria': (vm.Ingenieria > 0)? vm.Ingenieria:0,
        'Salidas': (vm.Salidas)? vm.Salidas:0,
        'Clavetecnica': vm.Clavetecnica,
        'NoCasas': (vm.NCasas != null)? vm.NCasas:0,
        'NoNegocios': (vm.NNegocios != null)? vm.NNegocios:0,
        'NoLotes': (vm.NLotes != null)? vm.NLotes:0,
        'NoServicios': (vm.NServicios != null)? vm.NServicios:0,
        'FrenteANumero': (vm.FrenteN)? vm.FrenteN:''
      };
      areaTecnicaFactory.GetMODIFICAnap(ObjNAP).then(function(data){
        var log={
          'Modulo':'home.areatecnica',
          'Submodulo':'home.areatecnica.nuevanap',
          'Observaciones':'Se edito un Nap ',
          'Comando':JSON.stringify(ObjNAP),
          'Clv_afectada':vm.IdTap,
          'IdClassLog':2
         };
          logFactory.AddMovSist(log).then(function(result){ console.log('add'); });

        ngNotify.set('CORRECTO, se guardó el NAP.', 'success');
        Close();
      });
    }

    function GetNAP(){
      var Parametros = {
        'Op': 6,
        'IdTap': vm.IdTap,
        'Clave': '',
        'Sector': '',
        'Poste': '',
        'Colonia': '',
        'Calle': ''
      };
      areaTecnicaFactory.GetCONSULTAnap(Parametros).then(function(data){
        var NAP = data.GetCONSULTAnapResult[0];
        vm.Clavetecnica = NAP.Clavetecnica;
        vm.Ingenieria = NAP.Ingenieria;
        vm.Salidas = NAP.Salidas;
        vm.clv_sector = NAP.clv_sector;
        vm.clv_poste = NAP.clv_poste;
        GetHUBList(3);
        GetOLTList();
        GetColoniaList(0, 1);
        GetCalleList(0);
      });
    }

    function Close(){
      $uibModalInstance.close();
    }

    var vm = this;
    vm.Titulo = 'Nuevo NAP';
    vm.Icono = 'fa fa-plus';
    vm.BtnClose = 'Cancelar';
    vm.Clavetecnica = null;
    vm.clv_sector = 0;
    vm.clv_poste = 0;
    vm.Ingenieria = 0;
    vm.Salidas = 0;
    vm.NCasas = 0;
    vm.NNegocios = 0;
    vm.NLotes = 0;
    vm.NServicios = 0;
    vm.FormDis = false;
    vm.AfterSave = false;
    vm.SaveNAP = SaveNAP;
    vm.GetCalleList = GetCalleList;
    vm.Close = Close;
    init();

  });