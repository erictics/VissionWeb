'use strict';
angular
    .module('softvApp')
    .controller('ModalTarjetaOLTCtrl', function(areaTecnicaFactory, $uibModal, $uibModalInstance, ngNotify, $state, IdOLT, logFactory){

        function initData(){
            GetTarjetaOLTList();
        }
        
        function GetTarjetaOLTList(){
            areaTecnicaFactory.GetCatalogoTarjetasOlt(IdOLT).then(function(data){
                vm.TarjetaOLTList = data.GetCatalogoTarjetasOltResult;
                vm.ViewList = (vm.TarjetaOLTList.length > 0)? true:false;
            });
        }

        function SaveTarjeta(){
            var ObjTarjetaOLT = {
                'Calve': (vm.OP == 2)? vm.Clave:0,
                'descripcion': vm.Descripcion,
                'PUERTOS': vm.Puertos,
                'IdOlt': vm.IdOLT
            };
            areaTecnicaFactory.GetInsertaNueCatalogoTarjetasOlt(ObjTarjetaOLT).then(function(data){
                var log={
                    'Modulo':'home.areatecnica',
                    'Submodulo':'home.areatecnica.postes',
                    'Observaciones':'Se agrego una Tarjeta ',
                    'Comando':JSON.stringify(ObjTarjetaOLT),
                    'Clv_afectada':0,
                    'IdClassLog':1
                 };
                  logFactory.AddMovSist(log).then(function(result){ console.log('add'); });

                 var MSJ = (vm.OP == 1)? 'CORRECTO, se añadió una Tarjeta.' : 'CORRECTO, se guardó ls Tarjeta.';
                 ngNotify.set(MSJ , 'success');
                 GetTarjetaOLTList();
                 RecetForm();
            });
        }

        function UpdateTarjetaOLT(ObjTarjetaOLT){
            vm.Clave = ObjTarjetaOLT.Calve;
            vm.Descripcion = ObjTarjetaOLT.descripcion;
            vm.Puertos = ObjTarjetaOLT.PUERTOS;
            vm.OP = 2;
        }

        function RecetForm(){
            vm.Clave = '';
            vm.Descripcion = '';
            vm.Puertos = null;
            vm.OP = 1;
        }

        function Close() {
            $uibModalInstance.close();
        }

        var vm = this;
        vm.OP = 1;
        vm.SaveTarjeta = SaveTarjeta;
        vm.UpdateTarjetaOLT = UpdateTarjetaOLT;
        vm.RecetForm = RecetForm;
        vm.Close = Close;
        vm.IdOLT = IdOLT;
        initData();

    });