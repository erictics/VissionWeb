'use strict';
angular
  .module('softvApp')
  .controller('oltCtrl', function (areaTecnicaFactory, trabajosFactory, ngNotify, $uibModal,$scope, logFactory) {
    function initData() {
      getolt();
    }

    function getolt() {
      areaTecnicaFactory.GetMuestraDescOlt(0).then(function (data) {
        vm.postes = data.GetMuestraDescOltResult;
      });
    }

    function Add() {
      areaTecnicaFactory.GetValidaNueDescOLT(0, vm.descripcion).then(function(data){
        if(data.GetValidaNueDescOLTResult == 0){
          areaTecnicaFactory.GetInsertaNueDescOlt(0, vm.descripcion).then(function (result) {
            var log={
              'Modulo':'home.areatecnica',
              'Submodulo':'home.areatecnica.olt',
              'Observaciones':'Se agrego una Olt ',
              'Comando':JSON.stringify(0, vm.descripcion),
              'Clv_afectada':(0, vm.descripcion),
              'IdClassLog':1
            };
            logFactory.AddMovSist(log).then(function(result){ console.log('add'); });

            ngNotify.set('Correcto, Se guardo OLT', 'success');
            getolt();
           });
          }else{
          ngNotify.set('Error, La Descripción que ingresó ya existe', 'warn');
          getolt();
        }
      });
    }

    function Update(CLAVE,DESCRIPCION) {
      var CLAVE = CLAVE;
      var DESCRIPCION = DESCRIPCION;
      var OP=2;
      var modalInstance = $uibModal.open({
        animation: true,
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: 'views/areatecnica/ModalPoste.html',
        controller: 'ModalUpdatePosteCtrl',
        controllerAs: 'ctrl',
        backdrop: 'static',
        keyboard: false,
        class: 'modal-backdrop fade',
        size: 'sm',
        resolve: {          
          CLAVE: function () {
            return CLAVE;
          },
          DESCRIPCION: function() {
              return DESCRIPCION;
          }
          ,
          OP: function() {
              return OP;
          }                
        }
      });
      modalInstance.result.then(function () {
        getolt();
      });
    }

    function OpenViewTarjetaOLT(Id) {
      var IdOLT = Id;
      var modalInstance = $uibModal.open({
        animation: true,
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: 'views/areatecnica/ModalTarjetaOLT.html',
        controller: 'ModalTarjetaOLTCtrl',
        controllerAs: 'ctrl',
        backdrop: 'static',
        keyboard: false,
        class: 'modal-backdrop fade',
        size: 'md',
        resolve: {          
          IdOLT: function () {
            return IdOLT;
          }              
        }
      });
    }

    var vm = this;
    initData();    
    vm.Add = Add;
    vm.Update = Update;
    vm.titulo = 'OLT';
    vm.OpTarjeta = true;
    vm.OpenViewTarjetaOLT = OpenViewTarjetaOLT;
    
  });