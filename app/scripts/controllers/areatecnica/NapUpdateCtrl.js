'use strict';
angular
  .module('softvApp')
  .controller('NapUpdateCtrl', function (areaTecnicaFactory, $uibModalInstance, ngNotify, IdTap, logFactory) {

    function init(){
        GetNAP();
    }
    
    function GetHUBList(op){
      areaTecnicaFactory.GetConHub(vm.clv_sector, vm.clv_sector,  vm.clv_sector, op).then(function(data){
        vm.HUBList = data.GetConHubResult;
        vm.HUB = vm.HUBList[0];
      });
    }
    
    function GetOLTList(){
      areaTecnicaFactory.GetMUESTRAOlt(vm.clv_poste).then(function(data){
        vm.OLTList = data.GetMUESTRAOltResult;
        vm.OLT = vm.OLTList[0];
      });
    }
    
    function GetColoniaList(Op){
        var Colonia = (Op == 1)? 0:vm.clv_colonia
        areaTecnicaFactory.GetMuestraColoniaHub(Colonia, vm.clv_sector, Op).then(function(data){
            vm.ColoniaList = data.GetMuestraColoniaHubResult;
            vm.Colonia = vm.ColoniaList[0];
        });
    }

    function GetCalleList(op){
        var ClvColonia = (op == 0)? vm.Colonia.IdColonia:vm.clv_colonia;
        var ClvCalle = (op == 0)? 0:vm.clv_calle;
        var ObjCalle = {
            'Clv_Sector': vm.clv_sector,
            'Clv_Colonia': ClvColonia,
            'Clv_Calle': ClvCalle,
            'Op': op
      };
      areaTecnicaFactory.GetMuestraCalleHub(ObjCalle).then(function(data){
        vm.CalleList = data.GetMuestraCalleHubResult;
        vm.Calle = vm.CalleList[0];
      });
    }
    
    function SaveNAP(){
        var ObjNAP = {
            'IdTap': vm.IdTap,
            'clv_sector': vm.HUB.Clv_Sector,
            'clv_colonia': vm.Colonia.IdColonia,
            'clv_calle': vm.Calle.IdCalle,
            'clv_poste': vm.OLT.id,
            'Ingenieria': (vm.Ingenieria != null)? vm.Ingenieria:0,
            'Salidas': (vm.Salidas)? vm.Salidas:0,
            'Clavetecnica': vm.Clavetecnica,
            'NoCasas': (vm.NCasas != null)? vm.NCasas:0,
            'NoNegocios': (vm.NNegocios != null)? vm.NNegocios:0,
            'NoLotes': (vm.NLotes != null)? vm.NLotes:0,
            'NoServicios': (vm.NServicios != null)? vm.NServicios:0,
            'FrenteANumero': (vm.FrenteN)? vm.FrenteN:''
        };
        areaTecnicaFactory.GetMODIFICAnap(ObjNAP).then(function(data){
          var log={
            'Modulo':'home.areatecnica',
            'Submodulo':'home.areatecnica.nuevanap',
            'Observaciones':'Se edito un Nap ',
            'Comando':JSON.stringify(ObjNAP),
            'Clv_afectada':vm.IdTap,
            'IdClassLog':2
          };
            logFactory.AddMovSist(log).then(function(result){ console.log('add'); });
  
            ngNotify.set('CORRECTO, se guardó el NAP.', 'success');
            Close();
        });
    }

    function GetNAP(){
      var Parametros = {
        'Op': 6,
        'IdTap': vm.IdTap,
        'Clave': '',
        'Sector': '',
        'Poste': '',
        'Colonia': '',
        'Calle': ''
      };
      areaTecnicaFactory.GetCONSULTAnap(Parametros).then(function(data){
        var NAP = data.GetCONSULTAnapResult[0];
        vm.Clavetecnica = NAP.Clavetecnica;
        vm.Ingenieria = NAP.Ingenieria;
        vm.Salidas = NAP.Salidas;
        vm.clv_sector = NAP.clv_sector;
        vm.clv_poste = NAP.clv_poste;
        vm.Clavetecnica = NAP.Clavetecnica;
        vm.clv_colonia = NAP.clv_colonia;
        vm.clv_calle = NAP.clv_calle;
        vm.NCasas = NAP.NoCasas;
        vm.NNegocios = NAP.NoNegocios;
        vm.NLotes = NAP.NoLotes;
        vm.NServicios = NAP.NoServicios;
        vm.FrenteN = NAP.FrenteANumero;
        GetHUBList(3);
        GetOLTList();
        GetColoniaList(2);
        GetCalleList(1);
      });
    }
    
    function Close(){
      $uibModalInstance.close();
    }

    var vm = this;
    vm.Titulo = 'Editar NAP';
    vm.Icono = 'fa fa-edit';
    vm.BtnClose = 'Cancelar';
    vm.IdTap = IdTap;
    vm.FormDis = false;
    vm.AfterSave = true;
    vm.SaveNAP = SaveNAP;
    vm.GetCalleList = GetCalleList;
    vm.Close = Close;
    init();

  });