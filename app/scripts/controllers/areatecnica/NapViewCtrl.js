'use strict';
angular
  .module('softvApp')
  .controller('NapViewCtrl', function (areaTecnicaFactory, $uibModalInstance, ngNotify, IdTap) {

    function init(){
        GetNAP();
    }
    
    function GetHUBList(op){
      areaTecnicaFactory.GetConHub(vm.clv_sector, vm.clv_sector,  vm.clv_sector, op).then(function(data){
        vm.HUBList = data.GetConHubResult;
        vm.HUB = vm.HUBList[0];
      });
    }
    
    function GetOLTList(){
      areaTecnicaFactory.GetMUESTRAOlt(vm.clv_poste).then(function(data){
        vm.OLTList = data.GetMUESTRAOltResult;
        vm.OLT = vm.OLTList[0];
      });
    }
    
    function GetColoniaList(Op){
        var Colonia = (Op == 1)? 0:vm.clv_colonia
        areaTecnicaFactory.GetMuestraColoniaHub(Colonia, vm.clv_sector, Op).then(function(data){
            vm.ColoniaList = data.GetMuestraColoniaHubResult;
            vm.Colonia = vm.ColoniaList[0];
        });
    }

    function GetCalleList(op){
        var ClvColonia = (op == 0)? vm.Colonia.IdColonia:vm.clv_colonia;
        var ClvCalle = (op == 0)? 0:vm.clv_calle;
        var ObjCalle = {
            'Clv_Sector': vm.clv_sector,
            'Clv_Colonia': ClvColonia,
            'Clv_Calle': ClvCalle,
            'Op': op
      };
      areaTecnicaFactory.GetMuestraCalleHub(ObjCalle).then(function(data){
        vm.CalleList = data.GetMuestraCalleHubResult;
        vm.Calle = vm.CalleList[0];
      });
    }

    function GetNAP(){
      var Parametros = {
        'Op': 6,
        'IdTap': vm.IdTap,
        'Clave': '',
        'Sector': '',
        'Poste': '',
        'Colonia': '',
        'Calle': ''
      };
      areaTecnicaFactory.GetCONSULTAnap(Parametros).then(function(data){
        var NAP = data.GetCONSULTAnapResult[0];
        vm.Clavetecnica = NAP.Clavetecnica;
        vm.Ingenieria = NAP.Ingenieria;
        vm.Salidas = NAP.Salidas;
        vm.clv_sector = NAP.clv_sector;
        vm.clv_poste = NAP.clv_poste;
        vm.Clavetecnica = NAP.Clavetecnica;
        vm.clv_colonia = NAP.clv_colonia;
        vm.clv_calle = NAP.clv_calle;
        vm.NCasas = NAP.NoCasas;
        vm.NNegocios = NAP.NoNegocios;
        vm.NLotes = NAP.NoLotes;
        vm.NServicios = NAP.NoServicios;
        vm.FrenteN = NAP.FrenteANumero;
        GetHUBList(3);
        GetOLTList();
        GetColoniaList(2);
        GetCalleList(1);
      });
    }
    
    function Close(){
      $uibModalInstance.close();
    }

    var vm = this;
    vm.Titulo = 'Consultar NAP';
    vm.Icono = 'fa fa-eye';
    vm.BtnClose = 'Cancelar';
    vm.IdTap = IdTap;
    vm.FormDis = true;
    vm.AfterSave = false;
    vm.GetCalleList = GetCalleList;
    vm.Close = Close;
    init();

  });