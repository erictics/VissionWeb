'use strict'
angular
  .module('softvApp')
  .controller('ModalDeletehubCtrl', function (CatalogosFactory, $rootScope, areaTecnicaFactory, $uibModalInstance, ngNotify, $state, ObjHUB, logFactory) {

    function Delete() {
        areaTecnicaFactory.GetBorHub(ObjHUB.id).then(function (result) {
            if (result.GetBorHubResult === 1) {
                ngNotify.set('No se puede Eliminar. Hay una relación de un Nap con éste HUB.', 'error');
            } else {
                var log={
                    'Modulo':'home.areatecnica',
                    'Submodulo':'home.areatecnica.hub',
                    'Observaciones':'Se elimino un Hub ',
                    'Comando':JSON.stringify(ObjHUB.id),
                    'Clv_afectada':ObjHUB.id,
                    'IdClassLog':3
                 };
                 logFactory.AddMovSist(log).then(function(result){ console.log('add'); });

                ngNotify.set('El HUB se ha eliminado correctamente', 'success');
                cancel();
            }
        });
    }

    function cancel() {
      $uibModalInstance.close();
    }

    var vm = this;
    vm.nombre = ObjHUB.Descripcion;
    vm.Icono = 'fa fa-less';
    vm.cancel = cancel;
    vm.Delete = Delete;
    vm.Titulo = 'Eliminar Registro';
    
  });