'use strict';
angular
  .module('softvApp')
  .controller('hubCtrl', function (areaTecnicaFactory, $uibModal, $scope) {

    function init() {
      Gethub(0);
    }

    function Gethub(op) {
      var clv_Sector = 0;
      var Clv_Txt = (op === 1) ? vm.clave : '';
      var Descripcion = (op === 2) ? vm.descripcion : '';
      areaTecnicaFactory.GetConHub(clv_Sector, Clv_Txt, Descripcion, op).then(function (data) {
        vm.hubs = data.GetConHubResult;
        vm.clave = null;
        vm.descripcion = null;
      });
    }

    function Elimina(obj) {
      var ObjHUB = {
        'Descripcion': obj.Descripcion,
        'id': obj.Clv_Sector
      };
      var modalInstance = $uibModal.open({
        animation: true,
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: 'views/areatecnica/ModalDeletehub.html',
        controller: 'ModalDeletehubCtrl',
        controllerAs: 'ctrl',
        backdrop: 'static',
        keyboard: false,
        class: 'modal-backdrop fade',
        size: 'sm',
        resolve: {
          ObjHUB: function () {
            return ObjHUB;
          }
        }
      });
      modalInstance.result.then(function () {
          Gethub(0);
      });
    }

    function AddHub(op, id) {
      var opcion = {
        'opcion': op,
        'id': id
      };
      var modalInstance = $uibModal.open({
        animation: true,
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: 'views/areatecnica/ModalSector.html',
        controller: 'ModalAddhubCtrl',
        controllerAs: 'ctrl',
        backdrop: 'static',
        keyboard: false,
        class: 'modal-backdrop fade',
        size: 'md',
        resolve: {
          opcion: function () {
            return opcion;
          }
        }
      });
    }

    $scope.$on("reloadlista", function (e) {
      Gethub(0);
    });

    var vm = this;
    init();
    vm.Gethub = Gethub;
    vm.AddHub = AddHub;
    vm.Elimina = Elimina;

  });