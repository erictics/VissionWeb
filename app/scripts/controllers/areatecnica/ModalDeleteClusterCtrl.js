'use strict'
angular
  .module('softvApp')
  .controller('ModalDeleteClusterCtrl', function (clusterFactory, $rootScope, $uibModalInstance, ngNotify, $state, options, logFactory) {

    function cancel() {
      $uibModalInstance.close();
    }

    function Delete() {
      clusterFactory.GetInsertUpdateCluster(2, options.Clv_txt, options.Descripcion, options.Clv_cluster).then(function (data) {
        if(data.GetInsertUpdateClusterResult == 1){
          var log={
            'Modulo':'home.areatecnica',
            'Submodulo':'home.areatecnica.clusters',
            'Observaciones':'Se elimino un cluster ',
            'Comando':JSON.stringify(2, options.Clv_txt, options.Descripcion, options.Clv_cluster),
            'Clv_afectada':options.Clv_cluster,
            'IdClassLog':3
        };
        logFactory.AddMovSist(log).then(function(result){ console.log('add'); });

          ngNotify.set('CORRECTO, El Cluster se ha eliminado', 'success');
          cancel(); 
        }else{
          ngNotify.set('ERROR, No se puede Eliminar. Está relacionado con un sector.', 'error');
          cancel();
        }
      });
    }

    var vm = this;
    vm.nombre = options.Descripcion;
    vm.Icono = 'fa fa-less';
    vm.cancel = cancel;
    vm.Delete = Delete;
    vm.Titulo = 'Eliminar Registro';
    
  });