'use strict';

angular
  .module('softvApp')
  .controller('servicioClienteCtrl', function (atencionFactory, trabajosFactory,areaTecnicaFactory) {
    atencionFactory.getServicios().then(function (data) {
      vm.servicios = data.GetMuestraTipSerPrincipalListResult;
      vm.servicio = vm.servicios[0];
      obtentrabajos();
    });

    function obtentrabajos() {
      trabajosFactory.GetSoftv_GetTrabajoByClv_TipSer(vm.servicio.Clv_TipSerPrincipal)
        .then(function (result) {
          vm.trabajos = result.GetSoftv_GetTrabajoByClv_TipSerResult;
        });
    }

    function ObtenXtipo(){

      trabajosFactory.GetSoftv_GetTrabajoByClv_TipSer(vm.servicio.Clv_TipSerPrincipal)
      .then(function (result) {
        vm.trabajos = result.GetSoftv_GetTrabajoByClv_TipSerResult;
        var tra=[];
        vm.trabajos.forEach(function(item){
          if(item.tipo==vm.tipo.id){
            tra.push(item);
          }
        })
        vm.trabajos=tra;
      });
      
      /* console.log(vm.servicio.Clv_TipSerPrincipal,vm.tipo.id);
   
      areaTecnicaFactory.GetSoftv_GetTrabajoByClv_TipSerTipo(vm.servicio.Clv_TipSerPrincipal,vm.tipo.id).then(function(res){
        console.log(res)
      }); */
    }

    

    var vm = this;
    vm.obtentrabajos = obtentrabajos;
    vm.ObtenXtipo=ObtenXtipo;
    vm.tipos=[
      {
        'id':'O',
        'nombre':'Ordenes'
      },
      {
        'id':'Q',
        'nombre':'Quejas'
      },
      {
        'id':'M',
        'nombre':'Mantenimiento'
      }
    ]

  });