'use strict'
angular
  .module('softvApp')
  .controller('ModalAddSectorCtrl', function (areaTecnicaFactory, $uibModalInstance, ngNotify, $state, logFactory) {

    function initData() {
      GetColon(0);
    }

    function AddSector() {
      var Parametros = {
        'Descripcion': vm.Descripcion,
        'Clv_Txt': vm.Clv_Txt,
        'op': 0
      };
      areaTecnicaFactory.GetNueSector(Parametros).then(function (data) {
        console.log(data);
        if(data.GetNueSectorResult > 0){
          var log={
            'Modulo':'home.areatecnica',
            'Submodulo':'home.areatecnica.sectores',
            'Observaciones':'Se agrego un nuevo sector ',
            'Comando':JSON.stringify(Parametros),
            'Clv_afectada':0,
            'IdClassLog':1
           };
          logFactory.AddMovSist(log).then(function(result){ console.log('add'); });

          ngNotify.set('CORRECTO, se añadió un sector nuevo.', 'success');
          vm.Clv_Sector = data.GetNueSectorResult;
          vm.blockForm2 = false;
          vm.blocksave = true;
          vm.DisClv = true;
          vm.DisDesc = true;
         }else{
          ngNotify.set('ERROR, al añadir un sector nuevo.', 'warn');
        }
      });
    }

    function GetColon(op) {
      var Parametros = {
        'Clv_Colonia': (op === 0) ? vm.Colonia : 0,
        'Clv_Sector': (op === 0) ? vm.Clave_sector : 0,
        'op': op
      };
      areaTecnicaFactory.GetColonias(Parametros)
        .then(function (data) {
          vm.colonias = data.GetMuestraColoniaSecResult;
          vm.colonia = vm.colonias[0];
        });
    }

    function GetRelaColSect() {
      var Parametros = {
        'Clv_Sector': vm.Clv_Sector
      };
      areaTecnicaFactory.GetConRelSectorColonia(Parametros).then(function (data) {
        vm.RelColonias = data.GetConRelSectorColoniaResult;
      });
    }

    function NuevaRelacionSecColonia() {
      var Parametros = {
        'Clv_Sector': vm.Clv_Sector,
        'Clv_Colonia': vm.Colonia.IdColonia
      };
      areaTecnicaFactory.GetNueRelSectorCol(Parametros).then(function (data) {
        var result = data.GetNueRelSectorColoniaResult;
        if (data.GetNueRelSectorColoniaResult == 0) {
          var log={
            'Modulo':'home.areatecnica',
            'Submodulo':'home.areatecnica.sectores',
            'Observaciones':'Se agrego una relacion colonia con un sector ',
            'Comando':JSON.stringify(Parametros),
            'Clv_afectada':vm.Clv_Sector,
            'IdClassLog':47
           };
           logFactory.AddMovSist(log).then(function(result){ console.log('add'); });

          ngNotify.set('Se agrego correctamente la relación.', 'success');
         } else {
          ngNotify.set('La colonia ya esta relacionada con este sector.', 'warn');
        }
        GetRelaColSect();
      });
    }

    function BorrarRelacionSecColonia() {
      var Parametros = {
        'Clv_Colonia': vm.Colonia.IdColonia
      };
      areaTecnicaFactory.GetBorRelSectorColonia(Parametros).then(function (data) {
        var result = data.GetBorRelSectorColoniaResult;
        if (data.GetBorRelSectorColoniaResult == 0) {
          var log={
            'Modulo':'home.areatecnica',
            'Submodulo':'home.areatecnica.sectores',
            'Observaciones':'Se elimino una relacion colonia con un sector ',
            'Comando':JSON.stringify(Parametros),
            'Clv_afectada':vm.Colonia.IdColonia,
            'IdClassLog':48
          };
          logFactory.AddMovSist(log).then(function(result){ console.log('add'); });

          ngNotify.set('Se elimino correctamente la relacion.', 'success');
         } else {
          ngNotify.set('No se puede Eliminar. Hay una Relación de ésta Colonia con un Tap.', 'warn');
        }
        GetRelaColSect();
      });
    }

    function cancel() {
      $uibModalInstance.close();
    }

    var vm = this;
    initData();
    vm.Titulo = 'Nuevo Sector';
    vm.Icono = 'fa fa-plus';
    vm.DisClv = false;
    vm.DisDesc = false;
    vm.cancel = cancel;
    vm.AddSector = AddSector;
    vm.NuevaRelacionSecColonia = NuevaRelacionSecColonia;
    vm.BorrarRelacionSecColonia = BorrarRelacionSecColonia;
    vm.blockdelete1 = true;
    vm.blockForm2 = true;

  });
