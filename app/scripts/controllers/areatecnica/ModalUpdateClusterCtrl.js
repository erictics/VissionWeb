    'use strict'
    angular
      .module('softvApp')
      .controller('ModalUpdateClusterCtrl', function (clusterFactory, tapFactory, $uibModalInstance, ngNotify, $state, options, logFactory) {

        function init() {
          var params = {
            'opcion': 4,
            'clave': '',
            'descripcion': '',
            'clv_cluster': options.Clv_cluster
          };
          clusterFactory.GetMuestraCluster(params)
            .then(function (data) {
              vm.clave = data.GetMuestraClusterResult[0].Clv_txt;
              vm.descripcion = data.GetMuestraClusterResult[0].Descripcion;
              clusterFactory.GetMuestraRelClusterSector(options.Clv_cluster, 1)
                .then(function (rel) {
                  vm.relaciones = rel.GetMuestraRelClusterSectorResult;
                  clusterFactory.GetMuestraRelClusterSector(options.Clv_cluster, 2).then(function (result) {
                    vm.sectores = result.GetMuestraRelClusterSectorResult;
                  });
                });
            });
        }

        function cancel() {
          $uibModalInstance.close();
        }

        function validaRelacion(clv) {
          var count = 0;
          vm.relaciones.forEach(function (item) {
            count += (item.Clv_Sector === clv) ? 1 : 0
          });
          return (count > 0) ? true : false;
        }

        function addrelacion(op) {
          if (validaRelacion(vm.sector.Clv_Sector) === true) {
            ngNotify.set('La relación cluster-sector ya esta establecida', 'warn');
            return;
          }
          clusterFactory.GetQuitarEliminarRelClusterSector(op, vm.clv_cluster, vm.sector.Clv_Sector)
            .then(function (data) {
              var log={
                'Modulo':'home.areatecnica',
                'Submodulo':'home.areatecnica.clusters',
                'Observaciones':'Al editar se agrego una relacion con un clusters ',
                'Comando':JSON.stringify(vm.clv_cluster, vm.sector.Clv_Sector),
                'Clv_afectada':vm.clv_cluster,
                'IdClassLog':45
            };
            logFactory.AddMovSist(log).then(function(result){ console.log('add'); });

              muestrarelaciones();
              ngNotify.set('se agrego la relación correctamente', 'success');
            });
        }

        function muestrarelaciones() {
          clusterFactory.GetMuestraRelClusterSector(vm.clv_cluster, 1)
            .then(function (rel) {
              vm.relaciones = rel.GetMuestraRelClusterSectorResult;
            });
        }

        function deleterelacion(clv) {
          clusterFactory.GetQuitarEliminarRelClusterSector(2, vm.clv_cluster, clv).then(function (data) {
            var log={
              'Modulo':'home.areatecnica',
              'Submodulo':'home.areatecnica.clusters',
              'Observaciones':'Al editar se elimino una relacion con un clusters ',
              'Comando':JSON.stringify(2, vm.clv_cluster, clv),
              'Clv_afectada':vm.clv_cluster,
              'IdClassLog':46
             };
             logFactory.AddMovSist(log).then(function(result){ console.log('add'); });

            ngNotify.set('Se eliminó la relación correctamente', 'success');
            muestrarelaciones();
          });
        }

        function save() {
          clusterFactory.GetInsertUpdateCluster(1, vm.clave, vm.descripcion, vm.clv_cluster)
            .then(function (result) {
              if (result.GetInsertUpdateClusterResult > 0) {
                var log={
                  'Modulo':'home.areatecnica',
                  'Submodulo':'home.areatecnica.clusters',
                  'Observaciones':'Se edito un clusters ',
                  'Comando':JSON.stringify(1, vm.clave, vm.descripcion, vm.clv_cluster),
                  'Clv_afectada':vm.clv_cluster,
                  'IdClassLog':2
               };
               logFactory.AddMovSist(log).then(function(result){ console.log('add'); });

                ngNotify.set('El cluster se ha actualizado correctamente', 'success');
                cancel();
               } else {
                ngNotify.set('Existe un cluster registrado con la misma clave', 'error');
              }
            });
        }

        var vm = this;
        vm.clv_cluster = options.Clv_cluster;
        init();
        vm.save = save;
        vm.Titulo = ' Editar  Cluster';
        vm.Icono = 'fa fa-pencil-square-o';
        vm.cancel = cancel;
        vm.blockForm = false;
        vm.blocksave = false;
        vm.blockaddrelacion = false;
        vm.addrelacion = addrelacion;
        vm.deleterelacion = deleterelacion;
        vm.blockGuarda = false;

      });