'use strict';
angular
  .module('softvApp')
  .controller('ModalDeleteSectorCtrl', function (CatalogosFactory, $state, areaTecnicaFactory, $uibModalInstance, ngNotify, options, logFactory) {
     function init(){
         console.log(options);
        vm.Sector=options.Descripcion;
     }

    function Delete(){
        areaTecnicaFactory.GetBorSector(options.Clv_Sector).then(function(result){  
          var log={
            'Modulo':'home.areatecnica',
            'Submodulo':'home.areatecnica.sectores',
            'Observaciones':'Se elimino un sector ',
            'Comando':JSON.stringify(options.Clv_Sector),
            'Clv_afectada':options.Clv_Sector,
            'IdClassLog':3
           };
           logFactory.AddMovSist(log).then(function(result){ console.log('add'); }); 

           ngNotify.set('El sector se ha eliminado correctamente' ,'success');
           cancel();
         });
    }
    

    function cancel() {
      $uibModalInstance.close();
    }

    var vm = this;
    vm.Titulo = 'Eliminar Sector';
    vm.Icono = 'fa fa-less';
    vm.cancel = cancel;
    vm.Delete = Delete;
    init();
    
  });
