'use strict';
angular
  .module('softvApp')
  .controller('ClusterCtrl', function (clusterFactory, $uibModal) {

    function init() {
      buscar(1);
    }

    function buscar(op) {
      var params = {
        'opcion': op,
        'clave': (op === 2) ? vm.clave : '',
        'descripcion': (op === 3) ? vm.descripcion : '',
        'clv_cluster': 0
      };
      clusterFactory.GetMuestraCluster(params)
        .then(function (data) {
          vm.clusters=data.GetMuestraClusterResult;
        });
    }

    function AddCluster() {
      var modalInstance = $uibModal.open({
        animation: true,
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: 'views/areatecnica/ModalCluster.html',
        controller: 'ModalAddClusterCtrl',
        controllerAs: 'ctrl',
        backdrop: 'static',
        keyboard: false,
        class: 'modal-backdrop fade',
        size: 'md'
      });
      modalInstance.result.then(function () {
          buscar(1);
      });
    }

    function DetalleCluster(x) {
      var options=x;
      var modalInstance = $uibModal.open({
        animation: true,
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: 'views/areatecnica/ModalCluster.html',
        controller: 'ModalDetalleClusterCtrl',
        controllerAs: 'ctrl',
        backdrop: 'static',
        keyboard: false,
        class: 'modal-backdrop fade',
        size: 'md',
        resolve: {
					options: function () {
						return options;
					}
				}
      });
      modalInstance.result.then(function () {
          buscar(1);
      });
    }

    function UpdateCluster(x) {
     var options=x;
      var modalInstance = $uibModal.open({
        animation: true,
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: 'views/areatecnica/ModalCluster.html',
        controller: 'ModalUpdateClusterCtrl',
        controllerAs: 'ctrl',
        backdrop: 'static',
        keyboard: false,
        class: 'modal-backdrop fade',
        size: 'md',
        resolve: {
					options: function () {
						return options;
					}
				}
      });
      modalInstance.result.then(function () {
          buscar(1);
      });
    }

    function EliminaCluster(x) {
      var options=x;
      var modalInstance = $uibModal.open({
        animation: true,
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: 'views/areatecnica/ModalEliminarCluster.html',
        controller: 'ModalDeleteClusterCtrl',
        controllerAs: 'ctrl',
        backdrop: 'static',
        keyboard: false,
        class: 'modal-backdrop fade',
        size: 'sm',
        resolve: {
					options: function () {
						return options;
					}
				}
      });
      modalInstance.result.then(function () {
          buscar(1);
      });
    }

    var vm = this;
    init();
    vm.AddCluster = AddCluster;
    vm.UpdateCluster = UpdateCluster;
    vm.EliminaCluster = EliminaCluster;
    vm.DetalleCluster = DetalleCluster;
    vm.buscar = buscar;

  });