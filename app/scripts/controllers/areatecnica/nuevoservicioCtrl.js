'use strict';

angular
  .module('softvApp')
  .controller('nuevoservicioCtrl', function (atencionFactory, trabajosFactory, ngNotify, logFactory) {

    function init() {
      obtenarticulosacom();

      atencionFactory.getServicios().then(function (data) {
        vm.servicios = data.GetMuestraTipSerPrincipalListResult;
        vm.servicio = vm.servicios[0];
        trabajosFactory.GetMuestra_Imputables()
          .then(function (data) {
            vm.imputables = data.GetMuestra_ImputablesResult;
            trabajosFactory.GetMuestra_Articulos_Acometida()
              .then(function (data) {
              });
          });
      });
    }

    function obtenarticulosacom() {
      trabajosFactory.GetMuestra_Articulos_Acometida()
        .then(function (result) {
          vm.articulosacom = result.GetMuestra_Articulos_AcometidaResult;
        });
    }

    function obtenarticulos() {
      trabajosFactory.GetMuestra_Articulos_Clasificacion(vm.clasificacion.clvtipo)
        .then(function (data) {
          vm.articulos = data.GetMuestra_Articulos_ClasificacionResult;
        });
    }

    function save() {
      var params = {
        'clv_tipser': vm.servicio.Clv_TipSerPrincipal,
        'trabajo': vm.clave,
        'descripcion': vm.descripcion,
        'puntos': vm.individual,
        'pCuadrilla':(vm.pCuadrilla)?vm.pCuadrilla:0,
        'cobranza': 0,
        'tipo': vm.tipo,
        'prospectos': 0,
        'sica': (vm.proceso === null) ? false : vm.proceso,
        'secobramaterial': (vm.descarga === null) ? false : vm.descarga
      };
      trabajosFactory.GetSoftv_AddTrabajo(params).then(function (data) {

        if (data.GetSoftv_AddTrabajoResult === -1) {
          ngNotify.set('El trabajo no se pudo guardar ,por que existe una clave regitrada con el mismo valor', 'error');
        } else {
          var log={
            'Modulo':'home.areatecnica',
            'Submodulo':'home.areatecnica.nuevoservicio',
            'Observaciones':'Se agrego un nuevo Trabajo ',
            'Comando':JSON.stringify(params),
            'Clv_afectada':0,
            'IdClassLog':1
           };
          logFactory.AddMovSist(log).then(function(result){ console.log('add'); });

          vm.clv_trabajo = data.GetSoftv_AddTrabajoResult;
          vm.blocksave = true;
          vm.blockform = true;
          ngNotify.set('El trabajo se guardo con exito ,ahora puede continuar con la configuaración o salir del catálogo');
          if (vm.tipo === 'Q') {
            trabajosFactory.GetGuarda_imputablePorTrabajo(vm.clv_trabajo, vm.imputable.idimputable).then(function (result) {});
          }
        }
      });
    }

    function agregarelacion() {
      if(!vm.descripcionart){
        ngNotify.set('Selecciona un artículo','warn');
        return;
      }
      if(vm.cantidad < 1){
        ngNotify.set('Selecciona una cantidad Mayor a 0 para agregar el articulo','warn');
        return;
     }
     if(!vm.cantidad){
      ngNotify.set('Selecciona una cantidad Mayor a 0 para agregar el articulo','warn');
      return;
   }
      if (vm.clv_trabajo>0) {
        trabajosFactory.GetSoftv_AddRelMaterialTrabajo(vm.clasificacion.clvtipo, vm.descripcionart.clave, vm.cantidad, vm.clv_trabajo, vm.servicio.Clv_TipSerPrincipal)
          .then(function (data) {
            if (data.GetSoftv_AddRelMaterialTrabajoResult === -1) {
              ngNotify.set('El artículo ya esta registrado ', 'warn');
            }
            consultamateriales();
          });
        }else{
        var log={
          'Modulo':'home.areatecnica',
          'Submodulo':'home.areatecnica.nuevoservicio',
          'Observaciones':'Se agrego una Relacion de Material a un Trabajo ',
          'Comando':JSON.stringify(vm.clasificacion.clvtipo, vm.descripcionart.clave, vm.cantidad, vm.clv_trabajo, vm.servicio.Clv_TipSerPrincipal),
          'Clv_afectada':vm.clv_trabajo,
          'IdClassLog':51
         };
         logFactory.AddMovSist(log).then(function(result){ console.log('add'); });
         ngNotify.set('Guarde el trabajo para continuar la asignación ', 'warn');
      }
    }

    function consultamateriales() {
      trabajosFactory.GetConsultaRelMaterialTrabajos(vm.clv_trabajo)
        .then(function (data) {
          vm.relaciones = data.GetConsultaRelMaterialTrabajosResult;
        });
    }

    function bloquearmaterial() {
      vm.blockmateriales = (vm.descarga === true) ? false : true;
    }

    function DeleteRelacio(clvmaterial){
      trabajosFactory.GetSoftv_DeleteRelMaterialTrabajo(clvmaterial).then(function(data){
        ngNotify.set('Se eliminó la relación','success');
        consultamateriales();
      });
    }

    var vm = this;
    init();
    vm.bloquearmaterial = bloquearmaterial;
    vm.obtenarticulos = obtenarticulos;
    vm.titulo = 'Nuevo trabajo';
    vm.save = save;
    vm.blockdescarga = true;
    vm.clv_trabajo = 0;
    vm.blocksave = false;
    vm.agregarelacion = agregarelacion;
    vm.blockmateriales = true;
    vm.blockform = false;
    vm.showimputable = false;
    vm.DeleteRelacio = DeleteRelacio;
    vm.tipo='O'
  });
