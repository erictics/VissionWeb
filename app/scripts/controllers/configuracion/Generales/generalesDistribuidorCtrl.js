'use strict';
angular
  .module('softvApp')
  .controller('generalesDistribuidorCtrl', function ($state, rolFactory, atencionFactory, generalesSistemaFactory, globalService, $uibModal, ngNotify, logFactory) {

    function init() {
      atencionFactory.getPlazas().then(function (data) {
        vm.plazas = data.GetMuestra_Compania_RelUsuarioListResult;
        vm.Plaza = vm.plazas[0];
        ObtenDetalle();
      });
    }

    function ObtenDetalle() {
      generalesSistemaFactory.GetGeneralDistribuidor(vm.Plaza.id_compania).then(function (result) {
          vm.mensajeticket = result.GetGeneralDistribuidorResult.mensajeticket_mensaje;
          vm.mensajeactivo = result.GetGeneralDistribuidorResult.mensajeticket_activo;
          vm.bonificacion = result.GetGeneralDistribuidorResult.BonificacionMax;

          generalesSistemaFactory.GetMuestra_tecnicosDepartamentos(0).then(function (result) {
              vm.departamentos = result.GetMuestra_tecnicosDepartamentosResult;
              vm.Depatamento = vm.departamentos[0];
              obtentecnicos();

              generalesSistemaFactory.GetConPuestos(vm.Plaza.id_compania)
              .then(function (response) {
                  vm.tecnicosordenes = response.GetConPuestosResult;

                  generalesSistemaFactory.GetConsultatecnicosReporte(0, vm.Plaza.id_compania)
                  .then(function (response) {
                      var tecnicos = [];
                      response.GetConsultatecnicosReporteResult.forEach(function(element) {
                        element.tecnicos.forEach(function(T){
                          tecnicos.push(T);
                      });  
                  });
                  vm.TecnicoRepList = tecnicos;
                });

              });
          });
      });
    }

    function obtentecnicos() {
      generalesSistemaFactory.GetMuestra_TecnicosByFamili(1, vm.Depatamento.clv_puesto, vm.Plaza.id_compania)
        .then(function (result) {
          vm.TecnicoList = result.GetMuestra_TecnicosByFamiliResult;
        });
    }

    function guardamensaje() {
      generalesSistemaFactory.GetNueGeneralMsjTickets(vm.Plaza.id_compania, vm.mensajeticket, vm.mensajeactivo)
        .then(function (result) {
          var log={
            'Modulo':'home.configuracion',
            'Submodulo':'home.configuracion.generalsistemadistribuidor',
            'Observaciones':'Se edito el Mensaje de Ticket',
            'Comando':JSON.stringify(vm.Plaza.id_compania, vm.mensajeticket, vm.mensajeactivo),
            'Clv_afectada':0,
            'IdClassLog':2
          };
          logFactory.AddMovSist(log).then(function(result){ console.log('add'); });
          ngNotify.set('Se guardo el mensaje exitosamente', 'success');
        });
    }

    function guardaBonificacion() {
      generalesSistemaFactory.GetNUEBonificacionCajeras(vm.Plaza.id_compania,vm.bonificacion)
        .then(function (result) {
          var log={
            'Modulo':'home.configuracion',
            'Submodulo':'home.configuracion.generalsistemadistribuidor',
            'Observaciones':'Se edito la bonificacion maxima permitida',
            'Comando':JSON.stringify(vm.Plaza.id_compania,vm.bonificacion),
            'Clv_afectada':0,
            'IdClassLog':2
          };
          logFactory.AddMovSist(log).then(function(result){ console.log('add'); });
          ngNotify.set('Se guardo bonificacion exitosamente', 'success');
        });
    }

    function AddRelTecnicoOrdenes(){
      generalesSistemaFactory.GetNueRelOrdenesTecnicos(vm.Plaza.id_compania, vm.Depatamento.clv_puesto, vm.Tecnico.clv_tecnico)
      .then(function(data){
        var log={
          'Modulo':'home.configuracion',
          'Submodulo':'home.configuracion.generalsistemadistribuidor',
          'Observaciones':'Se agrego una relacion - tecnico - ordenes',
          'Comando':JSON.stringify(vm.Plaza.id_compania, vm.Depatamento.clv_puesto, vm.Tecnico.clv_tecnico),
          'Clv_afectada':0,
          'IdClassLog':55
        };
        logFactory.AddMovSist(log).then(function(result){ console.log('add'); });
        ObtenDetalle();
      });
    }

    function DeleteRelTecnicoOrdenes(clv_tecnico){
      generalesSistemaFactory.GetBorRelOrdenesTecnicos(vm.Plaza.id_compania, vm.Depatamento.clv_puesto, clv_tecnico)
      .then(function(data){
        var log={
          'Modulo':'home.configuracion',
          'Submodulo':'home.configuracion.generalsistemadistribuidor',
          'Observaciones':'Se elimino una relacion - tecnico - ordenes',
          'Comando':JSON.stringify(vm.Plaza.id_compania, vm.Depatamento.clv_puesto, clv_tecnico),
          'Clv_afectada':0,
          'IdClassLog':56
        };
        logFactory.AddMovSist(log).then(function(result){ console.log('add'); });
        ObtenDetalle();
      });
    }
    
    function AddRelTecnicoReportes(){
      console.log('si llego');
      console.log(vm.Plaza.id_compania, vm.Depatamento.clv_puesto, vm.Tecnico.clv_tecnico);
      generalesSistemaFactory.GetNueRel_Tecnicos_Quejas(vm.Plaza.id_compania, vm.Depatamento.clv_puesto, vm.Tecnico.clv_tecnico)
      .then(function(data){
        var log={
          'Modulo':'home.configuracion',
          'Submodulo':'home.configuracion.generalsistemadistribuidor',
          'Observaciones':'Se agrego una relacion - tecnico - reportes',
          'Comando':JSON.stringify(vm.Plaza.id_compania, vm.Depatamento.clv_puesto, vm.Tecnico.clv_tecnico),
          'Clv_afectada':0,
          'IdClassLog':57
        };
        logFactory.AddMovSist(log).then(function(result){ console.log('add'); });
        ObtenDetalle();
      });
    }

    function DeleteRelTecnicoReportes(clv_tecnico){
      generalesSistemaFactory.GetBorRel_Tecnicos_Quejas(vm.Plaza.id_compania, 0, clv_tecnico)
      .then(function(data){
        var log={
          'Modulo':'home.configuracion',
          'Submodulo':'home.configuracion.generalsistemadistribuidor',
          'Observaciones':'Se elimino una relacion - tecnico - reportes',
          'Comando':JSON.stringify(vm.Plaza.id_compania, 0, clv_tecnico),
          'Clv_afectada':0,
          'IdClassLog':58
        };
        logFactory.AddMovSist(log).then(function(result){ console.log('add'); });
        ObtenDetalle();
      });
    }

    var vm = this;
    init();
    vm.AddRelTecnicoOrdenes = AddRelTecnicoOrdenes;
    vm.obtentecnicos = obtentecnicos;
    vm.ObtenDetalle = ObtenDetalle;
    vm.guardamensaje = guardamensaje;
	  vm.guardaBonificacion=guardaBonificacion;
    vm.AddRelTecnicoOrdenes = AddRelTecnicoOrdenes;
    vm.DeleteRelTecnicoOrdenes = DeleteRelTecnicoOrdenes;
    vm.AddRelTecnicoReportes = AddRelTecnicoReportes;
    vm.DeleteRelTecnicoReportes = DeleteRelTecnicoReportes;

  });
