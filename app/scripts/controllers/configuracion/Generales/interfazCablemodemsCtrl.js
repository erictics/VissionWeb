'use strict';
angular
  .module('softvApp')
  .controller('interfazCablemodemsCtrl', function ($state,$localStorage,distribuidorFactory, CatalogosFactory, plazaFactory, $filter, rolFactory, atencionFactory, generalesSistemaFactory, globalService, $uibModal, ngNotify,ServiciosFactory) {

    function init() {
        cambioDistribuidor()
        distribuidorFactory.Getplaza(0,'').then(function (data) {
          vm.Distribuidores = data.GetPlaza_DistribuidoresNewResult;

          CatalogosFactory.GetTipServ_NewList().then(function(data){
            vm.Servicios = data.GetTipServ_NewListResult;

            ServiciosFactory.GetMedioList().then(function(result){
              vm.tecnologias=result.GetMedioListResult;
            });
          });
      });
    }

    function cambioDistribuidor() {     
      var ObjPlaza = {
        'opcion': 3,
        'razon': '',
        'idcompania': 1
      };
      plazaFactory.GetBrwMuestraCompanias(ObjPlaza).then(function (result) {
        vm.Plazas =  result.GetBrwMuestraCompaniasResult;
      });
    }

    function filtrar (op){
      var data = {
        'CLVDISTRIBUIDOR':(vm.distribuidor)?vm.distribuidor.Clv_Plaza:0,
        'CLVPLAZA':(vm.Plazas)? vm.Plazas.id_compania:0,
        'CLVTIPSER':(vm.servicio)?vm.servicio.Clv_TipSer:0,
        'Clv_Orden':(vm.clvorden)? vm.clvorden:0,
        'MacAddress':(vm.mac)? vm.mac:0,
        'fecha_habilitar':(vm.fechasolicitud)? $filter('date')(vm.fechasolicitud, 'yyyy/MM/dd'):'9999/09/09',
        'contratocompuesto': (vm.contrato)?vm.contrato:'',
        'IdMedio':(vm.tecnologia)?vm.tecnologia.IdMedio:0,
        'Op':op
      };
      generalesSistemaFactory.GetFILTROSINTERFAZ_CABLEMODEMS(data).then(function(result){
        vm.datos=result.GetFILTROSINTERFAZ_CABLEMODEMSResult
      });
    }

    var vm = this;
    vm.cambioDistribuidor=cambioDistribuidor;
    vm.filtrar=filtrar;
    init();   
    filtrar(1);

  });
