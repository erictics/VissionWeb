angular
.module('softvApp')
.controller('modalEligeLogoCtrl', function(generalesSistemaFactory, $uibModalInstance,tipo,ngNotify, logFactory){

    function initData(){       
    }

    function cancel() {
        $uibModalInstance.dismiss('cancel');
    }

    function ok() {       
         var file = $('#uploadImage').get(0).files;      
        generalesSistemaFactory.GuardaLogos(file, tipo).then(function(result) {  
          var log={
            'Modulo':'home.configuracion',
            'Submodulo':'home.configuracion.generalsistema',
            'Observaciones':'Se cambio un Logo ',
            'Comando':JSON.stringify(file, tipo),
            'Clv_afectada': 0,
            'IdClassLog':2
          };
          logFactory.AddMovSist(log).then(function(result){ console.log('add'); });
          ngNotify.set("Se guardo correctamente", "success");
          $uibModalInstance.close(true);          
        });
      }

    var vm = this;   
    vm.cancel = cancel;   
    vm.ok=ok;
    initData();
    
});