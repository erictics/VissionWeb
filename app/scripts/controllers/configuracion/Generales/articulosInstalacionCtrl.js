'use strict';
angular
.module('softvApp')
.controller('articulosInstalacionCtrl', function (logFactory, trabajosFactory, ngNotify, $state, generalesSistemaFactory) {

    function getArticulosAcometida(){
        trabajosFactory.GetMuestra_Articulos_Acometida().then(function(res){
        console.log(res.GetMuestra_Articulos_AcometidaResult);
        vm.articulosAcometida=res.GetMuestra_Articulos_AcometidaResult;
        // console.log(res.GetMuestra_Articulos_AcometidaResult);
        });
    }

    function CambioClasificacion(){
        if(!vm.clasificacion){
        ngNotify.set('Selecciona una clasificación');
        return;
     }
    trabajosFactory.GetMuestra_Articulos_Clasificacion(vm.clasificacion.clvtipo).then(function(res){
        vm.articulos=res.GetMuestra_Articulos_ClasificacionResult;
        console.log(res.GetMuestra_Articulos_ClasificacionResult);
      });
    }


    function muestraArticulos(){
        generalesSistemaFactory.GetMuestra_Art_Acom().then(function(result){
        console.log(result)
        vm.lista=result.GetMuestra_Art_AcomResult;
        });
    }

    function agregar(){
        console.log(vm.articulo.clave);
        console.log(vm.precio);
        generalesSistemaFactory.GetInserta_Precio_Art_Acom(vm.articulo.clave,vm.precio,0,1).then(function(res){
        var log={
            'Modulo':'home.configuracion',
            'Submodulo':'home.configuracion.articulosInstalacion',
            'Observaciones':'Se Agrego un Nuevo Articulo de Instalacion',
            'Comando':JSON.stringify(vm.articulo.clave,vm.precio,0,1),
            'Clv_afectada':0,
            'IdClassLog':1
        };
        logFactory.AddMovSist(log).then(function(result){ console.log('add'); });
        ngNotify.set('El articulo se agregó correctamente','success');
        muestraArticulos();
        });
    }

    function eliminar(item){
        generalesSistemaFactory.GetEliminar_Art_Acom(item.NoArticulo).then(function(res){
            var log={
                'Modulo':'home.configuracion',
                'Submodulo':'home.configuracion.articulosInstalacion',
                'Observaciones':'Se Elimino un Articulo de Instalacion',
                'Comando':JSON.stringify(item.NoArticulo),
                'Clv_afectada':0,
                'IdClassLog':3
        };
        logFactory.AddMovSist(log).then(function(result){ console.log('add'); });
        ngNotify.set('El articulo se eliminó correctamente','error');
        muestraArticulos();
        });
    }

    var vm = this;
    getArticulosAcometida();
    vm.CambioClasificacion=CambioClasificacion;
    vm.agregar=agregar;
    vm.eliminar=eliminar;
    muestraArticulos();

});
