"use strict";
angular
  .module("softvApp")
  .controller("generalesSistemaCtrl", function($state, $uibModal, ngNotify, globalService, generalesSistemaFactory, logFactory, $localStorage) {
    
    function init() {
      generalesSistemaFactory.GetPeriodoscorte(0, 1).then(function(response) {
        vm.periodos = response.GetPeriodoscorteResult;
        vm.Periodo = vm.periodos[0];
 
        generalesSistemaFactory.GetCONSULTAGENERALESDESC(vm.Periodo.Clv_periodo, 1).then(function(data) {
            vm.periodoCorte = data.GetCONSULTAGENERALESDESCResult;
 
            generalesSistemaFactory.GetImpuestos(1, 1).then(function(data) {
              console.log(data);
              vm.impuestos = data.GetImpuestosResult;

              generalesSistemaFactory.GetspConsultaRangosCobroMaterial(1).then(function(data) {
                vm.rangos = data.GetspConsultaRangosCobroMaterialResult;

                generalesSistemaFactory.GetConPromocionGen(1).then(function(data){
                  console.log(data);
                  vm.docexcatorce = data.GetConPromocionGenResult.docexcatorce;
                  vm.seisxsiete = data.GetConPromocionGenResult.seisxsiete;
                });
              });
            });
        });
      });

      GetBonificacion();
      GetTipoUsuarioList();
      GetBonGenTipoUsuarioList();
      GetBonAutTipoUsuario();
    }

  function Guardarperiodo() {}

  function GuardarImpuestos() {
    var Parametros = {
      Id: 1,
      IVA: vm.impuestos.IVA ? vm.impuestos.IVA : 0,
      IEPS: vm.impuestos.IEPS ? vm.impuestos.IEPS : 0,
      siIEPS: vm.impuestos.siIEPS,
      Cta_IEPS: 0,
      Calculo1: vm.IVAIEPS,
      idcompania: 1,
      ivaFrontera: vm.impuestos.ivaFrontera ? vm.impuestos.ivaFrontera : 0
    };
    console.log(Parametros);
    generalesSistemaFactory.GetNueTabla_Impuestos(Parametros).then(function(result) {
      var log={
        'Modulo':'home.configuracion',
        'Submodulo':'home.configuracion.generalsistema',
        'Observaciones':'Se modificaron los conceptos de impuestos ',
        'Comando':JSON.stringify(Parametros),
        'Clv_afectada':0,
        'IdClassLog':2
      };
      logFactory.AddMovSist(log).then(function(result){ console.log('add'); });
      ngNotify.set("Los conceptos de impuestos se  guardaron correctamente","success");
      });
    }

    function Getlogos() {
      generalesSistemaFactory.Getlogos().then(function(result) {
        console.log(result);
        console.log(globalService.getUrllogos() + "/" + result.GetlogosResult[0].Valor);
        vm.logo1=globalService.getUrllogos() + "/" + result.GetlogosResult[0].Valor;
        vm.logo2=globalService.getUrllogos() + "/" + result.GetlogosResult[1].Valor;
        vm.logo3=globalService.getUrllogos() + "/" + result.GetlogosResult[2].Valor;
        vm.logo4=globalService.getUrllogos() + "/" + result.GetlogosResult[3].Valor;
      });
    }

    function eliminarango(id) {
      generalesSistemaFactory.GetspEliminaRangosCobroMaterial(id).then(function(result) {
        var log={
          'Modulo':'home.configuracion',
          'Submodulo':'home.configuracion.generalsistema',
          'Observaciones':'Se elimino un rango en cobro de material ',
          'Comando':JSON.stringify(id),
          'Clv_afectada': id,
          'IdClassLog':3
        };
        logFactory.AddMovSist(log).then(function(result){ console.log('add'); });
        ngNotify.set("El Rango se ha eliminado correctamente", "warn");
      });
    }

    function Guardarcobro() {
      var obj = {
        id: 0,
        inicio: vm.rangoinicial,
        final: vm.rangofinal,
        maximo: vm.pagosdiferidos,
        idcompania: 1
    };
    generalesSistemaFactory.GetspAgregaRangosCobroMaterial(obj).then(function(data) {
      var log={
        'Modulo':'home.configuracion',
        'Submodulo':'home.configuracion.generalsistema',
        'Observaciones':'Se guardo un Rango ',
        'Comando':JSON.stringify(obj),
        'Clv_afectada':0,
        'IdClassLog':1
      };
      logFactory.AddMovSist(log).then(function(result){ console.log('add'); });
      console.log(data);
      ngNotify.set("El Rango se ha guardado correctamente", "success");
      });
    }

    function detalleperiodo() {
      generalesSistemaFactory.GetCONSULTAGENERALESDESC(vm.Periodo.Clv_periodo, 1).then(function(data) {
        console.log(data);
        vm.periodoCorte = data.GetCONSULTAGENERALESDESCResult;
      });
    }

    function guardarPreferencias() {
      var obj = {
        NombreSistema: vm.nombresistema,
        TituloNav: vm.titulomenu,
        ColorMenu: vm.hexPicker.colormenu,
        ColorMenuLetra: vm.hexPicker.colormenuletra,
        ColorNav: vm.hexPicker.colornavegacion,
        ColorNavLetra: vm.hexPicker.colornavegacionletra,
        MensajeHome: vm.mensajeinicio,
        ColorFondo: vm.hexPicker.colorfondo
      };
      generalesSistemaFactory.GetguardaPreferencia(obj).then(function(result) {
        var log={
          'Modulo':'home.configuracion',
          'Submodulo':'home.configuracion.generalsistema',
          'Observaciones':'Se edito la Preferencia ',
          'Comando':JSON.stringify(obj),
          'Clv_afectada':0,
          'IdClassLog':2
        };
        logFactory.AddMovSist(log).then(function(result){ console.log('add'); });
        ngNotify.set("Se guardo correctamente", "success");
      });
    }

    function detallePreferencia() {
      generalesSistemaFactory.GetDetallePreferencias().then(function(result) {
        console.log(result);
        var detalle = result.GetDetallePreferenciasResult;
        vm.nombresistema = detalle.NombreSistema;
        vm.mensajeinicio = detalle.MensajeHome;
        vm.titulomenu = detalle.TituloNav;
        vm.hexPicker.colormenu = detalle.ColorMenu;
        vm.hexPicker.colormenuletra = detalle.ColorMenuLetra;
        vm.hexPicker.colornavegacion = detalle.ColorNav;
        vm.hexPicker.colornavegacionletra = detalle.ColorNavLetra;
        vm.hexPicker.colorfondo = detalle.ColorFondo;
      });
    }

    function eligeLogo(op){
      var modalInstance = $uibModal.open({
        animation: true,
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: 'views/configuracion/modalEligeLogo.html',
        controller: 'modalEligeLogoCtrl',
        controllerAs: 'ctrl',
        backdrop: 'static',
        keyboard: false,
        class: 'modal-backdrop fade',
        size: 'md',
        resolve: {
          tipo: function () {
            return op;
          }
      }
      });
      modalInstance.result.then(function () {
        Getlogos();
      });
    }

    function AddAutBonTipoUsuario(Op, Clv){
      if((Op == 1 && vm.TipoUsuario != undefined) || (Op == 2 && Clv != undefined)){
        var ObjBonificacion = {
          'Clv_TipoUsuario': (Op == 1)? vm.TipoUsuario.Clv_TipoUsuario:Clv,
          'Op': Op
        };
        generalesSistemaFactory.GetAddBonTipoUsuarioAutoriza(ObjBonificacion).then(function(data){
          var log={
            'Modulo':'home.configuracion',
            'Submodulo':'home.configuracion.generalsistema',
            'Observaciones':'Se agrego una bonificacion a un usuario ',
            'Comando':JSON.stringify(ObjBonificacion),
            'Clv_afectada':0,
            'IdClassLog':1
          };
          logFactory.AddMovSist(log).then(function(result){ console.log('add'); });
          var Msj = (Op == 1)? 'Se agregó Tipo de Usuario':'Se eliminó Tipo de Usuario';
          ngNotify.set("Correcto, " + Msj, "success");
          GetTipoUsuarioList();
          GetBonGenTipoUsuarioList();
          GetBonAutTipoUsuario();
          });
       }else{
        ngNotify.set("Error, Tiene que ingresar un Usuario", "warn");
      }
    }

    function SaveIdentificacion(){
      if(vm.TipoIdentificacion != undefined && vm.TipoIdentificacion != null){
      var Parametros = {
        'Id': 0,
        'descripcion': vm.TipoIdentificacion
      };
      if(vm.Update == false){
        generalesSistemaFactory.GetSP_AddTipoIde(vm.TipoIdentificacion).then(function(data){
          if(data.GetSP_AddTipoIdeResult == 1){
            var log={
              'Modulo':'home.configuracion',
              'Submodulo':'home.configuracion.generalsistema',
              'Observaciones':'Se agrego un Tipo de Identificacion ',
              'Comando':JSON.stringify(Parametros),
              'Clv_afectada':0,
              'IdClassLog':1
            };
            logFactory.AddMovSist(log).then(function(result){ console.log('add'); });
            DetalleIdentificacion()
            ngNotify.set('CORRECTO, Se añadió un Tipo de Identificacion', 'success');
            Limpiar();
          }else{
            ngNotify.set('ERROR, Al añadir un Tipo de Identificacion', 'warn');
          }
        });
      }else{
        var Parametros = {
          'Id': vm.IdUpdate,
          'descripcion': vm.TipoIdentificacion
          };
            generalesSistemaFactory.GetSP_UpdateTipoIde(vm.IdUpdate, vm.TipoIdentificacion).then(function(data){
              var log={
                'Modulo':'home.configuracion',
                'Submodulo':'home.configuracion.generalsistema',
                'Observaciones':'Se edito un Tipo de Identificacion ',
                'Comando':JSON.stringify(Parametros),
                'Clv_afectada':0,
                'IdClassLog':2
              };
              logFactory.AddMovSist(log).then(function(result){ console.log('add'); });
              DetalleIdentificacion();
              ngNotify.set('CORRECTO, Se edito Tipo de Identificacion', 'success');
              Reset();
            });
          }
      }
    }

    function DetalleIdentificacion(){
      generalesSistemaFactory.GetListaTbl_TipoIden(vm.Id, 0).then(function(data){
        vm.lista = data.GetListaTbl_TipoIdenResult;
      });
    }

    function UpdateIdentificacion(Id, TipoIdentificacion){
      vm.TipoIdentificacion = TipoIdentificacion;
      vm.IdUpdate = Id;
      vm.Update = true;
      DetalleIdentificacion();
    }

    function DeleteIdentificacion(Id){
      generalesSistemaFactory.GetSP_DeleteTipoIde(Id).then(function(data){
        console.log(data);
        if(data.GetSP_DeleteTipoIdeResult == 1){
          var log={
            'Modulo':'home.configuracion',
            'Submodulo':'home.configuracion.generalsistema',
            'Observaciones':'Se elimino tipo de identificacion ',
            'Comando':JSON.stringify(Id),
            'Clv_afectada':Id,
            'IdClassLog':3
          };
          logFactory.AddMovSist(log).then(function(result){ console.log('add'); });
          DetalleIdentificacion();
          ngNotify.set('CORRECTO, Se elimino Tipo de Identificacion', 'success');
         }else{
          ngNotify.set('ERROR, No se puede eliminar el Tipo de Identificacion esta relacionado', 'warn');
        }
      });
    }

    function Reset(){
      vm.Update = false;
      vm.TipoIdentificacion = "";
    }

    function Limpiar(){
      vm.TipoIdentificacion = "";
    }

    function SavePromocion(){
      var Parametros = {
          'docexcatorce': vm.docexcatorce,
          'seisxsiete': vm.seisxsiete,
	        'IdCompania': 1
      };
      generalesSistemaFactory.GetModificaPromocionesGeneral2(vm.docexcatorce, vm.seisxsiete, 1).then(function(data){
        console.log(data);
        if(data.GetModificaPromocionesGeneral2Result == -1){
          var log={
            'Modulo':'home.configuracion',
            'Submodulo':'home.configuracion.generalsistema',
            'Observaciones':'Se editaron las promociones ',
            'Comando':JSON.stringify(Parametros),
            'Clv_afectada':0,
            'IdClassLog': 61
          };
          logFactory.AddMovSist(log).then(function(result){ console.log('add'); });
          ngNotify.set('CORRECTO, Se edito las promociones', 'success');
        } else{
          ngNotify.set('ERROR, No se puede editar las promociones', 'warn');
        }
      });
    }


    var vm = this;
    init();
    detallePreferencia();
    Getlogos();
    vm.Guardarperiodo = Guardarperiodo;
    vm.GuardarImpuestos = GuardarImpuestos;
    vm.Guardarcobro = Guardarcobro;
    vm.detalleperiodo = detalleperiodo;
    vm.guardarPreferencias = guardarPreferencias;
    vm.eligeLogo=eligeLogo;
    vm.eliminarango = eliminarango;
    vm.hexPicker = {};
    vm.SaveIdentificacion = SaveIdentificacion;
    vm.DeleteIdentificacion = DeleteIdentificacion;
    vm.UpdateIdentificacion = UpdateIdentificacion;
    vm.Reset = Reset;
    vm.Update = false;
    DetalleIdentificacion();
    vm.SavePromocion = SavePromocion;



/* "use strict";
angular
  .module("softvApp")
  .controller("generalesSistemaCtrl", function(
    $state,
    $uibModal,
    ngNotify,
    globalService,
    generalesSistemaFactory
  ) {
    var vm = this;
    init();
    //detallePreferencia();
    Getlogos();
    vm.Guardarperiodo = Guardarperiodo;
    vm.GuardarImpuestos = GuardarImpuestos;
    vm.Guardarcobro = Guardarcobro;
    vm.detalleperiodo = detalleperiodo;
    vm.guardarPreferencias = guardarPreferencias;
   
    vm.eligeLogo=eligeLogo;
    vm.hexPicker = {};

    vm.uploader = new FileUploader({
      filters: [
        {
          name: "yourName1",
          fn: function(item) {
            var count = 0;
            vm.uploader.queue.forEach(function(f) {             
              count += f._file.idtipo === item.idtipo ? 1 : 0;
            });
            if (count > 0) {
              ngNotify.set(
                "Un archivo con ese mismo nombre ya fue seleccionado",
                "warn"
              );
              return false;
            } else {
              return true;
            }
          }
        }
      ]
    });

    vm.uploader.onAfterAddingFile = function(fileItem) {
      fileItem.file.idtipo = vm.tipoimagen.Idtipo;
      fileItem.file.tipo = vm.tipoimagen.Nombre;
      fileItem._file.idtipo = vm.tipoimagen.Idtipo;
      fileItem._file.tipo = vm.tipoimagen.Nombre;
    };
*/
    vm.BonificacionDis = ($localStorage.currentUser.tipoUsuario == 40)? true:false;
    vm.AddGenBonTipoUsuario = AddGenBonTipoUsuario;
    vm.AddAutBonTipoUsuario = AddAutBonTipoUsuario;
    vm.GetBonificacion = GetBonificacion;
    vm.SaveBonificacion = SaveBonificacion;
    
    /*

    function init() {
      generalesSistemaFactory.GetPeriodoscorte(0, 1).then(function(response) {
        vm.periodos = response.GetPeriodoscorteResult;
        vm.Periodo = vm.periodos[0];
          generalesSistemaFactory
            .GetCONSULTAGENERALESDESC(vm.Periodo.Clv_periodo, 1)
            .then(function(data) {           
              vm.periodoCorte = data.GetCONSULTAGENERALESDESCResult;   
                  generalesSistemaFactory
                    .GetImpuestos(1, 1)
                    .then(function(data) {
                      vm.impuestos = data.GetImpuestosResult;

              generalesSistemaFactory
                .GetspConsultaRangosCobroMaterial(1)
                .then(function(data) {
                  vm.rangos = data.GetspConsultaRangosCobroMaterialResult;
                });
            });
            GetBonificacion();
            GetTipoUsuarioList();
            GetBonGenTipoUsuarioList();
            GetBonAutTipoUsuario();
      });
    })

    function Guardarperiodo() {}

    function GuardarImpuestos() {
      var Parametros = {
        Id: 1,
        IVA: vm.impuestos.IVA ? vm.impuestos.IVA : 0,
        IEPS: vm.impuestos.IEPS ? vm.impuestos.IEPS : 0,
        siIEPS: vm.impuestos.siIEPS,
        Cta_IEPS: 0,
        Calculo1: vm.IVAIEPS,
        idcompania: 1,
        ivaFrontera: vm.impuestos.ivaFrontera ? vm.impuestos.ivaFrontera : 0
      };
      generalesSistemaFactory
        .GetNueTabla_Impuestos(Parametros)
        .then(function(result) {
          ngNotify.set("Los conceptos de inpuestos se  guardaron correctamente", "success");
        });
    }

    function guardaLogo() {
      var file_options = [];
      var files = [];
      var tipos = [];
      var count = 0;
      vm.uploader.queue.forEach(function(f) {
        var options = {
          IdImagen: 0,
          Tipo: f._file.idtipo,
          Nombre: f._file.name
        };
        file_options.push(options);
        tipos.push(f._file.idtipo);
        files.push(f._file);
      });
      if (count > 1) {
        ngNotify.set(
          "El número de imagenes con el mismo tipo se ha sobrepasado maximo 2",
          "error"
        );
        return;
      }
      generalesSistemaFactory
        .GuardaLogos(files, file_options, [])
        .then(function(result) {
          ngNotify.set("Se guardo correctamente", "success");
          Getlogos();
        });
    }

    function Getlogos() {
      generalesSistemaFactory.Getlogos().then(function(result) {
        result.forEach(function(item) {
          item.Valor = globalService.getUrllogos() + "/" + item.Valor;
        });
        vm.tiposimg = result;
      });
    }

    function eliminarango(id) {
      generalesSistemaFactory
        .GetspEliminaRangosCobroMaterial(id)
        .then(function(result) {
          ngNotify.set("El rango se ha eliminado correctamente", "warn");
        });
    }

    function Guardarcobro() {
      var obj = {
        id: 0,
        inicio: vm.rangoinicial,
        final: vm.rangofinal,
        maximo: vm.pagosdiferidos,
        idcompania: 1
      };
      generalesSistemaFactory
        .GetspAgregaRangosCobroMaterial(obj)
        .then(function(data) {
          ngNotify.set("El rango se ha guardado correctamente", "success");
        });
    }

    function detalleperiodo() {
      generalesSistemaFactory
        .GetCONSULTAGENERALESDESC(vm.Periodo.Clv_periodo, 1)
        .then(function(data) {
          vm.periodoCorte = data.GetCONSULTAGENERALESDESCResult;
        });
    }

    function guardarPreferencias() {
      var obj = {
        NombreSistema: vm.nombresistema,
        TituloNav: vm.titulomenu,
        ColorMenu: vm.hexPicker.colormenu,
        ColorMenuLetra: vm.hexPicker.colormenuletra,
        ColorNav: vm.hexPicker.colornavegacion,
        ColorNavLetra: vm.hexPicker.colornavegacionletra,
        MensajeHome: vm.mensajeinicio,
        ColorFondo: vm.hexPicker.colorfondo
      };
      generalesSistemaFactory.GetguardaPreferencia(obj).then(function(result) {
        ngNotify.set("Se guardo correctamente", "success");
      });
    }

    function detallePreferencia() {
      generalesSistemaFactory.GetDetallePreferencias().then(function(result) {
        var detalle = result.GetDetallePreferenciasResult;
        vm.nombresistema = detalle.NombreSistema;
        vm.mensajeinicio = detalle.MensajeHome;
        vm.titulomenu = detalle.TituloNav;
        vm.hexPicker.colormenu = detalle.ColorMenu;
        vm.hexPicker.colormenuletra = detalle.ColorMenuLetra;
        vm.hexPicker.colornavegacion = detalle.ColorNav;
        vm.hexPicker.colornavegacionletra = detalle.ColorNavLetra;
        vm.hexPicker.colorfondo = detalle.ColorFondo;
      });
    }
    */
    function GetBonificacion(){
      generalesSistemaFactory.GetConsultaBonficacion().then(function(data){
        vm.Bonificacion = data.GetConsultaBonficacionResult.Bonificacion
      });
    }

    function SaveBonificacion(){
      generalesSistemaFactory.GetSaveBonficacion(vm.Bonificacion).then(function(data){
        GetBonificacion();
        ngNotify.set("Correcto, Se Guardó Bonificación", "success");
      });
    }

    function GetTipoUsuarioList(){
      generalesSistemaFactory.GetBonificacionTipoUsarioDisList().then(function(data){
        vm.TipoUsuarioList = data.GetBonificacionTipoUsarioDisListResult;
      });
    }

    function GetBonGenTipoUsuarioList(){
      generalesSistemaFactory.GetBonificacionTipoUsarioList().then(function(data){
        vm.TipoBonGenTipoUsuarioList = data.GetBonificacionTipoUsarioListResult;
      });
    }

    function GetBonAutTipoUsuario(){
      generalesSistemaFactory.GetBonificacionAutorizaTipoUsarioList().then(function(data){
        vm.BonAutTipoUsuarioList = data.GetBonificacionAutorizaTipoUsarioListResult;
      });
    }

    function AddGenBonTipoUsuario(Op, Clv){
      if((Op == 1 && vm.TipoUsuario != undefined) || (Op == 2 && Clv != undefined)){
        var ObjBonificacion = {
          'Clv_TipoUsuario': (Op == 1)? vm.TipoUsuario.Clv_TipoUsuario:Clv,
          'Op': Op
        };
        generalesSistemaFactory.GetAddBonTipoUsuario(ObjBonificacion).then(function(data){
          var Msj = (Op == 1)? 'Se agregó Tipo de Usuario':'Se eliminó Tipo de Usuario';
          ngNotify.set("Correcto, " + Msj, "success");
          GetTipoUsuarioList();
          GetBonGenTipoUsuarioList();
          GetBonAutTipoUsuario();
        });
      }else{
        ngNotify.set("Error, Tiene que ingresar un usuario", "warn");
      }
    }

    /*function AddAutBonTipoUsuario(Op, Clv){
      if((Op == 1 && vm.TipoUsuario != undefined) || (Op == 2 && Clv != undefined)){
        var ObjBonificacion = {
          'Clv_TipoUsuario': (Op == 1)? vm.TipoUsuario.Clv_TipoUsuario:Clv,
          'Op': Op
        };
        generalesSistemaFactory.GetAddBonTipoUsuarioAutoriza(ObjBonificacion).then(function(data){
          var Msj = (Op == 1)? 'Se agregó Tipo de Usuario':'Se eliminó Tipo de Usuario';
          ngNotify.set("Correcto, " + Msj, "success");
          GetTipoUsuarioList();
          GetBonGenTipoUsuarioList();
          GetBonAutTipoUsuario();
        });
      }else{
        ngNotify.set("Error, Tiene que ingresar un usuario", "warn");
      }
    }*/
  
  });