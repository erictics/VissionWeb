'use strict';
angular
  .module('softvApp')
  .controller('nuevoUsuarioCtrl', function ($state,GastosFactory, usuarioFactory, globalService,ngNotify ,$uibModal, $filter, rolFactory, encuestasFactory, $localStorage, logFactory) {

    this.$onInit = function () {
      GetDepartamento();
      rolFactory.GetRolList().then(function (data) {
        vm.Roles = data.GetRolListResult;
        usuarioFactory.GetConsultaIdentificacionUsuario(0, '').then(function (result) {
          vm.Indentificaciones = result.GetConsultaIdentificacionUsuarioResult;
          
          encuestasFactory.GetMuestra_DistribuidoresEncList().then(function (data) {
            vm.distribuidores = data.GetMuestra_DistribuidoresEncListResult;
            var ObjGrupoVenta = {
              'Clv_Grupo': 0,
              'Op': 1,
              'Clv_Usuario': $localStorage.currentUser.idUsuario
            };
            usuarioFactory.GetConGrupoVentas(ObjGrupoVenta).then(function(data){
              vm.GrupoVentaList = data.GetConGrupoVentasResult;
             
            });
          });
        });
      });
    };

    function muestraplazas() {
      encuestasFactory.Muestra_PlazaEnc(vm.distribuidor.Clv_Plaza).then(function (data) {
        console.log(data);
        vm.plazas = data.GetMuestra_PlazaEncListResult;
      });
    }

    function agregaRelacion() {
      usuarioFactory.GetAgregaEliminaRelCompaniaUsuario(vm.IdUser, vm.plaza.id_compania, 1)
        .then(function (response) {
          var log={
            'Modulo':'home.configuracion',
            'Submodulo':'home.configuracion.nuevousuario',
            'Observaciones':'Se agrego una Relacion de Compania con Usuario ',
            'Comando':JSON.stringify(vm.IdUser, vm.plaza.id_compania, 1),
            'Clv_afectada':0,
            'IdClassLog':53
          };
          logFactory.AddMovSist(log).then(function(result){ console.log('add'); });

          ngNotify.set('Relación agregada','success');
          usuarioFactory.GetAgregaEliminaRelCompaniaUsuario(vm.IdUser, 0, 3)
            .then(function (result) {
              vm.relaciones = result.GetAgregaEliminaRelCompaniaUsuarioResult;
            });
        });  
    }

    function eliminarelacion(x) {
      usuarioFactory.GetAgregaEliminaRelCompaniaUsuario(vm.IdUser, x.id_compania, 2)
        .then(function (response) {
          var log={
            'Modulo':'home.configuracion',
            'Submodulo':'home.configuracion.nuevousuario',
            'Observaciones':'Se elimino una Relacion de Compania con Usuario ',
            'Comando':JSON.stringify(vm.IdUser, x.id_compania, 2),
            'Clv_afectada':vm.IdUser,
            'IdClassLog':54
          };
          logFactory.AddMovSist(log).then(function(result){ console.log('add'); });

          ngNotify.set('Se eliminó la relación','warn');
          usuarioFactory.GetAgregaEliminaRelCompaniaUsuario(vm.IdUser, 0, 3)
            .then(function (result) {
              vm.relaciones = result.GetAgregaEliminaRelCompaniaUsuarioResult;
            });
        });
    }

    function GetDepartamento(){
      GastosFactory.GetDepartamento().then(function(response){
        vm.departamentos=response.GetDepartamentoResult;      
      });
    }

    function validar_pass(){
      if(FormUsuario.pass1.value != FormUsuario.pass2.value){
          ngNotify.set('Las Contraseñas no coinciden','warn');
          return false;
      }else{
        return true;
      }
      }
      
    function Guardar() {
      if(validar_pass() == true){
      var Parametros = {
        'Clave': 0,
        'Clv_Usuario': vm.Clave,
        'Domicilio': '',
        'Colonia': '',
        'FechaIngreso': '',// $filter('date')(vm.fechaingreso, 'dd/MM/yyyy'),
        'FechaSalida':'', //$filter('date')(vm.fechabaja, 'dd/MM/yyyy'),
        'Activo': vm.activo,
        'Pasaporte': vm.pass1,
        'Clv_TipoUsuario': vm.rol.IdRol,
        'SlcGrupoVenta': vm.SlcGrupoVenta,
        'CATV': false,
        'Facturacion': true,
        'Boletos': false,
        'Mizar_AN': 0,
        'RecibeMensaje': vm.recibemensaje,
        'NotaDeCredito': 0,
        'RecibeMensajeDocumentos': 0,
        'Nombre': vm.Nombre,
        'IdDepartamento':(vm.departamento)?vm.departamento.IdDpto:0,
        'usuarioCorporativo':vm.usuarioCorporativo,
        'Email':vm.Email
       }
      };
       usuarioFactory.GetAddUsuarioSoftv(Parametros).then(function (data) {
        var log={
          'Modulo':'home.configuracion',
          'Submodulo':'home.configuracion.nuevousuario',
          'Observaciones':'Se agrego un nuevo Usuario ',
          'Comando':JSON.stringify(Parametros),
          'Clv_afectada':0,
          'IdClassLog':1
        };
        logFactory.AddMovSist(log).then(function(result){ console.log('add'); });

        vm.IdUser = data.GetAddUsuarioSoftvResult.Clave;
        vm.blockForm = false;
        vm.blockrelaciones = false;
        vm.blocksave = true;
        usuarioFactory.GetSoftvweb_GetUsuarioSoftvbyId(vm.IdUser).then(function (data) {
          vm.ClvUsuarioR = data.GetSoftvweb_GetUsuarioSoftvbyIdResult.Clv_Usuario
          var ObjGrupoVentaRel = {
            'Clv_Usuario': vm.ClvUsuarioR,
            'Clv_Grupo': vm.GrupoVenta.Clv_Grupo
          };
          usuarioFactory.GetNueRelUsuarioGrupoVentas(ObjGrupoVentaRel).then(function(data){
            ngNotify.set('El usuario se ha guardado correctamente ,ahora puedes asignar el acesso a distribuidores/plazas','success');
           // $state.go('home.configuracion.usuarios');
          });
        });
      });
    }

    var vm = this;
    vm.IdUser = 0;
    vm.titulo='Nuevo Usuario';
    vm.Guardar = Guardar;
    vm.muestraplazas = muestraplazas;
    vm.agregaRelacion = agregaRelacion;
    vm.eliminarelacion = eliminarelacion;
    // vm.blockForm=false;
    // vm.blockrelaciones=true;
    // vm.blocksave = false;
    vm.oculta=true;
    vm.validar_pass= validar_pass;
  });
