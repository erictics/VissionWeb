'use strict';
angular
  .module('softvApp')
  .controller('editaUsuarioCtrl',function ($state, usuarioFactory,GastosFactory,ngNotify ,$filter,globalService, $uibModal, rolFactory, encuestasFactory, $stateParams, $localStorage, logFactory) {

    this.$onInit = function () {
      GetDepartamento();
      rolFactory.GetRolList().then(function (data) {
        vm.Roles = data.GetRolListResult;
        usuarioFactory.GetConsultaIdentificacionUsuario(0, '').then(function (result) {
          vm.Indentificaciones = result.GetConsultaIdentificacionUsuarioResult;
          encuestasFactory.GetMuestra_DistribuidoresEncList().then(function (data) {
            vm.distribuidores = data.GetMuestra_DistribuidoresEncListResult;
            var ObjGrupoVenta = {
              'Clv_Grupo': 0,
              'Op': 1,
              '': $localStorage.currentUser.idUsuario
            };
            usuarioFactory.GetConGrupoVentas(ObjGrupoVenta).then(function(data){
              vm.GrupoVentaList = data.GetConGrupoVentasResult;
              usuarioFactory.GetSoftvweb_GetUsuarioSoftvbyId($stateParams.id).then(function (data) {
                var user = data.GetSoftvweb_GetUsuarioSoftvbyIdResult;
                //console.log(user)
                vm.Clave = user.Clv_Usuario;
                vm.Nombre = user.Nombre;
                vm.pass2 = '';
                vm.pass1 = '';
                vm.fechaingreso = user.FechaIngreso;
                vm.fechabaja = user.FechaSalida;
                vm.activo = user.Activo;
                vm.recibemensaje = user.RecibeMensaje;
                vm.departamento=user.IdDepartamento;
                vm.usuarioCorporativo=user.usuarioCorporativo;
                vm.Email=user.Email;
                vm.SlcGrupoVenta=user.SlcGrupoVenta;
                vm.Roles.forEach(function (item) {
                  if (item.IdRol === user.Clv_TipoUsuario) {
                    vm.rol = item;
                  }
                });
                vm.departamentos.forEach(function(item){
                  if(item.IdDpto===user.IdDepartamento){
                      vm.departamento=item;
                  }
                });  
                
                vm.Indentificaciones.forEach(function (item) {
                  if (item.Clave === user.Clv_IdentificacionUsuario) {
                    vm.identificacion = item;
                  }
                });
                usuarioFactory.GetAgregaEliminaRelCompaniaUsuario(vm.IdUser, 0, 3)
                .then(function (result) {
                  vm.relaciones = result.GetAgregaEliminaRelCompaniaUsuarioResult;
                });
                var Obj = {
                  'Op': 0,
                  'Clv_Usuario': vm.Clave
                }
                usuarioFactory.GetConRelUsuarioGrupoVentas(Obj).then(function(data){
                  vm.ClvGrupo = data.GetConRelUsuarioGrupoVentasResult.Clv_Grupo;
                  vm.GrupoVentaList.forEach(function(item){
                    if(item.Clv_Grupo == vm.ClvGrupo){
                      vm.GrupoVenta = item;
                    }
                  });
                });
              });
            });
          });
        });
      });
    };

    function muestraplazas() {
      encuestasFactory.Muestra_PlazaEnc(vm.distribuidor.Clv_Plaza).then(function (data) {
        vm.plazas = data.GetMuestra_PlazaEncListResult;
      });
    }

    function agregaRelacion() {
      usuarioFactory.GetAgregaEliminaRelCompaniaUsuario(vm.IdUser, vm.plaza.id_compania, 1)
        .then(function (response) {
          var log={
            'Modulo':'home.configuracion',
            'Submodulo':'home.configuracion.editausuario',
            'Observaciones':'Se agrego una Relacion de Compania con Usuario ',
            'Comando':JSON.stringify(vm.IdUser, vm.plaza.id_compania, 1),
            'Clv_afectada':0,
            'IdClassLog':53
          };
          logFactory.AddMovSist(log).then(function(result){ console.log('add'); });

          ngNotify.set('Relación agregada','success');
          usuarioFactory.GetAgregaEliminaRelCompaniaUsuario(vm.IdUser, 0, 3)
            .then(function (result) {
              vm.relaciones = result.GetAgregaEliminaRelCompaniaUsuarioResult;
            });
        });
    }

    function eliminarelacion(x) {
      usuarioFactory.GetAgregaEliminaRelCompaniaUsuario(vm.IdUser, x.id_compania, 2)
        .then(function (response) {
          var log={
            'Modulo':'home.configuracion',
            'Submodulo':'home.configuracion.editausuario',
            'Observaciones':'Se elimino una Relacion de Compania con Usuario ',
            'Comando':JSON.stringify(vm.IdUser, x.id_compania, 2),
            'Clv_afectada':vm.IdUser,
            'IdClassLog':54
          };
          logFactory.AddMovSist(log).then(function(result){ console.log('add'); });

           ngNotify.set('Se eliminó la relación','warn');
          usuarioFactory.GetAgregaEliminaRelCompaniaUsuario(vm.IdUser, 0, 3)
            .then(function (result) {
              vm.relaciones = result.GetAgregaEliminaRelCompaniaUsuarioResult;
            });
        });
    }

    function GetDepartamento(){
      GastosFactory.GetDepartamento().then(function(response){
        vm.departamentos=response.GetDepartamentoResult;
       console.log(response);
      });
    }

    
    function Guardar() {
      var Parametros = {
        'Clave': vm.IdUser,
        'Clv_Usuario': vm.Clave,
        'Domicilio': '',
        'Colonia': '',
        'FechaIngreso':'', //$filter('date')(vm.fechaingreso, 'dd/MM/yyyy'),
        'FechaSalida':'',//$filter('date')(vm.fechabaja, 'dd/MM/yyyy'),
        'Activo': vm.activo,
        'Pasaporte': vm.pass1,
        'Clv_TipoUsuario': vm.rol.IdRol,
        'SlcGrupoVenta': vm.SlcGrupoVenta,
        'CATV': false,
        'Facturacion': true,
        'Boletos': false,
        'Mizar_AN': 0,
        'RecibeMensaje': vm.recibemensaje,
        'NotaDeCredito': 0,
        'RecibeMensajeDocumentos': 0,
        'Nombre': vm.Nombre,
        'IdDepartamento':(vm.departamento)?vm.departamento.IdDpto:0,
        'usuarioCorporativo':vm.usuarioCorporativo,
        'Email':vm.Email
      };

      usuarioFactory.GetEditUsuarioSoftv(Parametros).then(function (data) {   
        var log={
          'Modulo':'home.configuracion',
          'Submodulo':'home.configuracion.editausuario',
          'Observaciones':'Se edito un Usuario ',
          'Comando':JSON.stringify(Parametros),
          'Clv_afectada':vm.IdUser,
          'IdClassLog':2
        };
        logFactory.AddMovSist(log).then(function(result){ console.log('add'); });

        vm.blockForm = true;
        vm.blockrelaciones = false;
        vm.blocksave = true;
        ngNotify.set('El Usuario se ha editado correctamente','success');
        if(vm.GrupoVenta.Clv_Grupo){
          var ObjGrupoVentaRel = {
            'Clv_Usuario': vm.Clave,
            'Clv_Grupo': vm.GrupoVenta.Clv_Grupo
          };
          usuarioFactory.GetNueRelUsuarioGrupoVentas(ObjGrupoVentaRel).then(function(data){
            vm.GrupoVentaList = data.GetConGrupoVentasResult;
          });
        }
      });
    }


    var vm = this;
    vm.IdUser = $stateParams.id;
    vm.titulo = 'Edita Usuario';
    vm.Guardar=Guardar;
    vm.agregaRelacion = agregaRelacion;
    vm.eliminarelacion = eliminarelacion;
    vm.muestraplazas = muestraplazas;
    vm.oculta=false;
  });
