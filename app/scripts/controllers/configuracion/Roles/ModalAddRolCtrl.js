'user strict';
angular.module('softvApp')
	.controller('ModalAddRolCtrl', function($uibModalInstance, rolFactory, $rootScope, ngNotify, logFactory) {
		function cancel() {
			$uibModalInstance.dismiss('cancel');
		}

		function Guarda() {
			var log={
				'Modulo':'home.configuracion',
				'Submodulo':'home.configuracion.roles',
				'Observaciones':'Se agrego un nuevo Rol ',
				'Comando':JSON.stringify(rol),
				'Clv_afectada':0,
				'IdClassLog':1
			  };
			  logFactory.AddMovSist(log).then(function(result){ console.log('add'); });

			var nombre = vm.Nombre;
			var status = vm.Status;
			rolFactory.AddRol(nombre, status);
		}
		var vm = this;
		vm.cancel = cancel;
		vm.Guarda = Guarda;
	});
