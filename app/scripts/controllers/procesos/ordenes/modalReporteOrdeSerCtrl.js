(function () {
  'use strict';

  angular
    .module('softvApp')
    .controller('modalReporteOrdeSerCtrl', modalReporteOrdeSerCtrl);

  modalReporteOrdeSerCtrl.inject = ['$uibModalInstance', '$uibModal', 'ContratoMaestroFactory', 'ngNotify', '$rootScope', 'clv_orden', '$sce', 'globalService'];

  function modalReporteOrdeSerCtrl($uibModalInstance, $uibModal, ordenesFactory, ngNotify, $rootScope, $sce, globalService, clv_orden) {
    var vm = this;
    vm.cancel = cancel;
    vm.ok = ok;


    this.$onInit = function () {
      var obj = {
        'reporte':{
          'fechaejeInicial':'',
          'fechaejeFinal':'',
          'fechasolFinal':'',
          'fechasolInicial':'',
          'estatus':'',
          'Clv_inicio':clv_orden,
          'Clv_fin':clv_orden,
          'Op':1,
          'Clv_trabajo':0,          
          'OpOrdenar':0,
          'plazas':[],
          'estados':[],
          'ciudades':[],
          'localidades':[],
          'colonias':[],
          'calles':[],
          'distribuidores':[]
        }
      };
      console.log(obj);
      ordenesFactory.GetReporteOrdenServicio(obj).then(function (resp) {
          console.log(resp);
        var id = resp.GetReporteOrdenesResult;
        vm.url = globalService.getUrlReportes() + '/Reportes/' + id;
        vm.urlReal = $sce.trustAsResourceUrl(vm.url);
      });
    }

    function cancel() {
      $uibModalInstance.dismiss('cancel');
    }

    function ok() {

    }
  }
})();
