'use strict';
angular
  .module('softvApp')
  .controller('ModalDescargaExtCtrl', function ($uibModalInstance, $uibModal, options, DescargarMaterialFactory, $rootScope, ngNotify, $localStorage, $state) {

  function init(){
    vm.Detalle=options.Detalle;
    DescargarMaterialFactory.GetMuestra_Detalle_Bitacora(options.SctTecnico.CLV_TECNICO, vm.IAlma).then(function (data) {
        vm.Material = data.GetMuestra_Detalle_Bitacora_2ListResult;
        muestraExtensiones();
        DescargarMaterialFactory.GetSoftv_GetDescargaMaterialEstaPagado(options.ClvOrden, options.Tipo_Descargar)
        .then(function (data) {
            vm.pagado = data.GetSoftv_GetDescargaMaterialEstaPagadoResult.Pagado;

            DescargarMaterialFactory.GetSoftv_DimeSiTieneBitacora(options.ClvOrden, options.Tipo_Descargar).then(function (data) {
                if (data.GetSoftv_DimeSiTieneBitacoraResult !== null) {
                    vm.No_Bitacora = data.GetSoftv_DimeSiTieneBitacoraResult.NoBitacora;
                    ObtenArticulos();
                }
              });
        });
    });
  }


  function ObtenArticulos(){
    vm.articulos_tabla=[];   
    DescargarMaterialFactory.GetDescargaMaterialArticulosByIdClvOrden(options.ClvOrden, options.Tipo_Descargar,vm.Extension.Id)
    .then(function (data) {          
        var art = data.GetGetDescargaMaterialArticulosByIdClvOrdenListResult;
        var ObjDescargaMat = {};
        ObjDescargaMat.IdTecnico = options.SctTecnico.CLV_TECNICO;
        ObjDescargaMat.ClvOrden = options.ClvOrden;
        ObjDescargaMat.IdAlmacen = vm.IAlma;
        ObjDescargaMat.Accion = (vm.No_Bitacora === 0) ? 'Agregar' : 'Modificar';
        ObjDescargaMat.IdBitacora = vm.No_Bitacora;
        ObjDescargaMat.TipoDescarga = options.Tipo_Descargar;
        for (var a = 0; a < art.length; a++) {
          var Articulos = {};
          Articulos.NoArticulo = art[a].NOARTICULO;
          Articulos.Cantidad = art[a].CANTIDADUTILIZADA;
          Articulos.EsCable = art[a].ESCABLE;
          Articulos.MetrajeInicio = art[a].METRAJEINICIO;
          Articulos.MetrajeFin = art[a].METRAJEFIN;
          Articulos.MetrajeInicioExt = art[a].METRAJEINICIOEXTERIOR;
          Articulos.MetrajeFinExt = art[a].METRAJEFINEXTERIOR;
          Articulos.NumExt = art[a].NoExt;          
          Articulos.Descripcion=art[a].Descripcion;
          if(Articulos.EsCable){
            if(Articulos.MetrajeInicio > 0 && Articulos.MetrajeFin > 0 ){
              Articulos.TipoMetraje= 'I';
            }else{
              Articulos.TipoMetraje= 'E';
            } 
          }        
          Articulos.Tipo = 'G';
          Articulos.idDescarga=art[a].idDescarga;
          vm.articulos_.push(Articulos);
        }
      });
  }

  function EliminarArticulo(obj,index) {     
    vm.articulos_[index].Cantidad = 0;     
  }

  function ObtenArticulosExtension () {     
    vm.listaMetrajes=[];  
    if(vm.Extension.Id === 0){
      vm.listaMetrajes.push({
      Clave: 'I',
      descripcion: 'Interior'
      },{
        Clave: 'E',
        descripcion: 'Exterior'
      });
    } else {
      vm.listaMetrajes.push({
        Clave: 'I',
        descripcion: 'Interior'
       });
    }
  }

  function muestraExtensiones (){    
     DescargarMaterialFactory.GetUspLlenaComboExtensionesList(options.NumExt,options.ClvOrden).then(function(res){

        res.GetUspLlenaComboExtensionesListResult.forEach(function(item) {
            vm.extensiones.push({
                Id: item.ID,
                Descripcion : item.DESCRIPCION
            });
        });
        vm.Extension=vm.extensiones[0];
     });
    
  }

  function cancel() {
    $uibModalInstance.dismiss('cancel');
  }

  function BuscarTipoArticulo() {
    vm.listaMetrajes=[];
    if(vm.Extension.Id === 0){
      vm.listaMetrajes.push({
      Clave: 'I',
      descripcion: 'Interior'
      },{
        Clave: 'E',
        descripcion: 'Exterior'
      });

    } else {

      vm.listaMetrajes.push({
        Clave: 'I',
        descripcion: 'Interior'
       });

    }


    DescargarMaterialFactory.GetSoftv_ObtenTipoMaterial(catUnidadClave, Tipo, Articulo, vm.SlctArticulo.IdArticulo)
    .then(function (data) {

      vm.TipoArticulo = data.GetSoftv_ObtenTipoMaterialResult.Tipo;
      vm.MostrarCD = false;
      vm.MostrarMII = false;
      vm.MostrarMIE = false;
      vm.MostrarMFI = false;
      vm.MostrarMFE = false;
      vm.MostrarTM = false;
      

      if (vm.TipoArticulo === 'Metros') {
        if(vm.TipoMetraje.Clave === 'I') {         
          vm.MostrarMII = true;         
          vm.MostrarMFI = true;       
          vm.MostrarTM = true;
        } else if(vm.TipoMetraje.Clave === 'E') {         
          vm.MostrarMIE = true;       
          vm.MostrarMFE = true;
          vm.MostrarTM = true;
        }

      } else if (vm.TipoArticulo === 'Piezas') {
        vm.MostrarCD = true;      
      }

      CantidadArticulo = 0;
      vm.CantidadDescarga = "";
      vm.MetrajeII = "";
      vm.MetrajeIE = "";
      vm.MetrajeFI = "";
      vm.MetrajeFE = "";
    });
  }

  function BuscarNombreArticulo() {
    if (vm.SlctMaterial) {
      DescargarMaterialFactory.GetMuestra_Descripcion_Articulo_2List(options.SctTecnico.CLV_TECNICO, vm.SlctMaterial.catTipoArticuloClave, vm.IAlma)
      .then(function (data) {
        vm.DescripcionArticulo = data.GetMuestra_Descripcion_Articulo_2ListResult;
        vm.SlctArticulo.IdArticulo = "";
        vm.MostrarCD = false;
        vm.MostrarMII = false;
        vm.MostrarMIE = false;
        vm.MostrarMFI = false;
        vm.MostrarMFE = false;
        vm.MostrarTM = false;
      });

    }
  }

  function SlctTipoMetraje () {
    if(vm.TipoMetraje.Clave == 'I') {
      vm.MostrarMFE = false;
      vm.MostrarMIE = false;
      vm.MostrarMII = true;
      vm.MostrarMFI = true;
    }else if(vm.TipoMetraje.Clave == 'E') {
      vm.MostrarMII = false;
      vm.MostrarMFI = false;
      vm.MostrarMIE = true;
      vm.MostrarMFE = true;
    }
  }

  function obtieneCantidadAgregada(Nombre,CantidadActual){

    var cantidad=0;
    vm.articulos_.forEach(function(art){
         
         if(art.Descripcion === Nombre && art.Tipo=== 'N'){
           cantidad = cantidad + art.Cantidad;
         }
    });  
    return cantidad+CantidadActual;
  }

  function AgregarArticulo() {
    if (vm.SlctArticulo) {
      DescargarMaterialFactory.GetSoftv_ExistenciasTecnico(vm.SlctArticulo.IdArticulo, options.SctTecnico.CLV_TECNICO, vm.IAlma)
      .then(function (data) {

        vm.Existe = data.GetSoftv_ExistenciasTecnicoResult.Existe;

        DescargarMaterialFactory.GetSoftv_ObtenTipoMaterial(catUnidadClave, Tipo, Articulo, vm.SlctArticulo.IdArticulo)
        .then(function (data) {

          vm.TipoArticulo = data.GetSoftv_ObtenTipoMaterialResult.Tipo;
          var vCD = 0;
          var vMII = 0;
          var vMIE = 0;
          var vMFI = 0;
          var vMFE = 0;
          var TArt = false;

          if (vm.TipoArticulo === 'Metros') {
            if ((vm.MetrajeII !== undefined && vm.MetrajeII > 0 &&
              vm.MetrajeFI !== undefined && vm.MetrajeFI > 0 ) ||
              (vm.MetrajeIE !== undefined && vm.MetrajeIE > 0 &&
              vm.MetrajeFE !== undefined && vm.MetrajeFE > 0)) {              

              if (vm.MetrajeFI > vm.MetrajeII || vm.MetrajeFE > vm.MetrajeIE) {

                  if(vm.TipoMetraje.Clave === 'I') {
                    vMII = vm.MetrajeII;
                    vMFI = vm.MetrajeFI;
                    vMIE = 0;
                    vMFE = 0;
                    CantidadArticulo = (vMFI - vMII);
                  }else if(vm.TipoMetraje.Clave === 'E'){
                    vMII = 0;
                    vMFI = 0;
                    vMIE = vm.MetrajeIE;
                    vMFE = vm.MetrajeFE;
                    CantidadArticulo = (vMFE - vMIE);
                  }
                  vCD = 0;
                  TArt = true;

              } else {
                CantidadArticulo = 0;
                ngNotify.set('El metraje final tiene que ser mayor al de inicio.', 'error');
              }
            } else {
              CantidadArticulo = 0;
              ngNotify.set('Ingresa una cantidad válida para  metraje del artículo.', 'error');
            }
          } else if (vm.TipoArticulo === 'Piezas') {

            if (vm.CantidadDescarga !== undefined && vm.CantidadDescarga > 0) {
              vCD = vm.CantidadDescarga;
              vMII = 0;
              vMIE = 0;
              vMFI = 0;
              vMFE = 0;
              CantidadArticulo = vCD;
              TArt = false;
            } else {
              CantidadArticulo = 0;
              ngNotify.set('Ingresa la cantidad del artículo.', 'error');
            }
          }

          vm.CantidadDescarga = "";
          vm.MetrajeII = "";
          vm.MetrajeIE = "";
          vm.MetrajeFI = "";
          vm.MetrajeFE = "";

          if (CantidadArticulo > 0) {

            if ( obtieneCantidadAgregada( vm.SlctArticulo.Nombre , CantidadArticulo)  <= vm.SlctArticulo.Cantidad) {
              var Articulos = {};
              Articulos.Descripcion = vm.SlctArticulo.Nombre;
              Articulos.NoArticulo = vm.SlctArticulo.IdInventario;
              Articulos.Cantidad = CantidadArticulo;
              Articulos.EsCable = TArt;
              Articulos.MetrajeInicio = vMII;
              Articulos.MetrajeFin = vMFI;
              Articulos.TipoMetraje=vm.TipoMetraje.Clave;
              Articulos.MetrajeInicioExt = vMIE;
              Articulos.MetrajeFinExt = vMFE;             
              Articulos.NumExt = vm.Extension.Id;
              Articulos.Tipo = 'N';             

             if (ExisteArticulo(Articulos) === false) {
                vm.articulos_.push(Articulos);

             }
             ObtenArticulosExtension();               
              
            } else {
              ngNotify.set('No tiene material suficiente, solo cuenta con: ' + vm.SlctArticulo.Cantidad + ' pzs.', 'error');
            }
          }

        });

      });
    } else {
      ngNotify.set('Selecciona un artículo.', 'error');
    }
  }

  function ExisteArticulo(Articulo) {
    var count = 0;
    
    if(vm.articulos_.length > 0) {
    vm.articulos_.forEach(function(e) {

      if(e.NoArticulo === Articulo.NoArticulo && e.NumExt === Articulo.NumExt) {           
        if(Articulo.EsCable === true) {
         
          if(Articulo.TipoMetraje === e.TipoMetraje && e.NumExt === Articulo.NumExt){
            
            e.Cantidad=Articulo.Cantidad;
            e.MetrajeInicio=Articulo.MetrajeInicio;
            e.MetrajeFin=Articulo.MetrajeFin;            
            e.MetrajeInicioExt=Articulo.MetrajeInicioExt;
            e.MetrajeFinExt=Articulo.MetrajeFinExt;
            count=count+1;
          }
       
        }else{

          e.Cantidad=Articulo.Cantidad;
          count=count+1;
        }

      }else{
     
      }
      
    });
    
  }
   
    return (count > 0) ? true:false;
  }

  function ok() {
   
    if (vm.articulos_.length > 0) {
      var ObjDescargaMat = {};
      ObjDescargaMat.IdTecnico = options.SctTecnico.CLV_TECNICO;
      ObjDescargaMat.ClvOrden = options.ClvOrden;
      ObjDescargaMat.IdAlmacen = vm.IAlma;
      ObjDescargaMat.Accion = (vm.No_Bitacora == 0) ? 'Agregar' : 'Modificar';
      ObjDescargaMat.IdBitacora = vm.No_Bitacora;
      ObjDescargaMat.TipoDescarga = options.Tipo_Descargar;  
   
      DescargarMaterialFactory.GetAddDescargaMaterialArt(ObjDescargaMat, vm.articulos_).then(function (data) {     
        DescargarMaterialFactory.GetchecaBitacoraTecnico(options.ClvOrden, options.Tipo_Descargar).then(function(data){ 
            vm.DesMatRes = data.GetchecaBitacoraTecnicoResult; 
            ngNotify.set('Se guardó exitosamente la bitácora #' + vm.DesMatRes.idBitacora + ' para la orden #' + options.ClvOrden,'success'); 
            $uibModalInstance.close(vm.DesMatRes.idBitacora);
          }); 
      });
    } else {
      ngNotify.set('Necesita agregar un artículo primero.', 'error');
    }

  }



 var vm = this;
 vm.IAlma = 0;

 vm.BuscarNombreArticulo=BuscarNombreArticulo;
 vm.BuscarTipoArticulo=BuscarTipoArticulo;
 vm.extensiones = [];
 //vm.TipoMetraje.Clave = 'I';
 var IdArticulo = "";
 var catUnidadClave = 0;
 var Tipo = "";
 var Articulo = "";
 var CantidadArticulo = 0;
 vm.MostrarCD = false;
 vm.MostrarMII = false;
 vm.MostrarMIE = false;
 vm.MostrarMFI = false;
 vm.MostrarMFE = false;
 vm.MostrarTM = false;
 vm.articulos_ = [];
 vm.articulos_tabla = [];
  vm.SlctTipoMetraje=SlctTipoMetraje;
 vm.AgregarArticulo=AgregarArticulo;
 vm.ObtenArticulosExtension=ObtenArticulosExtension;
 vm.ok=ok;
 vm.EliminarArticulo=EliminarArticulo;
 vm.cancel=cancel;
 vm.listaMetrajes= [];
 vm.listaMetrajes.push({
  Clave: 'I',
  descripcion: 'Interior'
  },{
    Clave: 'E',
    descripcion: 'Exterior'
  });

 vm.TipoMetraje =vm.listaMetrajes[0] ;

  init();

});