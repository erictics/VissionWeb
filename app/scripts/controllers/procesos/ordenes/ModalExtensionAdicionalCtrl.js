'use strict';

angular
    .module('softvApp')
    .controller('ModalExtensionAdicionalCtrl', function($uibModalInstance,ordenesFactory,$uibModal, $rootScope, ngNotify, $state, options){

        function init(){
            ordenesFactory.GetCONCONEX(options.Clave, options.Clv_Orden, options.Contrato)
            .then(function(res){
              vm.extensiones= res.GetCONCONEXResult;             
              vm.numero=res.GetCONCONEXResult;
            });
           
        }

        function ok(){
           if( vm.extensiones == 0){
            ordenesFactory.GetNueConex(options.Clave,options.Clv_Orden,options.Contrato,vm.numero).then(function(res){
                $uibModalInstance.dismiss('cancel');
            });

           }else{

            ordenesFactory.GetMODCONEX(options.Clave,options.Clv_Orden,options.Contrato,vm.numero).then(function(res){
            $uibModalInstance.dismiss('cancel');
            });

          }
           
      

                     
        }

        function cancel() {
            $uibModalInstance.dismiss('cancel');
        }

        var vm = this; 
        init();      
        vm.ok = ok;
        vm.cancel = cancel;
        vm.block=options.detalle;

    });