(function () {
    'use strict';
  
    angular
      .module('softvApp')
      .controller('ModalPreguntaDescargaCtrl', ModalPreguntaDescargaCtrl);  
      ModalPreguntaDescargaCtrl.inject = ['$uibModalInstance','bitacora'];
  
    function ModalPreguntaDescargaCtrl($uibModalInstance,bitacora) {
      var vm = this;
      vm.cancel = cancel;
      vm.ok = ok; 
      this.$onInit = function () {
       vm.mensaje='¿Estas seguro que quieres salir? hay una bitácora de descarga de material #'+bitacora+' generada para esta orden';
      };  
      function cancel() {
        $uibModalInstance.dismiss('cancel');
      }  
      function ok() {
        $uibModalInstance.close(1);
      }
    }
  })();
  