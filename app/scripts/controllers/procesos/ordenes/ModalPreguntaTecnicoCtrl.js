(function () {
    'use strict';
  
    angular
      .module('softvApp')
      .controller('ModalPreguntaTecnicoCtrl', ModalPreguntaTecnicoCtrl);  
      ModalPreguntaTecnicoCtrl.inject = ['$uibModalInstance'];
  
    function ModalPreguntaTecnicoCtrl($uibModalInstance) {
      var vm = this;
      vm.cancel = cancel;
      vm.ok = ok; 
      this.$onInit = function () {
       vm.mensaje='¿Estas seguro que el tecnico que seleccionaste es correcto ?';
      };  
      function cancel() {
        $uibModalInstance.dismiss('cancel');
      }  
      function ok() {
        $uibModalInstance.close(1);
      }
    }
  })();