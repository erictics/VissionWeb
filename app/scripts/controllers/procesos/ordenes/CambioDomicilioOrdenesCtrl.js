(function () {
    'use strict';

    angular
        .module('softvApp')
        .controller('CambioDomicilioOrdenesCtrl', CambioDomicilioOrdenesCtrl);

    CambioDomicilioOrdenesCtrl.inject = ['$uibModalInstance', 'cajasFactory', 'items', '$rootScope', 'ngNotify', 'ordenesFactory', '$uibModal'];
    function CambioDomicilioOrdenesCtrl($uibModalInstance, cajasFactory, items, $rootScope, ngNotify, ordenesFactory, $uibModal) {
        var vm = this;
        vm.Obj = null;
        vm.entrecalles = null;
        vm.OpenEntreCalles = OpenEntreCalles;
        vm.ResetEntreCalles = ResetEntreCalles;
        vm.cancel = cancel;
        vm.changeCiudad = changeCiudad;
        vm.changeLocalidad = changeLocalidad;
        vm.changeColonia = changeColonia;
        vm.ok = ok;
        vm.Detalle=items.Detalle;

        this.$onInit = function () {
            console.log(items);
            if (items.isUpdate) {
                cajasFactory.dameCiudades(items.contrato).then(function (data) {
                    vm.ciudades = data.GetCiudadCAMDOListResult;
                    vm.ciudades.forEach(function (item, index) {
                        if (item.Clv_Ciudad == items.datosCamdo.Clv_Ciudad) {
                            vm.selectedCiudad = vm.ciudades[index];
                        }
                    });
                });
                cajasFactory.dameLocalidades(items.contrato, items.datosCamdo.Clv_Ciudad).then(function (data) {
                    vm.localidades = data.GetLocalidadCAMDOListResult;
                    vm.localidades.forEach(function (item, index) {
                        if (item.Clv_Localidad == items.datosCamdo.Clv_Localidad) {
                            vm.selectedLocalidad = vm.localidades[index];
                        }
                    });
                });
                cajasFactory.dameColonias(items.contrato, items.datosCamdo.Clv_Localidad).then(function (data) {
                    vm.colonias = data.GetColoniaCAMDOListResult;
                    vm.colonias.forEach(function (item, index) {
                        if (item.CLV_COLONIA == items.datosCamdo.Clv_Colonia) {
                            vm.selectedColonia = vm.colonias[index];
                        }
                    });
                });
                cajasFactory.dameCalles(items.contrato, items.datosCamdo.Clv_Colonia).then(function (data) {
                    vm.calles = data.GetCalleCAMDOListResult;
                    vm.calles.forEach(function (item, index) {
                        if (item.Clv_calle == items.datosCamdo.Clv_Calle) {
                            vm.selectedCalle = vm.calles[index];
                        }
                    });
                });
                vm.numero = items.datosCamdo.NUMERO;
                vm.numeroInterior = items.datosCamdo.Num_int;
                vm.telefono = items.datosCamdo.TELEFONO;
                vm.entreCalles = items.datosCamdo.ENTRECALLES;
                vm.Obj = {
                    'CalleNorte': items.datosCamdo.CalleNorte,
                    'CalleSur': items.datosCamdo.CalleSur,
                    'CalleEste': items.datosCamdo.CalleEste,
                    'CalleOeste': items.datosCamdo.CalleOeste,
                    'Casa': items.datosCamdo.Casa,
                    'referencia': items.datosCamdo.Referencia,
                    'entrecalles': items.datosCamdo.ENTRECALLES
                }
            } else {
                cajasFactory.dameCiudades(items.contrato).then(function (data) {
                    vm.ciudades = data.GetCiudadCAMDOListResult;
                });
            }
        }


        function changeCiudad() {
            ResetEntreCalles();
            cajasFactory.dameLocalidades(items.contrato, vm.selectedCiudad.Clv_Ciudad).then(function (data) {
                vm.localidades = data.GetLocalidadCAMDOListResult;
            });
        }

        function changeLocalidad() {
            ResetEntreCalles();
            cajasFactory.dameColonias(items.contrato, vm.selectedLocalidad.Clv_Localidad).then(function (data) {
                vm.colonias = data.GetColoniaCAMDOListResult;
            });
        }

        function changeColonia() {
            ResetEntreCalles();
            cajasFactory.dameCalles(items.contrato, vm.selectedColonia.CLV_COLONIA).then(function (data) {
                vm.calles = data.GetCalleCAMDOListResult;
            });
        }

        function ResetEntreCalles(){
            console.log('Reset');
            vm.entreCalles = null;
            vm.Obj = null;
        }

        function OpenEntreCalles(){
			console.log('X');
            var Obj = {
				'ClvColonia': vm.selectedColonia.CLV_COLONIA,
            	'Clv_calle': vm.selectedCalle.Clv_calle,
                'Obj': vm.Obj,
                'Detalle' : vm.Detalle
			};
			console.log(Obj);
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'views/catalogos/CroquisEntreCallesForm.html',
                controller: 'CroquisEntreCallesOrdSerCtrl',
                controllerAs: 'ctrl',
                backdrop: 'static',
                keyboard: false,
                class: 'modal-backdrop fade',
                size: 'lg',
                resolve: {
                    Obj: function () {
                        return Obj;
                    }
                }
            });
            modalInstance.result.then(function (Obj) {
                console.log(Obj);
                if(Obj != null){
                    vm.Obj = Obj;
                    vm.entreCalles = vm.Obj.entrecalles;
                }

                vm.calles.forEach(function(element, key){
                    if(element.Clv_calle == vm.Obj.callep){
                        vm.selectedCalle = vm.calles[key];
                    }
                });
            });
        }

        function ok() {
            var objCAMDOFAC = {
                'clv_detalle': items.clv_detalle_orden,
                'clv_orden': items.clv_orden,
                'contrato': items.contrato,
                'calle': vm.selectedCalle.Clv_calle,
                'numero': vm.numero,
                'entrecalles': vm.entreCalles,
                'colonia': vm.selectedColonia.CLV_COLONIA,
                'telefono': vm.telefono,
                'ciudad': vm.selectedCiudad.Clv_Ciudad,
                'numinterior': vm.numeroInterior,
                'localidad': vm.selectedLocalidad.Clv_Localidad,
                'CalleNorte': vm.Obj.CalleNorte,
                'CalleSur': vm.Obj.CalleSur,
                'CalleEste': vm.Obj.CalleEste,
                'CalleOeste': vm.Obj.CalleOeste,
                'Casa': vm.Obj.Casa,
                'Referencia': vm.Obj.referencia,
            };
            console.log(objCAMDOFAC);
            ordenesFactory.addCambioDomicilio(objCAMDOFAC).then(function (data) {
                $uibModalInstance.dismiss('cancel');
                $rootScope.$emit('actualiza_tablaServicios');
            });
        }

        function cancel() {
            $uibModalInstance.dismiss('cancel');
        }
    }
})();
