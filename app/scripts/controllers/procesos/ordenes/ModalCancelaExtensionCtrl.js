'use strict';

angular
    .module('softvApp')
    .controller('ModalCancelaExtensionCtrl', function($uibModalInstance,ordenesFactory,$uibModal, $rootScope, ngNotify, $state, options){

        function init(){          

            ordenesFactory.GetDameExteciones_Cli(options.Contrato).then(function(res){
               
                 console.log(res);
                vm.extensiones=res.GetDameExteciones_CliResult.Extenciones;
            });
           
        }

        function ok(){
            if(vm.extensiones < vm.extensionesCan){
            ngNotify.set('La cantidad a cancelar excede el numero de extensiones instaladas en el contrato');
            }            
            ordenesFactory.GetNUECANEX(options.Clave,options.Clv_Orden,options.Contrato,vm.extensionesCan).then(function(res){
              console.log(res);
            });
        }

        function cancel() {
            $uibModalInstance.dismiss('cancel');
        }

        var vm = this; 
        init();      
        vm.ok = ok;
        vm.cancel = cancel;
      

    });