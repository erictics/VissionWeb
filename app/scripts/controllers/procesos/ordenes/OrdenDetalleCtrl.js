'use strict';
angular
  .module('softvApp')
  .controller('OrdenDetalleCtrl', function ($rootScope, $uibModal,DescargarMaterialFactory, ngNotify, $localStorage, $state, $stateParams, ordenesFactory) {
    
    this.$onInit = function () {
      vm.clv_orden = $stateParams.id;
      ordenesFactory.ConsultaOrdSer(vm.clv_orden).then(function (data) {
        console.log(data);
        var conceptos = data.GetDeepConsultaOrdSerResult;
        vm.Clv_Orden = conceptos.Clv_Orden;
        vm.Clv_TipSer = conceptos.Clv_TipSer;
        vm.Contrato = conceptos.Contrato;
        vm.ContratoCom = conceptos.ContratoCom;
        vm.Fec_Eje = conceptos.Fec_Eje;
        vm.Fec_Sol = conceptos.Fec_Sol;
        vm.FechaEjecucionReal = conceptos.FechaEjecucionReal;
        vm.NoBitacora = conceptos.NoBitacora;
        vm.NombreTecnico = conceptos.NombreTecnico;
        vm.Obs = conceptos.Obs;
        vm.Clv_status = conceptos.STATUS;
        vm.Visita1 = conceptos.Visita1;
        vm.Visita2 = conceptos.Visita2;
        vm.Ejecuto = conceptos.UserEjecuto;
        vm.Genero = conceptos.UserGenero;
        vm.Clv_Tecnico = conceptos.Clv_Tecnico;
        vm.TecnicoCuadrilla = conceptos.TecnicoCuadrilla;
        vm.detalleTrabajo = detalleTrabajo;
        vm.ValidarDescargaMaterialOrden = ValidarDescargaMaterialOrden;

        ordenesFactory.GetConHIHF_OrdenQeja($stateParams.id).then(function(data){
          var HoraI = data.GetConHIHF_OrdenQejaResult.HoraIni;
          vm.HoraInicio = HoraI;
          var HoraF = data.GetConHIHF_OrdenQejaResult.HoraFin;
          vm.HoraFin = HoraF;
        });

        for (var t = 0; t < vm.Status.length; t++) {
          if (vm.Status[t].Clave == vm.Clv_status) {
            vm.Estatus = vm.Status[t];
          }
        }

        for (var s = 0; s < vm.TipSer.length; s++) {
          if (vm.TipSer[s].Clave == vm.Clv_TipSer) {
            vm.Tip_Ser = vm.TipSer[s];
          }
        }

        if (vm.Clv_TipSer == '1') {
          vm.ClvTipSer1 = true;
        } else if (vm.Clv_TipSer == '2') {
          vm.ClvTipSer2 = true;
        }

        ordenesFactory.getContratoReal(vm.ContratoCom).then(function (data) {
          var conceptos_Contrato = data.GetuspBuscaContratoSeparado2ListResult[0];

          ordenesFactory.buscarCliPorContrato(conceptos_Contrato.ContratoBueno).then(function (data) {
            vm.datosCli = data.GetDeepBUSCLIPORCONTRATO_OrdSerResult;
          });

          ordenesFactory.serviciosCliente(conceptos_Contrato.ContratoBueno).then(function (data) {
            vm.servicios = data.GetDameSerDelCliFacListResult;
          });
        });
      });

      ordenesFactory.consultaTablaServicios(vm.clv_orden).then(function (data) {
        vm.trabajosTabla = data.GetBUSCADetOrdSerListResult;
      });

    }

    function getTime(date) {
      var HoraIni = date.split(' ');
      if (HoraIni.length == 3) {
        return HoraIni[2];
      } else if (HoraIni.length == 4) {
        var hora = HoraIni[3].split(':');
        if (hora[0].length == 1) {
          return '0' + HoraIni[3];
        } else {
          return HoraIni[3];
        }
      }
    }


    function ValidarDescargaMaterialOrden() {
      if (vm.NombreTecnico != undefined) {
         DescargarMaterialFactory.GetUspChecaSiTieneExtensiones(vm.clv_orden).then(function(res){
          if(res.GetUspChecaSiTieneExtensionesResult.BND === 1){
            DescargaMaterialExtensiones(res.GetUspChecaSiTieneExtensionesResult.NUMEXT);
          }else{
            DescargaMaterialOrden();
          }
        });
      } else {
        ngNotify.set("No tiene Tecnico asignado, no puedes Descargar Material","error");
      }
    }

    function DescargaMaterialExtensiones ( NoExt ) {
      ordenesFactory.ConsultaOrdSer(vm.clv_orden).then(function (data) {
        var conceptos = data.GetDeepConsultaOrdSerResult;
          var CLV_TECNICO = conceptos.Clv_Tecnico;
          var NOMBRE = conceptos.NombreTecnico;
      var options = {};
      options.ClvOrden = vm.clv_orden;
      //options.SctTecnico = vm.Clv_Tec;
      options.SctTecnico = {BaseIdUser: 0, BaseRemoteIp: null, CLV_TECNICO, ClvOrdSer: null, NOMBRE};
      options.Tipo_Descargar = "O";
      options.Detalle = false;
      options.NumExt= NoExt;

      var modalInstance = $uibModal.open({
        animation: vm.animationsEnabled,
        ariaLabelledBy: "modal-title",
        ariaDescribedBy: "modal-body",
        templateUrl: "views/procesos/ModalDescargaExt.html",
        controller: "ModalDescargaExtCtrl",
        controllerAs: "ctrl",
        backdrop: "static",
        keyboard: false,
        size: "lg",
        resolve: {
          options: function() {
            return options;
          }
        }
      });
      modalInstance.result.then(function(idBitacoraSOFTV) {
        vm.idBitacoraSOFTV = idBitacoraSOFTV;
      });
    });
    }

    function DescargaMaterialOrden() {
      ordenesFactory.ConsultaOrdSer(vm.clv_orden).then(function(data){
        var conceptos = data.GetDeepConsultaOrdSerResult;
          var CLV_TECNICO = conceptos.Clv_Tecnico;
          var NOMBRE = conceptos.NombreTecnico;
      var options = {};
      options.ClvOrden = vm.clv_orden;
      //options.SctTecnico = vm.Clv_Tec;
      options.SctTecnico = {BaseIdUser: 0, BaseRemoteIp: null, CLV_TECNICO, ClvOrdSer: null, NOMBRE};
      options.Tipo_Descargar = "O";
      options.Detalle = false;

      var modalInstance = $uibModal.open({
        animation: vm.animationsEnabled,
        ariaLabelledBy: "modal-title",
        ariaDescribedBy: "modal-body",
        templateUrl: "views/procesos/ModalDescargaMaterial.html",
        controller: "ModalDescargaMaterialCtrl",
        controllerAs: "ctrl",
        backdrop: "static",
        keyboard: false,
        size: "lg",
        resolve: {
          options: function() {
            return options;
          }
        }
      });
      modalInstance.result.then(function(idBitacoraSOFTV) {
        vm.idBitacoraSOFTV = idBitacoraSOFTV;
        var Obj = {
          'Clv': vm.clv_orden,
          'Op': 2
        };
        DescargarMaterialFactory.GetchecaBitacoraTecnico(vm.clv_orden,"O").then(function(data) {  
          if (data.GetchecaBitacoraTecnicoResult != null) {
            vm.idBitacora = data.GetchecaBitacoraTecnicoResult.idBitacora;
            vm.idTecnicoBitacora = data.GetchecaBitacoraTecnicoResult.clvTecnico;
              ordenesFactory.MuestraRelOrdenesTecnicos(vm.clv_orden).then(function(data) {
                vm.tecnico = data.GetMuestraRelOrdenesTecnicosListResult;
                  if (vm.idTecnicoBitacora > 0) {
                    for (var a = 0; a < vm.tecnico.length; a++) {
                      if (vm.tecnico == vm.idTecnicoBitacora) {
                        vm.Clv_Tec = vm.tecnico;
                        vm.blockTecnico = true;
                      }
                    }
                  }
              });
          }else{
            ordenesFactory.GetConTecnicoAgenda(Obj).then(function (data) {
              vm.idTecnicoBitacora = data.GetConTecnicoAgendaResult.clv_tecnico;
                ordenesFactory.MuestraRelOrdenesTecnicos(vm.clv_orden).then(function(data) {
                  vm.tecnico = data.GetMuestraRelOrdenesTecnicosListResult;
                    if (vm.idTecnicoBitacora > 0) {
                       for (var a = 0; a < vm.tecnico.length; a++) {
                        if (vm.tecnico == vm.idTecnicoBitacora) {
                          vm.Clv_Tec = vm.tecnico;
                          vm.blockTecnico = true;
                        }
                      }
                    }
                });
            });
          }
        });
      });

      });
    }


    function detalleTrabajo(trabajo, x) { 
      if(vm.Clv_status=='E'){
      var items = {};
      vm.clv_detalle=x.Clave;
      items.contrato = vm.Contrato;
      if (x.Descripcion.toLowerCase().includes('ipaqu') ||
        x.Descripcion.toLowerCase().includes('bpaqu') ||
        x.Descripcion.toLowerCase().includes('dpaqu') ||
        x.Descripcion.toLowerCase().includes('rpaqu') ||
        x.Descripcion.toLowerCase().includes('ipaqut') ||
        x.Descripcion.toLowerCase().includes('bpaqt') ||
        x.Descripcion.toLowerCase().includes('dpaqt') ||
        x.Descripcion.toLowerCase().includes('rpaqt') ||
        x.Descripcion.toLowerCase().includes('bpaad') ||
        x.Descripcion.toLowerCase().includes('bsedi')) {
        items.clv_detalle_orden = x.Clave;
        items.clv_orden = x.Clv_Orden;
        items.descripcion = x.Descripcion.toLowerCase();
        items.servicio = vm.Clv_TipSer; 
        items.Detalle = false; 

        var modalInstance = $uibModal.open({
          animation: true,
          ariaLabelledBy: 'modal-title',
          ariaDescribedBy: 'modal-body',
          templateUrl: 'views/procesos/bajaServicios.html',
          controller: 'BajaServiciosCtrl',
          controllerAs: 'ctrl',
          backdrop: 'static',
          keyboard: false,
          size: 'md',
          resolve: {
            items: function () {
              return items;
            }
          }
        });
      } else if (
        x.Descripcion.toLowerCase().includes('camdo') ||
        x.Descripcion.toLowerCase().includes('cadig') ||
        x.Descripcion.toLowerCase().includes('canet')
      ) {

        ordenesFactory.consultaCambioDomicilio(vm.clv_detalle, x.Clv_Orden, vm.Contrato).then(function (data) {
          var items = {
            clv_detalle_orden: vm.clv_detalle,
            clv_orden: vm.Clv_Orden,
            contrato: vm.Contrato,
            isUpdate: (data.GetDeepCAMDOResult == null) ? false : true,
            datosCamdo: data.GetDeepCAMDOResult,
            Detalle:true
          };

          var modalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'views/facturacion/modalCambioDomicilio.html',
            controller: 'CambioDomicilioOrdenesCtrl',
            controllerAs: 'ctrl',
            backdrop: 'static',
            keyboard: false,
            size: 'md',
            resolve: {
              items: function () {
                return items;
              }
            }
          });
        });
      } else if (
        x.Descripcion.toLowerCase().includes('iante') ||
        x.Descripcion.toLowerCase().includes('inlnb') ||
        x.Descripcion.toLowerCase().includes('iapar') ||

        x.Descripcion.toLowerCase().includes('iantx') ||
        x.Descripcion.toLowerCase().includes('inups') ||
        x.Descripcion.toLowerCase().includes('itrip') ||
        x.Descripcion.toLowerCase().includes('iradi') ||
        x.Descripcion.toLowerCase().includes('irout') ||
        x.Descripcion.toLowerCase().includes('icabm') ||
        x.Descripcion.toLowerCase().includes('ecabl') ||
        x.Descripcion.toLowerCase().includes('econt')

      ) {

        vm.NOM = x.Descripcion.split(' ');
        var items_ = {
          'Op': 'M',
          'Trabajo': vm.NOM[0],
          'Contrato': vm.Contrato,
          'ClvTecnico': vm.NombreTecnico,
          'Clave': x.Clave,
          'ClvOrden': x.Clv_Orden,
          'Detalle':false
        };

        var modalInstance = $uibModal.open({
          animation: true,
          ariaLabelledBy: 'modal-title',
          ariaDescribedBy: 'modal-body',
          templateUrl: 'views/procesos/ModalAsignaAparato.html',
          controller: 'ModalAsignaAparatoCtrl',
          controllerAs: 'ctrl',
          backdrop: 'static',
          keyboard: false,
          size: 'md',
          resolve: {
            items: function () {
              return items_;
            }
          }
        });
      } else if (

        x.Descripcion.toLowerCase().includes('rante') ||
        x.Descripcion.toLowerCase().includes('relnb') ||
        x.Descripcion.toLowerCase().includes('rcabl') ||
        x.Descripcion.toLowerCase().includes('rcont') ||
        x.Descripcion.toLowerCase().includes('rapar') ||
        x.Descripcion.toLowerCase().includes('rantx') ||
        x.Descripcion.toLowerCase().includes('riapar') ||
        x.Descripcion.toLowerCase().includes('retca') ||
        x.Descripcion.toLowerCase().includes('rradi') ||
        x.Descripcion.toLowerCase().includes('rrout') ||
        x.Descripcion.toLowerCase().includes('rapag')
      ) {
        vm.TrabajoRetiro = true;

      } else if (
        x.Descripcion.toLowerCase().includes('ccabm') ||
        x.Descripcion.toLowerCase().includes('cantx')
      ) {
        vm.NOM = x.Descripcion.split(' ');
        var items_ = {
          'Op': 'M',
          'Trabajo': vm.NOM[0],
          'Contrato': vm.Contrato,
          'ClvTecnico': vm.NombreTecnico,
          'Clave': x.Clave,
          'ClvOrden': x.Clv_Orden,
          'Detalle':false
        };

        var modalInstance = $uibModal.open({
          animation: true,
          ariaLabelledBy: 'modal-title',
          ariaDescribedBy: 'modal-body',
          templateUrl: 'views/procesos/ModalAsignaAparato.html',
          controller: 'ModalCambioAparatoCtrl',
          controllerAs: 'ctrl',
          backdrop: 'static',
          keyboard: false,
          size: 'md',
          resolve: {
            items: function () {
              return items_;
            }
          }
        });
      } else if(
          x.Descripcion.toLowerCase().includes('isnet') ||
          x.Descripcion.toLowerCase().includes('isdig') ||
          x.Descripcion.toLowerCase().includes('istva')
          ){
          var items_ = {
            'clv_orden': x.Clv_Orden,
            'Clv_Tecnico': vm.NombreTecnico,
            'Detalle':false
          };

          var modalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'views/procesos/ModalInstalaServicio.html',
            controller: 'ModalInstalaServicioCtrl',
            controllerAs: 'ctrl',
            backdrop: 'static',
            keyboard: false,
            size: 'lg',
            resolve: {
              items: function () {
                return items_;
              }
            }
          });
      }else {
        console.log('este trabajo no esta implementado');
      }
      }
    }

    function MuestraAgenda() {
      var options = {};
      options.clv_queja_orden = vm.clv_orden;
      options.opcion = 1;
      var modalInstance = $uibModal.open({
        animation: vm.animationsEnabled,
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: 'views/procesos/ModalAgendaQueja.html',
        controller: 'ModalAgendaQuejaCtrl',
        controllerAs: 'ctrl',
        backdrop: 'static',
        keyboard: false,
        size: 'sm',
        resolve: {
          options: function () {
            return options;
          }
        }
      });
    }

    var vm = this;
    vm.titulo = 'Detalle Orden #' + $stateParams.id;
    vm.DescargaMaterialOrden = DescargaMaterialOrden;
    vm.MuestraAgenda = MuestraAgenda;
    vm.Status = [{
        'Clave': 'P',
        'Nombre': 'Pendiente'
      },
      {
        'Clave': 'V',
        'Nombre': 'Con Visita'
      },
      {
        'Clave': 'E',
        'Nombre': 'Ejecutada'
      }
    ];

    vm.TipSer = [{
        'Clave': '1',
        'Nombre': 'Es Hotel'
      },
      {
        'Clave': '2',
        'Nombre': 'Solo Internet'
      }
    ];

    vm.ReturnCli = ($stateParams.op != '')? true:false;
    vm.opR = $stateParams.op;
    
  });
