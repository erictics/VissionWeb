(function () {
  'use strict';

  angular
    .module('softvApp')
    .controller('ModalAgregarServicioCtrl', ModalAgregarServicioCtrl);

  ModalAgregarServicioCtrl.inject = ['$uibModal', '$uibModalInstance', 'ordenesFactory', 'items', '$rootScope', 'ngNotify'];

  function ModalAgregarServicioCtrl($uibModal, $uibModalInstance, ordenesFactory, items, $rootScope, ngNotify, $localStorage) {
    var vm = this;
    vm.cancel = cancel;
    vm.guardaDetalle = guardaDetalle;
    vm.changeTrabajo = changeTrabajo;
    vm.realizar = true;

    this.$onInit = function () {
     
      ordenesFactory.dimeServicio(items.contrato).then(function (data) {
        vm.servicios = data.GetDime_Que_servicio_Tiene_clienteListResult;
      });
    }

    function changeTrabajo() {   
      ordenesFactory.muestraTrabajo(vm.selectedServicio.clv_tipser, $localStorage.currentUser.tipoUsuario).then(function (data) {
        vm.tipoTrabajo = data.GetMUESTRATRABAJOSPorTipoUsuarioListResult;
      });
    }

    function guardaDetalle() {
      var TrabajoDesc = vm.selectedTrabajo.Descripcion.split(' - ');
      vm.TrabajoSelect = TrabajoDesc[0];

      ordenesFactory.validaOrden(items.contrato, vm.selectedTrabajo.Clv_Trabajo).then(function (data) {
        if (data.GetDeepVALIDAOrdenQuejaResult.Msg != null) {
          ngNotify.set(data.GetDeepVALIDAOrdenQuejaResult.Msg, 'info');
        } else {
          var realiza = 0;
          if (vm.realizar) {
            realiza = 1;
          }
          var detalle = {
            clave: items.clv_orden,
            trabajo: vm.selectedTrabajo.Clv_Trabajo,
            observaciones: vm.observaciones,
            seRealiza: realiza
          };

           if (vm.selectedTrabajo.Descripcion.toLowerCase().includes('bcabm')) {
              ngNotify.set('La baja de cablemodem se genera de forma automática en el momento que todos los servicios pasen a baja.', 'info');
              return;
            } else if (vm.selectedTrabajo.Descripcion.toLowerCase().includes('bapar')) {
              ngNotify.set('La baja de aparato digital se genera de forma automática en el momento que todos los servicios pasen a baja.', 'info');
              return;
            }
          ordenesFactory.addDetalleOrden(detalle).then(function (data) {
            vm.clv_detalle_orden = data.AddDetOrdSerResult;

            $rootScope.$emit('detalle_orden', vm.clv_detalle_orden);
             if (vm.TrabajoSelect.toLowerCase() ==  'camdo' || 
                vm.TrabajoSelect.toLowerCase() ==  'cadig' ||
                vm.TrabajoSelect.toLowerCase() ==  'canet') {
              items.clv_detalle_orden = vm.clv_detalle_orden;
              items.isUpdate = false;
              var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'views/facturacion/modalCambioDomicilio.html',
                controller: 'CambioDomicilioOrdenesCtrl',
                controllerAs: 'ctrl',
                backdrop: 'static',
                keyboard: false,
                size: 'md',
                resolve: {
                  items: function () {
                    return items;
                  }
                }
              });
            } else if (vm.TrabajoSelect.toLowerCase() == 'ipaqu' ||
              vm.TrabajoSelect.toLowerCase() == 'bpaqu' ||
              vm.TrabajoSelect.toLowerCase() == 'dpaqu' ||
              vm.TrabajoSelect.toLowerCase() == 'rpaqu' ||
              vm.TrabajoSelect.toLowerCase() == 'ipaqut' ||
              vm.TrabajoSelect.toLowerCase() == 'bpaqt' ||
              vm.TrabajoSelect.toLowerCase() == 'dpaqt' ||
              vm.TrabajoSelect.toLowerCase() == 'rpaqt' ||
              vm.TrabajoSelect.toLowerCase() == 'bpaad' ||
              vm.TrabajoSelect.toLowerCase() == 'bsedi') {
              items.clv_detalle_orden = vm.clv_detalle_orden;
              items.descripcion = vm.selectedTrabajo.Descripcion.toLowerCase();
              items.servicio = vm.selectedServicio;
              var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'views/procesos/bajaServicios.html',
                controller: 'BajaServiciosCtrl',
                controllerAs: 'ctrl',
                backdrop: 'static',
                keyboard: false,
                size: 'lg',
                resolve: {
                  items: function () {
                    return items;
                  }
                }
              });
            } else if (
              vm.TrabajoSelect.toLowerCase() == 'iante' ||
              vm.TrabajoSelect.toLowerCase() == 'inlnb' ||
              vm.TrabajoSelect.toLowerCase() == 'iapar' ||
              vm.TrabajoSelect.toLowerCase() == 'riapar' ||
              vm.TrabajoSelect.toLowerCase() == 'iantx' ||
              vm.TrabajoSelect.toLowerCase() == 'iradi' ||
              vm.TrabajoSelect.toLowerCase() == 'irout' ||
              vm.TrabajoSelect.toLowerCase() == 'icabm' ||
              vm.TrabajoSelect.toLowerCase() == 'ecabl' ||
              vm.TrabajoSelect.toLowerCase() == 'econt' ||
              vm.TrabajoSelect.toLowerCase() == 'rante' ||
              vm.TrabajoSelect.toLowerCase() == 'relnb' ||
              vm.TrabajoSelect.toLowerCase() == 'rcabl' ||
              vm.TrabajoSelect.toLowerCase() == 'rcont' ||
              vm.TrabajoSelect.toLowerCase() == 'rapar' ||
              vm.TrabajoSelect.toLowerCase() == 'rantx' ||
              vm.TrabajoSelect.toLowerCase() == 'retca' ||
              vm.TrabajoSelect.toLowerCase() == 'rradi' ||
              vm.TrabajoSelect.toLowerCase() == 'rrout' ||
              vm.TrabajoSelect.toLowerCase() == 'rapag'
            ) {
              vm.NOM = vm.selectedTrabajo.Descripcion.split(' ');
             
              var items_ = {
                'Op': 'N',
                'Trabajo': vm.NOM[0],
                'Contrato': items.contrato,
                'ClvTecnico': items.clv_tecnico,
                'Clave': items.clv_orden,
                'ClvOrden':items.clv_orden
              };

              var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'views/procesos/ModalAsignaAparato.html',
                controller: 'ModalAsignaAparatoCtrl',
                controllerAs: 'ctrl',
                backdrop: 'static',
                keyboard: false,
                size: 'md',
                resolve: {
                  items: function () {
                    return items_;
                  }
                }
              });


            } 
            else if (vm.TrabajoSelect.toLowerCase() == 'isnet' || 
              vm.TrabajoSelect.toLowerCase() == 'isdig' || 
              vm.TrabajoSelect.toLowerCase() == 'istva') {

              vm.NOM = vm.selectedTrabajo.Descripcion.split(' ');
              var items_ = {
                'Trabajo': vm.NOM[0],
                'Contrato': items.contrato,
                'Clave': vm.clv_detalle_orden,
                'ClvOrden':items.clv_orden,
                'Clv_TipSer': vm.selectedServicio.clv_tipser
              };
              var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'views/procesos/ModalInstalaServicioOrdenManual.html',
                controller: 'ModalInstalaServicioOrdenManualCtrl',
                controllerAs: 'ctrl',
                backdrop: 'static',
                keyboard: false,
                size: 'md',
                resolve: {
                  items: function () {
                    return items_;
                  }
                }
              });
            }else if(vm.TrabajoSelect.toLowerCase() ==  'conex'){
              
                      var options={
                        Clave: vm.clv_detalle_orden,
                        Clv_Orden: items.clv_orden,
                        Contrato:items.contrato,
                        tipo:'N',
                        detalle:false
                      };
                      var modalInstance = $uibModal.open({
                        animation: vm.animationsEnabled,
                        ariaLabelledBy: "modal-title",
                        ariaDescribedBy: "modal-body",
                        templateUrl: "views/procesos/ModalExtensionAdicional.html",
                        controller: "ModalExtensionAdicionalCtrl",
                        controllerAs: "ctrl",
                        backdrop: "static",
                        keyboard: false,
                        size: "sm",
                        resolve: {
                          options: function() {
                            return options;
                          }
                        }
                      });
                      
            }
            else if(vm.TrabajoSelect.toLowerCase().includes('canex') ){                   
                      var options = {
                        Clave: vm.clv_detalle_orden,
                        Clv_Orden: items.clv_orden,
                        Contrato:items.contrato,
                        tipo:'N',
                        detalle:false
                      };
                      var modalInstance = $uibModal.open({
                        animation: vm.animationsEnabled,
                        ariaLabelledBy: "modal-title",
                        ariaDescribedBy: "modal-body",
                        templateUrl: "views/procesos/ModalCancelaExtension.html",
                        controller: "ModalCancelaExtensionCtrl",
                        controllerAs: "ctrl",
                        backdrop: "static",
                        keyboard: false,
                        size: "sm",
                        resolve: {
                          options: function() {
                            return options;
                          }
                        }
                      });
                      
            }else{
             
            }

            $rootScope.$emit('actualiza_tablaServicios');
            $uibModalInstance.dismiss('cancel');
          });
        }
      });
    }

    function cancel() {
      $uibModalInstance.dismiss('cancel');
    }
  }
})();
