'use strict';
angular
	.module('softvApp')
	.controller('ModalConsultaClienteMasivoZonaCtrl', function (QuejaMasivaFactory, $uibModalInstance, $uibModal, ngNotify, clv_queja, FormEditQM, $localStorage) {

		function initData(){
            GetAll();
		}

        function GetAll(){
            GetEstadoQM();
            GetEstadoQMDis();
            GetCiudadQM();
            GetCiudadQMDis();
            GetLocalidadQM();
            GetLocalidadQMDis();
            GetColoniaQM();
            GetColoniaQMDis();
            GetCalleQM();
            GetCalleQMDis();
        }

        function GetEstadoQM(){
            QuejaMasivaFactory.GetSoftvWeb_ListaQMEstados(vm.clv_queja).then(function(data){
                vm.EstadoInc = data.GetSoftvWeb_ListaQMEstadosResult;
            });
        }

        function GetEstadoQMDis(){
            var Obj = {
                'Clv_Queja': vm.clv_queja,
                'Clave': $localStorage.currentUser.idUsuario
            };
            QuejaMasivaFactory.GetSoftvWeb_ListaQMEstadoDis(Obj).then(function(data){
                vm.EstadoDisp = data.GetSoftvWeb_ListaQMEstadoDisResult;
            });
        }

        function AddEstado(Obj, OP){
            if(vm.FormEditQM == true){
            var List = [
                {'Clv_Estado': Obj.Clv_Estado}
            ];
            var ObjAdd = {
                'ListEstado': List,
                'Clv_Queja': vm.clv_queja,
                'OP': OP,
                'Clave': $localStorage.currentUser.idUsuario
            };
            QuejaMasivaFactory.GetSoftvWeb_UpdateEstadoQM(ObjAdd).then(function(data){
                GetAll();
            });
            }
        }

        function AddAllEstado(OP){
            var List = [];
            if(OP == 1){
                vm.EstadoDisp.forEach(function(element) {
                    var Obj = {'Clv_Estado': element.Clv_Estado}
                    List.push(Obj);
                });
            }else if(OP == 2){
                vm.EstadoInc.forEach(function(element) {
                    var Obj = {'Clv_Estado': element.Clv_Estado}
                    List.push(Obj);
                });
            }
            var ObjAdd = {
                'ListEstado': List,
                'Clv_Queja': vm.clv_queja,
                'OP': OP,
                'Clave': $localStorage.currentUser.idUsuario
            };
            QuejaMasivaFactory.GetSoftvWeb_UpdateEstadoQM(ObjAdd).then(function(data){
                GetAll();
            });
        }

        function GetCiudadQM(){
            QuejaMasivaFactory.GetSoftvWeb_ListaQMCiudades(vm.clv_queja).then(function(data){
                vm.CiudadInc = data.GetSoftvWeb_ListaQMCiudadesResult;
            });
        }

        function GetCiudadQMDis(){
            var Obj = {
                'Clv_Queja': vm.clv_queja,
                'Clave': $localStorage.currentUser.idUsuario
            };
            QuejaMasivaFactory.GetSoftvWeb_ListaQMCiudadDis(Obj).then(function(data){
                vm.CiudadDisp = data.GetSoftvWeb_ListaQMCiudadDisResult;
            });
        }

        function AddCiudad(Obj, OP){
            if(vm.FormEditQM == true){
            var List = [
                {'Clv_Ciudad': Obj.Clv_Ciudad}
            ];
            var ObjAdd = {
                'ListCiudad': List,
                'Clv_Queja': vm.clv_queja,
                'OP': OP,
                'Clave': $localStorage.currentUser.idUsuario
            };
            QuejaMasivaFactory.GetSoftvWeb_UpdateCiudadQM(ObjAdd).then(function(data){
                GetAll();
            });
            }
        }

        function AddAllCiudad(OP){
            var List = [];
            if(OP == 1){
                vm.CiudadDisp.forEach(function(element){
                    var Obj = {'Clv_Ciudad': element.Clv_Ciudad}
                    List.push(Obj);
                });
            }else if(OP == 2){
                vm.CiudadInc.forEach(function(element){
                    var Obj = {'Clv_Ciudad': element.Clv_Ciudad}
                    List.push(Obj);
                });
            }
            var ObjAdd = {
                'ListCiudad': List,
                'Clv_Queja': vm.clv_queja,
                'OP': OP,
                'Clave': $localStorage.currentUser.idUsuario
            };
            QuejaMasivaFactory.GetSoftvWeb_UpdateCiudadQM(ObjAdd).then(function(data){
                GetAll();
            });
        }

        function GetLocalidadQM(){
            QuejaMasivaFactory.GetSoftvWeb_ListaQMLocalidades(vm.clv_queja).then(function(data){
                vm.LocalidadInc = data.GetSoftvWeb_ListaQMLocalidadesResult;
            });
        }

        function GetLocalidadQMDis(){
            var Obj = {
                'Clv_Queja': vm.clv_queja,
                'Clave': $localStorage.currentUser.idUsuario
            };
            QuejaMasivaFactory.GetSoftvWeb_ListaQMLocalidadDis(Obj).then(function(data){
                vm.LocalidadDisp = data.GetSoftvWeb_ListaQMLocalidadDisResult;
            });
        }

        function AddLocalidad(Obj, OP){
            if(vm.FormEditQM == true){
            var List = [
                {'Clv_Localidad': Obj.Clv_Localidad}
            ];
            var ObjAdd = {
                'ListLocalidad': List,
                'Clv_Queja': vm.clv_queja,
                'OP': OP,
                'Clave': $localStorage.currentUser.idUsuario
            };
            QuejaMasivaFactory.GetSoftvWeb_UpdateLocalidadQM(ObjAdd).then(function(data){
                GetAll();
            });
            }
        }

        function AddAllLocalidad(OP){
            var List = [];
            if(OP == 1){
                vm.LocalidadDisp.forEach(function(element){
                    var Obj = {'Clv_Localidad': element.Clv_Localidad};
                    List.push(Obj);
                });
            }else if(OP == 2){
                vm.LocalidadInc.forEach(function(element){
                    var Obj = {'Clv_Localidad': element.Clv_Localidad};
                    List.push(Obj);
                });
            }
            var ObjAdd = {
                'ListLocalidad': List,
                'Clv_Queja': vm.clv_queja,
                'OP': OP,
                'Clave': $localStorage.currentUser.idUsuario
            };
            QuejaMasivaFactory.GetSoftvWeb_UpdateLocalidadQM(ObjAdd).then(function(data){
                GetAll();
            });
        }

        function GetColoniaQM(){
            QuejaMasivaFactory.GetSoftvWeb_ListaQMColonias(vm.clv_queja).then(function(data){
                vm.ColoniaInc = data.GetSoftvWeb_ListaQMColoniasResult;
            });
        }

        function GetColoniaQMDis(){
            var Obj = {
                'Clv_Queja': vm.clv_queja,
                'Clave': $localStorage.currentUser.idUsuario
            };
            QuejaMasivaFactory.GetSoftvWeb_ListaQMColoniaDis(Obj).then(function(data){
                vm.ColoniaDisp = data.GetSoftvWeb_ListaQMColoniaDisResult;
            });
        }

        function AddColonia(Obj, OP){
            if(vm.FormEditQM == true){
            var List = [
                {'clv_colonia': Obj.clv_colonia}
            ];
            var ObjAdd = {
                'ListColonia': List,
                'Clv_Queja': vm.clv_queja,
                'OP': OP,
                'Clave': $localStorage.currentUser.idUsuario
            };
            QuejaMasivaFactory.GetSoftvWeb_UpdateColoniaQM(ObjAdd).then(function(data){
                GetAll();
            });
            }
        }

        function AddAllColonia(OP){
            var List = [];
            if(OP == 1){
                vm.ColoniaDisp.forEach(function(element){
                    var Obj = {'clv_colonia': element.clv_colonia};
                    List.push(Obj);
                });
            }else if(OP == 2){
                vm.ColoniaInc.forEach(function(element){
                    var Obj = {'clv_colonia': element.clv_colonia};
                    List.push(Obj);
                });
            }
            var ObjAdd = {
                'ListColonia': List,
                'Clv_Queja': vm.clv_queja,
                'OP': OP,
                'Clave': $localStorage.currentUser.idUsuario
            };
            QuejaMasivaFactory.GetSoftvWeb_UpdateColoniaQM(ObjAdd).then(function(data){
                GetAll();
            });
        }

        function GetCalleQM(){
            QuejaMasivaFactory.GetSoftvWeb_ListaQMCalles(vm.clv_queja).then(function(data){
                vm.CalleInc = data.GetSoftvWeb_ListaQMCallesResult;
            });
        }

        function GetCalleQMDis(){
            var Obj = {
                'Clv_Queja': vm.clv_queja,
                'Clave': $localStorage.currentUser.idUsuario
            };
            QuejaMasivaFactory.GetSoftvWeb_ListaQMCallesDis(Obj).then(function(data){
                vm.CalleDisp = data.GetSoftvWeb_ListaQMCallesDisResult;
            });
        }

        function AddCalle(Obj, OP){
            if(vm.FormEditQM == true){
            var List = [
                {'Clv_Calle': Obj.Clv_Calle}
            ];
            var ObjAdd = {
                'ListCalle': List,
                'Clv_Queja': vm.clv_queja,
                'OP': OP,
                'Clave': $localStorage.currentUser.idUsuario
            };
            QuejaMasivaFactory.GetSoftvWeb_UpdateCalleQM(ObjAdd).then(function(data){
                GetAll();
            });
            }
        }

        function AddAllCalle(OP){
            var List = [];
            if(OP == 1){
                vm.CalleDisp.forEach(function(element){
                    var Obj = {'Clv_Calle': element.Clv_Calle};
                    List.push(Obj);
                });
            }else if(OP == 2){
                vm.CalleInc.forEach(function(element){
                    var Obj = {'Clv_Calle': element.Clv_Calle};
                    List.push(Obj);
                });
            }
            var ObjAdd = {
                'ListCalle': List,
                'Clv_Queja': vm.clv_queja,
                'OP': OP,
                'Clave': $localStorage.currentUser.idUsuario
            };
            QuejaMasivaFactory.GetSoftvWeb_UpdateCalleQM(ObjAdd).then(function(data){
                GetAll();
            });
        }

		function Close() {
            if(vm.CalleInc.length > 0){
                $uibModalInstance.close();
                if(vm.FormEditQM == true){
                ngNotify.set('CORRECTO, Se Guardo el Área.', 'success');
                }
            }else{
                ngNotify.set('ERROR, No se Puede generar un reporte sin seleccionar alguna Calle(s).', 'warn');
            }
		}
		
		var vm = this;
        vm.FormEditQM = FormEditQM;
        vm.clv_queja = clv_queja;
        vm.AddEstado = AddEstado;
        vm.AddAllEstado = AddAllEstado;
        vm.AddCiudad = AddCiudad;
        vm.AddAllCiudad = AddAllCiudad;
        vm.AddLocalidad = AddLocalidad;
        vm.AddAllLocalidad = AddAllLocalidad;
        vm.AddColonia = AddColonia;
        vm.AddAllColonia = AddAllColonia;
        vm.AddCalle = AddCalle;
        vm.AddAllCalle = AddAllCalle;
		vm.Close= Close;
		initData();

	});