'use strict';
angular
	.module('softvApp')
	.controller('ModalBuscaClienteMasivoCtrl', function (QuejaMasivaFactory, $uibModalInstance, $uibModal, ngNotify, $localStorage, FormEditQM) {

		function initData(){
			GetClienteList();
			GetEstadoList();
		}

		function GetClienteList(){
			var ObjCliente = {
				'IdUsuario': $localStorage.currentUser.idUsuario,
				'IdEstado': (vm.Estado != null && vm.Estado != undefined && vm.Estado != '')? vm.Estado.Clv_Estado:null,
				'IdMunicipio': (vm.Cuidad != null && vm.Cuidad != undefined && vm.Cuidad != '')? vm.Cuidad.Clv_Ciudad:null,
				'IdLocalidad': (vm.Localidad != null && vm.Localidad != undefined && vm.Localidad != '')? vm.Localidad.Clv_Localidad:null,
				'IdColonia': (vm.Colonia != null && vm.Colonia != undefined && vm.Colonia != '')? vm.Colonia.clv_colonia:null,
				'Calle': (vm.Calle != null && vm.Calle != undefined && vm.Calle != '')? vm.Calle.Clv_Calle:null,
				'Clv_Queja': 0
			};
			QuejaMasivaFactory.GetSoftvWeb_MQClientes(ObjCliente).then(function(data){
				vm.ClienteList = data.GetSoftvWeb_MQClientesResult;
				vm.ShowList = (vm.ClienteList.length > 0)? true:false;
				vm.ClienteList.forEach(function(element) {
					vm.ListInc.forEach(function(ele){
						if(element.Contrato == ele.Contrato){
							element.Selection = true;
						}
					});
				});
			});
		}

		function GetEstadoList(){
			QuejaMasivaFactory.GetSoftvWeb_QMEstados().then(function(data){
				vm.EstadoList = data.GetSoftvWeb_QMEstadosResult;
			});
		}

		function GetCiudadList(){
			vm.CiudadList = null;
			vm.LocalidadList = null;
			vm.ColoniaList = null;
			vm.CalleList = null;
			if(vm.Estado != null && vm.Estado != undefined && vm.Estado != ''){
				vm.ListEstado = [];
				var Obj = {'Clv_Estado': vm.Estado.Clv_Estado};
				vm.ListEstado.push(Obj);
				QuejaMasivaFactory.GetSoftvWeb_QMCiudades(vm.ListEstado).then(function(data){
					vm.CiudadList = data.GetSoftvWeb_QMCiudadesResult;
					GetClienteList();
				});
			}
		}

		function GetLocalidadList(){
			vm.LocalidadList = null;
			vm.ColoniaList = null;
			vm.CalleList = null;
			if(vm.Cuidad != null && vm.Cuidad != undefined && vm.Cuidad != ''){
				vm.ListCiudad = [];
				var Obj = {'Clv_Ciudad': vm.Cuidad.Clv_Ciudad};
				vm.ListCiudad.push(Obj);
				QuejaMasivaFactory.GetSoftvWeb_QMLocalidades(vm.ListCiudad, vm.ListEstado).then(function(data){
					vm.LocalidadList = data.GetSoftvWeb_QMLocalidadesResult;
					GetClienteList();
				});
			}
		}

		function GetColoniaList(){
			vm.ColoniaList = null;
			vm.CalleList = null;
			if(vm.Localidad != null && vm.Localidad != undefined && vm.Localidad != ''){
				vm.ListLocalidad = [];
				var Obj = {'Clv_Localidad': vm.Localidad.Clv_Localidad};
				vm.ListLocalidad.push(Obj);
				QuejaMasivaFactory.GEtSoftvWeb_QMColonias(vm.ListLocalidad, vm.ListCiudad, vm.ListEstado).then(function(data){
					vm.ColoniaList = data.GEtSoftvWeb_QMColoniasResult;
					GetClienteList();
				});
			}
		}

		function GetCalleList(){
			vm.CalleList = null;
			if(vm.Colonia != null && vm.Colonia != undefined && vm.Colonia != ''){
				vm.ListColonia = [];
				var Obj = {'clv_colonia': vm.Colonia.clv_colonia};
				vm.ListColonia.push(Obj);
				QuejaMasivaFactory.GetSoftvWeb_QMCalles(vm.ListColonia, vm.ListLocalidad, vm.ListCiudad, vm.ListEstado).then(function(data){
					vm.CalleList = data.GetSoftvWeb_QMCallesResult;
					GetClienteList();
				});
			}
		}

		function Seleccionar(ObjCli, Op){
			if(Op == 1){
				vm.ClienteList.forEach(function(element) {
					if(ObjCli.Contrato == element.Contrato){
						element.Selection = true;
						vm.ListInc.push(element);
					}	
				});
			}else if(Op == 2){
				vm.ClienteList.forEach(function(element) {
					if(ObjCli.Contrato == element.Contrato){
						element.Selection = false;
					}	
				});

				vm.ListInc.forEach(function(element, key){
					if(ObjCli.Contrato == element.Contrato){
						vm.ListInc.splice(key, 1);
					}
				});
			}
		}

		function Save(){
			vm.ListCliente = [];
			vm.ListInc.forEach(function(element){
				var Obj = {
					'Contrato': element.Contrato
				};
				vm.ListCliente.push(Obj);
			});
			vm.ObjCliente = vm.ListInc;
			ngNotify.set('CORRECTO, Se agregaron contratos al Reporte de Mantenimiento.', 'success');
			Close();
		}

		function Close() {
			$uibModalInstance.close(vm.ObjCliente);
		}

		var vm = this;
		vm.FormEditQM = FormEditQM;
		vm.ListInc = [];
		vm.ObjCliente = null;
		vm.GetClienteList = GetClienteList;
		vm.GetCiudadList = GetCiudadList;
		vm.GetLocalidadList = GetLocalidadList;
		vm.GetColoniaList = GetColoniaList;
		vm.GetCalleList = GetCalleList;
		vm.Seleccionar = Seleccionar;
		vm.Save = Save;
		vm.Close= Close;
		initData();

	});
