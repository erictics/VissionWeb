'use strict';
angular
	.module('softvApp')
	.controller('AddQuejaMasivaCtrl', function (QuejaMasivaFactory, $state, atencionFactory, ngNotify, $uibModal, $filter, $localStorage) {

		function initData(){
			GetProblemaList();
			GetTecnicoList();
			GetTrabajos();
		}

		function OpenBuscar(){
			if(vm.TipoBusqueda.Id == 1){
				OpenBuscaClienteMasivo();
			}else{
				OpenBuscaClienteMasivoZona();
			}
		}

		function GetProblemaList(){
			atencionFactory.GetClasificacionProblemas().then(function (data) {
				vm.Problemas = data.GetuspConsultaTblClasificacionProblemasListResult;
				vm.Problema = vm.Problemas[0];
			});
		}

		function GetTecnicoList(){
			QuejaMasivaFactory.GetQMTecnicos().then(function (data) {
				vm.Tecnicos = data.GetQMTecnicosResult;
				vm.Tecnicos2 = data.GetQMTecnicosResult;
			});
		}

		function GetTrabajos(){
			QuejaMasivaFactory.GetMUESTRATRABAJOSQUEJAS_MasivosList().then(function(data){
				vm.Trabajos = data.GetMUESTRATRABAJOSQUEJAS_MasivosListResult;
			});
		}

		function OpenBuscaClienteMasivo() {
			var FormEditQM = vm.FormEditQM;
			var modalInstance = $uibModal.open({
				animation: true,
				ariaLabelledBy: 'modal-title',
				ariaDescribedBy: 'modal-body',
				templateUrl: 'views/procesos/ModalBuscaClienteMasivo.html',
				controller: 'ModalBuscaClienteMasivoCtrl',
				controllerAs: 'ctrl',
				backdrop: 'static',
				keyboard: false,
				class: 'modal-backdrop fade',
				size: "lg",
				resolve: {
					FormEditQM: function () {
						return FormEditQM;
					}
				}
			});
			modalInstance.result.then(function(ObjCliente) {
				if(ObjCliente != null){
					vm.LstQuejaCliente = [];
					ObjCliente.forEach(function(element){
						var Obj = {
							'Contrato': element.Contrato
						};
						vm.LstQuejaCliente.push(Obj);
			   		});
					vm.LstQuejaEdo = [];
					vm.LstQuejaCd = [];
					vm.LstQuejaLoc = [];
					vm.LstQuejaCol = [];
					vm.LstQuejaCalle = [];
					vm.Form2 = true;
				}else{
					vm.Form2 = false;
				}
            });
		}

		 function OpenBuscaClienteMasivoZona() {
			var modalInstance = $uibModal.open({
				animation: true,
				ariaLabelledBy: 'modal-title',
				ariaDescribedBy: 'modal-body',
				templateUrl: 'views/procesos/ModalBuscaClienteMasivoZona.html',
				controller: 'ModalBuscaClienteMasivoZonaCtrl',
				controllerAs: 'ctrl',
				backdrop: 'static',
				keyboard: false,
				class: 'modal-backdrop fade',
				size: "lg",
			});
			modalInstance.result.then(function(ObjArea){
			   	if(ObjArea != null){
					vm.LstQuejaEdo = [];
					vm.LstQuejaCd = [];
					vm.LstQuejaLoc = [];
					vm.LstQuejaCol = [];
					vm.LstQuejaCalle = [];
					ObjArea.ListEstado.forEach(function(element) {
						var Obj = {
							'Edo': element.Clv_Estado
						};
						vm.LstQuejaEdo.push(Obj);
					});
					ObjArea.ListCiudad.forEach(function(element) {
						var Obj = {
							'Cd': element.Clv_Ciudad
						};
						vm.LstQuejaCd.push(Obj);
					});
					ObjArea.ListLocalidad.forEach(function(element) {
						var Obj = {
							'Loc': element.Clv_Localidad
						};
						vm.LstQuejaLoc.push(Obj);
					});
					ObjArea.ListColonia.forEach(function(element) {
						var Obj = {
							'Col': element.clv_colonia
						};
						vm.LstQuejaCol.push(Obj);
					});
					ObjArea.ListCalle.forEach(function(element) {
						var Obj = {
							'Calle': element.Clv_Calle
						};
						vm.LstQuejaCalle.push(Obj);
					});
					vm.LstQuejaCliente = [];
			  		vm.Form2 = true;
				}else{
					vm.Form2 = false;
				}
            });
		}

		function SaveQM(){
			var Obj = {
				'Clave_Tecnico': 0,
				'ClvTecnicoSec': 0,  
				'ClvProblema': vm.Problema.clvProblema,
				'Problema': vm.DetalleProblema,
				'MensajeMostrar': vm.Mensaje,
				'Usuario': $localStorage.currentUser.usuario,
				'IdUsuario': $localStorage.currentUser.idUsuario,
				'Clv_Trabajo': (vm.Trabajo != undefined && vm.Trabajo != null)? vm.Trabajo.CLV_TRABAJO:0,
				'Solucion': (vm.ProblemaReal != undefined && vm.ProblemaReal != null)? vm.ProblemaReal:''
			};
			var LstQuejaEdo = (vm.TipoBusqueda.Id == 2)? vm.LstQuejaEdo:{'Edo': 0};
			var LstQuejaCd = (vm.TipoBusqueda.Id == 2)? vm.LstQuejaCd:{'Cd': 0};
			var LstQuejaLoc = (vm.TipoBusqueda.Id == 2)? vm.LstQuejaLoc:{'Loc': 0};
			var LstQuejaCol = (vm.TipoBusqueda.Id == 2)? vm.LstQuejaCol:{'Col': 0};
			var LstQuejaCalle = (vm.TipoBusqueda.Id == 2)? vm.LstQuejaCalle:{'Calle': 0};
			var LstQuejaCliente = (vm.TipoBusqueda.Id == 1)? vm.LstQuejaCliente:{'Contrato': 0};

			var ObjQM = {
				'Obj': Obj,
				'LstQuejaEdo': LstQuejaEdo,
				'LstQuejaCd': LstQuejaCd,
				'LstQuejaLoc': LstQuejaLoc,
				'LstQuejaCol': LstQuejaCol,
				'LstQuejaCalle': LstQuejaCalle,
				'LstQuejaCliente': LstQuejaCliente,
				'Clave': $localStorage.currentUser.idUsuario
			};
			QuejaMasivaFactory.GetAddQuejaMasiva(ObjQM).then(function(data){
				vm.Clv_Queja = data.GetAddQuejaMasivaResult[0].Clv_Queja;
				if(vm.Clv_Queja > 0){
					ngNotify.set('CORRECTO, Se guardó Reporte de Mantenimiento.', 'success');
					$state.go('home.procesos.reportesmasivos');
					OpenReporteQM();
				}else{
					ngNotify.set('ERROR, No se guardó Reporte Masivo.', 'warn');
				}
			});
		}
		
		function OpenReporteQM(){
			var Clv_Queja = vm.Clv_Queja;
			var modalInstance = $uibModal.open({
				animation: true,
				ariaLabelledBy: 'modal-title',
				ariaDescribedBy: 'modal-body',
				templateUrl: 'views/procesos/ReporteQuejaMantenimiento.html',
				controller: 'ReporteQMCtrl',
				controllerAs: 'ctrl',
				backdrop: 'static',
				keyboard: false,
				class: 'modal-backdrop fade',
				size: 'lg',
				resolve: {
					Clv_Queja: function () {
						return Clv_Queja;
					}
				}
			});
		}

		function SetTipoQM(){
			vm.LstQuejaEdo = [];
			vm.LstQuejaCd = [];
			vm.LstQuejaLoc = [];
			vm.LstQuejaCol = [];
			vm.LstQuejaCalle = [];
			vm.LstQuejaCliente = [];
			vm.Form2 = false;
		}

		function cancel() {
			$uibModalInstance.close();
		}

		var vm = this;
		vm.titulo = "Nuevo Reporte de Mantenimiento";
		vm.DFP = true;
		vm.DFE = true;
		vm.Dis1 = false;
		vm.BtnAdd = false;
    	vm.Dis2 = false;
		vm.View1 = false;
		vm.LstQuejaEdo = [];
		vm.LstQuejaCd = [];
		vm.LstQuejaLoc = [];
		vm.LstQuejaCol = [];
		vm.LstQuejaCalle = [];
		vm.LstQuejaCliente = [];
		vm.TipoBuscar = [
			{
				'Id': 1,
				'Desc': 'Contrato'
			},
			{
				'Id': 2,
				'Desc': 'Área'
			}
		];
		vm.TipoBusqueda = vm.TipoBuscar[0];
		vm.Form1 = true;
		vm.Form2 = false;
		vm.BtnSave = true;
		vm.BtnDescM = true;
		vm.Estatus = 'P';
		vm.OpenBuscar = OpenBuscar;
		vm.FormEditQM = true;
		vm.BtnSave = true;
		vm.SetTipoQM = SetTipoQM;
		vm.SaveQM = SaveQM;
		initData();
		
		vm.cancel = cancel;
		vm.FechaSolicitud = $filter('date')(new Date(), 'dd/MM/yyyy');
		vm.HoraSolicitud = $filter('date')(new Date(), 'hh:mm:ss');
		vm.OpenBuscaClienteMasivo = OpenBuscaClienteMasivo;
		vm.OpenBuscaClienteMasivoZona = OpenBuscaClienteMasivoZona;
    
  });