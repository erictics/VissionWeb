'use strict';

angular
    .module('softvApp')
    .controller('AddMensajeQMCtrl', function(QuejaMasivaFactory, $uibModalInstance, $uibModal, ngNotify, $state){

        function initData(){
            GetTecnicoList();
        }

        function GetTecnicoList(){
			QuejaMasivaFactory.GetQMTecnicos().then(function (data) {
				console.log(data);
				vm.Tecnicos = data.GetQMTecnicosResult;
				vm.Tecnicos2 = data.GetQMTecnicosResult;
			});
		}

        function SaveMSJ(){
            vm.ObjQM = {
                'MSJ': vm.Mensaje
            };
            Close();
        }

        function Close() {
            $uibModalInstance.close(vm.ObjQM);
        }

        var vm = this;
        vm.ObjQM = null;
        vm.SaveMSJ = SaveMSJ;
        vm.Close = Close;
        initData();
        
    });