'use strict';
angular
    .module('softvApp')
    .controller('ReporteQMCtrl', function(QuejaMasivaFactory, $uibModalInstance, $uibModal, ngNotify, $state, $sce, globalService, Clv_Queja){

        function initData(){
            QuejaMasivaFactory.GetSp_ReporteQuejaMasiva(Clv_Queja).then(function(data){
                vm.FileName = $sce.trustAsResourceUrl(globalService.getUrlReportes() + '/Reportes/' + data.GetSp_ReporteQuejaMasivaResult);
            });
        }

        function cancel() {
            $uibModalInstance.close(vm.ObjQM);
        }

        var vm = this;
        vm.Clv_Queja = Clv_Queja;
        vm.cancel = cancel;
        initData();
        
    });