'use strict';
angular
  .module('softvApp')
  .controller('QuejaMasivaViewCtrl', function (QuejaMasivaFactory,DescargarMaterialFactory, atencionFactory, $state, ngNotify, $uibModal, $stateParams, $localStorage) {
    
    function initData(){
      GetQM();
    }

    function GetQM(){
      QuejaMasivaFactory.GetQuejasMasivasList($stateParams.id).then(function(data){
        var QM = data.GetQuejasMasivasListResult[0];
        vm.clv_queja = QM.Clv_Queja;
        var fsolicitud = QM.Fecha_Soliciutud1.split(' ');
        vm.FechaSolicitud = fsolicitud[0];
        vm.HoraSolicitud =  getTime(QM.Fecha_Soliciutud1);
        vm.Estatus = QM.Status;
        vm.Clv_status = QM.Status;
        vm.UsuarioGenero = QM.UsuarioGenero;
        vm.UsuarioEjecuto = QM.UsuarioEjecuto;
        vm.TecnicoAgenda = QM.NombreTecAge;
        vm.TurnoAgenda = QM.TurnoAge;
        vm.FechaAgenda = QM.FechaAge;
        vm.ComentarioAgenda = QM.ComentarioAge;
        vm.DetalleProblema = QM.Problema;
        vm.Observaciones = QM.Observaciones;
        vm.DetalleSolucion = QM.Solucion;
        vm.Clv_problema = QM.ClvProblema;
        vm.Mensaje = QM.MensajeMostrar;
        vm.clv_tecnico = QM.Clave_Tecnico;
        vm.clv_tecnico2 = QM.ClvTecnicoSec;
        vm.ProblemaReal = QM.Solucion;
        vm.Clv_trabajo = QM.Clv_Trabajo;
        vm.Observaciones = QM.Observaciones;

        if (QM.Fecha_Ejecucion1 != null) {
          var fejecucion = QM.Fecha_Ejecucion1.split(' ');
          vm.FechaEjecucion = fejecucion[0];
          var horaEjecucion = getTime(QM.Fecha_Ejecucion1);
          vm.HoraEjecucion = horaEjecucion;
        }

        if (QM.FechaProceso != null) {
          var fproceso = QM.FechaProceso.split(' ');
          vm.FechaProceso = fproceso[0];
          vm.HoraProceso = getTime(QM.FechaProceso);
        }

        if (QM.EjecucuionReal != null) {
          var fEjecucuionReal = QM.EjecucuionReal.split(' ');
          vm.FechaEjecucuionReal = fEjecucuionReal[0];
          vm.HoraEjecucuionReal = getTime(QM.EjecucuionReal);
        }

        atencionFactory.GetClasificacionProblemas().then(function (data) {
          vm.Problemas = data.GetuspConsultaTblClasificacionProblemasListResult;
          for (var a = 0; a < vm.Problemas.length; a++) {
            if (vm.Problemas[a].clvProblema == vm.Clv_problema) {
              vm.Problema = vm.Problemas[a];
            }
          }
        });

        QuejaMasivaFactory.GetQMTecnicos().then(function (data) {

          vm.Tecnicos = data.GetQMTecnicosResult;
          vm.Tecnicos.forEach(function(element, key) {
            if(element.clv_tecnico == vm.clv_tecnico){
              vm.Tecnico = vm.Tecnicos[key];
            }
          });
          vm.Tecnicos2 = data.GetQMTecnicosResult;
          vm.Tecnicos2.forEach(function(element, key) {
            if(element.clv_tecnico == vm.clv_tecnico2){
              vm.Tecnico2 = vm.Tecnicos2[key];
            }
          });

          DescargarMaterialFactory.GetchecaBitacoraTecnico(vm.clv_queja,"M").then(function(data) {
            if (data.GetchecaBitacoraTecnicoResult !== null) {
                vm.idTecnicoBitacora =data.GetchecaBitacoraTecnicoResult.clvTecnico;
                vm.BlockTecnico = true;
                vm.Tecnicos.forEach(function(element, key) {
                  if(element.clv_tecnico === vm.idTecnicoBitacora){
                    vm.Tecnico = vm.Tecnicos[key];
                  }
                });
              }else{
                vm.BtnDescM = true;
              }     
            });
        });
        GetTrabajos(vm.Clv_trabajo);
        GetTipoQueja();
      });
    }

    function GetTrabajos(Clv_trabajo){
			QuejaMasivaFactory.GetMUESTRATRABAJOSQUEJAS_MasivosList().then(function(data){
				vm.Trabajos = data.GetMUESTRATRABAJOSQUEJAS_MasivosListResult;
        if(Clv_trabajo > 0){
          vm.Trabajos.forEach(function(element, key){
            if(element.CLV_TRABAJO == Clv_trabajo){
              vm.Trabajo = vm.Trabajos[key];
            }
          });
        } 
			});
		}

    function OpenConsulta(){
      if(vm.TipoBusqueda.Id == 1){
				var clv_queja = vm.clv_queja;
        var FormEditQM = vm.FormEditQM;
        var modalInstance = $uibModal.open({
          animation: vm.animationsEnabled,
          ariaLabelledBy: 'modal-title',
          ariaDescribedBy: 'modal-body',
          templateUrl: 'views/procesos/ModalBuscaClienteMasivo.html',
          controller: 'ModalConsultaClienteMasivoCtrl',
          controllerAs: 'ctrl',
          backdrop: 'static',
          keyboard: false,
          size: 'lg',
          resolve: {
            clv_queja: function () {
              return clv_queja;
            },
            FormEditQM: function () {
              return FormEditQM;
            }
          }
        });
        modalInstance.result.then(function() {
        });
			}else{
        var clv_queja = vm.clv_queja;
        var FormEditQM = vm.FormEditQM;
        var modalInstance = $uibModal.open({
          animation: vm.animationsEnabled,
          ariaLabelledBy: 'modal-title',
          ariaDescribedBy: 'modal-body',
          templateUrl: 'views/procesos/ModalConsultaClienteMasivoZona.html',
          controller: 'ModalConsultaClienteMasivoZonaCtrl',
          controllerAs: 'ctrl',
          backdrop: 'static',
          keyboard: false,
          size: 'lg',
          resolve: {
            clv_queja: function () {
              return clv_queja;
            },
            FormEditQM: function () {
              return FormEditQM;
            }
          }
        });
        modalInstance.result.then(function() {
        });
			}
    }

    function GetTipoQueja(){
      QuejaMasivaFactory.GetTipoQM(vm.clv_queja).then(function(data){
        var Id = (data.GetTipoQMResult.MSJ == 'C')? 1:2;
        vm.TipoBuscar.forEach(function(element, key){
          if(element.Id == Id){
            vm.TipoBusqueda = vm.TipoBuscar[key];
          }
        });
      });
    }

    function DescargaMaterial() {
      if (vm.Tecnico === null){
        ngNotify.set('Seleccione un técnico para continuar', 'warn');
      } 
      var Tecnico = {};
      Tecnico.CLV_TECNICO = vm.Tecnico.clv_tecnico;
      Tecnico.Nombre = vm.Tecnico.tecnico;
      var options = {};
      options.Detalle = true;
      options.ClvOrden = vm.clv_queja;
      options.ClvBitacora = vm.idBitacora;
      options.SctTecnico = Tecnico;
      options.Tipo_Descargar = "M";

      var modalInstance = $uibModal.open({
        animation: vm.animationsEnabled,
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: 'views/procesos/ModalDescargaMaterialMantenimiento.html',
        controller: 'ModalDescargaMaterialMantenimientoCtrl',
        controllerAs: 'ctrl',
        backdrop: 'static',
        keyboard: false,
        size: 'lg',
        resolve: {
          options: function () {
            return options;
          }
        }
      });
    }

    function toDate(dateStr) {
      var parts = dateStr.split("/");
      return new Date(parts[2], parts[1] - 1, parts[0]);
    }

    function getTime(date) {
      var fejecucion = date.split(' ');
      if (fejecucion.length == 3) {
        return fejecucion[2];
      } else if (fejecucion.length == 4) {
        var hora = fejecucion[3].split(':');
        if (hora[0].length == 1) {
          return '0' + fejecucion[3];
        } else {
          return fejecucion[3];
        }
      }
    }

    var vm = this;
    vm.titulo = "Consultar Reporte de Mantenimiento";
    vm.TipoBuscar = [
			{
				'Id': 1,
				'Desc': 'Contrato'
			},
			{
				'Id': 2,
				'Desc': 'Área'
			}
		];
    vm.FormEditQM = false;
    vm.BtnSave = false;
    vm.Dis1 = true;
    vm.Dis2 = true;
    vm.View1 = true;
    vm.Form1 = false;
    vm.Form2 = true;
    vm.BlockTecnico=true;
    vm.BlockTecnico2=true;
	  vm.BtnSave = false;
    vm.FormEdit = false;
    var DateRep = {};
		vm.BtnDescM = false;
    vm.OpenConsulta = OpenConsulta;
    vm.DescargaMaterial=DescargaMaterial;
    initData();
  });