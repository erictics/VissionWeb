'use strict';
angular
	.module('softvApp')
	.controller('ModalBuscaClienteMasivoZonaCtrl', function (QuejaMasivaFactory, $uibModalInstance, $uibModal, ngNotify) {

		function initData(){
			GetEstadosList();
		}

		function GetEstadosList(){
			QuejaMasivaFactory.GetSoftvWeb_QMEstados().then(function(data){
				var EstadoList = data.GetSoftvWeb_QMEstadosResult;
				EstadoList.forEach(function(element){
					var Obj = {
						'Id': element.Clv_Estado,
						'Nombre': element.Nombre
					};
					vm.List.push(Obj);
				});
			});
		}

		function GetCiudadesList(){
			vm.ListInc.forEach(function(element){
				var Obj = {'Clv_Estado': element.Id};
				vm.ListEstado.push(Obj);
			});
			vm.List = [];
			vm.ListInc = [];
			QuejaMasivaFactory.GetSoftvWeb_QMCiudades(vm.ListEstado).then(function(data){
				var CiudadList = data.GetSoftvWeb_QMCiudadesResult;
				CiudadList.forEach(function(element){
					var Obj = {
						'Id': element.Clv_Ciudad,
						'Nombre': element.Nombre
					};
					vm.List.push(Obj);
				});
				vm.titulo = 'Ciudades';
				vm.Phase = 1;
			});
		}

		function GetLocalidadesList(){
			vm.ListInc.forEach(function(element){
				var Obj = {'Clv_Ciudad': element.Id};
				vm.ListCiudad.push(Obj);
			});
			vm.List = [];
			vm.ListInc = [];
			QuejaMasivaFactory.GetSoftvWeb_QMLocalidades(vm.ListCiudad, vm.ListEstado).then(function(data){
				var LocalidadList = data.GetSoftvWeb_QMLocalidadesResult;
				LocalidadList.forEach(function(element){
					var Obj = {
						'Id': element.Clv_Localidad,
						'Nombre': element.Nombre
					};
					vm.List.push(Obj);
				});
				vm.titulo = 'Localidades';
				vm.Phase = 2;
			});
		}

		function GetColoniaList(){
			vm.ListInc.forEach(function(element){
				var Obj = {'Clv_Localidad': element.Id};
				vm.ListLocalidad.push(Obj);
			});
			vm.List = [];
			vm.ListInc = [];
			QuejaMasivaFactory.GEtSoftvWeb_QMColonias(vm.ListLocalidad, vm.ListCiudad, vm.ListEstado).then(function(data){
				var ColoniaList = data.GEtSoftvWeb_QMColoniasResult;
				ColoniaList.forEach(function(element){
					var Obj = {
						'Id': element.clv_colonia,
						'Nombre': element.Nombre
					};
					vm.List.push(Obj);
				});
				vm.titulo = 'Colonias';
				vm.Phase = 3;
			});
		}

		function GetCalleList(){
			vm.ListInc.forEach(function(element){
				var Obj = {'clv_colonia': element.Id};
				vm.ListColonia.push(Obj);
			});
			vm.List = [];
			vm.ListInc = [];
			QuejaMasivaFactory.GetSoftvWeb_QMCalles(vm.ListColonia, vm.ListLocalidad, vm.ListCiudad, vm.ListEstado).then(function(data){
				var CalleList = data.GetSoftvWeb_QMCallesResult;
				CalleList.forEach(function(element){
					var Obj = {
						'Id': element.Clv_Calle,
						'Nombre': element.NOMBRE
					};
					vm.List.push(Obj);
				});
				vm.titulo = 'Calles';
				vm.Phase = 4;
			});
		}
		
		function Save(){
			vm.ListInc.forEach(function(element){
				var Obj = {'Clv_Calle': element.Id};
				vm.ListCalle.push(Obj);
			});
			vm.ObjArea = {
				'ListEstado': vm.ListEstado,
				'ListCiudad': vm.ListCiudad,
				'ListLocalidad': vm.ListLocalidad,
				'ListColonia': vm.ListColonia,
				'ListCalle': vm.ListCalle
			};
			ngNotify.set('CORRECTO, Se agrego Área al Reporte de Mantenimiento.', 'success');
			Close();
		}

		function AddData(Obj, Op){
			if(Op == 1){
                vm.ListInc.push(Obj);
                vm.List.forEach(function(element, key){
                    if(element.Id == Obj.Id){
                        vm.List.splice(key, 1);
                    }
                });
            }else if(Op == 2){
                vm.List.push(Obj);
                vm.ListInc.forEach(function(element, key){
                    if(element.Id == Obj.Id){
                        vm.ListInc.splice(key, 1);
                    }
                });
            }
		}

		function AddAll(Op){
            if(Op == 1){
                vm.List.forEach(function(element){
                    vm.ListInc.push(element);
                });
                vm.List = [];
            }else if(Op == 2){
                vm.ListInc.forEach(function(element){
                    vm.List.push(element);
                });
                vm.ListInc = [];
            }
        }

		function Next(){
			switch (vm.Phase) {
				case 0:
					GetCiudadesList();
				break;
				case 1:
					GetLocalidadesList();
				break;
				case 2:
					GetColoniaList();
				break;
				case 3:
					GetCalleList();
				break;
				case 4:
					Save();
				break;
			}
		}

		function Close() {
			$uibModalInstance.close(vm.ObjArea);
		}
		
		var vm = this;
		vm.titulo = 'Estados';
		vm.Phase = 0;
		vm.List = [];
		vm.ListInc = [];
		vm.ListEstado = [];
		vm.ListCiudad = [];
		vm.ListLocalidad = [];
		vm.ListColonia = [];
		vm.ListCalle = [];
		vm.ObjArea = null;
		vm.AddData = AddData;
		vm.AddAll = AddAll;
		vm.Next = Next;
		vm.Close= Close;
		initData();

	});