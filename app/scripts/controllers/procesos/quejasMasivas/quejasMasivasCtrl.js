'use strict';
angular
	.module('softvApp')
	.controller('quejasMasivasCtrl', function (QuejaMasivaFactory, $state, ngNotify, $uibModal) {
		
		function initData(){
			GetQMList(0);
			GetEstadoList();
		}

		function EnterReporte(event) {
			if (event.keyCode == 13) {
				GetQMList(2);
			}
		  }

		function GetQMList(Op){
			var ObjQMList = {
				'Op': Op,
				'Clv_Queja': (vm.Reporte != null && vm.Reporte != undefined && vm.Reporte != '')? vm.Reporte:null,
				'Status': (vm.Status != null && vm.Status != undefined && vm.Status != '')? vm.Status:null,
				'Edo': (vm.Estado != null && vm.Estado != undefined && vm.Estado != '')? vm.Estado.Clv_Estado:null,
				'Cd': (vm.Cuidad != null && vm.Cuidad != undefined && vm.Cuidad != '')? vm.Cuidad.Clv_Ciudad:null,
				'Loc': (vm.Localidad != null && vm.Localidad != undefined && vm.Localidad != '')? vm.Localidad.Clv_Localidad:null,
				'Col': (vm.Colonia != null && vm.Colonia != undefined && vm.Colonia != '')? vm.Colonia.clv_colonia:null,
				'Calle': (vm.Calle != null && vm.Calle != undefined && vm.Calle != '')? vm.Calle.Clv_Calle:null
			};
			QuejaMasivaFactory.GetBusquedaQuejasMasivasList(ObjQMList).then(function(data){
				console.log(data);
				vm.QMList = data.GetBusquedaQuejasMasivasListResult;
				vm.ViewList = (vm.QMList.length > 0)? true:false ;
				limpiar();
			});
		}

		function limpiar(){
			vm.Reporte = "";
		}

		function GetEstadoList(){
			QuejaMasivaFactory.GetSoftvWeb_QMEstados().then(function(data){
				vm.EstadoList = data.GetSoftvWeb_QMEstadosResult;
			});
		}

		function GetCiudadList(){
			vm.CiudadList = null;
			vm.LocalidadList = null;
			vm.ColoniaList = null;
			vm.CalleList = null;
			if(vm.Estado != null && vm.Estado != undefined && vm.Estado != ''){
				vm.ListEstado = [];
				var Obj = {'Clv_Estado': vm.Estado.Clv_Estado};
				vm.ListEstado.push(Obj);
				QuejaMasivaFactory.GetSoftvWeb_QMCiudades(vm.ListEstado).then(function(data){
					vm.CiudadList = data.GetSoftvWeb_QMCiudadesResult;
					GetQMList(3);
				});
			}
		}

		function GetLocalidadList(){
			vm.LocalidadList = null;
			vm.ColoniaList = null;
			vm.CalleList = null;
			if(vm.Cuidad != null && vm.Cuidad != undefined && vm.Cuidad != ''){
				vm.ListCiudad = [];
				var Obj = {'Clv_Ciudad': vm.Cuidad.Clv_Ciudad};
				vm.ListCiudad.push(Obj);
				QuejaMasivaFactory.GetSoftvWeb_QMLocalidades(vm.ListCiudad, vm.ListEstado).then(function(data){
					vm.LocalidadList = data.GetSoftvWeb_QMLocalidadesResult;
					GetQMList(8);
				});
			}
		}

		function GetColoniaList(){
			vm.ColoniaList = null;
			vm.CalleList = null;
			if(vm.Localidad != null && vm.Localidad != undefined && vm.Localidad != ''){
				vm.ListLocalidad = [];
				var Obj = {'Clv_Localidad': vm.Localidad.Clv_Localidad};
				vm.ListLocalidad.push(Obj);
				QuejaMasivaFactory.GEtSoftvWeb_QMColonias(vm.ListLocalidad, vm.ListCiudad, vm.ListEstado).then(function(data){
					vm.ColoniaList = data.GEtSoftvWeb_QMColoniasResult;
					GetQMList(9);
				});
			}
		}

		function GetCalleList(){
			vm.CalleList = null;
			if(vm.Colonia != null && vm.Colonia != undefined && vm.Colonia != ''){
				vm.ListColonia = [];
				var Obj = {'clv_colonia': vm.Colonia.clv_colonia};
				vm.ListColonia.push(Obj);
				QuejaMasivaFactory.GetSoftvWeb_QMCalles(vm.ListColonia, vm.ListLocalidad, vm.ListCiudad, vm.ListEstado).then(function(data){
					vm.CalleList = data.GetSoftvWeb_QMCallesResult;
					GetQMList(10);
				});
			}
		}

		var vm = this;
		vm.GetCiudadList = GetCiudadList;
		vm.GetLocalidadList = GetLocalidadList;
		vm.GetColoniaList = GetColoniaList;
		vm.GetCalleList = GetCalleList;
		vm.GetQMList = GetQMList;
		vm.EnterReporte = EnterReporte;
		vm.limpiar = limpiar;
		initData();

	});