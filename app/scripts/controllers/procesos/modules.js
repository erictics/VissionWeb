'use strict';
angular
  .module('softvApp')
  .config(function ($stateProvider) {
    var states = [{
        name: 'home.procesos',
        abstract: true,
        template: '<div ui-view></div>'
      },
      {
        name: 'home.procesos.atencion',
        data: {
          pageTitle: 'SOFTV | ATENCIÓN TELEFÓNICA',
          permissions: {
            only: ['atenciontelefonicaSelect'],
            options: {
              reload: false
            }
          }
        },
        url: '/atencion',
        templateUrl: 'views/procesos/atencion.html',
        controller: 'AtencionCtrl',
        controllerAs: '$ctrl'
      },
      {
        name: 'home.procesos.atencionNueva',
        data: {
          pageTitle: 'SOFTV | NUEVA ATENCIÓN TELEFÓNICA',
        },
        url: '/atencion/nueva',
        templateUrl: 'views/procesos/atencionNueva.html',
        controller: 'AtencionNuevaCtrl',
        controllerAs: '$ctrl'
      },
      {
        name: 'home.procesos.atencionEditar',
        data: {
          pageTitle: 'SOFTV | EDITAR ATENCIÓN TELEFÓNICA',
        },
        url: '/atencion/editar/:id',
        templateUrl: 'views/procesos/atencionNueva.html',
        controller: 'AtencionEditarCtrl',
        controllerAs: '$ctrl'
      },
      {
        name: 'home.procesos.atencionDetalle',
        data: {
          pageTitle: 'SOFTV | DETALLE ATENCIÓN TELEFÓNICA',
        },
        url: '/atencion/detalle/:id',
        templateUrl: 'views/procesos/atencionNueva.html',
        controller: 'AtencionDetalleCtrl',
        controllerAs: '$ctrl'
      },
      {
        name: 'home.procesos.ordenes',
        data: {
          pageTitle: 'SOFTV | ORDENES DE SERVICIO',
          permissions: {
            only: ['ordenesservicioSelect'],
            options: {
              reload: false
            }
          }
        },
        url: '/ordenes',
        templateUrl: 'views/procesos/ordenesServicio.html',
        controller: 'OrdenesServicioCtrl',
        controllerAs: '$ctrl'
      },
      {
        name: 'home.procesos.ordenNueva',
        data: {
          pageTitle: 'SOFTV | NUEVA ORDEN DE SERVICIO'
        },
        url: '/ordenes/nueva',
        templateUrl: 'views/procesos/nuevaOrdenServicio.html',
        controller: 'OrdenNuevaCtrl',
        controllerAs: '$ctrl'
      },
      {
        name: 'home.procesos.ordendetalle',
        data: {
          pageTitle: 'SOFTV | NUEVA ORDEN DE SERVICIO'
        },
        url: '/ordenes/detalle/:id/:op',
        templateUrl: 'views/procesos/detalleOrdenServicio.html',
        controller: 'OrdenDetalleCtrl',
        controllerAs: '$ctrl'
      },
      {
        name: 'home.procesos.ordenEjecutada',
        data: {
          pageTitle: 'SOFTV | ORDEN DE SERVICIO EJECUTADA'
        },
        url: '/ordenes/ejecutada?id',
        templateUrl: 'views/procesos/nuevaOrdenServicio.html',
        controller: 'ordenEjecutadaCtrl',
        controllerAs: '$ctrl'
      },
      {
        name: 'home.procesos.buscarCliente',
        data: {
          pageTitle: 'SOFTV | BUSCAR CLIENTE'
        },
        url: '/buscarCliente',
        templateUrl: 'views/procesos/buscarNuevo.html',
        controller: 'BuscarNuevoCtrl',
        controllerAs: '$ctrl'
      },
      {
        name: 'home.procesos.reportes',
        data: {
          pageTitle: 'SOFTV | REPORTES',
          permissions: {
            only: ['reportesdefallasSelect'],
            options: {
              reload: false
            }
          }
        },
        url: '/quejas',
        templateUrl: 'views/procesos/Quejas.html',
        controller: 'quejasCtrl',
        controllerAs: '$ctrl'
      },

      {
        name: 'home.procesos.reportesmasivos',
        data: {
          pageTitle: 'SOFTV | REPORTES DE MANTENIMIENTO',
          permissions: {
            only: ['reportesdemantenimientoSelect'],
            options: {
              reload: false
            }
          }
        },
        url: '/quejasMasivas',
        templateUrl: 'views/procesos/QuejasMasivas.html',
        controller: 'quejasMasivasCtrl',
        controllerAs: '$ctrl'
      },
      {
        name: 'home.procesos.reportesmasivosnuevo',
        data: {
          pageTitle: 'SOFTV | REPORTES MASIVOS NUEVO',
        },
        url: '/quejasMasivas/nueva',
        templateUrl: 'views/procesos/QuejaMasivaForm.html',
        controller: 'AddQuejaMasivaCtrl',
        controllerAs: '$ctrl'
      },
      {
        name: 'home.procesos.ejecutaquejamasiva',
        data: {
          pageTitle: 'SOFTV | EJECUTA REPORTE MASIVO'
        },
        url: '/quejasMasivas/ejecuta/:id',
        templateUrl: 'views/procesos/QuejaMasivaForm.html',
        controller: 'QuejaMasivaEjecutaCtrl',
        controllerAs: '$ctrl'
      },
      {
        name: 'home.procesos.quejamasivaconsulta',
        data: {
          pageTitle: 'SOFTV | EJECUTA REPORTE MASIVO'
        },
        url: '/quejasMasivas/consulta/:id',
        templateUrl: 'views/procesos/QuejaMasivaForm.html',
        controller: 'QuejaMasivaViewCtrl',
        controllerAs: '$ctrl'
      },
      {
        name: 'home.procesos.ejecutaqueja',
        data: {
          pageTitle: 'SOFTV | EJECUTA REPORTE'
        },
        url: '/quejas/ejecuta/:id/:contrato/:servicio',
        templateUrl: 'views/procesos/QuejaEjecuta.html',
        controller: 'QuejaEjecutaCtrl',
        controllerAs: '$ctrl'
      },
      {
        name: 'home.procesos.detallequeja',
        data: {
          pageTitle: 'SOFTV | DETALLE REPORTE'
        },
        url: '/quejas/detalle/:id/:contrato/:servicio/:op',
        templateUrl: 'views/procesos/QuejaEjecuta.html',
        controller: 'QuejaDetalleCtrl',
        controllerAs: '$ctrl'
      },
      {
        name: 'home.procesos.devolucion',
        data: {
          pageTitle: 'SOFTV | DETALLE REPORTE',
          permissions: {
            only: ['devoluciondeaparatosSelect'],
            options: {
              reload: false
            }
          }
        },
        url: '/devolucionAlmacen',
        templateUrl: 'views/procesos/DevolucionAlmacen.html',
        controller: 'DevolucionAlmacenCtrl',
        controllerAs: '$ctrl'
      },
      {
        name: 'home.procesos.agenda',
        data: {
          pageTitle: 'SOFTV | AGENDA',
          permissions: {
            only: ['agendaSelect'],
            options: {
              reload: false
            }
          }
        },
        url: '/agenda',
        templateUrl: 'views/procesos/agenda.html',
        controller: 'agendaCtrl',
        controllerAs: '$ctrl'
      },
      {
        name: 'home.procesos.agendaDetalle',
        data: {
          pageTitle: 'SOFTV | DETALLE AGENDA',
        },
        url: '/agenda/detalle/:id/:cliente',
        templateUrl: 'views/procesos/agendaUpdate.html',
        controller: 'agendaDetalleCtrl',
        controllerAs: '$ctrl'
      },
      {
        name: 'home.procesos.agendaEditar',
        data: {
          pageTitle: 'SOFTV | EDITAR AGENDA',
        },
        url: '/agenda/editar/:id/:cliente',
        templateUrl: 'views/procesos/agendaUpdate.html',
        controller: 'agendaUpdateCtrl',
        controllerAs: '$ctrl'
      },
      {
        name: 'home.procesos.reset',
        data: {
          pageTitle: 'SOFTV | RESET DE APARATOS',
          permissions: {
            only: ['resetdeaparatosSelect'],
            options: {
                reload: false
            }
         }
        },
        url: '/reset',
        templateUrl: 'views/procesos/resetAparatos.html',
        controller: 'resetAparatosCtrl',
        controllerAs: '$ctrl'
      },
      {
        name: 'home.procesos.pruebainternet',
        data: {
          pageTitle: 'SOFTV | PRUEBA DE INTERNET',
          permissions: {
            only: ['pruebadeinternetSelect'],
            options: {
                reload: false
            }
         }
        },
        url: '/pruebainternet',
        templateUrl: 'views/procesos/pruebaInternet.html',
        controller: 'pruebaInternetCtrl',
        controllerAs: '$ctrl'
      },
      {
        name: 'home.procesos.recontratacion',
        data: {
          pageTitle: 'SOFTV | RECONTRATACIÓN',
          permissions: {
            only: ['recontratacionSelect'],
            options: {
              reload: false
            }
          }
        },
        url: '/recontratacion',
        templateUrl: 'views/procesos/Recontratacion.html',
        controller: 'RecontratacionCtrl',
        controllerAs: '$ctrl'
      
    },     
      {
        name: 'home.procesos.cambioservicio',
        data: {
          pageTitle: 'SOFTV | CAMBIO DE SERVICIO',
          permissions: {
            only: ['cambiodeservicioSelect'],
            options: {
              reload: false
            }
          }
        },
        url: '/cambioservicio',
        templateUrl: 'views/procesos/cambioServicio.html',
        controller: 'cambioServicioCtrl',
        controllerAs: '$ctrl'
      },
      {
        name: 'home.procesos.cambioservicionuevo',
        data: {
          pageTitle: 'SOFTV | NUEVO CAMBIO DE SERVICIO',
        },
        url: '/cambioservicio/nuevo',
        templateUrl: 'views/procesos/cambioServicioNuevo.html',
        controller: 'cambioServicioNuevoCtrl',
        controllerAs: '$ctrl'
      },
      {
        name: 'home.procesos.fueraarea',
        data: {
          pageTitle: 'SOFTV | FUERA DE AREA',
          permissions: {
            only: ['fueradeareaSelect'],
            options: {
              reload: false
            }
          }
        },
        url: '/fueraarea',
        templateUrl: 'views/procesos/fueraArea.html',
        controller: 'fueraAreaCtrl',
        controllerAs: '$ctrl'
      },

    ];
    states.forEach(function (state) {
      $stateProvider.state(state);
    });
  });
