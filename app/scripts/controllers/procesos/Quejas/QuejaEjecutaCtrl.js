'use strict';
angular
  .module('softvApp')
  .controller('QuejaEjecutaCtrl', function ($state, ngNotify, DescargarMaterialFactory, QuejaMasivaFactory, $location, $uibModal, ordenesFactory, $stateParams, atencionFactory, quejasFactory, $localStorage, $filter) {

    function InitalData() {
      vm.clv_queja = $stateParams.id;
      vm.contrato = $stateParams.contrato;
      vm.Servicio = $stateParams.servicio;
      quejasFactory.ValidaQueja(vm.clv_queja).then(function (data) {
        if (data.GetDeepValidaQuejaCompaniaAdicResult.Valida === 0) {
          var param = {};
          param.contrato = vm.contrato;
          param.servicio = vm.Servicio;
          param.op = 0;

          atencionFactory.buscarCliente(param).then(function (data) {
            var detalle = data.GetuspBuscaContratoSeparado2ListResult[0];
            var contrato = detalle.ContratoBueno;
            vm.GlobalContrato = contrato;
            vm.NombreCliente = detalle.Nombre + detalle.Apellido_Paterno + " " + detalle.Apellido_Materno;
            vm.Calle = detalle.CALLE;
            vm.Numero = detalle.NUMERO;
            vm.Colonia = detalle.COLONIA;
            vm.Ciudad = detalle.CIUDAD;
            vm.Telefono = 123456;

            atencionFactory.getServiciosCliente(contrato).then(function (data) {
              vm.ServiciosCliente = data.GetDameSerDelCliFacListResult;

              quejasFactory.ConsultaQueja($stateParams.id).then(function (data) {
                console.log(data);
                var detqueja = data.GetQuejasListResult[0];
                vm.UsuarioGenero = detqueja.UsuarioGenero;
                vm.UsuarioEjecuto = detqueja.UsuarioEjecuto;
                vm.TecnicoAgenda = detqueja.NombreTecAge;
                vm.TurnoAgenda = detqueja.TurnoAge;
                vm.FechaAgenda = detqueja.FechaAge;
                vm.ComentarioAgenda = detqueja.ComentarioAge;
                vm.DetalleProblema = detqueja.Problema;
                vm.Observaciones = detqueja.Observaciones;
                vm.DetalleSolucion = detqueja.Solucion;
                var fsolicitud = detqueja.Fecha_Soliciutud.split(' ');
                vm.FechaSolicitud = fsolicitud[0];
               // var hora = getTime(detqueja.Fecha_Soliciutud);
               /*console.log(fsolicitud[0]);
               var hora = .split(':');
               console.log(hora);*/
                vm.HoraSolicitud = timeStr(fsolicitud[1]); 

                if (detqueja.Fecha_Ejecucion != null) {
                  var fejecucion = detqueja.Fecha_Ejecucion.split(' ');
                  vm.FechaEjecucion = fejecucion[0];
                  //var horaEjecucion = getTime(detqueja.Fecha_Ejecucion);
                  //vm.HoraEjecucion = horaEjecucion;
                  vm.HoraEjecucion = timeStr(fejecucion[1]);
                }

                if (detqueja.FechaProceso != null) {
                  var fproceso = detqueja.FechaProceso.split(' ');
                  vm.FechaProceso = fproceso[0];
                  //vm.HoraProceso = getTime(detqueja.FechaProceso);
                  vm.HoraProceso = timeStr(fproceso[1]);
                }

                if (detqueja.Visita1 != null) {
                  var fvisita1 = detqueja.Visita1.split(' ');
                  vm.Fechavisita1 = fvisita1[0];
                  //vm.Horavisita1 = getTime(detqueja.Visita1);
                  vm.Horavisita1 = timeStr(fvisita1[1]);
                  vm.FVisita3 = true;
                  vm.FVisita2 = true;
                }

                if (detqueja.Visita2 != null) {
                  var fvisita2 = detqueja.Visita2.split(' ');
                  vm.Fechavisita2 = fvisita2[0];
                  //vm.Horavisita2 = getTime(detqueja.Visita2);
                  vm.Horavisita2 = timeStr(fvisita2[1]);
                  vm.FVisita3 = true;
                  vm.FVisita1 = true;
                }

                if (detqueja.Visita3 != null) {
                  var fvisita3 = detqueja.Visita3.split(' ');
                  vm.Fechavisita3 = fvisita3[0];
                  //vm.Horavisita3 = getTime(detqueja.Visita3);
                  vm.Horavisita3 = timeStr(fvisita3[1]);
                  vm.FVisita1 = true;
                  vm.FVisita2 = true;
                }

                if (detqueja.EjecucuionReal != null) {
                  var fEjecucuionReal = detqueja.EjecucuionReal.split(' ');
                  vm.FechaEjecucuionReal = fEjecucuionReal[0];
                  //vm.HoraEjecucuionReal = getTime(detqueja.EjecucuionReal);
                  vm.HoraEjecucuionReal = timeStr(fEjecucuionReal[3]);
                  console.log(vm.HoraEjecucuionReal);
                }

                DateRep.FEjec = vm.FechaEjecucion;
                DateRep.HEjec = vm.HoraEjecucion ;
                DateRep.FVis1 = vm.Fechavisita1;
                DateRep.HVis1 = vm.Horavisita1;
                DateRep.FVis2 = vm.Fechavisita2;
                DateRep.HVis2 = vm.Horavisita2;
                DateRep.FVis3 = vm.Fechavisita3;
                DateRep.HVis3 = vm.Horavisita3;
                DateRep.FPro = vm.FechaProceso;
                DateRep.HPro = vm.HoraProceso;
                vm.Departamento = detqueja.Clasificacion;
                vm.Clv_trabajo = detqueja.Clv_Trabajo;
                vm.Clv_prioridad = detqueja.clvPrioridadQueja;
                vm.Clv_problema = detqueja.clvProblema;
                vm.ProblemaReal = detqueja.Solucion;
                vm.Visita = detqueja.Visita;
                vm.Clv_status = detqueja.Status;
                vm.Estatus = 'E';
                Bloqueo(true);
                var Obj = {
                  'Clv': vm.clv_queja,
                  'Op': 1
                };
                /*ordenesFactory.GetConTecnicoAgenda(Obj).then(function (data) {
                  var Ta = data.GetConTecnicoAgendaResult.clv_tecnico;*/
                  /*vm.idTecnicoBitacora = data.GetDameCitaOrdenQuejaResult.nombre;*/
                DescargarMaterialFactory.GetchecaBitacoraTecnico(vm.clv_queja, 'Q').then(function (data) {
                  if (data.GetchecaBitacoraTecnicoResult != null) {
                    vm.idBitacora = data.GetchecaBitacoraTecnicoResult.idBitacora;
                    vm.idTecnicoBitacora = data.GetchecaBitacoraTecnicoResult.clvTecnico;
                  }else{
                    ordenesFactory.GetConTecnicoAgenda(Obj).then(function (data) {
                      vm.idTecnicoBitacora = data.GetConTecnicoAgendaResult.clv_tecnico;
                    });
                  }

                  quejasFactory.ObtenTecnicos(vm.GlobalContrato).then(function (data) {
                    vm.Tecnicos = data.GetMuestra_Tecnicos_AlmacenListResult;
                    if (detqueja.Clave_Tecnico != null && vm.idTecnicoBitacora > 0) {
                      for (var a = 0; a < vm.Tecnicos.length; a++) {
                        if (vm.Tecnicos[a].clv_Tecnico == vm.idTecnicoBitacora) {
                          vm.Tecnico = vm.Tecnicos[a];
                          vm.BlockTecnico = true;
                        }
                      }
                    }
                  });
                });

                atencionFactory.MuestraTrabajos(vm.Servicio).then(function (data) {
                  vm.Trabajos = data.GetMUESTRATRABAJOSQUEJASListResult;
                  for (var a = 0; a < vm.Trabajos.length; a++) {
                    if (vm.Trabajos[a].CLV_TRABAJO == vm.Clv_trabajo) {
                      vm.Trabajo = vm.Trabajos[a];
                    }
                  }
                });

                quejasFactory.ObtenPrioridad().then(function (data) {
                  vm.Prioridades = data.GetSoftv_GetPrioridadQuejaListResult;
                  for (var a = 0; a < vm.Prioridades.length; a++) {
                    if (vm.Prioridades[a].clvPrioridadQueja == vm.Clv_prioridad) {
                      vm.Prioridad = vm.Prioridades[a];
                    }
                  }
                });

                atencionFactory.getServicios().then(function (data) {
                  vm.Servicios = data.GetMuestraTipSerPrincipalListResult;
                  for (var a = 0; a < vm.Servicios.length; a++) {
                    if (vm.Servicios[a].Clv_TipSerPrincipal == vm.Servicio) {
                      vm.Servicio = vm.Servicios[a];
                    }
                  }
                });

                ordenesFactory.serviciosCliente(vm.GlobalContrato).then(function (data) {
                  vm.servicioscli = data.GetDameSerDelCliFacListResult;
                });

                atencionFactory.GetClasificacionProblemas().then(function (data) {
                  vm.Problemas = data.GetuspConsultaTblClasificacionProblemasListResult;
                  for (var a = 0; a < vm.Problemas.length; a++) {
                    if (vm.Problemas[a].clvProblema == vm.Clv_problema) {
                      vm.Problema = vm.Problemas[a];
                    }
                  }
                });
              });
            });
          });
        } else {
          $state.go('home.procesos.reportes');
          ngNotify.set('El Reporte de falla pertenece a un contrato de plazas adicionales al usuario, no puede ejecutarla', 'error');
        }
      });
    }

    function timeStr(time){
      var hora = time.split(':');
      return hora[0] + ':' + hora[1];
    }

    function dateParse(date) {
      var realdate = date.split(" ")
      var strDate = realdate[0];
      var dateParts = strDate.split("/");
      var date = new Date(dateParts[2], (dateParts[1] - 1), dateParts[0]);
      if (dateParts[0].length == 1) {
        var dia = '0' + ateParts[0];
      }
    }

    function getTime(date) {
      var fejecucion = date.split(' ');
      if (fejecucion.length == 3) {
        return fejecucion[2];
      } else if (fejecucion.length == 4) {
        var hora = fejecucion[3].split(':');
        if (hora[0].length == 1) {
          return '0' + fejecucion[3];
        } else {
          return fejecucion[3];
        }
      }
    }

    function abrirBonificacion() {
      var detalle = {};
      detalle.Block = true;
      detalle.Queja = vm.clv_queja;
      var modalInstance = $uibModal.open({
        animation: vm.animationsEnabled,
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: 'views/procesos/ModalBonificacion.html',
        controller: 'ModalBonificacionCtrl',
        controllerAs: 'ctrl',
        backdrop: 'static',
        keyboard: false,
        size: 'md',
        resolve: {
          detalle: function () {
            return detalle;
          }
        }
      });
    }

    function toDate(dateStr) {
      var parts = dateStr.split("/");
      return new Date(parts[2], parts[1] - 1, parts[0]);
    }

    function ValidaFecha(fechaIngresada, fechasolicitud) {
      var _fechaHoy = new Date();
      var _fechaIngresada = toDate(fechaIngresada);
      var _fechasolicitud = toDate(fechasolicitud);
      if ((_fechaIngresada > _fechasolicitud && _fechaIngresada < _fechaHoy) || _fechasolicitud.toDateString() === _fechaIngresada.toDateString()) {
        return true;
      } else {
        return false;
      }
    }

    function Bloqueo(aplicabloqueo) {
      if (vm.Estatus == 'E') {
        vm.FEjecucion = false;
        vm.blockHoraInicio =false;
        vm.blockHoraFin =false;
        vm.FVisita1 = true;
        vm.FVisita2 = true;
        vm.FVisita3 = true;
        vm.FProceso = true;
        vm.Iejecucion = 'input-yellow';
        vm.Ivisita = 'input-normal';
        vm.Iproceso = 'input-normal';
        vm.Ivisita1 = 'input-normal';
        vm.Ivisita2 = 'input-normal';
        vm.Ivisita3 = 'input-normal';
      } else if (vm.Estatus == 'V') {
        vm.HoraInicio = "";
        vm.HoraFin = "";
        vm.blockHoraInicio =true;
        vm.blockHoraFin =true;
        vm.FEjecucion = true;
        vm.FProceso = true;

        if (vm.Fechavisita1 == null) {
          vm.FVisita1 = false;
          vm.FVisita2 = true;
          vm.FVisita3 = true;

          vm.Ivisita1 = 'input-yellow';
          vm.Ivisita2 = 'input-normal';
          vm.Ivisita3 = 'input-normal';
          vm.Iejecucion = 'input-normal';
        }

        if (vm.Fechavisita2 == null && vm.Fechavisita1 != null) {
          vm.FVisita2 = false;
          vm.FVisita1 = true;
          vm.FVisita3 = true;

          vm.Ivisita2 = 'input-yellow';
          vm.Ivisita1 = 'input-normal';
          vm.Ivisita3 = 'input-normal';
          vm.Iejecucion = 'input-normal';
        }

        if (vm.Fechavisita3 == null && vm.Fechavisita2 != null) {
          vm.FVisita3 = false;
          vm.FVisita1 = true;
          vm.FVisita2 = true;

          vm.Ivisita2 = 'input-normal';
          vm.Ivisita1 = 'input-normal';
          vm.Ivisita3 = 'input-yellow';
          vm.Iejecucion = 'input-normal';
        }

      } else if (vm.Estatus == 'S') {
        vm.HoraInicio = "";
        vm.HoraFin = "";
        vm.blockHoraInicio =true;
        vm.blockHoraFin =true;
        if (vm.FechaProceso != null) {
          vm.FProceso = true;
          vm.Iproceso = 'input-normal';
        } else {
          vm.FProceso = false;
          vm.Iproceso = 'input-yellow';
        }
        vm.FEjecucion = true
        vm.FVisita1 = true;
        vm.FVisita2 = true;
        vm.FVisita3 = true;

        vm.Iejecucion = 'input-normal';
        vm.Ivisita1 = 'input-normal';
        vm.Ivisita2 = 'input-normal';
        vm.Ivisita3 = 'input-normal';
      }
    }

    function CambiaEstatus() {
      console.log('CambiaEstatus-DateRep: ', DateRep);
      vm.FechaEjecucion = DateRep.FEjec;
      vm.HoraEjecucion = DateRep.HEjec;
      vm.Fechavisita1 = DateRep.FVis1;
      vm.Horavisita1 = DateRep.HVis1;
      vm.Fechavisita2 = DateRep.FVis2;
      vm.Horavisita2 = DateRep.HVis2;
      vm.Fechavisita3 = DateRep.FVis3;
      vm.Horavisita3 = DateRep.HVis3;
      vm.FechaProceso = DateRep.FPro;
      vm.HoraProceso = DateRep.HPro;
      Bloqueo();
    }

    function Ejecutaqueja() {
      var modalInstance = $uibModal.open({
        animation: vm.animationsEnabled,
        ariaLabelledBy: "modal-title",
        ariaDescribedBy: "modal-body",
        templateUrl: "views/procesos/ModalPreguntaTecnico.html",
        controller: "ModalPreguntaTecnicoCtrl",
        controllerAs: "$ctrl",
        backdrop: "static",
        keyboard: false,
        size: "sm",
        resolve: {}
      });
      modalInstance.result.then(function(submitVar) {
        if(vm.TecnicoCuadrilla){
          if(vm.TecnicoCuadrilla.clv_Tecnico === vm.Tecnico.clv_Tecnico){
            ngNotify.set('No puedes seleccionar el mismo técnico, elige otro para continuar', 'warn');
            return;
          }
        }
        
        quejasFactory.ValidaQueja(vm.clv_queja).then(function (data) {
          if (data.GetDeepValidaQuejaCompaniaAdicResult.Valida == 0) {
            quejasFactory.BuscaBloqueado(vm.GlobalContrato).then(function (bloqueado) {
              if (bloqueado.GetDeepBuscaBloqueadoResult.Bloqueado == 0) {

                if (vm.Estatus == 'E') {
                  if (vm.FechaEjecucion == undefined) {
                    ngNotify.set('Seleccione la fecha de ejecución', 'warn');
                    return;
                  } else {
                    if (ValidaFecha(vm.FechaEjecucion, vm.FechaSolicitud) == false) {
                      ngNotify.set('La fecha de ejecución no puede ser menor a la fecha de solicitud ni mayor a la fecha actual', 'warn');
                      return;
                    } 
                    else if (vm.FechaSolicitud == vm.FechaEjecucion){
                            if(vm.HoraEjecucion <= vm.HoraSolicitud){
                              ngNotify.set('La hora de ejecución no puede ser menor o igual a la hora de solicitud', 'warn');
                              return;
                            }
                    }
                  }
                  
                  if (vm.Fechavisita1 != undefined &&
                    vm.Fechavisita2 == undefined &&
                    vm.Fechavisita3 == undefined) {
                    if (ValidaFecha(vm.FechaEjecucion, vm.Fechavisita1) == false) {
                      ngNotify.set('La fecha de ejecucion no puede ser menor a la fecha de visita 1', 'warn');
                      return;
                    }
                    if (vm.Fechavisita1 == vm.FechaEjecucion){
                      if(vm.HoraEjecucion <= vm.Horavisita1){
                        ngNotify.set('La hora de ejecución no puede ser menor o igual a la hora de visita 1', 'warn');
                        return;
                      }
                    }
                   }
                  
                  if (vm.Fechavisita1 != undefined &&
                    vm.Fechavisita2 != undefined &&
                    vm.Fechavisita3 == undefined) {
                      if (ValidaFecha(vm.FechaEjecucion, vm.Fechavisita2) == false) {
                        ngNotify.set('La fecha de ejecucion no puede ser menor a la fecha de visita 2', 'warn');
                        return;
                      }
                      if (vm.Fechavisita2 == vm.FechaEjecucion){
                        if(vm.HoraEjecucion <= vm.Horavisita2){
                          ngNotify.set('La hora de ejecución no puede ser menor o igual a la hora de visita 2', 'warn');
                          return;
                        }
                      }
                   }

                  if (vm.HoraFin <= vm.HoraInicio){
                    ngNotify.set('La hora fin no puede ser menor o igual a la hora inicio',  'warn');
                    return;
                  }

                 }
                else if (vm.Estatus == 'V') {

                  if (vm.Fechavisita1 != undefined &&
                    vm.Fechavisita2 == undefined &&
                    vm.Fechavisita3 == undefined) {
                    if (ValidaFecha(vm.Fechavisita1, vm.FechaSolicitud) == false) {
                      ngNotify.set('La fecha de visita no puede ser menor a la fecha de solicitud ni mayor a la fecha actual', 'warn');
                      return;
                    }
                    else if (vm.FechaSolicitud == vm.Fechavisita1){
                      if(vm.Horavisita1 <= vm.HoraSolicitud){
                        ngNotify.set('La hora de visita 1 no puede ser menor o igual a la hora de solicitud', 'warn');
                        return;
                      }
                    }
                  }

                  else if (vm.Fechavisita1 != undefined &&
                    vm.Fechavisita2 != undefined &&
                    vm.Fechavisita3 == undefined) {
                    if (ValidaFecha(vm.Fechavisita2, vm.Fechavisita1) == false) {
                      ngNotify.set('La fecha de visita 2 no puede ser menor a la fecha de visita 1', 'warn');
                      return;
                    }
                    else if (vm.Fechavisita1 == vm.Fechavisita2){
                      if(vm.Horavisita2 <= vm.Horavisita1){
                        ngNotify.set('La hora de visita 2 no puede ser menor o igual a la hora de visita 1', 'warn');
                        return;
                      }
                    }
                  }

                  else if (vm.Fechavisita1 != undefined &&
                    vm.Fechavisita2 != undefined &&
                    vm.Fechavisita3 != undefined) {
                    if (ValidaFecha(vm.Fechavisita3, vm.Fechavisita2) == false) {
                      ngNotify.set('La fecha de visita 3 no puede ser menor a la fecha de visita 2', 'warn');
                      return;
                    }
                    else if (vm.Fechavisita2 == vm.Fechavisita3){
                      if(vm.Horavisita3 <= vm.Horavisita2){
                        ngNotify.set('La hora de visita 3 no puede ser menor o igual a la hora de visita 2', 'warn');
                        return;
                      }
                    }
                  }

                 }

                else if (vm.Estatus == 'S') {
                  if (ValidaFecha(vm.FechaProceso, vm.FechaSolicitud) == false) {
                    ngNotify.set('La fecha de proceso no puede ser menor a la fecha de solicitud ni mayor a la fecha actual', 'warn');
                    return;
                  }
                }
  
                if (vm.Estatus == 'E') {
                  var F = vm.FechaEjecucion.split('/');
                  console.log(F);
                  var FechaEjecucion = F[2] + '/' + F[1] + '/' + F[0] + ' ' + vm.HoraEjecucion;
                  console.log(FechaEjecucion);
                  var obj = {};
                  obj.Clv_Queja = vm.clv_queja;
                  obj.Status = 'E';
                  obj.Fecha_Ejecucion = FechaEjecucion;
                  obj.Visita1 = '';
                  obj.Visita2 = '';
                  obj.Visita3 = '';
                  obj.HV1 = '';
                  obj.HV2 = '';
                  obj.HV3 = '';
                  obj.FechaProceso = '';
                  obj.HP = '';
                  obj.TecnicoCuadrilla = (vm.TecnicoCuadrilla)?vm.TecnicoCuadrilla.clv_Tecnico:-1;
                  obj.Visita = false;
                  obj.Clv_Tecnico = vm.Tecnico.clv_Tecnico;
                  obj.clvProblema = vm.Problema.clvProblema;
                  obj.clvPrioridadQueja = vm.Prioridad.clvPrioridadQueja;
                  obj.Solucion = vm.ProblemaReal;
                  obj.Clv_Trabajo = (vm.Trabajo == null || vm.Trabajo == undefined) ? 0 : vm.Trabajo.CLV_TRABAJO;
                  quejasFactory.UpdateQuejas(obj).then(function (data) {
                    if(vm.Estatus == 'E'){
                      var obj= {
                         'Clv_orden': parseInt(vm.clv_queja),
                         'horaInicio':vm.HoraInicio,
                         'horaFin':vm.HoraFin,
                         'opcion':2
                       }
                       quejasFactory.GetGuardaHoraOrden(obj).then(function(res){
                         console.log(res);
                       });
                     }
  
                    ngNotify.set('El Reporte de falla se aplicó  correctamente', 'success');
                    $state.go('home.procesos.reportes');
                  });
                }
                if (vm.Estatus == 'V') {
                  vm.HoraInicio = "";
                  vm.HoraFin = "";
                  var obj = {};
                  obj.Clv_Queja = vm.clv_queja;
                  obj.Status = 'V';
                  obj.Fecha_Ejecucion = '';
                  obj.Visita1 = (vm.Fechavisita1 == undefined) ? '' : vm.Fechavisita1;
                  obj.Visita2 = (vm.Fechavisita2 == undefined) ? '' : vm.Fechavisita2;
                  obj.Visita3 = (vm.Fechavisita2 == undefined) ? '' : vm.Fechavisita3;
                  obj.HV1 = (vm.Horavisita1 == undefined) ? '' : vm.Horavisita1;
                  obj.HV2 = (vm.Horavisita2 == undefined) ? '' : vm.Horavisita2;
                  obj.HV3 = (vm.Horavisita3 == undefined) ? '' : vm.Horavisita3;
                  obj.FechaProceso = '';
                  obj.HP = '';
                  obj.Visita = false;
                  obj.Clv_Tecnico = vm.Tecnico.clv_Tecnico;
                  obj.clvProblema = vm.Problema.clvProblema;
                  obj.clvPrioridadQueja = vm.Prioridad.clvPrioridadQueja;
                  obj.Solucion = vm.ProblemaReal;
                  obj.Clv_Trabajo = (vm.Trabajo == null || vm.Trabajo == undefined) ? 0 : vm.Trabajo.CLV_TRABAJO;
                  quejasFactory.UpdateQuejas(obj).then(function (data) {
  
                    ngNotify.set('El Reporte de falla se aplicó  correctamente', 'success');
                    $state.go('home.procesos.reportes');
                  });
                }

                if (vm.Estatus == 'S') {
                  var obj = {};
                  obj.Clv_Queja = vm.clv_queja;
                  obj.Status = 'S';
                  obj.Fecha_Ejecucion = '';
                  obj.Visita1 = '';
                  obj.Visita2 = '';
                  obj.Visita3 = '';
                  obj.HV1 = '';
                  obj.HV2 = '';
                  obj.HV3 = '';
                  obj.FechaProceso = vm.FechaProceso;
                  obj.HP = vm.HoraProceso;
                  obj.Visita = false;
                  obj.Clv_Tecnico = vm.Tecnico.clv_Tecnico;
                  obj.clvProblema = vm.Problema.clvProblema;
                  obj.clvPrioridadQueja = vm.Prioridad.clvPrioridadQueja;
                  obj.Solucion = vm.ProblemaReal;
                  obj.Clv_Trabajo = (vm.Trabajo == null || vm.Trabajo == undefined) ? 0 : vm.Trabajo.CLV_TRABAJO;
                  quejasFactory.UpdateQuejas(obj).then(function (data) {
  
                    ngNotify.set('El Reporte de falla se aplicó  correctamente', 'success');
                    $state.go('home.procesos.reportes');
                  });
                }
              } else {
                ngNotify.set('El cliente, ha sido bloqueado, por lo que no se podrá ejecutar la orden', 'error');
              }
            });
          } else {
            ngNotify.set('El Reporte pertenece a un contrato de plazas adicionales al usuario, no puede ejecutarla', 'error');
          }
        });
      });
    }

    function MuestraAgenda() {
      var options = {};
      options.TecnicoAgenda = vm.TecnicoAgenda;
      options.TurnoAgenda = vm.TurnoAgenda;
      options.FechaAgenda = vm.FechaAgenda;
      options.ComentarioAgenda = vm.ComentarioAgenda;
      options.opcion = 2;
      console.log('options',options);

      var modalInstance = $uibModal.open({
        animation: vm.animationsEnabled,
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: 'views/procesos/ModalAgendaQueja.html',
        controller: 'ModalAgendaQuejaCtrl',
        controllerAs: 'ctrl',
        backdrop: 'static',
        keyboard: false,
        size: 'sm',
        resolve: {
          options: function () {
            return options;
          }
        }
      });
    }

    function DescargaMaterial() {
      if (vm.Tecnico == null) ngNotify.set('Seleccione un técnico para continuar', 'warn');
      var Tecnico = {};
      Tecnico.CLV_TECNICO = vm.Tecnico.clv_Tecnico;
      Tecnico.Nombre = vm.Tecnico.Nombre;
      var options = {};
      options.Detalle = false;
      options.ClvOrden = vm.clv_queja;
      options.ClvBitacora = vm.idBitacora;
      options.SctTecnico = Tecnico;
      options.Tipo_Descargar = "Q";

      var modalInstance = $uibModal.open({
        animation: vm.animationsEnabled,
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: 'views/procesos/ModalDescargaMaterial.html',
        controller: 'ModalDescargaMaterialCtrl',
        controllerAs: 'ctrl',
        backdrop: 'static',
        keyboard: false,
        size: 'lg',
        resolve: {
          options: function () {
            return options;
          }
        }
      });
    }

    function GeneraQM(){
      quejasFactory.ValidaQueja(vm.clv_queja).then(function (data) {
        if (data.GetDeepValidaQuejaCompaniaAdicResult.Valida == 0) {
          quejasFactory.BuscaBloqueado(vm.GlobalContrato).then(function (bloqueado) {
            if (bloqueado.GetDeepBuscaBloqueadoResult.Bloqueado == 0) {
              QuejaMasivaFactory.GetValidaServCliQM(vm.GlobalContrato).then(function(data){
                if(data.GetValidaServCliQMResult.Resultado == true){
                  var modalInstance = $uibModal.open({
                    animation: true,
                    ariaLabelledBy: 'modal-title',
                    ariaDescribedBy: 'modal-body',
                    templateUrl: 'views/procesos/AddMensajeQM.html',
                    controller: 'AddMensajeQMCtrl',
                    controllerAs: 'ctrl',
                    backdrop: 'static',
                    keyboard: false,
                    class: 'modal-backdrop fade',
                    size: 'sm'
                  });
                  modalInstance.result.then(function (ObjQM) {
                    if(ObjQM != null){
                      vm.MSJQM = ObjQM.MSJ;
                      SaveQM();
                    }
                  });
                }else{
                  ngNotify.set('Error, el Cliente seleccionado no tiene Servicios Instalados', 'warn');
                }
              });
            } else {
              ngNotify.set('El cliente, ha sido bloqueado, por lo que no se podrá ejecutar la orden', 'error');
            }
          });
        }else{
          ngNotify.set('El Reporte pertenece a un contrato de plazas adicionales al usuario, no puede ejecutarla', 'error');
        }
      });
    }

    function GetFechaP(){
      var F = new Date()
      var D = F.getDate();
      var M = F.getMonth() + 1;
      var Y = F.getFullYear();
      return D + '/' + M + '/' + Y
    }

    function GetHoraP(){
      var F = new Date()
      var HP = F.getHours();
      var M = F.getMinutes();
      if(HP > 12){
        var H = (HP -12)
        var S = 'pm'
      }else{
        var H = HP
        var S = 'am'
      }
      return H + ':' + M + '' + S
    }

    function SaveQM(){
      var obj = {};
      obj.Clv_Queja = vm.clv_queja;
      obj.Status = 'S';
      obj.Fecha_Ejecucion = '';
      obj.Visita1 = '';
      obj.Visita2 = '';
      obj.Visita3 = '';
      obj.HV1 = '';
      obj.HV2 = '';
      obj.HV3 = '';
      obj.FechaProceso = GetFechaP();
      obj.HP = GetHoraP();
      obj.Visita = false;
      obj.Clv_Tecnico = 0;
      obj.clvProblema = vm.Problema.clvProblema;
      obj.clvPrioridadQueja = vm.Prioridad.clvPrioridadQueja;
      obj.Solucion = vm.ProblemaReal;
      obj.Clv_Trabajo = (vm.Trabajo == null || vm.Trabajo == undefined) ? 0 : vm.Trabajo.CLV_TRABAJO;
      quejasFactory.UpdateQuejas(obj).then(function (data) {
        var Obj = {
          'Clave_Tecnico': 0,
          'ClvTecnicoSec': 0,  
          'ClvProblema': vm.Problema.clvProblema,
          'Problema': vm.DetalleProblema,
          'MensajeMostrar': vm.MSJQM,
          'Usuario': $localStorage.currentUser.usuario,
          'IdUsuario': $localStorage.currentUser.idUsuario,
          'Clv_Trabajo': 0,
				  'Solucion': ''
        };
        var LstQuejaEdo = {'Edo': 0};
        var LstQuejaCd = {'Cd': 0};
        var LstQuejaLoc = {'Loc': 0};
        var LstQuejaCol = {'Col': 0};
        var LstQuejaCalle = {'Calle': 0};
        var LstQuejaCliente = [{'Contrato': vm.GlobalContrato}];
        var ObjQM = {
          'Obj': Obj,
          'LstQuejaEdo': LstQuejaEdo,
          'LstQuejaCd': LstQuejaCd,
          'LstQuejaLoc': LstQuejaLoc,
          'LstQuejaCol': LstQuejaCol,
          'LstQuejaCalle': LstQuejaCalle,
          'LstQuejaCliente': LstQuejaCliente,
          'Clave': $localStorage.currentUser.idUsuario
        };
        QuejaMasivaFactory.GetAddQuejaMasiva(ObjQM).then(function(data){
          vm.Clv_Queja = data.GetAddQuejaMasivaResult[0].Clv_Queja;
          if(vm.Clv_Queja > 0){
            ngNotify.set('CORRECTO, Se guardó Reporte de Mantenimiento.', 'success');
            $state.go('home.procesos.reportesmasivos');
            OpenReporteQM();
          }else{
            ngNotify.set('ERROR, No se guardó Reporte Masivo.', 'error');
          }
        });
      });
    }

    function OpenReporteQM(){
			var Clv_Queja = vm.Clv_Queja;
			var modalInstance = $uibModal.open({
				animation: true,
				ariaLabelledBy: 'modal-title',
				ariaDescribedBy: 'modal-body',
				templateUrl: 'views/procesos/ReporteQuejaMantenimiento.html',
				controller: 'ReporteQMCtrl',
				controllerAs: 'ctrl',
				backdrop: 'static',
				keyboard: false,
				class: 'modal-backdrop fade',
				size: 'lg',
				resolve: {
					Clv_Queja: function () {
						return Clv_Queja;
					}
				}
			});
		}

    function changeHora(){
      vm.HoraFin = vm.HoraEjecucion;
    }

    var vm = this;
    InitalData();
    vm.Titulo = 'Ejecutar Reporte De Falla';
    vm.ShowEje = true;
    vm.ShowDet = false;
    vm.BloqElemnt = false;
    vm.abrirBonificacion = abrirBonificacion;
    vm.CambiaEstatus = CambiaEstatus;
    vm.Ejecutaqueja = Ejecutaqueja;
    vm.DescargaMaterial = DescargaMaterial;
    vm.MuestraAgenda = MuestraAgenda;
    vm.GeneraQM = GeneraQM;
    vm.Iprioridad = true;
    vm.IDetProblema = true;
    vm.IClasproblema = true;
    vm.Iprobreal = false;
    vm.Iobser = true;
    vm.IEstatus = true;
    vm.idBitacora = 0;
    vm.idTecnicoBitacora = 0;
    vm.changeHora = changeHora;
    var DateRep = {};

  });
