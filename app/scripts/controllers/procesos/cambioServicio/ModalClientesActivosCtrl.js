'use strict';
angular
  .module('softvApp')
  .controller('ModalClientesActivosCtrl', function ($uibModalInstance, procesoFactory, $uibModal, $rootScope, ngNotify, $localStorage) {

    function initialData() {
      buscar(3);
    }

    function EnterContrato(event){
      if(event.keyCode == 13){
        buscar(0);
       
      }
    }
    function EnterNombre(event){
      if(event.keyCode == 13){
        buscar(1);
      }
    }
    function EnterDomicilio(event){
      if(event.keyCode == 13){
        buscar(2);
      }
    }
    function EnterSetTopBox(event){
      if(event.keyCode == 13){
        buscar(6);
      }
    }

    function buscar(op) {
      if (!vm.calle) {
        vm.calle = ''
      }
      if (!vm.numero) {
        vm.numero = ''
      }
      var Parametros = {
        'contrato': (op == 0) ? vm.contrato.split('-')[0] : 0,
        'nombre': (op == 1) ? vm.nombre : '',
        'calle': (op == 2) ? vm.calle : '',
        'numero': (op == 2) ? vm.numero : '',
        'ciudad': (op == 2) ? vm.ciudad : '',
        'op': op,
        'clvColonia': 0,
        'idcompania': (op == 0) ? vm.contrato.split('-')[1] : 0,
        'SETUPBOX': (op == 6) ? vm.SETUPBOX : '',
        'TARJETA': 0,
        'ClvUsuario': $localStorage.currentUser.idUsuario
      };
      procesoFactory.GetuspDameClientesActivos(Parametros).then(function (result) {
        vm.Clientes = result.GetuspDameClientesActivosResult;
        limpiarInput();
      });

    }

    function limpiarInput(){
      vm.contrato = "";
      vm.nombre = "";
      vm.calle = "";
      vm.numero = "";
      vm.ciudad = "";
      vm.SETUPBOX = "";
      vm.colonia = "";
    }

    function ok(item) {
      $uibModalInstance.close(item);
    }

    function cancel() {
      $uibModalInstance.dismiss('cancel');
    }

    var vm = this;
    vm.cancel = cancel;
    initialData();
    vm.buscar = buscar;
    vm.EnterContrato = EnterContrato;
    vm.EnterNombre = EnterNombre;
    vm.EnterDomicilio = EnterDomicilio;
    vm.EnterSetTopBox = EnterSetTopBox;
    vm.limpiarInput = limpiarInput;
    vm.ok = ok;
  
  });
