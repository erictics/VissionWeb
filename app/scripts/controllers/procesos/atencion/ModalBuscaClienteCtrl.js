'use strict';
angular
	.module('softvApp')
	.controller('ModalBuscaClienteCtrl', function ($uibModalInstance, $uibModal, atencionFactory, QuejaMasivaFactory, $rootScope, ngNotify, $localStorage, options) {

		function initialData() {
			var obje = {};
			obje.servicio = options.CLV_TIPSER;
			obje.op = 3;
			obje.colonia = 0;
			atencionFactory.buscarCliente(obje).then(function (data) {
				vm.Clientes = data.GetuspBuscaContratoSeparado2ListResult;
				if (vm.Clientes.length == 0) {
					vm.sinRegistros = true;
					vm.conRegistros = false;
				} else {
					vm.sinRegistros = false;
					vm.conRegistros = true;
				}
			});
		}

		function EnterContrato(event) {
			if (event.keyCode == 13) {
				BusquedaporContrato();
			}
		  }

		function BusquedaporContrato() {
			if (!(/^\d{1,9}-\d{1,9}$/.test(vm.BUcontrato))) {
				ngNotify.set('El número de contrato está formado por 2 grupos de números con un guion intermedio p.e. (1234-1)', 'primary');
			} else {
				var obje = {};
				obje.contrato = vm.BUcontrato;
				obje.servicio = options.CLV_TIPSER;
				obje.colonia = 0;
				obje.op = 0;
				atencionFactory.buscarCliente(obje).then(function (data) {
					vm.Clientes = data.GetuspBuscaContratoSeparado2ListResult;
					vm.BUcontrato = null;
					if (vm.Clientes.length == 0) {
						vm.sinRegistros = true;
						vm.conRegistros = false;
					} else {
						vm.sinRegistros = false;
						vm.conRegistros = true;
					}
				});
			}
		}

		function EnterPorNombre(event){
			if(event.keyCode == 13){
				BusquedaporNombre();
			}
		}
		function BusquedaporNombre() {
			var obje = {};
			obje.nombre = vm.BUnombre;
			obje.paterno = vm.BUapaterno;
			obje.materno = vm.BUamaterno;
			obje.colonia = 0;
			obje.servicio = options.CLV_TIPSER;
			obje.op = 1;
			atencionFactory.buscarCliente(obje).then(function (data) {
				vm.Clientes = data.GetuspBuscaContratoSeparado2ListResult;
				vm.BUnombre = null;
				vm.BUapaterno = null;
				vm.BUamaterno = null;
				if (vm.Clientes.length == 0) {
					vm.sinRegistros = true;
					vm.conRegistros = false;
				} else {
					vm.sinRegistros = false;
					vm.conRegistros = true;
				}
			});
		}

		function EnterPorDireccion(event){
			if(event.keyCode == 13){
				BusquedaporDireccion();
			}
		}
		function BusquedaporDireccion() {
			var obje = {};
			obje.servicio = options.CLV_TIPSER;
			obje.calle = vm.BUcalle;
			obje.numero = vm.BUnumero;
			obje.colonia = 0;
			//obje.colonia
			obje.op = 2;
			atencionFactory.buscarCliente(obje).then(function (data) {
				vm.Clientes = data.GetuspBuscaContratoSeparado2ListResult;
				vm.BUcalle = null;
				vm.BUnumero = null;
				if (vm.Clientes.length == 0) {
					vm.sinRegistros = true;
					vm.conRegistros = false;
				} else {
					vm.sinRegistros = false;
					vm.conRegistros = true;
				}
			});
		}

		function EnterPorAparato(event){
			if(event.keyCode == 13){
				BusquedaporAparato();
			}
		}
		function BusquedaporAparato() {
			var obje = {};
			obje.servicio = options.CLV_TIPSER;
			obje.setupbox = vm.BUaparato;
			obje.op = 5;
			obje.colonia = 0;
			atencionFactory.buscarCliente(obje).then(function (data) {
				vm.Clientes = data.GetuspBuscaContratoSeparado2ListResult;
				if (vm.Clientes.length == 0) {
					vm.sinRegistros = true;
					vm.conRegistros = false;
				} else {
					vm.sinRegistros = false;
					vm.conRegistros = true;
				}
				vm.BUaparato = "";
			});
		}

		function Seleccionar(cliente) {
			vm.ListCliente = [];
			var Obj = {'Contrato': cliente.ContratoBueno};
			vm.ListCliente.push(Obj);
			QuejaMasivaFactory.GetSoftvWeb_ValidaQMContrato(vm.ListCliente).then(function(data){
				var MSJ = data.GetSoftvWeb_ValidaQMContratoResult.MSJ;
				if(data.GetSoftvWeb_ValidaQMContratoResult.Resultado == false){
					$uibModalInstance.dismiss('cancel');
					$rootScope.$emit('cliente_seleccionado', cliente);
				}else if(data.GetSoftvWeb_ValidaQMContratoResult.Resultado == true){
					ngNotify.set('ERROR, Ya Cuenta Con un Reporte de Mantenimiento, ' + MSJ, 'warn');
				}
			});
		}

		function cancel() {
			$uibModalInstance.dismiss('cancel');
		}

		var vm = this;
		vm.cancel = cancel;
		vm.Seleccionar = Seleccionar;
		initialData();
		vm.BusquedaporNombre = BusquedaporNombre;
		vm.EnterPorNombre = EnterPorNombre;
		vm.BusquedaporDireccion = BusquedaporDireccion;
		vm.EnterPorDireccion = EnterPorDireccion;
		vm.BusquedaporAparato = BusquedaporAparato;
		vm.EnterPorAparato = EnterPorAparato;
		vm.BusquedaporContrato = BusquedaporContrato;
		vm.EnterContrato = EnterContrato;
		
	});
