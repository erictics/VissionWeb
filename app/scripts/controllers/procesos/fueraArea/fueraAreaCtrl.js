'use strict';
angular
	.module('softvApp')
	.controller('fueraAreaCtrl', function ($state, procesoFactory,ngNotify,RecontratacionFactory, atencionFactory, $localStorage, $uibModal) {


        function OpenSearchCliente(){
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body', 
                templateUrl: 'views/procesos/ModaRecontratacionCliente.html',
                controller: 'ModalFAClienteCtrl',
                controllerAs: 'ctrl',
                backdrop: 'static',
                keyboard: false,
                class: 'modal-backdrop fade',
                size: 'lg'
            });
            modalInstance.result.then(function (ObjCliente) {
                GetCliente(2, ObjCliente.IdContrato, ObjCliente.ContratoCompuesto);
            });
        }


        function SetCliente(){
            if(vm.Cliente != undefined){
            if (event.keyCode === 13) {
                event.preventDefault();
                GetCliente(1, 0, vm.Cliente.ContratoCom);
            }
            }else{
                vm.CheckNull = true;
            }

            vm.CheckNull = ((vm.Cliente.ContratoCom != null && vm.Cliente.ContratoCom != undefined && vm.Cliente.ContratoCom != '') &&(/^\d{1,9}-\d{1,9}$/.test(vm.Cliente.ContratoCom)))? true:vm.CheckNull;
        }


        function SetNombre(N, S, AP, AM){
            if(S == null && AM == null){
                return N + ' ' + AP;
            }else if(S != null && AM == null){
                return N + ' ' + S + ' ' + AP;
            }else if(S == null && AM != null){
                return N + ' ' + AP + ' ' + AM;
            }else if(S != null && AM != null){
                return N + ' ' + S + ' ' + AP + ' ' + AM;
            }
        }

        
        function GetCliente(Op, IdContrato, ContratoCompuesto){ 
            vm.Cliente={};
            vm.servicios=[];
            procesoFactory.GetInfoContratoContratado(Op,IdContrato,ContratoCompuesto).then(function(data){
                var ClienteResult = data.GetInfoContratoContratadoResult;
                 
                vm.CheckNull = false;
                if(ClienteResult.CONTRATO != null){
                    vm.Cliente = ClienteResult;
                    vm.IdContrato = vm.Cliente.CONTRATO;
                    vm.Nombre = SetNombre(vm.Cliente.Nombre, vm.Cliente.SegundoNombre, vm.Cliente.Apellido_Paterno, vm.Cliente.Apellido_Materno);   
                    procesoFactory.GetServiciosContratoFueraArea(vm.IdContrato).then(function(res){
                     vm.servicios= res.GetServiciosContratoFueraAreaResult
                    });
                }else{
                    ngNotify.set('Atención: no se encontró algún resultado con este contrato', 'warn');                  
                    vm.CheckNull = true;
                }
                
            });
           
        }

        function AfectarServicio(item){

            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body', 
                templateUrl: 'views/procesos/ModalPreguntaAtencion.html',
                controller: 'ModalAutorizacionFACtrl',
                controllerAs: '$ctrl',
                backdrop: 'static',
                keyboard: false,
                class: 'modal-backdrop fade',
                size: 'sm',
                resolve: {
                    item: function () {
                      return item;
                    }
                }
            });
            modalInstance.result.then(function (clv_unicanet) {                
                var claves=[];
                claves.push(clv_unicanet);
                procesoFactory.GetafectaContratoFueraArea(claves).then(function(res){
                    console.log(res);
                   
                    procesoFactory.GetServiciosContratoFueraArea(vm.IdContrato).then(function(res){
                        vm.servicios= res.GetServiciosContratoFueraAreaResult;
                        ngNotify.set('El servicio se cambio a fuera de área Correctamente','success');       
                    });
                });
               console.log(res);
            });

        }
      
        
        var vm = this;
        vm.OpenSearchCliente=OpenSearchCliente;
       vm.AfectarServicio=AfectarServicio;
       vm.SetCliente=SetCliente;
     

    });