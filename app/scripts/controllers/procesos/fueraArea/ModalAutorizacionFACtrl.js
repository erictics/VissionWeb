'use strict';

angular
    .module('softvApp')
    .controller('ModalAutorizacionFACtrl', function(RecontratacionFactory, procesoFactory,$uibModalInstance,item, $uibModal, ngNotify, $state, $localStorage){

        function ok() {            
            $uibModalInstance.close(item.clv_unicanet);
        }

        function cancel() {
            $uibModalInstance.dismiss('cancel');
        }

        var vm = this;
     
        vm.ok = ok;
        vm.Pregunta='¿Estas seguro de cambiar el servicio '+item.Descripcion+'  a fuera de area ?';
        vm.Titulo='Atención';
        vm.btncancel='Cancelar';
        vm.btnok='Cambiar';
        vm.cancel=cancel;
    });