'use strict';

angular
    .module('softvApp')
    .controller('RecontratacionCtrl', function(RecontratacionFactory, ngNotify, $uibModal, $rootScope, $state, $localStorage){

        function OpenSearchCliente(){
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body', 
                templateUrl: 'views/procesos/ModaRecontratacionCliente.html',
                controller: 'ModaRecontratacionClienteCtrl',
                controllerAs: 'ctrl',
                backdrop: 'static',
                keyboard: false,
                class: 'modal-backdrop fade',
                size: 'lg'
            });
            modalInstance.result.then(function (ObjCliente) {
                GetCliente(ObjCliente.Op, ObjCliente.IdContrato, ObjCliente.ContratoCompuesto);
            });
        }

        function SetCliente(){
            if(vm.Cliente != undefined){
            if (event.keyCode === 13) {
                event.preventDefault();
                GetCliente(1, 0, vm.Cliente.ContratoCom);
            }
            }else{
                vm.CheckNull = true;
            }

            vm.CheckNull = ((vm.Cliente.ContratoCom != null && vm.Cliente.ContratoCom != undefined && vm.Cliente.ContratoCom != '') &&(/^\d{1,9}-\d{1,9}$/.test(vm.Cliente.ContratoCom)))? true:vm.CheckNull;
        }

        function GetCliente(Op, IdContrato, ContratoCompuesto){
            ResetMod();
            var ObjCliente = {
                'Op': Op,
                'IdContrato': IdContrato,
                'ContratoCompania': ContratoCompuesto
            }
            RecontratacionFactory.GetInfoContratoEnBaja(ObjCliente).then(function(data){
                var ClienteResult = data.GetInfoContratoEnBajaResult;
                vm.CheckNull = false;
                if(ClienteResult.CONTRATO != null){
                    vm.Cliente = ClienteResult;
                    vm.IdContrato = vm.Cliente.CONTRATO;
                    vm.Nombre = SetNombre(vm.Cliente.Nombre, vm.Cliente.SegundoNombre, vm.Cliente.Apellido_Paterno, vm.Cliente.Apellido_Materno);
                    vm.ShowServicios = true;
                    RecontratacionFactory.GetDameClv_Session().then(function(data){
                        vm.ClvSession = data.GetDameClv_SessionResult;
                        RecontratacionFactory.GetuspCobraAdeudo_CuandoRecontrata_Msj(vm.IdContrato).then(function(data){
                            vm.CobroAdeudo = parseFloat(data.GetuspCobraAdeudo_CuandoRecontrata_MsjResult.MSJ);
                            vm.Total = vm.CobroAdeudo + vm.CostoRecontratacion + vm.CostoContratacion;
                        });
                    });
                }else{
                    ngNotify.set('ERROR, no se encontró algún resultado con este contrato o aun no le han instalado servicios.', 'warn');
                    ResetMod();
                    vm.CheckNull = true;
                }
            });
        }

        function GetCostoREcontratacion(){
            RecontratacionFactory.GetSp_CalculaRecontratacionPrevio(vm.ClvSession).then(function(data){
                vm.CostoRecontratacion = data.GetSp_CalculaRecontratacionPrevioResult.CostoRecontratacion;
                RecontratacionFactory.GetSp_CalculaContratacionPrevio(vm.ClvSession).then(function(data){
                    vm.CostoContratacion = data.GetSp_CalculaContratacionPrevioResult.CostoRecontratacion;
                    vm.Total = vm.CobroAdeudo + vm.CostoRecontratacion + vm.CostoContratacion;
                });
            });
        }

        function OpenAddServicioRecontratacion(){
            var ObjCliente = {
                'IdContrato': vm.IdContrato,
                'ClvSession': vm.ClvSession,
                'IdRecon': vm.IdRecon
            };
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body', 
                templateUrl: 'views/procesos/ModalServicioRecontratacion.html',
                controller: 'ModalAddServicioRecontratacionCtrl',
                controllerAs: 'ctrl',
                backdrop: 'static',
                keyboard: false,
                class: 'modal-backdrop fade',
                size: 'sm',
                resolve: {
                    ObjCliente: function () {
                        return ObjCliente;
                    }
                }
            });
            modalInstance.result.then(function (IdRecon) {
                vm.IdRecon = IdRecon;
                GetServicioList();
            });
        }

        function OpenAddServicioAdicionalRecontratacion(IdRecon, Clv_UnicaNet){
            var ObjCliente = {
                'IdContrato': vm.IdContrato,
                'ClvSession': vm.ClvSession,
                'IdRecon': IdRecon,
                'Clv_UnicaNet': Clv_UnicaNet
            };
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body', 
                templateUrl: 'views/procesos/ModalServicioAdicionalRecontratacion.html',
                controller: 'ModalServicioAdicionalRecontratacionAddCtrl',
                controllerAs: 'ctrl',
                backdrop: 'static',
                keyboard: false,
                class: 'modal-backdrop fade',
                size: 'sm',
                resolve: {
                    ObjCliente: function () {
                        return ObjCliente;
                    }
                }
            });
            modalInstance.result.then(function (IdRecon) {
                vm.IdRecon = IdRecon;
                GetServicioList();
            });
        }

        function GetServicioList(){
            RecontratacionFactory.GetArbolRecontratacion(vm.ClvSession).then(function(data){
                vm.ServicioList = data.GetArbolRecontratacionResult;
                vm.expandedNodes = [];
                angular.forEach(vm.ServicioList, function(value, key) {
                    vm.expandedNodes.push(value);
                });
                GetDetalleRecon(vm.ServicioList[0]);
                GetCostoREcontratacion();
            });
        }

        function GetDetalleRecon(ObjRecon){
            vm.Clv_TipSer = ObjRecon.Clv_TipSer;
            var ObjReTmp = {
                'ClvSession': vm.ClvSession,
                'IdRecon': ObjRecon.IdRecon
            };
            RecontratacionFactory.GetDetalleRecontratacionTmp(ObjReTmp).then(function(data){
                var SerReco = data.GetDetalleRecontratacionTmpResult;
                vm.ReServicio = SerReco.Servicio;
                vm.ReStatus = 'Recontratación';
                vm.ReMedio = (SerReco.Medio != null)? SerReco.Medio:'Definir en la instalación';
                vm.ReTvConPago = (SerReco.TvConPago != null)? SerReco.TvConPago:0;
                vm.ReTvSonPago = (SerReco.TvSinPago != null)? SerReco.TvSinPago:0;
                vm.ShowDetSer = true;
            });
        }

        function OpenConfirmar(){
            if(vm.IdContrato > 0 && vm.ClvSession > 0 && vm.ServicioList.length > 0){
                var modalInstance = $uibModal.open({
                    animation: true,
                    ariaLabelledBy: 'modal-title',
                    ariaDescribedBy: 'modal-body', 
                    templateUrl: 'views/procesos/ConfirmarRecontratacion.html',
                    controller: 'ConfirmarRecontratacionCtrl',
                    controllerAs: 'ctrl',
                    backdrop: 'static',
                    keyboard: false,
                    class: 'modal-backdrop fade',
                    size: 'sm'
                });
                modalInstance.result.then(function(){
                    SaveRecontratacion();
                });
            }else{
                ngNotify.set('ERROR, aun no se ha seleccionado el cliente y/o no se han agregado los servicios a recontratar.', 'warn');
            }
        }

        function SaveRecontratacion(){
            if(vm.IdContrato > 0 && vm.ClvSession > 0 && vm.ServicioList.length > 0){
                var ObjRecontratacion = {
                    'Contrato': vm.IdContrato,
                    'Clv_Usuario': $localStorage.currentUser.usuario,
                    'ClvSession': vm.ClvSession
                };
                RecontratacionFactory.GetGrabaReContratacion(ObjRecontratacion).then(function(data){
                    ngNotify.set('CORRECTO, se guardó recontratación.', 'success');
                    ResetMod();
                });
            }else{
                ngNotify.set('ERROR, aun no se ha seleccionado el cliente y/o no se han agregado los servicios a recontratar.', 'warn');
            }
        }

        function SetNombre(N, S, AP, AM){
            if(S == null && AM == null){
                return N + ' ' + AP;
            }else if(S != null && AM == null){
                return N + ' ' + S + ' ' + AP;
            }else if(S == null && AM != null){
                return N + ' ' + AP + ' ' + AM;
            }else if(S != null && AM != null){
                return N + ' ' + S + ' ' + AP + ' ' + AM;
            }
        }

        function ResetMod(){
            vm.Cliente = null;
            vm.IdContrato = 0;
            vm.Nombre = '';
            vm.ServicioList = [];
            vm.ShowServicios = false;
            vm.CobroAdeudo = 0;
            vm.ShowDetSer = false;
            vm.CostoRecontratacion = 0;
            vm.CostoContratacion = 0;
            vm.Total = 0;
            if(vm.ClvSession > 0){
                DeleteClvSession();
            }
        }

        function DeleteClvSession(){
            RecontratacionFactory.GetBorReconSession(vm.ClvSession).then(function(data){
                vm.ClvSession = 0;
            });
        }

        var vm = this;
        vm.IdContrato = 0;
        vm.ClvSession = 0;
        vm.ServicioList = [];
        vm.IdRecon = 0;
        vm.ShowServicios = false;
        vm.CheckNull = false;
        vm.ShowDetSer = false;
        vm.CobroAdeudo = 0;
        vm.CostoRecontratacion = 0;
        vm.CostoContratacion = 0;
        vm.Total = 0;
        vm.OpenSearchCliente = OpenSearchCliente;
        vm.SetCliente = SetCliente;
        vm.OpenAddServicioRecontratacion = OpenAddServicioRecontratacion;
        vm.OpenAddServicioAdicionalRecontratacion = OpenAddServicioAdicionalRecontratacion;
        vm.SaveRecontratacion = SaveRecontratacion;
        vm.ResetMod = ResetMod;
        vm.GetDetalleRecon = GetDetalleRecon;
        vm.OpenConfirmar = OpenConfirmar;
    });