'use strict';
angular
    .module('softvApp')
    .controller('ConfirmarRecontratacionCtrl', function($uibModalInstance, $uibModal, ngNotify, $state, $localStorage){

        function OK(){
            $uibModalInstance.close(vm.IdRecon);
        }

        function Cancel(){
            $uibModalInstance.dismiss('cancel');
        }

        var vm = this;
        vm.OK = OK;
        vm.Cancel = Cancel;

    });